package searching.demo;

import searching.algorithms.Node;
import searching.algorithms.SearchUtil;
import searching.slagalica.KonfiguracijaSlagalice;
import searching.slagalica.Slagalica;

import static searching.slagalica.gui.SlagalicaViewer.display;

/** A GUI demo of the puzzle. */
public class SlagalicaMain {

    /**
     * Entry point of the program.
     *
     * @param args a single string, the initial puzzle state
     */
    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Invalid number of arguments. Expected: 1, actual: " + args.length);
            return;
        }

        String puzzle = args[0];

        if (puzzle.length() != 9 || !puzzle.matches("\\d{9}")) {
            System.out.println("Invalid initial puzzle.");
            return;
        }

        int[] checker = new int[9];
        int[] arr = puzzle.chars().map(i -> i - 48).toArray();

        for (int value : arr) {
            if (checker[value] == 1) {
                System.out.println("Initial puzzle contains duplicate digits.");
                return;
            } else {
                checker[value] = 1;
            }
        }

        Slagalica slagalica = new Slagalica(
            new KonfiguracijaSlagalice(arr)
        );

        Node<KonfiguracijaSlagalice> rjesenje =
            SearchUtil.bfsv(slagalica, slagalica, slagalica);

        display(rjesenje);
    }
}
