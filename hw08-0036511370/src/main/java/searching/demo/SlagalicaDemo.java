package searching.demo;

import searching.algorithms.Node;
import searching.algorithms.SearchUtil;
import searching.slagalica.KonfiguracijaSlagalice;
import searching.slagalica.Slagalica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** A console demo for the puzzle. */
public class SlagalicaDemo {

    /**
     * Entry point of the program.
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        Slagalica slagalica = new Slagalica(
            new KonfiguracijaSlagalice(new int[] { 1, 6, 4, 5, 0, 2, 8, 7, 3 })
        );

        Node<KonfiguracijaSlagalice> rjesenje =
            SearchUtil.bfsv(slagalica, slagalica, slagalica);

        if (rjesenje == null) {
            System.out.println("Nisam uspio pronaći rješenje.");
            System.out.println();
            System.out.println(new KonfiguracijaSlagalice(new int[] { 1, 6, 4, 5, 0, 2, 8, 7, 3 }));
        } else {
            System.out.println(
                "Imam rješenje. Broj poteza je: " + rjesenje.getCost() + "\n"
            );

            List<KonfiguracijaSlagalice> lista = new ArrayList<>();

            Node<KonfiguracijaSlagalice> trenutni = rjesenje;

            while (trenutni != null) {
                lista.add(trenutni.getState());
                trenutni = trenutni.getParent();
            }

            Collections.reverse(lista);

            lista.forEach(k -> {
                System.out.println(k);
                System.out.println();
            });
        }
    }
}