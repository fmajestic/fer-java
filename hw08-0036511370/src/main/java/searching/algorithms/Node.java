package searching.algorithms;

/**
 * Models a single search tree node.
 *
 * @param <S> type of the state that is stored
 */
public class Node<S> {

    /** Stored state of the node. */
    private S state;

    /** Cost of reaching this node. */
    private double cost;

    /** Parent of the node. */
    private Node<S> parent;

    /**
     * Creates a new node with the given parameters.
     *
     * @param parent parent of the node
     * @param state  state to be stored
     * @param cost   the cost of reaching this node
     */
    public Node(Node<S> parent, S state, double cost) {
        this.state = state;
        this.cost = cost;
        this.parent = parent;
    }

    /**
     * Gets the stored state.
     *
     * @return the stored state
     */
    public S getState() {
        return state;
    }

    /**
     * Gets the cost of reaching this node
     *
     * @return the cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * Gets the parent of this node, or null if the node has no parent.
     *
     * @return the parent, or null
     */
    public Node<S> getParent() {
        return parent;
    }
}
