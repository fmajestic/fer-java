package searching.algorithms;

/**
 * Models a single state space transition with an associated cost.
 *
 * @param <S> type of the state to store
 */
public class Transition<S> {

    /** The state to transition into. */
    private S state;

    /** The cost of the transition. */
    private double cost;

    /**
     * Creates a new transition with the given parameters
     *
     * @param state the state to transition into
     * @param cost  the cost of the transition
     */
    public Transition(S state, double cost) {
        this.state = state;
        this.cost = cost;
    }

    /**
     * Gets the state that would be transitioned into.
     *
     * @return the state
     */
    public S getState() {
        return state;
    }

    /**
     * Gets the cost of the transition.
     *
     * @return the cost
     */
    public double getCost() {
        return cost;
    }
}
