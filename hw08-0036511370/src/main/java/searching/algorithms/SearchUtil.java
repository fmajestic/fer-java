package searching.algorithms;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/** Utility methods for state-space searching. */
public class SearchUtil {

    /**
     * Breadth-first search.
     * <p>
     * A simple BFS that has no memory of already checked states.
     *
     * @param s0   the initial state
     * @param succ the successor state(s)
     * @param goal the final state to seek
     * @param <S>  type of the states that are searched
     *
     * @return The final node in the search tree, or null if no satisfying state was found
     */
    public static <S> Node<S> bfs(
        Supplier<S> s0,
        Function<S, List<Transition<S>>> succ,
        Predicate<S> goal
    ) {

        List<Node<S>> toVisit = new LinkedList<>(
            Collections.singletonList(new Node<>(null, s0.get(), 0))
        );

        while (!toVisit.isEmpty()) {

            Node<S> ni = toVisit.remove(0);

            if (goal.test(ni.getState())) {
                return ni;
            }

            toVisit.addAll(
                succ.apply(ni.getState())
                    .stream()
                    .map(tr -> new Node<>(ni, tr.getState(), ni.getCost() + tr.getCost()))
                    .collect(Collectors.toList())
            );
        }

        return null;
    }

    /**
     * Breadth-first search (visited).
     * <p>
     * An optimized BFS that keeps track of already visited states.
     *
     * @param s0   the initial state
     * @param succ the successor state(s)
     * @param goal the final state to seek
     * @param <S>  type of the states that are searched
     *
     * @return The final node in the search tree, or null if no satisfying state was found
     */
    public static <S> Node<S> bfsv(
        Supplier<S> s0,
        Function<S, List<Transition<S>>> succ,
        Predicate<S> goal
    ) {

        List<Node<S>> toVisit = new LinkedList<>();
        toVisit.add(new Node<>(null, s0.get(), 0));

        Set<S> visited = new HashSet<>();
        visited.add(s0.get());

        while (!toVisit.isEmpty()) {

            Node<S> ni = toVisit.remove(0);

            if (goal.test(ni.getState())) {
                return ni;
            }

            var children = succ.apply(ni.getState())
                .stream()
                .filter(tr -> !visited.contains(tr.getState()))
                .collect(Collectors.toList());

            toVisit.addAll(
                children.stream()
                    .map(tr -> new Node<>(ni, tr.getState(), ni.getCost() + tr.getCost()))
                    .collect(Collectors.toList())
            );

            visited.addAll(
                children.stream()
                    .map(Transition::getState)
                    .collect(Collectors.toList())
            );
        }

        return null;
    }
}
