package searching.slagalica;

import searching.algorithms.Transition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/** Models a 9-tile puzzle. */
public class Slagalica implements
    Predicate<KonfiguracijaSlagalice>,
    Supplier<KonfiguracijaSlagalice>,
    Function<KonfiguracijaSlagalice, List<Transition<KonfiguracijaSlagalice>>> {

    /** The initial puzzle state. */
    private KonfiguracijaSlagalice initial;

    /**
     * Creates a new puzzle with the given initial state
     * @param initial the initial state
     */
    public Slagalica(KonfiguracijaSlagalice initial) {
        this.initial = initial;
    }

    /**
     * Finds and returns a list of all possible states the puzzle can transition to from the current configuration.
     *
     * @param config the current puzzle configuration
     *
     * @return a list of all possible transitions ("moves").
     */
    @Override
    public List<Transition<KonfiguracijaSlagalice>> apply(KonfiguracijaSlagalice config) {
        List<Transition<KonfiguracijaSlagalice>> ret = new ArrayList<>();

        int index = config.indexOfSpace();
        int[] field = config.getPolje();

        // Move up
        if (index - 3 >= 0) {
            ret.add(new Transition<>(new KonfiguracijaSlagalice(swap(field, index, index - 3)), 1));
        }

        // Move left
        if (index - 1 >= 0) {
            ret.add(new Transition<>(new KonfiguracijaSlagalice(swap(field, index, index - 1)), 1));
        }

        // Move down
        if (index + 3 < field.length) {
            ret.add(new Transition<>(new KonfiguracijaSlagalice(swap(field, index, index + 3)), 1));
        }

        // Move right
        if (index + 1 < field.length) {
            ret.add(new Transition<>(new KonfiguracijaSlagalice(swap(field, index, index + 1)), 1));
        }

        return ret;
    }

    /**
     * Checks if the current configuration is the final solution of the puzzle.
     *
     * @param config the puzzle configuration to check
     *
     * @return true if the solution is correct, false otherwise
     */
    @Override
    public boolean test(KonfiguracijaSlagalice config) {
        return Arrays.equals(
            config.getPolje(),
            new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 0 }
        );
    }

    /**
     * Gets the default initial puzzle configuration.
     *
     * @return the default initial puzzle configuration
     */
    @Override
    public KonfiguracijaSlagalice get() {
        return initial;
    }

    /**
     * Swaps two values in an array.
     * <p>
     * This is done by creating a copy of the array first, so the modifications do not affect the original array.
     *
     * @param array the array to swap in
     * @param i     the first index
     * @param j     the second index
     *
     * @return a new array with elements at i and j swapped
     *
     * @throws IndexOutOfBoundsException if either index is out of bounds of the array
     */
    private int[] swap(int[] array, int i, int j) {
        int[] ret = array.clone();

        int tmp = ret[i];
        ret[i] = ret[j];
        ret[j] = tmp;

        return ret;
    }
}
