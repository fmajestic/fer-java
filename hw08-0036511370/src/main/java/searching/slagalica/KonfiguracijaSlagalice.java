package searching.slagalica;

import java.util.Arrays;

/** Represents a single puzzle configuration, a state. */
public class KonfiguracijaSlagalice {

    /** The puzzle field. */
    private int[] polje;

    /**
     * Creates a new puzzle with the given initial field.
     *
     * @param polje the initial field
     */
    public KonfiguracijaSlagalice(int[] polje) {
        this.polje = polje;
    }

    /**
     * Gets the puzzle field.
     *
     * @return the puzzle
     */
    public int[] getPolje() {
        return polje;
    }

    /**
     * Gets the index of the empty tile.
     *
     * @return the index
     */
    int indexOfSpace() {
        for (int i = 0; i < polje.length; i++) {
            if (polje[i] == 0) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Gets the hash code for a configuration.
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        return Arrays.hashCode(polje);
    }

    /**
     * Checks if this configuration is equal to another object.
     * <p>
     * The result is true only if {@code o} is also a configuration,
     * and has the exact same layout.
     *
     * @param o the object to compare to
     *
     * @return true if the ohter object is an identical configuration, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KonfiguracijaSlagalice)) {
            return false;
        }

        KonfiguracijaSlagalice that = (KonfiguracijaSlagalice) o;

        return Arrays.equals(polje, that.polje);

    }

    /**
     * Returns a string representation of the puzzle as a 3x3 grid.
     * <p>
     * The empty filed is shown as a '*'.
     *
     * @return the puzzle as a string (3x3 grid)
     */
    @Override
    public String toString() {

        var sb = new StringBuilder();

        for (int i = 0; i < polje.length; i++) {
            sb.append(polje[i] == 0 ? "*" : polje[i]).append(' ');
            if (i % 3 == 2 && i < polje.length - 1) { // da i nakon zadnjeg reda ne doda newline
                sb.append("\n");
            }
        }

        return sb.toString();
    }
}
