package hr.fer.zemris.java.hw06.shell;

/** Models a shell status. */
public enum ShellStatus {
    /** Signals to the shell that it should continue working. */
    CONTINUE,

    /** Signals the shell to terminate. */
    TERMINATE
}
