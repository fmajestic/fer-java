package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * hexdump
 * <p>
 * Prints a hexdump of a file.
 */
public class HexdumpCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "hexdump";

    /**
     * Prints the hexdump of a file, 16 bytes per line.
     * <p>
     * The entire file is read into memory, then printed line by line.
     *
     * @param env       the environment to invoke in
     * @param arguments a single path to a file
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args;

        try {
            args = CommandUtil.getArguments(arguments, true);
        } catch (IllegalArgumentException ex) {
            env.writeln("Invalid argument(s): " + ex.getMessage());
            return ShellStatus.CONTINUE;
        }

        if (args.size() != 1) {
            env.writeln("Invalid number of arguments. Expected: 1, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }

        Path file = CommandUtil.resolvePath(env, args.get(0));

        if (file == null) {
            env.writeln("File path is invalid.");
            return ShellStatus.CONTINUE;
        }

        byte[] bytes;

        try {
            bytes = Files.readAllBytes(file);
        } catch (IOException ex) {
            env.writeln("Error reading file " + ex.getMessage());
            return ShellStatus.CONTINUE;
        }

        for (int i = 0; i < bytes.length + 15; i += 16) {
            env.writeln(String.format(
                "%08x: %s|%s | %s",
                i,
                bytesToHex(bytes, i, 8),
                bytesToHex(bytes, i + 8, 8),
                bytesToAscii(bytes, i, 16)
            ));
        }


        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: hexdump FILE",
            "Prints the hexdump of a file, 16 bytes per line.",
            "Bytes that are valid 7-bit ASCII characters are printed,",
            "all others are replaced with a dot - '.'"
        );
    }

    /**
     * Formats a segment of an array, padding with whitespace if there are not enough elements in the array.
     *
     * @param bytes the array to read values from
     * @param index the starting index
     * @param count the number of bytes to read
     *
     * @return the bytes from the array as hex strings, with spaces between
     */
    private String bytesToHex(byte[] bytes, int index, int count) {

        String[] strings = new String[count];

        int i;

        // Add as much from the array as possible
        for (i = 0; i < count && index + i < bytes.length; i++) {
            strings[i] = String.format("%02X", bytes[index + i]);
        }

        // If i didn't reach count, that means we ran out of numbers and should pad with whitespace
        for (; i < count; i++) {
            strings[i] = " ".repeat(2);
        }

        return String.join(" ", strings);
    }

    /**
     * Converts a segment of an array to a string, converting any printable byte
     * to a corresponding character, otherwise replacing it with a . (dot).
     *
     * @param bytes the array to read values from
     * @param index the starting index
     * @param count the number of bytes to read
     *
     * @return the segment as a string
     */
    private String bytesToAscii(byte[] bytes, int index, int count) {

        var sb = new StringBuilder();

        // Add as much from the array as possible.
        // It's ok if we don't read all 'count' bytes, since that means this is the end of the dump
        for (int i = 0; i < count && index + i < bytes.length; i++) {

            if (bytes[index + i] >= 32) {
                sb.append((char) bytes[index + i]);
            } else {
                sb.append('.');
            }
        }

        return sb.toString();
    }
}