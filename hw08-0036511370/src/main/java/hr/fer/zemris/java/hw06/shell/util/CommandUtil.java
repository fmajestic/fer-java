package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.Environment;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** Provides utility methods for parsing arguments and resolving paths. */
public class CommandUtil {

    /**
     * Creates a list of arguments from a string.
     * The list is built by splitting the input string at one or more whitespace characters (regex \s+).
     *
     * @param input the string to get arguments from
     *
     * @return a list of arguments
     */
    public static List<String> getArguments(String input) {
        return getArguments(input, false);
    }

    /**
     * Creates a list of arguments from a string.
     *
     * @param input              the string to get arguments from
     * @param parseQuotedStrings whether quoted strings should be parsed or not
     *
     * @return a list of arguments
     */
    public static List<String> getArguments(String input, boolean parseQuotedStrings) {

        if (!parseQuotedStrings) {
            return input.isEmpty() ? new ArrayList<>() : Arrays.asList(input.split("\\s+"));
        }

        List<String> args = new ArrayList<>();
        ArgumentLexer lexer = new ArgumentLexer(input.toCharArray());

        for (String arg = lexer.getToken(); arg != null; arg = lexer.nextToken()) {
            args.add(arg);
        }


        return args;
    }

    /**
     * Resolves a path against the environment's current directory.
     * @param env the environment to use
     * @param path the path to resolve
     * @return the resolved path, or null if the given path is invalid
     */
    public static Path resolvePath(Environment env, String path) {
        try {
            return env.getCurrentDirectory().resolve(Paths.get(path));
        } catch (InvalidPathException ex) {
            return null;
        }
    }

    /**
     * A bare-bones lexer for getting arguments from a string.
     * Supports escaping of \ and " in strings.
     */
    private static class ArgumentLexer {

        /** The data to get arguments from. */
        private char[] data;

        /** The current index. */
        private int i;

        /** Length of the data, to reduce verbosity. */
        private int len;

        /**
         * The current token.
         * Must be initialized to something because of the call to generateToken
         */
        private String token = "";

        /**
         * Creates a new lexer with the given data.
         *
         * @param data the data to tokenize.
         */
        ArgumentLexer(char[] data) {
            this.data = data;
            len = data.length;

            generateToken();
        }

        /**
         * Generates and returns the next token.
         * <p>
         * If the returned value is null, that means end of input has been reached
         * and this method should not be called again.
         *
         * @return the next token, or null if end-of-input has been reached
         *
         * @throws IllegalArgumentException if called after end-of-input is reached
         * @throws IllegalStateException    if there is an unclosed quoted string
         */
        String nextToken() {
            generateToken();
            return getToken();
        }

        /**
         * Gets the current token.
         * <p>
         * If the returned value is null, that means end of input has been reached
         * and {@link #nextToken()} should not be called again
         *
         * @return the current token
         */
        String getToken() {
            return token;
        }

        /**
         * Generates the next token and assigns the value to {@link #token}.
         *
         * @throws IllegalArgumentException if called after end-of-input is reached
         * @throws IllegalStateException    if there is an unclosed quoted string
         */
        private void generateToken() {

            if (token == null) {
                throw new IllegalStateException("Already reached end of input.");
            }

            if (i >= len) {
                token = null;
                return;
            }

            boolean isQuoted = false;

            skipWhitespace();

            if (data[i] == '"') {
                isQuoted = true;
                i++;
            }

            int start = i;

            for (; i < len; i++) {
                if (isQuoted) {
                    if (data[i] == '"' && data[i - 1] != '\\') {
                        break;
                    }
                } else if (Character.isWhitespace(data[i])) {
                    break;
                }
            }

            int end = i;

            if (isQuoted) {
                i++;
            }

            if (isQuoted && i < len) {
                if (!Character.isWhitespace(data[i])) {
                    throw new IllegalArgumentException(
                        "Quoted string must be followed by at least one whitespace character."
                    );
                }
            }

            String res = new String(data, start, end - start);

            token = res.replace("\\\"", "\"").replace("\\\\", "\\");
        }

        /** Skips so that the index points to the first next non-whitespace character. */
        private void skipWhitespace() {
            for (; i < len; i++) {
                if (!Character.isWhitespace(data[i])) {
                    return;
                }
            }
        }
    }
}
