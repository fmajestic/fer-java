package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * mkdir - make directory
 * <p>
 * Creates a directory structure.
 */
public class MkdirCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "mkdir";

    /**
     * Creates a new directory.
     * <p>
     * If any of the parent directories are missing, they are created too.
     *
     * @param env       the environment to invoke in
     * @param arguments must contain a single path
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args;
        try {
            args = CommandUtil.getArguments(arguments, true);
        } catch (IllegalArgumentException ex) {
            env.writeln("Invalid argument(s): " + ex.getMessage());
            return ShellStatus.CONTINUE;
        }

        if (args.size() != 1) {
            env.writeln("Invalid number of arguments. Expected: 1, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }


        Path dir = CommandUtil.resolvePath(env, args.get(0));

        if (dir == null) {
            env.writeln(String.format("Invalid path: %s", args.get(0)));
            return ShellStatus.CONTINUE;
        }

        if (Files.exists(dir)) {
            env.writeln(String.format("A subdirectory or file '%s' already exists.", dir.toString()));
            return ShellStatus.CONTINUE;
        }

        try {
            Files.createDirectories(dir);
        } catch (IOException | SecurityException ex) {
            env.writeln("Error creating directory(es): " + ex.getMessage());
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: mkdir DIRECTORY",
            "Creates a new directory.",
            "Any missing parent directories are also created."
        );
    }
}
