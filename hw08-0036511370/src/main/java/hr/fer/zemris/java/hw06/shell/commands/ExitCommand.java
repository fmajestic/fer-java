package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import java.util.List;

import static hr.fer.zemris.java.hw06.shell.ShellStatus.TERMINATE;

/**
 * exit
 * <p>
 * Exits the shell.
 */
public class ExitCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "exit";

    /**
     * Sends a {@code TERMINATE} signal to the shell.
     *
     * @param env       the environment to invoke in
     * @param arguments ignored
     *
     * @return {@link ShellStatus#TERMINATE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        return TERMINATE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {

        return List.of(
            "Usage: exit",
            "Exits the shell. Any passed arguments are ignored."
        );
    }
}
