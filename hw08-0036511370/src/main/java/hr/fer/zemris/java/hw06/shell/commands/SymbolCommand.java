package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;
import hr.fer.zemris.java.hw06.shell.util.SymbolGetters;
import hr.fer.zemris.java.hw06.shell.util.SymbolGetters.ISymbolGetter;
import hr.fer.zemris.java.hw06.shell.util.SymbolSetters;
import hr.fer.zemris.java.hw06.shell.util.SymbolSetters.ISymbolSetter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Used to display or change the shell symbols.
 * <p>
 * The valid symbols are PROMPT, MULTILINE, MORELINES.
 */
public class SymbolCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "symbol";

    private static Map<String, ISymbolGetter> symbolGetters = new HashMap<>();
    private static Map<String, ISymbolSetter> symbolSetters = new HashMap<>();

    static {
        symbolGetters.put("PROMPT", SymbolGetters.PROMPT);
        symbolGetters.put("MORELINES", SymbolGetters.MORELINES);
        symbolGetters.put("MULTILINE", SymbolGetters.MULTILINE);

        symbolSetters.put("PROMPT", SymbolSetters.PROMPT);
        symbolSetters.put("MORELINES", SymbolSetters.MORELINES);
        symbolSetters.put("MULTILINE", SymbolSetters.MULTILINE);
    }

    /**
     * Displays or changes an environment symbol.
     * <p>
     * Valid symbols are PROMPT, MULTILINE, MORELINES.
     *
     * @param env       the environment to invoke in
     * @param arguments a valid symbol name, optionally followed by a new character
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args = CommandUtil.getArguments(arguments);

        if (args.size() == 1) {
            displaySymbol(args.get(0), env);
        } else if (args.size() == 2) {
            updateSymbol(args.get(0), args.get(1), env);
        } else {
            env.writeln("Invalid number of arguments. Expected: 1 or 2, actual: " + args.size());
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: symbol SYMBOL [CHARACTER]",
            "Displays or changes the SYMBOL. Valid symbols are: PROMPT, MULTILINE, MORELINES.",
            "When called with only one argument, prints the currently used symbol.",
            "When called with two arguments, the specified symbol is updated to be the given character."
        );
    }

    /**
     * Prints the given symbol to the environment, or an error message if the symbol name is unknown.
     *
     * @param name name of the symbol to print
     * @param env  the environment to print to
     */
    private void displaySymbol(String name, Environment env) {

        ISymbolGetter getter = symbolGetters.get(name);

        if (getter != null) {
            env.writeln(String.format("Symbol for %s is '%c'", name, getter.get(env)));
        } else {
            env.writeln(String.format("Unknown symbol: %s", name));
        }
    }

    /**
     * For the given environment, updates a symbol.
     *
     * @param name      the name of the symbol
     * @param newSymbol the new symbol
     * @param env       the environment to update
     */
    private void updateSymbol(String name, String newSymbol, Environment env) {

        if (newSymbol.length() != 1) {
            env.writeln(String.format("New symbol must be a single character, got: %s", newSymbol));
            return;
        }

        ISymbolSetter setter = symbolSetters.get(name);

        if (setter != null) {
            char oldChar = symbolGetters.get(name).get(env);
            char newChar = newSymbol.charAt(0);

            env.writeln(String.format("Symbol for %s changed from '%c' to '%c'", name, oldChar, newChar));
            setter.set(newChar, env);
        } else {
            env.writeln(String.format("Unknown symbol: %s", name));
        }
    }
}
