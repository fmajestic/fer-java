package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import java.util.List;
import java.util.Objects;

/**
 * pwd - print working directory
 * <p>
 * Prints the current working directory.
 */
public class PwdCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "pwd";

    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        Objects.requireNonNull(env);

        if (arguments.length() != 0) {
            env.writeln("Too many arguments. Expected: 0.");
            return ShellStatus.CONTINUE;
        }

        env.writeln(env.getCurrentDirectory().toString());

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: pwd",
            "Prints the current working directory"
        );
    }
}
