package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

/**
 * popd - pop directory
 * <p>
 * Pops a directory from the stack and changes into it.
 */
public class PopdCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "popd";

    /**
     * Pops a single directory off the cd stack and changes into it.
     *
     * @param env       the environment to invoke in
     * @param arguments must be empty
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        if (arguments.length() != 0) {
            env.writeln("Invalid number of arguments. Expected: 0.");
        }

        //noinspection unchecked
        Stack<Path> cdstack = (Stack<Path>) env.getSharedData("cdstack");

        if (cdstack == null || cdstack.isEmpty()) {
            env.writeln("No directories on the stack.");
            return ShellStatus.CONTINUE;
        }

        Path dir = cdstack.pop();

        if (Files.exists(dir)) {
            env.setCurrentDirectory(dir);
        } else {
            env.writeln("Directory was popped but does not exist. Current directory not changed.");
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: popd",
            "Pops a directory off the stack and changes into it."
        );
    }
}
