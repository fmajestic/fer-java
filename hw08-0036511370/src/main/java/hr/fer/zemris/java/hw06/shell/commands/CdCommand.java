package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * cd - change directory
 * <p>
 * Changes the current working directory.
 */
public class CdCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "cd";

    /**
     * Updates the environment's current directory via {@link Environment#setCurrentDirectory(Path)}.
     *
     * @param env       the environment to invoke in
     * @param arguments a single path to a directory, or nothing
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args = CommandUtil.getArguments(arguments, true);

        if (args.size() > 1) {
            env.writeln("Invalid number of arguments. Expected: 0 or 1, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }

        if (args.size() == 0) {
            env.setCurrentDirectory(env.getCurrentDirectory().resolve(Paths.get(".")));
        } else {

            try {
                env.setCurrentDirectory(CommandUtil.resolvePath(env, args.get(0)));
            } catch (InvalidPathException ex) {
                env.writeln(String.format("Error for '%s': %s", ex.getInput(), ex.getReason()));
            }
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: cd DIRECTORY",
            "Changes the current working directory to DIRECTORY, if it exists.",
            "When called with no arguments, changes the current directory to the initial one."
        );
    }
}
