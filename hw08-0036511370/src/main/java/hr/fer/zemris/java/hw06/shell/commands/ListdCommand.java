package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

/**
 * listd - list directories
 * <p>
 * Lists all the directories currently on the stack.
 */
public class ListdCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "listd";

    /**
     * Lists all directories currently on the cdstack, in order from first added to last added.
     *
     * @param env       the environment to invoke in
     * @param arguments must be empty
     *
     * @return the shell status after execution
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        if (arguments.length() != 0) {
            env.writeln("Invalid number of arguments. Expected: 0.");
        }

        //noinspection unchecked
        Stack<Path> cdstack = (Stack<Path>) env.getSharedData("cdstack");

        if (cdstack != null && !cdstack.isEmpty()) {
            cdstack.forEach(p -> env.writeln(p.toString()));
        } else {
            env.writeln("No directories on the stack.");
        }

        return ShellStatus.CONTINUE;

    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: listd",
            "Lists all directories currently on the stack."
        );
    }
}
