package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.ShellIOException;

import java.io.IOError;
import java.util.Objects;

/** A wrapper around the {@link java.io.Console} class. */
public class SystemConsoleWrapper implements Console {

    /** The backing console. */
    private java.io.Console console;

    /**
     * Creates a new console wrapper with the given system console.
     * <p>
     * The given console can't be null.
     *
     * @param console the backing system console to use
     *
     * @throws NullPointerException if {@code console} is null
     */
    public SystemConsoleWrapper(java.io.Console console) {
        this.console = Objects.requireNonNull(console);
    }

    /** {@inheritDoc} */
    @Override
    public void print(String text) {
        console.printf("%s", text);
    }

    /** {@inheritDoc} */
    @Override
    public void println(String text) {
        console.printf("%s%n", text);
    }

    /** {@inheritDoc} */
    @Override
    public String nextLine() {
        try {
            return console.readLine();
        } catch (IOError ex) {
            throw new ShellIOException("Error reading from stdin.");
        }
    }
}
