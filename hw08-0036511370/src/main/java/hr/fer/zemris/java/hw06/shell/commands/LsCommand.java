package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * ls - list
 * <p>
 * Lists information about files and folders in the given dictionary.
 */
public class LsCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "ls";

    /** The date format used when printing file info. */
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * Gets the list of all direct children of the given directory and prints them.
     *
     * @param env       the environment to invoke in
     * @param arguments empty, or a single path to a directory
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args = CommandUtil.getArguments(arguments, true);

        if (args.size() > 1) {
            env.writeln("Invalid number of arguments. Expected: 1 or none, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }

        Path src;

        try {
            src = env.getCurrentDirectory()
                .resolve(
                    args.size() == 0
                    ? Paths.get(".")
                    : Paths.get(args.get(0))
                );
        } catch (InvalidPathException ex) {
            env.writeln("Invalid path: " + ex.getMessage());
            return ShellStatus.CONTINUE;
        }

        if (Files.isRegularFile(src)) {
            env.writeln("'" + src.toString() + "' is a file.");
            return ShellStatus.CONTINUE;
        }

        if (Files.notExists(src)) {
            env.writeln("Directory '" + src.toString() + "' does not exist.");
            return ShellStatus.CONTINUE;
        }

        try (DirectoryStream<Path> files = Files.newDirectoryStream(src)) {
            for (var file : files) {

                BasicFileAttributes attrib = Files.getFileAttributeView(
                    file, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS
                ).readAttributes();

                FileTime creationTime = attrib.creationTime();

                String formattedDateTime = dateFormat.format(new Date(creationTime.toMillis()));

                boolean directory = Files.isDirectory(file);
                boolean readable = Files.isReadable(file);
                boolean writable = Files.isWritable(file);
                boolean executable = Files.isExecutable(file);

                env.writeln(String.format(
                    "%s %10d %s %s",
                    getAttributeString(directory, readable, writable, executable),
                    Files.size(file),
                    formattedDateTime,
                    file.getFileName()
                ));
            }
        } catch (IOException ex) {
            env.writeln("Error reading directory contents.");
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} **/
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} **/
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: ls [DIRECTORY]",
            "List information about the files in DIRECTORY (the current directory by default).",
            "Entries are sorted alphabetically by name.",
            "The output columns are: ATTRIBUTES, SIZE, CREATED_ON, NAME"
        );
    }

    /**
     * Gets a string representation of basic file attributes.
     * <p>
     * For all arguments true, the return string is {@code "drwx"}.
     * For every false argument, the corresponding letter is changed to a -, like {@code "-rw-"}.
     *
     * @param d is the file a directory
     * @param r is the file readable
     * @param w is the file writable
     * @param x is the file executable
     *
     * @return a string representing the attributes
     */
    private String getAttributeString(boolean d, boolean r, boolean w, boolean x) {
        return String.format(
            "%c%c%c%c",
            d ? 'd' : '-',
            r ? 'r' : '-',
            w ? 'w' : '-',
            x ? 'x' : '-'
        );
    }
}
