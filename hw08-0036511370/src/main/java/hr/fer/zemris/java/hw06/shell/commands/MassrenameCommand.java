package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;
import hr.fer.zemris.java.hw06.shell.util.FilterResult;
import hr.fer.zemris.java.hw06.shell.util.NameBuilder;
import hr.fer.zemris.java.hw06.shell.util.NameBuilderParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hr.fer.zemris.java.hw06.shell.util.MassRenameActions.*;

/**
 * massrename - batch renaming of files
 */
public class MassrenameCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "massrename";

    /**
     * Filters all files that are direct children of {@code dir} based on a regular expression.
     * <p>
     * The regular expression is compiled as <b><i>case insensitive</i></b>.
     * <p>
     * An exception is thrown if the given path does not point to a directory, or an IO error occurs.
     *
     * @param dir   the parent directory
     * @param regex the regular expression to check filenames against
     *
     * @return the list of results that fully match the regular expression
     *
     * @throws IOException if an IO error occurs
     */
    private static List<FilterResult> filter(Path dir, String regex) throws IOException {

        List<FilterResult> ret = new ArrayList<>();
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

        for (Path file : Files.newDirectoryStream(dir)) {

            if (Files.isDirectory(file)) {
                continue;
            }

            Matcher matcher = pattern.matcher(file.getFileName().toString());

            // Calling matches() advances the matcher, so groups are generated
            // and calls to group(int) won't throw
            if (matcher.matches()) {
                ret.add(new FilterResult(file, matcher));
            }
        }

        return ret;
    }

    /**
     * Prints the contents of a file, with an optional charset.
     *
     * @param env       the environment to invoke in
     * @param arguments arguments of the command
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args = CommandUtil.getArguments(arguments, true);

        if (args.size() < 4 || args.size() > 5) {
            env.writeln("Invalid number of arguments. Expected: 4 or 5, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }

        Path srcDir = CommandUtil.resolvePath(env, args.get(0));
        Path destDir = CommandUtil.resolvePath(env, args.get(1));

        String command = args.get(2);
        String regex = args.get(3);
        String substitution = null;

        if (!command.matches("filter|groups|show|execute")) {
            env.writeln("Invalid command.");
            return ShellStatus.CONTINUE;
        }

        if (srcDir == null || Files.notExists(srcDir)) {
            env.writeln("Source directory is invalid or doesn't exist.");
            return ShellStatus.CONTINUE;
        } else if (destDir == null || Files.notExists(destDir)) {
            env.writeln("Destination directory is invalid or doesn't exist.");
            return ShellStatus.CONTINUE;
        }

        if (command.matches("show|execute")) {
            if (args.size() != 5) {
                env.writeln(String.format("Command '%s' requires a substitution as an additional argument.", command));
                return ShellStatus.CONTINUE;
            }

            substitution = args.get(4);
        }

        try {
            List<FilterResult> files = filter(srcDir, regex);

            if (command.matches("filter|groups")) {
                SimpleAction action = command.equals("filter") ? printFilename : printGroups;

                for (FilterResult file : files) {
                    action.invoke(env, file);
                }
            } else {
                NameBuilder builder = new NameBuilderParser(substitution).getNameBuilder();
                SubstitutionAction action;

                if (command.equals("show")) {
                    action = printNewFilename;
                } else {
                    action = renameFile;

                    // Save directories for moving
                    env.setSharedData("__massrename_srcdir", srcDir);
                    env.setSharedData("__massrename_destdir", destDir);
                }

                for (FilterResult file : files) {
                    action.invoke(env, file, builder);
                }

                // Erase data, not good practice to leave it laying around
                env.setSharedData("__massrename_srcdir", null);
                env.setSharedData("__massrename_destdir", null);
            }
        } catch (IOException ex) {
            env.writeln(String.format("IO error: %s", ex.getMessage()));
        } catch (InvalidPathException ex) {
            env.writeln(String.format("Invalid path: %s", ex.getMessage()));
        } catch (IllegalArgumentException ex) {
            env.writeln(ex.getMessage());
        } catch (IndexOutOfBoundsException ex) {
            env.writeln(String.format("Substitution group index does not exist: %s", ex.getMessage()));
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: massrename SRCDIR DESTDIR COMMAND PATTERN [SUBSTITUTION]",
            "Performs a batch rename of all files in SRCDIR that match PATTERN according to the substitution pattern.",
            "Note that `show' and `invoke' require the additional SUBSTITUTION argument.",
            "Available commands:",
            "\tfilter\t\tonly printFilename matching filenames, don't rename",
            "\tgroups\t\tonly show matched groups, don't rename",
            "\tshow\t\tonly show what each name would change to, don't rename",
            "\tinvoke\t\tactually perform the rename"
        );
    }
}
