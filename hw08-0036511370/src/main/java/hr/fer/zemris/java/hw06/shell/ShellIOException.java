package hr.fer.zemris.java.hw06.shell;

/**
 * Thrown when a fatal IO error occurs during shell operation.
 * <p>
 * A fatal error is considered an error after which communication with the user is no longer possible.
 */
public class ShellIOException extends RuntimeException {

    /**
     * Default constructor. Delegated to {@code super()}.
     */
    public ShellIOException() {
        super();
    }

    /**
     * Constructor with an error message. Delegated to {@code super(message)}
     * @param message the error message
     */
    public ShellIOException(String message) {
        super(message);
    }
}
