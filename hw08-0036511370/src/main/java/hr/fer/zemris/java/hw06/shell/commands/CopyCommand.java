package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * copy
 * <p>
 * Copies a file to a destination, asking to confirm an overwrite if the destination file already exists.
 */
public class CopyCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "copy";

    /**
     * Copies a file to a destination, prompting when it would overwrite an existing file.
     * <p>
     * The files are copied by opening buffered streams on each, then reading and writing in 1 kB chunks.
     *
     * @param env       the environment to invoke in
     * @param arguments a path to a file, then a path to a destination (file/directory)
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args;
        try {
            args = CommandUtil.getArguments(arguments, true);
        } catch (IllegalArgumentException ex) {
            env.writeln(ex.getMessage());
            return ShellStatus.CONTINUE;
        }

        if (args.size() != 2) {
            env.writeln("Invalid number of arguments. Expected: 2, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }

        Path src = CommandUtil.resolvePath(env, args.get(0));
        Path dest = CommandUtil.resolvePath(env, args.get(1));

        if (src == null || dest == null) {
            env.writeln("Source or destination path is invalid.");
            return ShellStatus.CONTINUE;
        }

        if (Files.notExists(src)) {
            env.writeln(String.format("File '%s' not found.", src.toString()));
            return ShellStatus.CONTINUE;
        }

        if (!Files.isRegularFile(src)) {
            env.writeln(String.format("'%s' is a directory.", src.toString()));
            return ShellStatus.CONTINUE;
        }

        if (Files.isDirectory(dest)) {

            if (Files.notExists(dest)) {
                env.writeln(String.format("Destination folder '%s' does not exist.", dest.toString()));
                return ShellStatus.CONTINUE;
            }

            dest = dest.resolve(src.getFileName());
        }

        if (Files.exists(dest)) {

            String response = getOverwriteConfirmation(env);

            if (response.matches("(?i)n|no")) {
                env.writeln("Cancelling...");
                return ShellStatus.CONTINUE;
            }
        }

        try {
            copyFile(src, dest);
        } catch (IOException ex) {
            env.write("Error reading file: " + ex.getMessage());
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: copy SRC DEST",
            "Copies the file SRC to DEST. If DEST is a directory, the source file name is used.",
            "If the destination file exists, you will be prompted to confirm an overwrite."
        );
    }

    /**
     * Prompts the user to confirm an overwrite, and returns the result.
     *
     * Valid input is "y", "yes", "n", or "no" (upper or lowercase).
     *
     * @param env the environment to read from and write to
     *
     * @return the user input, one of {@code ["y", "yes", "n", "no"]}
     */
    private String getOverwriteConfirmation(Environment env) {
        String response;
        do {
            env.write("Destination file exists. Overwrite [y/n]? ");
            response = env.readLine();
        } while (!response.matches("(?i)y|n|yes|no"));
        return response;
    }

    /**
     * Copies a source file to a destination in 1 kB chunks.
     * <p>
     * Both of the given paths must be files.
     *
     * @param src  the source file
     * @param dest the destination file
     */
    private void copyFile(Path src, Path dest) throws IOException {
        try (InputStream is = new BufferedInputStream(Files.newInputStream(src));
             OutputStream os = new BufferedOutputStream(Files.newOutputStream(dest))) {

            int nread;
            byte[] buf = new byte[4096];

            while ((nread = is.read(buf)) > 0) {
                os.write(buf, 0, nread);
            }
        }
    }
}
