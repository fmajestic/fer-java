package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.Environment;

/**
 * Contains getters for retrieving all currently available environment symbols.
 *
 * The advantage of using strategies like this is easy addition of new symbols.
 * All that you need to add is a new strategy (method reference), and an entry in the getter map.
 */
public class SymbolGetters {

    /** Getter for the PROMPT symbol. */
    public static final ISymbolGetter PROMPT = Environment::getPromptSymbol;

    /** Getter for the MORELINES symbol. */
    public static final ISymbolGetter MORELINES = Environment::getMorelinesSymbol;

    /** Getter for the MULTILINE symbol. */
    public static final ISymbolGetter MULTILINE = Environment::getMultilineSymbol;


    /** A getter for an environment symbol. */
    @FunctionalInterface
    public interface ISymbolGetter {

        /**
         * Gets a symbol from the given environment.
         *
         * @param env the environment to get from
         *
         * @return the specified symbol
         */
        char get(Environment env);
    }
}
