package hr.fer.zemris.java.hw06.shell.util;

/** Models an IO console for communication with the user. */
public interface Console {

    /** The default console that can be used to communicate with the user. */
    Console DEFAULT = System.console() == null
                      ? new StreamConsole(System.in, System.out)
                      : new SystemConsoleWrapper(System.console());

    /**
     * Prints some text to the console.
     *
     * @param text the text to print
     */
    void print(String text);

    /**
     * Prints some text to the console, followed by a newline.
     *
     * @param text the text to print
     */
    void println(String text);

    /**
     * Reads the next line of input from the console.
     *
     * @return the entered line
     */
    String nextLine();

}
