package hr.fer.zemris.java.hw06.shell.util;

/**
 * Models a builder that creates a new file name based on a source file,
 * and stores the result in a given string builder.
 * <p>
 * Using a {@code StringBuilder} together with the {@link #then(NameBuilder)} method
 * enables easy composition of different builders to create the final name.
 */
@FunctionalInterface
public interface NameBuilder {

    /**
     * Executes the stored action on the given arguments.
     *
     * @param file a file filter result
     * @param sb   the builder to append to
     */
    void execute(FilterResult file, StringBuilder sb);

    /**
     * Allows for creation of composite builders, chaining another builder to this one
     * and returning the combined builder.
     *
     * @param other the builder to invoke after this one.
     *
     * @return a new composite builder
     */
    default NameBuilder then(NameBuilder other) {
        return (fr, sb) -> {
            this.execute(fr, sb);
            other.execute(fr, sb);
        };
    }
}
