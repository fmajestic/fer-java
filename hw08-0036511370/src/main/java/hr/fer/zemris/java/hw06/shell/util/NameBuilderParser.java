package hr.fer.zemris.java.hw06.shell.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hr.fer.zemris.java.hw06.shell.util.NameBuilders.group;
import static hr.fer.zemris.java.hw06.shell.util.NameBuilders.text;

/** A parser for file name conversion expressions. */
public class NameBuilderParser {

    /** The expression to be parsed. */
    private String expression;

    /** The name builder. */
    private NameBuilder nameBuilder = null;

    /**
     * Initializes the parser with an expression.
     *
     * @param expression the expression to parse
     */
    public NameBuilderParser(String expression) {
        this.expression = expression;
    }

    /**
     * Generates and returns the full name builder based on the given expression.
     * <p>
     * Note that the builder is only generated once, every call to this method after the first one
     * will return the same builder object.
     *
     * @return the full name builder
     *
     * @throws IllegalArgumentException if the given expression contains an invalid substitution rule
     */
    public NameBuilder getNameBuilder() {

        if (nameBuilder == null) {
            nameBuilder = NameBuilders.EMPTY;
            generateFullBuilder(expression);
        }

        return nameBuilder;
    }

    /**
     * Generates a full name builder from the given expression.
     *
     * @param expression the expression to create the builder for
     *
     * @throws IllegalArgumentException if the expression contains an invalid substitution rule
     */
    private void generateFullBuilder(String expression) throws IllegalArgumentException {

        Pattern tagPattern = Pattern.compile("\\$\\{(?<rule>.*?)}", Pattern.UNICODE_CASE);

        Matcher matcher = tagPattern.matcher(expression);

        int textStart = 0;

        while (matcher.find()) {

            String rule = matcher.group("rule").trim();

            nameBuilder = nameBuilder.then(text(expression.substring(textStart, matcher.start())));

            if (rule.matches("\\d+")) {

                nameBuilder = nameBuilder.then(group(Integer.parseInt(rule)));
            } else if (rule.matches("\\d+\\s*(,\\s*\\d+)?")) {

                String[] parts = rule.split(",");

                char padding = ' ';
                int minWidth;
                int groupIndex = Integer.parseInt(parts[0]);

                if (parts[1].trim().startsWith("0") && parts[1].length() > 1) {
                    padding = '0';
                    parts[1] = parts[1].substring(1);
                }

                minWidth = Integer.parseInt(parts[1]);

                nameBuilder = nameBuilder.then(group(groupIndex, padding, minWidth));
            } else {
                throw new IllegalArgumentException("Invalid substitution rule: " + matcher.group());
            }

            textStart = matcher.end();
        }

        nameBuilder = nameBuilder.then(text(expression.substring(textStart)));
    }
}
