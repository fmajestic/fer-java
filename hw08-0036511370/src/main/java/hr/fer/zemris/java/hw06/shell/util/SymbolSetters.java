package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.Environment;

/**
 * Contains setters for setting all currently available environment symbols.
 * <p>
 * The advantage of using strategies like this is easy addition of new symbols.
 * All that you need to do is add a new strategy and an entry in the getter map.
 */
public class SymbolSetters {

    /** Setter for the PROMPT symbol. */
    public static final ISymbolSetter PROMPT = (c, env) -> env.setPromptSymbol(c);

    /** Setter for the MORELINES symbol. */
    public static final ISymbolSetter MORELINES = (c, env) -> env.setMorelinesSymbol(c);

    /** Setter for the MULTILINE symbol. */
    public static final ISymbolSetter MULTILINE = (c, env) -> env.setMultilineSymbol(c);

    /** A setter for an environment symbol. */
    public interface ISymbolSetter {

        /**
         * Sets a new symbol for the given environment.
         *
         * @param c   the new symbol
         * @param env the environment to get from
         */
        void set(char c, Environment env);
    }
}
