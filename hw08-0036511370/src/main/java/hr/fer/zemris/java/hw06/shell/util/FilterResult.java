package hr.fer.zemris.java.hw06.shell.util;

import java.nio.file.Path;
import java.util.Objects;
import java.util.regex.Matcher;

/**
 * Contains information about a single file that got filtered.
 */
public class FilterResult {

    /** The pattern match result for this file. */
    private Matcher matchResult;

    /** The file that was accepted by te filter. */
    private Path file;

    /**
     * Creates a new filter result for a file, with associated info.
     *
     * @param file the file
     * @param matchResult the matcher result for this file
     */
    public FilterResult(Path file, Matcher matchResult) {
        this.file = file;
        this.matchResult = matchResult;
    }

    /**
     * The number of filter groups in this file's name.
     *
     * @return he number of groups
     */
    public int numberOfGroups() {
        return matchResult.groupCount();
    }

    /**
     * Gets the filter group at the specified index.
     *
     * @param index the index to get from
     *
     * @return the group at the index
     *
     * @throws IndexOutOfBoundsException if the given index is negative or greater than the number of groups
     */
    public String group(int index) {

        Objects.checkIndex(index,matchResult.groupCount() + 1);

        return matchResult.group(index);
    }

    /**
     * Returns the string representation of the file,
     * specifically by getting {@link Path#getFileName()}, then calling {@link Path#toString()} on it.
     *
     * @return the file name.
     */
    @Override
    public String toString() {
        return file.getFileName().toString();
    }
}
