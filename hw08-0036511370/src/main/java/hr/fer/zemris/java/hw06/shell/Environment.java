package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Path;
import java.util.SortedMap;

/**
 * Models a shell environment for command execution.
 */
public interface Environment {

    /**
     * Reads a line of input from the console.
     *
     * @return the read line
     *
     * @throws ShellIOException if an error occurs during reading
     */
    String readLine() throws ShellIOException;

    /**
     * Writes the given text to the console.
     *
     * @param text the text to write
     *
     * @throws ShellIOException if an error occurs during output
     */
    void write(String text) throws ShellIOException;

    /**
     * Writes the given text to the console, followed by a newline.
     *
     * @param text the text to write
     *
     * @throws ShellIOException if an error occurs during output
     */
    void writeln(String text) throws ShellIOException;

    /**
     * Gets the map of all available commands.
     *
     * @return the map of all commands
     */
    SortedMap<String, ShellCommand> commands();

    /**
     * Gets the symbol printed when entering multiline commands.
     * <p>
     * This symbol is printed instead of the regular prompt when a command is being entered
     * in multiline mode with a more-lines symbol.
     *
     * @return the current multiline symbol
     */
    Character getMultilineSymbol();

    /**
     * Sets the symbol for multiline commands.
     * <p>
     * This symbol is printed instead of the regular prompt when a command is being entered
     * in multiline mode with a more-lines symbol.
     *
     * @param symbol the new multiline symbol
     */
    void setMultilineSymbol(Character symbol);

    /**
     * Gets the current prompt symbol.
     *
     * @return the current prompt symbol
     */
    Character getPromptSymbol();

    /**
     * Sets the prompt symbol.
     *
     * @param symbol the new prompt symbol
     */
    void setPromptSymbol(Character symbol);

    /**
     * Gets the current more-lines symbol.
     * <p>
     * This symbol, when entered as the last character in input, indicates line continuation,
     * i.e. that the command spans multiple lines.
     *
     * @return the current more-lines symbol
     */
    Character getMorelinesSymbol();

    /**
     * Sets the more-lines symbol.
     * <p>
     * This symbol, when entered as the last character in input, indicates line continuation,
     * i.e. that the command spans multiple lines.
     *
     * @param symbol the new more-lines symbol
     */
    void setMorelinesSymbol(Character symbol);

    /**
     * Gets the current working directory.
     *
     * @return the current directory
     */
    Path getCurrentDirectory();

    /**
     * Sets the current working directory.
     *
     * @param path the new working directory
     */
    void setCurrentDirectory(Path path);

    /**
     * Gets the shared data from a key.
     *
     * If no such key exists, null is returned.
     *
     * @param key the key the data is associated with
     * @return the stored data, or null if no such key exists
     */
    Object getSharedData(String key);

    /**
     * Stores data to be shared later.
     *
     * @param key the key for accessing the data
     * @param value the data to store
     */
    void setSharedData(String key, Object value);
}
