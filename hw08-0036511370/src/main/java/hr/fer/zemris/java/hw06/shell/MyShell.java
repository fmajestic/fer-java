package hr.fer.zemris.java.hw06.shell;

import java.util.Map;

import static hr.fer.zemris.java.hw06.shell.ShellStatus.CONTINUE;
import static hr.fer.zemris.java.hw06.shell.ShellStatus.TERMINATE;

/**
 * A simple shell.
 * <p>
 * Supported commands: ls, cat, mkdir, copy, tree, hexdump, charset,
 * help, exit, cd, pwd, pushd, popd, dropd, listd
 */
public class MyShell {

    /**
     * Entry point of the program.
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        Environment env = new ShellEnvironment();
        Map<String, ShellCommand> commands = env.commands();

        ShellStatus status = CONTINUE;
        env.writeln("Welcome to MyShell v 1.0");

        do {
            try {
                env.write(String.format("%c ", env.getPromptSymbol()));

                String line = readInput(env);

                String[] parts = line.split("\\s+", 2);

                String commandName = getCommandName(parts);
                String arguments = getArguments(parts);

                ShellCommand command = commands.get(commandName);

                if (command == null) {
                    env.writeln(String.format("'%s' is not recognized as a command.", commandName));
                    continue;
                }

                status = command.executeCommand(env, arguments);
            } catch (ShellIOException ex) {
                status = TERMINATE;
            }
        } while (status != TERMINATE);

    }

    /**
     * Gets the command name from parts of a string that has been split.
     *
     * @param parts the parts of the input string
     *
     * @return the command name, or an empty string if the array is empty
     */
    private static String getCommandName(String[] parts) {
        return parts.length > 0 ? parts[0] : "";
    }

    /**
     * Gets the command arguments from parts of a string that has been split.
     *
     * @param parts the parts of the input string
     *
     * @return the command arguments, or an empty string if the array has no 2nd element
     */
    private static String getArguments(String[] parts) {
        return parts.length > 1 ? parts[1] : "";
    }

    /**
     * Reads user input, handling a command that spans multiple lines.
     *
     * @param env the environment to read from
     *
     * @return the entered command and arguments as a single string, with no line continuation characters
     */
    private static String readInput(Environment env) {
        char altPrompt = env.getMultilineSymbol();
        char continuation = env.getMorelinesSymbol();

        String input;
        var sb = new StringBuilder();

        while ((input = env.readLine()).endsWith(Character.toString(continuation))) {
            sb.append(input, 0, input.length() - 1);
            env.write(String.format("%c ", altPrompt));
        }

        return sb.append(input).toString();
    }
}
