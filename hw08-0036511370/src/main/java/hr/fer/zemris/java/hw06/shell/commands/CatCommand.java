package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * cat - concatenate
 * <p>
 * Displays contents of a text file to the console, using an optional custom charset.
 */
public class CatCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "cat";

    /**
     * Prints the contents of a file, with an optional charset.
     *
     * @param env       the environment to invoke in
     * @param arguments a single path to a file, followed by an optional charset
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> args = CommandUtil.getArguments(arguments, true);

        if (args.size() < 1 || args.size() > 2) {
            env.writeln("Invalid number of arguments. Expected: 1 or 2, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }

        Charset charset;
        Path src = CommandUtil.resolvePath(env, args.get(0));

        if (src == null) {
            env.writeln("Source is invalid.");
            return ShellStatus.CONTINUE;
        }


        try {
            charset = args.size() == 2 ? Charset.forName(args.get(1)) : Charset.defaultCharset();
        } catch (IllegalCharsetNameException ex) {
            env.writeln("Illegal charset name: " + args.get(1));
            return ShellStatus.CONTINUE;
        } catch (UnsupportedCharsetException ex) {
            env.writeln("Unsupported charset: " + args.get(1));
            env.writeln("See the list of available charsets with the command 'charsets'.");
            return ShellStatus.CONTINUE;
        }


        if (Files.isDirectory(src)) {
            env.writeln("'" + src.toString() + "' is a directory.");
            return ShellStatus.CONTINUE;
        }

        if (Files.notExists(src)) {
            env.writeln("File '" + src.toString() + "' does not exist.");
            return ShellStatus.CONTINUE;
        }

        try {
            Files.readAllLines(src, charset).forEach(env::writeln);
        } catch (IOException ex) {
            env.writeln(String.format("Error: could not read file '%s'", src.toString()));
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
            "Usage: cat FILE [CHARSET]",
            "Displays the contents of a file, using an optional custom charset."
        );
    }
}
