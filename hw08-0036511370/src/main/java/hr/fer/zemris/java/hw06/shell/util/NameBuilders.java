package hr.fer.zemris.java.hw06.shell.util;

public class NameBuilders {

    /**
     * The empty name builder. Performs no action on either of the arguments.
     * <p>
     * Useful for initialization and avoiding constant null checks.
     */
    public static final NameBuilder EMPTY = (fr, sb) -> { };

    /**
     * Appends some text to the name.
     *
     * @param t the text to append
     *
     * @return this builder
     */
    public static NameBuilder text(String t) {
        return (fr, sb) -> sb.append(t);
    }

    /**
     * Appends a matched group to the name.
     *
     * @param index index of the group to append
     *
     * @return this builder
     */
    public static NameBuilder group(int index) {
        return (fr, sb) -> sb.append(fr.group(index));
    }

    /**
     * Appends a matched group to the name, padded to minWidth with the specified charcter.
     *
     * @param index    index of the group to append
     * @param padding  the character to pad with
     * @param minWidth the minimal width of the group
     *
     * @return this builder
     */
    public static NameBuilder group(int index, char padding, int minWidth) {
        return (fr, sb) -> {
            String text = fr.group(index);
            sb.append(Character.toString(padding).repeat(minWidth - text.length()));
            sb.append(text);
        };
    }
}
