package hr.fer.zemris.java.hw06.shell;

import java.util.Collections;
import java.util.List;

/** Models a shell command. */
public interface ShellCommand {

    /**
     * Executes the command within an environment, using the given arguments.
     *
     * @param env       the environment to invoke in
     * @param arguments the command arguments
     *
     * @return {@link ShellStatus#CONTINUE} if the command executed successfully,
     *     {@link ShellStatus#TERMINATE} if a fatal error occurs
     */
    ShellStatus executeCommand(Environment env, String arguments);

    /**
     * Gets the command name.
     *
     * @return the command name
     */
    String getCommandName();

    /**
     * Gets the command description as an unmodifiable list of strings.
     * <p>
     * Each string is a line of output.
     *
     * @return the description
     *
     * @see Collections#unmodifiableList(List)
     */
    List<String> getCommandDescription();
}
