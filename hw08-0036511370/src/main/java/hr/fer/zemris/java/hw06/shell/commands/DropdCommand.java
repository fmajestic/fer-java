package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

/**
 * dropd - drop directory
 * <p>
 * Pops a directory off the stack, ignoring the result.
 */
public class DropdCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "dropd";

    /**
     * Pops a single directory off the cdstack, ignoring the returned value.
     *
     * @param env       the environment to invoke in
     * @param arguments must be empty
     *
     * @return the shell status after execution
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        if (arguments.length() != 0) {
            env.writeln("Invalid number of arguments. Expected: 0.");
        }

        //noinspection unchecked
        Stack<Path> cdstack = (Stack<Path>) env.getSharedData("cdstack");

        if (cdstack != null && !cdstack.isEmpty()) {
            cdstack.pop();
        } else {
            env.writeln("No directories on the stack.");
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return List.of(
                "Usage: dropd",
                "Pops a directory off the stack, ignoring the result."
            );
    }
}
