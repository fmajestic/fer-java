package hr.fer.zemris.java.hw06.shell;

import hr.fer.zemris.java.hw06.shell.commands.*;
import hr.fer.zemris.java.hw06.shell.util.Console;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** The implementation of {@link Environment} used by {@link MyShell}. */
public class ShellEnvironment implements Environment {

    /** All available commands. */
    private SortedMap<String, ShellCommand> commands;

    /** Shared data. */
    private Map<String, Object> sharedData = new HashMap<>();

    /** The prompt character. */
    private char prompt;

    /** The more-lines character */
    private char morelines;

    /** The line continuation character. */
    private char multiline;

    /** The current console we are reading from / writing to. */
    private Console console = Console.DEFAULT;

    /** The current working directory. */
    private Path currentDirectory;

    /**
     * Creates a new shell environment with the given console for user IO.
     *
     * @param console the console to use for keyboard IO
     */
    public ShellEnvironment(Console console) {
        this();
        this.console = console;
    }

    /** Creates a new shell environment with a default console for user IO. */
    public ShellEnvironment() {
        prompt = '>';
        multiline = '|';
        morelines = '\\';

        setCurrentDirectory(Paths.get("."));

        commands = new TreeMap<>(Stream.of(
                new LsCommand(),
                new CatCommand(),
                new CopyCommand(),
                new HelpCommand(),
                new ExitCommand(),
                new TreeCommand(),
                new MkdirCommand(),
                new SymbolCommand(),
                new HexdumpCommand(),
                new CharsetsCommand(),
                new CdCommand(),
                new PwdCommand(),
                new PopdCommand(),
                new PushdCommand(),
                new DropdCommand(),
                new ListdCommand(),
                new MassrenameCommand()
            ).collect(Collectors.toMap(ShellCommand::getCommandName, Function.identity()))
        );
    }

    /** {@inheritDoc} */
    @Override
    public String readLine() throws ShellIOException {
        return console.nextLine();
    }

    /** {@inheritDoc} */
    @Override
    public void write(String text) throws ShellIOException {
        console.print(text);
    }

    /** {@inheritDoc} */
    @Override
    public void writeln(String text) throws ShellIOException {
        console.println(text);
    }

    /** {@inheritDoc} */
    @Override
    public SortedMap<String, ShellCommand> commands() {
        return Collections.unmodifiableSortedMap(commands);
    }

    /** {@inheritDoc} */
    @Override
    public Character getMultilineSymbol() {
        return multiline;
    }

    /** {@inheritDoc} */
    @Override
    public void setMultilineSymbol(Character symbol) {
        this.multiline = symbol;
    }

    /** {@inheritDoc} */
    @Override
    public Character getPromptSymbol() {
        return prompt;
    }

    /** {@inheritDoc} */
    @Override
    public void setPromptSymbol(Character symbol) {
        this.prompt = symbol;
    }

    /** {@inheritDoc} */
    @Override
    public Character getMorelinesSymbol() {
        return morelines;
    }

    /** {@inheritDoc} */
    @Override
    public void setMorelinesSymbol(Character symbol) {
        this.morelines = symbol;
    }

    /** {@inheritDoc} */
    @Override
    public Path getCurrentDirectory() {
        return currentDirectory;
    }

    /**
     * {@inheritDoc}
     *
     * @throws NullPointerException if {@code path} is null
     */
    @Override
    public void setCurrentDirectory(Path path) {
        Path actual =
            Objects.requireNonNull(path, "Current directory can't be null").normalize().toAbsolutePath();

        if (Files.notExists(actual) || !Files.isDirectory(actual)) {
            throw new InvalidPathException(path.toString(), "Path does not exist or does not point to a directory.");
        }

        currentDirectory = actual;
    }

    /** {@inheritDoc} */
    @Override
    public Object getSharedData(String key) {
        return sharedData.get(key);
    }

    /** {@inheritDoc} */
    @Override
    public void setSharedData(String key, Object value) {
        sharedData.put(key, value);
    }
}
