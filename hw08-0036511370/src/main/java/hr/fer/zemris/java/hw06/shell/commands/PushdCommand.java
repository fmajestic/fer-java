package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

/**
 * pushd - push directory.
 * <p>
 * Pushes a directory onto the stack and changes into it.
 */
public class PushdCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "pushd";

    /** Description of the command, a list of lines. */
    private static List<String> description;

    static {
        description = List.of(
            "Usage: pushd DIRECTORY",
            "Pushes a directory onto the directory stack, and changes the current directory to it.",
            "If the given path does not exist or is not a directory, this command does not modify",
            "the stack or the current directory."
        );
    }

    /**
     * Pushes a directory onto the directory stack, and changes the current directory to it.
     *
     * @param env       the environment to invoke in
     * @param arguments a single path to a directory
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args = CommandUtil.getArguments(arguments);

        if (args.size() != 1) {
            env.writeln("Invalid number of arguments. Expected: 1, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }


        Path dir = CommandUtil.resolvePath(env, args.get(0));

        if (dir == null) {
            env.writeln(String.format("Invalid path: %s", args.get(0)));
            return ShellStatus.CONTINUE;
        }

        if (Files.notExists(dir) || !Files.isDirectory(dir)) {
            env.writeln("Invalid path.");
            return ShellStatus.CONTINUE;
        }

        if (env.getSharedData("cdstack") == null) {
            env.setSharedData("cdstack", new Stack<Path>());
        }

        //noinspection unchecked
        Stack<Path> cdstack = (Stack<Path>) env.getSharedData("cdstack");

        cdstack.push(dir);

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return description;
    }
}
