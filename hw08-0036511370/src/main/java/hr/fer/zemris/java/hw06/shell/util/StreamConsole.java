package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.ShellIOException;

import java.io.*;

/**
 * An IO console that uses streams for communication.
 */
public class StreamConsole implements Console {

    /** The input stream. */
    private BufferedReader in;

    /** The output stream. */
    private PrintStream out;

    /**
     * Creates a new stream console that uses the given streams.
     *
     * @param in  the input stream
     * @param out the output stream
     */
    StreamConsole(InputStream in, PrintStream out) {
        this.in = new BufferedReader(new InputStreamReader(in));
        this.out = out;
    }

    /** {@inheritDoc} */
    @Override
    public void print(String text) {
        out.print(text);
    }

    /** {@inheritDoc} */
    @Override
    public void println(String text) {
        out.println(text);
    }

    /** {@inheritDoc} */
    @Override
    public String nextLine() {
        try {
            return in.readLine();
        } catch (IOException ex) {
            throw new ShellIOException("Error reading from the input stream.");
        }
    }
}
