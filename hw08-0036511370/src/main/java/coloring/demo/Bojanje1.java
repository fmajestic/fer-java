package coloring.demo;

import marcupic.opjj.statespace.coloring.FillApp;

/** The simplest demo. */
public class Bojanje1 {
    /**
     * Entry point of the program
     * @param args ignored
     */
    public static void main(String[] args) {
        FillApp.run(FillApp.OWL, null); // ili FillApp.ROSE
    }
}
