package coloring.algorithms;


import marcupic.opjj.statespace.coloring.Picture;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/** Fills an area of one color with a new color. */
public class Coloring implements
    Supplier<Pixel>,
    Consumer<Pixel>,
    Function<Pixel, List<Pixel>>,
    Predicate<Pixel> {

    /** The initial point. */
    private Pixel reference;

    /** The picture to paint on. */
    private Picture picture;

    /** The fill color. */
    private int fillColor;

    /** The initial color. */
    private int refColor;

    /**
     * Creates a new coloring object with the given parameters.
     *
     * @param reference the initial, source point
     * @param picture   the picture the reference pixel belongs to
     * @param fillColor the fill color
     */
    public Coloring(Pixel reference, Picture picture, int fillColor) {
        this.reference = reference;
        this.picture = picture;
        this.fillColor = fillColor;
        this.refColor = picture.getPixelColor(reference.x, reference.y);
    }

    /**
     * Changes the color of the given pixel to the fill color.
     *
     * @param pixel the pixel to change
     */
    @Override
    public void accept(Pixel pixel) {
        picture.setPixelColor(pixel.x, pixel.y, fillColor);
    }

    /**
     * Gets the list of all neighbours of the given pixel (not including the diagonal ones).
     *
     * @param pixel the pixel to get neighbours of
     *
     * @return a list of all the neighbours
     */
    @Override
    public List<Pixel> apply(Pixel pixel) {
        return List.of(
            new Pixel(pixel.x, pixel.y - 1), // up
            new Pixel(pixel.x + 1, pixel.y), // right
            new Pixel(pixel.x, pixel.y + 1), // down
            new Pixel(pixel.x - 1, pixel.y)  // left
        );
    }

    /**
     * Tests if the given pixel has the same color as the initial color.
     *
     * @param pixel the pixel to chekc
     *
     * @return true if the colors match, false otherwise
     */
    @Override
    public boolean test(Pixel pixel) {
        return picture.getPixelColor(pixel.x, pixel.y) == refColor;
    }

    /**
     * Gets the initial pixel.
     *
     * @return the initial pixel.
     */
    @Override
    public Pixel get() {
        return reference;
    }
}
