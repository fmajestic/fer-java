package coloring.algorithms;

/** Models a single image pixel. */
public class Pixel {

    /** The x-coordinate of the pixel. */
    public int x;

    /** The y coordinate of the pixel. */
    public int y;

    /**
     * Creates a new pixel with the given coordinates.
     *
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public Pixel(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets the hash code for this pixel. Calculated using both coordinates.
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    /**
     * Checks if another object is equal to this pixel.
     * Returns true only if the other object is a {@code Pixel} with the exact same coordinates.
     *
     * @param o the object to check
     *
     * @return true if the objects are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pixel)) {
            return false;
        }

        Pixel pixel = (Pixel) o;

        if (x != pixel.x) {
            return false;
        }
        return y == pixel.y;

    }

    /**
     * Returns a string representation of this pixel in the form "(x,y)".
     *
     * @return this pixel as a string
     */
    @Override
    public String toString() {
        return String.format("(%d,%d)", x, y);
    }
}
