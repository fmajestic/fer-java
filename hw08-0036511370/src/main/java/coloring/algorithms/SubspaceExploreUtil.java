package coloring.algorithms;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Contains utility methods for subspace exploration using different coloring.searching.algorithms.
 *
 * Currently supported: bfs, dfs, bfsv
 */
public class SubspaceExploreUtil {

    /**
     * Breadth-first search.
     *
     * A simple BFS with no memory of already visited pixels.
     *
     * @param s0 the initial value
     * @param process the action to perform on acceptable values
     * @param succ the neighbour finder
     * @param acceptable the value checker
     * @param <S> the type of the values
     */
    public static <S> void bfs(
        Supplier<S> s0,
        Consumer<S> process,
        Function<S, List<S>> succ,
        Predicate<S> acceptable
    ) {
        List<S> toVisit = new LinkedList<>(Collections.singletonList(s0.get()));

        while (!toVisit.isEmpty()) {
            S si = toVisit.remove(0);

            if (!acceptable.test(si)) {
                continue;
            }

            process.accept(si);

            toVisit.addAll(succ.apply(si));
        }
    }

    /**
     * Depth-first search.
     *
     * A simple DFS with no memory of already visited pixels.
     *
     * @param s0 the initial value
     * @param process the action to perform on acceptable values
     * @param succ the neighbour finder
     * @param acceptable the value checker
     * @param <S> the type of the values
     */
    public static <S> void dfs(
        Supplier<S> s0,
        Consumer<S> process,
        Function<S,List<S>> succ,
        Predicate<S> acceptable
    ) {
        List<S> toVisit = new LinkedList<>(Collections.singletonList(s0.get()));

        while (!toVisit.isEmpty()) {
            S si = toVisit.remove(0);

            if (!acceptable.test(si)) {
                continue;
            }

            process.accept(si);

            toVisit.addAll(0, succ.apply(si));
        }
    }

    /**
     * Breadth-first search.
     *
     * An optimized BFS that keeps track of already visited states.
     *
     * @param s0 the initial value
     * @param process the action to perform on acceptable values
     * @param succ the neighbour finder
     * @param acceptable the value checker
     * @param <S> the type of the values
     */
    public static <S> void bfsv(
        Supplier<S> s0,
        Consumer<S> process,
        Function<S,List<S>> succ,
        Predicate<S> acceptable
    ) {
        List<S> toVisit = new LinkedList<>(Collections.singletonList(s0.get()));
        Set<S> visited = new HashSet<>(Collections.singletonList(s0.get()));

        while (!toVisit.isEmpty()) {
            S si = toVisit.remove(0);

            if (!acceptable.test(si)) {
                continue;
            }

            process.accept(si);

            var children = succ.apply(si)
                .stream().filter(child -> !visited.contains(child)).collect(Collectors.toList());

            toVisit.addAll(children);
            visited.addAll(children);
        }
    }
}
