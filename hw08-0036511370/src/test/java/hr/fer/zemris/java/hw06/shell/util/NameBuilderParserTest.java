package hr.fer.zemris.java.hw06.shell.util;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NameBuilderParserTest {

    private static FilterResult exampleFilter;

    @BeforeAll
    static void setUp() {
        var matcher = Pattern.compile("slika(\\d+)-([^.]+)\\.jpg").matcher("slika1-zadar.jpg");

        //noinspection ResultOfMethodCallIgnored
        matcher.matches();
        exampleFilter = new FilterResult(Paths.get("slika1-zadar.jpg"), matcher);
    }


    @Test
    void homeworkExample() {
        String newName = "gradovi-zadar-001.jpg";
        String sub = "gradovi-${2}-${1,03}.jpg";

        NameBuilder nbp = new NameBuilderParser(sub).getNameBuilder();

        StringBuilder sb = new StringBuilder();

        nbp.execute(exampleFilter, sb);

        assertEquals(newName, sb.toString());
    }

    @Test
    void invalidSubstitution() {

        // Name builder generation is lazy, so the method has to be called explicitly
        assertThrows(IllegalArgumentException.class,
            () -> new NameBuilderParser("${03,-505}").getNameBuilder());

        assertThrows(IllegalArgumentException.class,
            () -> new NameBuilderParser("hello${world,03}").getNameBuilder());
    }

    @Test
    void groupIndexOutOfBounds() {

        assertThrows(IndexOutOfBoundsException.class,
            () -> new NameBuilderParser("${16,2}")
                .getNameBuilder()
                .execute(exampleFilter, new StringBuilder())
        );
    }
}