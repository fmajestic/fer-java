package hr.fer.zemris.java.raytracer;

import hr.fer.zemris.java.raytracer.model.*;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

import static java.lang.Math.*;

/** A simple, non-parallel ray-casting demo. */
public class RayCaster {

    /**
     * Entry point of the program.
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        RayTracerViewer.show(getIRayTracerProducer(),
            new Point3D(10, 0, 0),
            new Point3D(0, 0, 0),
            new Point3D(0, 0, 10),
            20, 20);
    }

    /**
     * Gets the producer.
     * <p>
     * The method calculates all necessary information, then does tracing for each pixel.
     *
     * @return the producer
     */
    private static IRayTracerProducer getIRayTracerProducer() {
        return (eye, view, viewUp, horizontal, vertical, width, height, requestNo, observer, cancel) -> {

            System.out.println("Započinjem izračune...");

            short[] red = new short[width * height];
            short[] green = new short[width * height];
            short[] blue = new short[width * height];

            Point3D upNorm = viewUp.normalize();

            Point3D zAxis = view.sub(eye).normalize();
            Point3D yAxis = upNorm.sub(zAxis.scalarMultiply(zAxis.scalarProduct(upNorm))).normalize();
            Point3D xAxis = zAxis.vectorProduct(yAxis).normalize();
            Point3D screenCorner = view
                .sub(xAxis.scalarMultiply(horizontal / 2))
                .add(yAxis.scalarMultiply(vertical / 2));

            Scene scene = RayTracerViewer.createPredefinedScene();

            int offset = 0;
            short[] rgb = new short[3];

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {

                    Point3D screenPoint = screenCorner
                        .add(xAxis.scalarMultiply(x * horizontal / (width - 1)))
                        .sub(yAxis.scalarMultiply(y * vertical / (height - 1)));

                    Ray ray = Ray.fromPoints(eye, screenPoint);

                    tracer(scene, ray, rgb);

                    red[offset] = rgb[0] > 255 ? 255 : rgb[0];
                    green[offset] = rgb[1] > 255 ? 255 : rgb[1];
                    blue[offset] = rgb[2] > 255 ? 255 : rgb[2];

                    offset++;
                }
            }

            System.out.println("Izračuni gotovi...");

            observer.acceptResult(red, green, blue, requestNo);

            System.out.println("Dojava gotova...");
        };
    }

    /**
     * Performs tracing for the given scene and ray, and stores the color value into {@code rgb}.
     * <pre>
     *     rgb[0] = red intensity
     *     rgb[0] = blue intensity
     *     rgb[0] = green intensity
     * </pre>
     *
     * @param scene the scene to trace in
     * @param ray   the ray to use
     * @param rgb   the array to store color information in
     */
    private static void tracer(Scene scene, Ray ray, short[] rgb) {
        rgb[0] = 15;
        rgb[1] = 15;
        rgb[2] = 15;

        RayIntersection intersection = findClosestIntersection(scene, ray);

        if (intersection == null) {
            rgb[0] = 0;
            rgb[1] = 0;
            rgb[2] = 0;
            return;
        }

        for (LightSource light : scene.getLights()) {

            if (isInShadow(scene, light, intersection)) {
                continue;
            }

            Point3D n = intersection.getNormal();
            Point3D p = intersection.getPoint();

            Point3D l = light.getPoint().sub(p).normalize();
            Point3D v = ray.start.sub(p).normalize();
            Point3D r = n.scalarMultiply(l.scalarProduct(n)).scalarMultiply(2).sub(l).normalize();

            double ln = max(0, n.scalarProduct(l));
            double rvn = pow(max(0, v.scalarProduct(r)), intersection.getKrn());

            rgb[0] += light.getR() * (intersection.getKdr() * ln + intersection.getKrr() * rvn);
            rgb[1] += light.getG() * (intersection.getKdg() * ln + intersection.getKrg() * rvn);
            rgb[2] += light.getB() * (intersection.getKdb() * ln + intersection.getKrb() * rvn);
        }
    }

    /**
     * Finds the closest intersection of an object in the scene and a ray.
     *
     * @param scene the scene to use objects from
     * @param ray   the ray to use
     *
     * @return the closest intersection, or {@code null} if no intersection was found
     */
    private static RayIntersection findClosestIntersection(Scene scene, Ray ray) {

        RayIntersection closest = null;

        for (GraphicalObject go : scene.getObjects()) {
            RayIntersection next = go.findClosestRayIntersection(ray);

            if (next == null) {
                continue;
            }

            if (closest == null) {
                closest = next;
                continue;
            }

            double clDistance = closest.getDistance();
            double nxDistance = next.getDistance();

            if (nxDistance < clDistance && abs(clDistance - nxDistance) >= 1e-8) {
                closest = next;
            }
        }

        return closest;
    }

    /**
     * Checks if an intersection is in the shadow of a light.
     *
     * @param scene        the scene to use
     * @param lightSource  the light source
     * @param intersection the intersection to check
     *
     * @return true if the ray is in a shadow, false otherwise
     */
    private static boolean isInShadow(Scene scene, LightSource lightSource, RayIntersection intersection) {
        Ray srcToInt = Ray.fromPoints(lightSource.getPoint(), intersection.getPoint());

        RayIntersection srcIntersection = findClosestIntersection(scene, srcToInt);

        double original = intersection.getDistance();
        double sourceClosest = srcIntersection.getDistance();

        return lessThan(sourceClosest, original)
            && !pointEquals(intersection.getPoint(), srcIntersection.getPoint());
    }

    /**
     * Checks if a real number is smaller than another, with some tolerance.
     *
     * @param lhs the left operand
     * @param rhs the right operand
     *
     * @return true if lhs is smaller than rhs, false otherwise
     */
    private static boolean lessThan(double lhs, double rhs) {
        return lhs < rhs
            && abs(lhs - rhs) >= 1e-8;
    }

    /**
     * Checks if two points are equal, with some tolerance.
     *
     * @param first  the first point to check
     * @param second the second point to check
     *
     * @return true if all the coordinates match, false otherwise
     */
    private static boolean pointEquals(Point3D first, Point3D second) {
        return abs(first.x - second.x) < 1e-8
            && abs(first.y - second.y) < 1e-8
            && abs(first.z - second.z) < 1e-8;
    }
}
