//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.java.raytracer;

import hr.fer.zemris.java.raytracer.model.*;

import javax.swing.*;
import java.lang.reflect.Constructor;


public class RayTracerViewer {
  public RayTracerViewer() {
  }

  private static GraphicalObject createSphere(Point3D center, double radius, double kdr, double kdg, double kdb, double krr, double krg, double krb, double krn) {
    try {
      Class<?> clazz = Class.forName("hr.fer.zemris.java.raytracer.model.Sphere");
      Constructor<?> constr = clazz.getConstructor(Point3D.class, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE);
      return (GraphicalObject) constr.newInstance(center, radius, kdr, kdg, kdb, krr, krg, krb, krn);
    } catch (Exception var19) {
      throw new RuntimeException("Could not create a Sphere.", var19);
    }
  }

  public static Scene createPredefinedScene() {
    Scene scene = new Scene();
    scene
        .add(createSphere(new Point3D(-2.0D, -2.0D, 2.0D), 5.0D, 1.0D, 1.0D, 1.0D, 0.5D, 0.5D, 0.5D, 10.0D))
        .add(createSphere(new Point3D(-2.0D, 2.0D, -3.0D), 5.0D, 1.0D, 1.0D, 1.0D, 0.5D, 0.5D, 0.5D, 10.0D))
        .add(new LightSource(new Point3D(10.0D, 5.0D, 5.0D), 100, 0, 0))
        .add(new LightSource(new Point3D(10.0D, -5.0D, 5.0D), 0, 80, 80))
        .add(new LightSource(new Point3D(2.0D, 5.0D, -1.0D), 80, 80, 0));

    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
        scene
            .add(createSphere(new Point3D(-5.0D, (double) i / 9.0D * 25.0D - 12.5D, (double) j / 9.0D * 25.0D - 12.5D), 0.5D, 1.0D, 1.0D, 1.0D, 0.5D, 0.5D, 0.5D, 4.0D));
      }
    }

    return scene;
  }

  public static Scene createPredefinedScene2() {
    Scene scene = new Scene();
    scene
        .add(createSphere(new Point3D(1.0D, -2.0D, 2.0D), 5.0D, 1.0D, 1.0D, 1.0D, 0.5D, 0.5D, 0.5D, 10.0D))
        .add(createSphere(new Point3D(1.0D, 2.0D, -3.0D), 5.0D, 1.0D, 1.0D, 1.0D, 0.5D, 0.5D, 0.5D, 10.0D))
        .add(new LightSource(new Point3D(10.0D, 15.0D, 5.0D), 100, 0, 0))
        .add(new LightSource(new Point3D(10.0D, -15.0D, 5.0D), 0, 80, 80))
        .add(new LightSource(new Point3D(10.0D, 5.0D, -1.0D), 80, 80, 0))
        .add(new LightSource(new Point3D(-100.0D, 0.0D, 0.0D), 10, 60, 10));

    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
        scene
            .add(createSphere(new Point3D(-5.0D, (double) i / 9.0D * 25.0D - 12.5D, (double) j / 9.0D * 25.0D - 12.5D), 0.5D, 1.0D, 1.0D, 1.0D, 0.5D, 0.5D, 0.5D, 4.0D));
      }
    }

    return scene;
  }

  public static void show(final IRayTracerProducer iRayTraceProducer) {
    if (SwingUtilities.isEventDispatchThread()) {
      showInEDT(iRayTraceProducer);
    }

    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        RayTracerViewer.showInEDT(iRayTraceProducer);
      }
    });
  }

  private static void showInEDT(IRayTracerProducer iRayTraceProducer) {
    RayTracerViewerFrame fvf = new RayTracerViewerFrame(iRayTraceProducer);
    fvf.setVisible(true);
  }

  public static void show(final IRayTracerProducer iRayTraceProducer, final Point3D eye, final Point3D view, final Point3D viewUp, final double horizontal, final double vertical) {
    if (SwingUtilities.isEventDispatchThread()) {
      showInEDT(iRayTraceProducer, eye, view, viewUp, horizontal, vertical);
    }

    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        RayTracerViewer.showInEDT(iRayTraceProducer, eye, view, viewUp, horizontal, vertical);
      }
    });
  }

  private static void showInEDT(IRayTracerProducer iRayTraceProducer, Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical) {
    RayTracerViewerFrame fvf = new RayTracerViewerFrame(iRayTraceProducer, eye, view, viewUp, horizontal, vertical);
    fvf.setVisible(true);
  }

  public static void show(final IRayTracerProducer iRayTraceProducer, final IRayTracerAnimator animator, final double horizontal, final double vertical) {
    if (SwingUtilities.isEventDispatchThread()) {
      showInEDT(iRayTraceProducer, animator, horizontal, vertical);
    }

    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        RayTracerViewer.showInEDT(iRayTraceProducer, animator, horizontal, vertical);
      }
    });
  }

  private static void showInEDT(IRayTracerProducer iRayTraceProducer, IRayTracerAnimator animator, double horizontal, double vertical) {
    RayTracerViewerAnimatorFrame fvf = new RayTracerViewerAnimatorFrame(iRayTraceProducer, animator, horizontal, vertical);
    fvf.setVisible(true);
  }
}
