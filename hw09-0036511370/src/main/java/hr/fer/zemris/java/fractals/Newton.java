package hr.fer.zemris.java.fractals;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

/** Drawing of fractals based on Newton-Raphson iteration. */
public class Newton {

    /**
     * The regular expression used to parse complex numbers.
     * <p>
     * Visualization: <a href="https://regex101.com/r/YlvIFX/10">https://regex101.com/r/YlvIFX/10</a>
     */

    private static final Pattern COMPLEX_REGEX = Pattern
            .compile("^(?!\\s)(?<real>[+-]?\\d+(?:\\.\\d+)?(?![i.\\d]))?(?<imaginary>[+-]?i(?:\\d+(?:\\.\\d+)?)?)?$");

    /**
     * Entry point of the program.
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
        System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");

        int counter = 1;
        Scanner sc = new Scanner(System.in);
        List<Complex> nums = new ArrayList<>();

        do {
            System.out.print("Root " + counter + "> ");

            String input = sc.nextLine();

            if (input.equals("done")) {
                if (counter > 2) {
                    break;
                } else {
                    System.out.println("Please enter at least 2 roots before ending.");
                }
            }

            try {
                Complex z = parse(input);
                nums.add(z);
                counter++;
            } catch (NumberFormatException ex) {
                System.out.println(ex.getMessage());
            }
        } while (true);

        System.out.println("Image of fractal will appear shortly. Thank you.");

        var rooted = new ComplexRootedPolynomial(Complex.ONE, nums.toArray(Complex[]::new));

        FractalViewer.show(new FractalProducer(rooted));

        sc.close();
    }

    /**
     * Parses a string into a complex number.
     *
     * @param s the string to parse
     *
     * @return the parsed number
     *
     * @throws NumberFormatException if the given string isn't a valid complex
     *                               number
     * @see #REGEX_PATTERN
     */
    private static Complex parse(String s) {
        double real = 0d, imaginary = 0d;

        var matcher = COMPLEX_REGEX.matcher(s.replaceAll("\\s+", "")); // just to make sure there is no whitespace

        // Attempts to match the string against the regex
        if (!matcher.matches()) {
            throw new NumberFormatException("Invalid complex number: " + s);
        }

        var strRe = matcher.group("real");
        var strIm = matcher.group("imaginary");

        if (strRe != null) {
            real = Double.parseDouble(strRe);
        }

        if (strIm != null) {
            // special case: i without number
            if (strIm.matches("[+-]?i")) {
                imaginary = Double.parseDouble(strIm.replace("i", "1"));
            } else {
                // remove 'i' from end of string and parse
                imaginary = Double.parseDouble(strIm.replace("i", ""));
            }
        }

        return new Complex(real, imaginary);
    }

    /** Creates a daemonic thread from a {@link Runnable}. */
    static class DaemonicThreadFactory implements ThreadFactory {
        /**
         * Creates and returns a daemonic thread from the runnable
         *
         * @param r the procedure for the thread
         *
         * @return a daemonic thread
         */
        @Override
        public Thread newThread(Runnable r) {
            var t = new Thread(r);
            t.setDaemon(true);
            return t;
        }
    }

    /** Produces Newton-Raphson fractals. */
    private static class FractalProducer implements IFractalProducer {

        /** The root polynomial. */
        private final ComplexRootedPolynomial rooted;

        /** The factor polynomial. */
        private final ComplexPolynomial polynomial;

        /** The thread pool used for parallelization. */
        private ExecutorService pool;

        /**
         * Creates a new producer for a polynomial
         *
         * @param rooted the polynomial to use
         */
        FractalProducer(ComplexRootedPolynomial rooted) {
            this.rooted = rooted;
            this.polynomial = rooted.toComplexPolynom();

            this.pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(),
                    new DaemonicThreadFactory());
        }

        /**
         * Produces the fractal by splitting it into {@code 8 * nrOfProcessors} jobs.
         *
         * @param reMin     minimal real value
         * @param reMax     maximal real value
         * @param imMin     minimal imaginary value
         * @param imMax     maximal imaginary value
         * @param width     width of the scene, in pixels
         * @param height    height of the scene, in pixels
         * @param requestNo number of the request
         * @param observer  the observer that accepts the result
         * @param cancel    cancellation token
         */
        @Override
        public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height,
                long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {

            System.out.println("Starting calculation...");

            int m = 16;

            short[] data = new short[width * height];

            final int stripCount = 8 * Runtime.getRuntime().availableProcessors();

            int yPerStrip = height / stripCount;

            List<Future<Void>> results = new ArrayList<>();

            for (int i = 0; i < stripCount; i++) {

                int yMin = i * yPerStrip;
                int yMax = (i + 1) * yPerStrip - 1;

                if (i == stripCount - 1) {
                    yMax = height - 1;
                }

                CalculationJob job = new CalculationJob(rooted, polynomial, reMin, reMax, imMin, imMax, width, height,
                        yMin, yMax, m, data, cancel);

                results.add(pool.submit(job));
            }

            for (Future<Void> posao : results) {
                try {
                    posao.get();
                } catch (InterruptedException | ExecutionException ignored) {

                }
            }

            System.out.println("Calculation finished. Notifying the observer (GUI).");

            observer.acceptResult(data, (short) (polynomial.order() + 1), requestNo);
        }
    }

    /** Calculates a part of the fractal. */
    private static class CalculationJob implements Callable<Void> {
        double reMin;
        double reMax;
        double imMin;
        double imMax;
        int width;
        int height;
        int yMin;
        int yMax;
        int m;
        short[] data;
        AtomicBoolean cancel;
        ComplexRootedPolynomial rootedPolynomial;
        ComplexPolynomial polynomial;
        ComplexPolynomial derived;

        CalculationJob(ComplexRootedPolynomial rooted, ComplexPolynomial polynomial, double reMin, double reMax,
                double imMin, double imMax, int width, int height, int yMin, int yMax, int m, short[] data,
                AtomicBoolean cancel) {

            this.rootedPolynomial = rooted;
            this.polynomial = polynomial;
            this.derived = polynomial.derive();
            this.reMin = reMin;
            this.reMax = reMax;
            this.imMin = imMin;
            this.imMax = imMax;
            this.width = width;
            this.height = height;
            this.yMin = yMin;
            this.yMax = yMax;
            this.m = m;
            this.data = data;
            this.cancel = cancel;
        }

        /**
         * Maps a pixel to the complex plane which is centered in the middle of the
         * given area.
         *
         * @param x     the pixel x coordinate
         * @param y     the pixel y coordinate
         * @param xMin  minimal x coordinate
         * @param xMax  maximal x coordinate
         * @param yMin  minimal y coordinate
         * @param yMax  maximal y coordinate
         * @param reMin minimal real value
         * @param reMax maximal real value
         * @param imMin minimal imaginary value
         * @param imMax maximal imaginary value
         *
         * @return the mapped complex number
         */
        @SuppressWarnings("SameParameterValue")
        private static Complex mapToComplexPlane(int x, int y, int xMin, int xMax, int yMin, int yMax, double reMin,
                double reMax, double imMin, double imMax) {

            int xRange = xMax - xMin;
            int yRange = yMax - yMin;

            double reRange = reMax - reMin;
            double imRange = imMax - imMin;

            return new Complex(reMin + x * reRange / xRange, imMax - y * imRange / yRange);
        }

        /** Actually performs the calculation. */
        @Override
        public Void call() {

            int index = yMin * width;

            for (int y = yMin; y <= yMax && !cancel.get(); ++y) {
                for (int x = 0; x < width; ++x) {
                    Complex zn = mapToComplexPlane(x, y, 0, width, 0, height, reMin, reMax, imMin, imMax);

                    // System.out.printf("Mapped (%d,%d) to (%s)%n", x,y,zn.toString());

                    int iter = 0;
                    double module;

                    double rootThreshold = 2e-3;
                    double convergenceThreshold = 1e-3;

                    do {
                        Complex numerator = polynomial.apply(zn);
                        Complex denominator = derived.apply(zn);
                        Complex znOld = zn;
                        Complex fraction = numerator.divide(denominator);
                        zn = zn.sub(fraction);
                        module = znOld.sub(zn).module();
                        iter++;
                    } while (module > convergenceThreshold && iter < m);

                    int rootIndex = rootedPolynomial.indexOfClosestRootFor(zn, rootThreshold);
                    data[index++] = (short) (rootIndex + 1);
                }
            }

            return null;
        }
    }
}
