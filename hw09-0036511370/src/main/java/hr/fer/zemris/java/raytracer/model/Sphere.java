package hr.fer.zemris.java.raytracer.model;

import static java.lang.Math.sqrt;

/** A sphere, defined by a center point and a radius. */
public class Sphere extends GraphicalObject {

    private Point3D center;
    private double radius;
    private double kdr;
    private double kdg;
    private double kdb;
    private double krr;
    private double krg;
    private double krb;
    private double kr;

    public Sphere(
        Point3D center, double radius,
        double kdr, double kdg, double kdb,
        double krr, double krg, double krb, double kr) {

        this.center = center;
        this.radius = radius;
        this.kdr = kdr;
        this.kdg = kdg;
        this.kdb = kdb;
        this.krr = krr;
        this.krg = krg;
        this.krb = krb;
        this.kr = kr;
    }

    @Override
    public RayIntersection findClosestRayIntersection(Ray ray) {

        Point3D oc = ray.start.sub(center);

        double a = ray.direction.scalarProduct(ray.direction);
        double b = 2 * oc.scalarProduct(ray.direction);
        double c = oc.scalarProduct(oc) - radius * radius;

        double discriminant = b * b - 4 * a * c;

        // No intersection
        if (discriminant < 0) {
            return null;
        }

        double t1 = (-b - sqrt(discriminant)) / (2 * a);
        double t2 = (-b + sqrt(discriminant)) / (2 * a);

        // Sphere behind ray
        if (t1 < 0 && t2 < 0)
            return null;

        // Ray origin inside of sphere, 1 intersection
        if (t1 < 0 && t2 > 0)
            return new SphereIntersection(ray.start.add(ray.direction.scalarMultiply(t2)), t2, false);

        // Ray origin outside of sphere, 2 intersections
        return new SphereIntersection(ray.start.add(ray.direction.scalarMultiply(t1)), t1, true);
    }

    public Point3D getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public double getKdr() {
        return kdr;
    }

    public void setKdr(double kdr) {
        this.kdr = kdr;
    }

    public double getKdg() {
        return kdg;
    }

    public void setKdg(double kdg) {
        this.kdg = kdg;
    }

    public double getKdb() {
        return kdb;
    }

    public void setKdb(double kdb) {
        this.kdb = kdb;
    }

    public double getKrr() {
        return krr;
    }

    public void setKrr(double krr) {
        this.krr = krr;
    }

    public double getKrg() {
        return krg;
    }

    public void setKrg(double krg) {
        this.krg = krg;
    }

    public double getKrb() {
        return krb;
    }

    public void setKrb(double krb) {
        this.krb = krb;
    }

    public double getKr() {
        return kr;
    }

    public void setKr(double kr) {
        this.kr = kr;
    }

    private class SphereIntersection extends RayIntersection {

        private SphereIntersection(Point3D point, double distance, boolean outer) {
            super(point, distance, outer);
        }

        @Override
        public Point3D getNormal() {
            return getPoint().sub(center).normalize();
        }

        @Override
        public double getKdr() {
            return Sphere.this.getKdr();
        }

        @Override
        public double getKdg() {
            return Sphere.this.getKdg();
        }

        @Override
        public double getKdb() {
            return Sphere.this.getKdb();
        }

        @Override
        public double getKrr() {
            return Sphere.this.getKrr();
        }

        @Override
        public double getKrg() {
            return Sphere.this.getKrg();
        }

        @Override
        public double getKrb() {
            return Sphere.this.getKrb();
        }

        @Override
        public double getKrn() {
            return Sphere.this.getKr();
        }
    }
}