package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.*;

/** Models and <b>immutable</b> complex number. */
public class Complex {

    /** The number zero. */
    public static final Complex ZERO = new Complex(0, 0);

    /** The real number one. */
    public static final Complex ONE = new Complex(1, 0);

    /** The real number negative one. */
    public static final Complex ONE_NEG = new Complex(-1, 0);

    /** The imaginary unit (i). */
    public static final Complex IM = new Complex(0, 1);

    /** The negative imaginary unit (-i). */
    public static final Complex IM_NEG = new Complex(0, -1);


    /** The real part. */
    private double real;

    /** The imaginary part. */
    private double imaginary;

    /**
     * Creates a new default complex number: (1, 0).
     */
    public Complex() {
        this(1, 0);
    }

    /**
     * Creates a new complex number with the given values.
     *
     * @param re the real part
     * @param im the imaginary part
     */
    public Complex(double re, double im) {
        this.real = re;
        this.imaginary = im;
    }

    /**
     * Creates a complex number from a magnitude and an angle.
     *
     * @param magnitude the magnitude
     * @param angle     the angle, in radians
     *
     * @return a new complex number
     */
    private static Complex fromMagnitudeAndAngle(double magnitude, double angle) {
        return new Complex(magnitude * cos(angle), magnitude * sin(angle));
    }

    /**
     * Normalizes the given angle to [0, 2pi].
     *
     * @param radians the angle to normalize, in radians
     *
     * @return the given angle, normalized to the interval [0, 2pi] radians
     */
    private static double normalizeAngle(double radians) {
        if (abs(radians) >= 2 * PI) {
            radians -= 2 * PI * (int) (radians / (2 * PI));
        }

        if (radians < 0) {
            radians += 2 * PI;
        }

        return radians;
    }

    /**
     * Gets the real part of the number.
     *
     * @return the real part
     */
    public double getReal() {
        return real;
    }

    /**
     * Gets the imaginary part of the number.
     *
     * @return the imaginary part
     */
    public double getImaginary() {
        return imaginary;
    }

    /**
     * Calculates the module of the complex number.
     *
     * @return the module of this number
     */
    public double module() {
        return sqrt(real * real + imaginary * imaginary);
    }

    /**
     * Calculates the product with another complex number and returns the result.
     *
     * @param c the right operand
     *
     * @return the result of the operation
     */
    public Complex add(Complex c) {
        return new Complex(real + c.real, imaginary + c.imaginary);
    }

    /**
     * Calculates the product with another complex number and returns the result.
     *
     * @param c the right operand
     *
     * @return the result of the operation
     */
    public Complex sub(Complex c) {
        return add(c.negate());
    }

    /**
     * Calculates the product with another complex number and returns the result.
     *
     * @param c the right operand
     *
     * @return the result of the operation
     */
    public Complex multiply(Complex c) {
        var magnitude = this.module() * c.module();
        var angle = this.getAngle() + c.getAngle();

        return fromMagnitudeAndAngle(magnitude, angle);
    }

    /**
     * Calculates the product with another complex number and returns the result.
     *
     * @param c the right operand
     *
     * @return the result of the operation
     */
    public Complex divide(Complex c) {
        var magnitude = this.module() / c.module();
        var angle = this.getAngle() - c.getAngle();

        return fromMagnitudeAndAngle(magnitude, angle);
    }

    /**
     * Returns a negated complex number.
     *
     * @return this number, negated
     */
    public Complex negate() {
        return new Complex(-real, -imaginary);
    }

    /**
     * Calculates the n-th power of the number.
     *
     * @param n the power, must be 0 or positive
     *
     * @return this number to the n-th power
     *
     * @throws IllegalArgumentException if n is negative
     */
    public Complex power(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

        return fromMagnitudeAndAngle(pow(module(), n), getAngle() * n);
    }

    /**
     * Calculates all of the n-th roots of the complex number
     *
     * @param n the root to calculate
     *
     * @return a list containing all the n-th roots
     */
    public List<Complex> root(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Root must be positive");
        }

        var roots = new ArrayList<Complex>(n);
        var angle = getAngle();
        var magnitude = pow(module(), 1.0 / n);

        for (int k = 0; k < n; k++) {
            roots.add(Complex.fromMagnitudeAndAngle(magnitude, (angle + 2 * k * PI) / n));
        }

        return roots;
    }

    /**
     * Returns the string representation of the number, in the form {@code a+ib}.
     *
     * @return this number as a string
     */
    @Override
    public String toString() {
        return String.format(Locale.US, "%.1f%ci%.1f", real, imaginary >= 0 ? '+' : '-', abs(imaginary));
    }

    /**
     * Gets the angle of this complex number.
     *
     * @return the angle, normalized to the interval {@code [0, 2pi]}
     */
    private double getAngle() {
        var radians = atan2(imaginary, real);
        return normalizeAngle(radians);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Complex)) {
            return false;
        }

        Complex that = (Complex) o;

        if (abs(real - that.real) > 1e-8) {
            return false;
        }
        return abs(imaginary - that.imaginary) < 1e-8;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(real);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(imaginary);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
