package hr.fer.zemris.math;

import java.io.DataInput;
import java.util.Objects;
import java.util.stream.Stream;

import static java.lang.Math.abs;

/** Models a complex polynomial determined by its roots. */
public class ComplexRootedPolynomial {

    /** Constant of the polynomial. */
    private Complex constant;

    /** Roots of the polynomial. */
    private Complex[] roots;

    /**
     * Creates a new polynomial with the given values.
     *
     * @param constant the constant
     * @param roots    the roots
     *
     * @throws NullPointerException if either of the arguments are null
     */
    public ComplexRootedPolynomial(Complex constant, Complex... roots) {
        this.constant = Objects.requireNonNull(constant, "Polynomial constant can't be null.");
        this.roots = Objects.requireNonNull(roots, "Polynomial roots can't be null.");
    }

    /**
     * Calculates polynomial value at point z.
     *
     * @param z the point to calculate for
     *
     * @return value of the polynomial at (for) z
     */
    public Complex apply(Complex z) {
        Complex result = constant;

        for (Complex root : roots) {
            result = result.multiply(z.sub(root));
        }

        return result;
    }

    /**
     * Converts this root representation to a "regular" (factorized) polynomial.
     *
     * @return this polynomial as a regular polynomial
     */
    public ComplexPolynomial toComplexPolynom() {
        ComplexPolynomial result = new ComplexPolynomial(roots[0].negate(), Complex.ONE);

        for (int i = 1; i < roots.length; i++) {
            result = result.multiply(new ComplexPolynomial(roots[i].negate(), Complex.ONE));
        }

        return result.multiply(new ComplexPolynomial(constant));
    }

    /**
     * Returns the string representation of this polynomial in the form:
     * <code>
     * z0*(z-z1)*(z-z2)*(z-z3)*...
     * </code>
     *
     * @return this polynomial as a string
     */
    @Override
    public String toString() {
        return String.format(
            "(%s)*%s",
            constant,
            String.join(
                "*",
                Stream.of(roots)
                    .map(c -> String.format("(z-(%s))", c.toString()))
                    .toArray(String[]::new)
            )
        );
    }

    /**
     * Finds the index of the root closest to the given number, that is within the threshold.
     * <p>
     * If there is no such root, -1 is returned.
     *
     * @param z         the number to check distance to
     * @param threshold the threshold when comparing distance
     *
     * @return a 0-based index, or -1 if no such root was found
     */
    public int indexOfClosestRootFor(Complex z, double threshold) {

        int ind = 0;
        double min = roots[0].sub(z).module();

        for (int i = 1; i < roots.length; i++) {
            var dist = roots[i].sub(z).module();

            if (dist < min && abs(dist - min) >= 1e-8) {
                ind = i;
                min = dist;
            }
        }

        return abs(min - threshold) >= 1e-8 ? ind : -1;
    }
}
