package hr.fer.zemris.math;

import java.util.Arrays;
import java.util.Objects;

/** Models a complex polynomial determined by its factors. */
public class ComplexPolynomial {

    /** Factors of the polynomial. */
    private Complex[] factors;

    /**
     * Creates a new polynomial with the given factors
     *
     * @param factors factors of the polynomial
     *
     * @throws NullPointerException if factors is null
     */
    public ComplexPolynomial(Complex... factors) {
        this.factors = Objects.requireNonNull(factors, "Polynomial factors can't be null.");
    }

    /**
     * Returns the order of this polynom.
     *
     * @return the order
     */
    public short order() {
        return (short) (factors.length - 1);
    }

    /**
     * Calculates the multiple of this polynomial with another polynomial and returns the result.
     *
     * @param p the polynomial to multiply by
     *
     * @return the result of the operation
     */
    public ComplexPolynomial multiply(ComplexPolynomial p) {

        Complex[] mulFactors = new Complex[factors.length + p.factors.length - 1];

        for (int i = 0; i < mulFactors.length; i++) {
            mulFactors[i] = factorOfPower(i, factors, p.factors);
        }

        return new ComplexPolynomial(mulFactors);
    }

    /**
     * Computes the factor for a given power of a polynomial.
     *
     * @param power  the power of z to calculate the factor for
     * @param first  the first polynomial being multiplied
     * @param second the second polynomial being multiplied
     *
     * @return the factor for the given power of the multiple
     */
    private Complex factorOfPower(int power, Complex[] first, Complex[] second) {
        Complex result = Complex.ZERO;

        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                if (i + j == power) {
                    result = result.add(first[i].multiply(second[j]));
                }
            }
        }

        return result;
    }

    /**
     * Calculates the first derivative of this polynomial.
     *
     * @return the first derivative
     */
    public ComplexPolynomial derive() {

        if (factors.length == 0) {
            return new ComplexPolynomial(Complex.ZERO);
        }

        Complex multiplier = Complex.ONE; // To avoid allocating a new Complex for each loop iteration
        Complex[] derived = new Complex[factors.length - 1];

        for (int i = 0; i < derived.length; i++) {
            derived[i] = factors[i + 1].multiply(multiplier);
            multiplier = multiplier.add(Complex.ONE);
        }

        return new ComplexPolynomial(derived);
    }

    /**
     * Calculates the polynomial value for a given number z.
     *
     * @param z the number to plug into the polynomial
     *
     * @return the value of the polynomial at z
     */
    public Complex apply(Complex z) {

        Complex result = Complex.ZERO;

        for (int i = 0; i < factors.length; i++) {
            result = result.add(factors[i].multiply(z.power(i)));
        }

        return result;
    }

    /**
     * Returns the string representation of this polynomial in the form
     * {@code (f1)*z^n+(f2)*z^n-1+...(fk-1)z^1+fk}
     *
     * @return this polynomial as a string
     */
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        for (int i = factors.length - 1; i > 0; i--) {
            sb.append("(").append(factors[i]).append(")*z^").append(i).append("+");
        }

        sb.append("(").append(factors[0]).append(")");

        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ComplexPolynomial)) {
            return false;
        }

        ComplexPolynomial that = (ComplexPolynomial) o;

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(factors, that.factors);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(factors);
    }
}
