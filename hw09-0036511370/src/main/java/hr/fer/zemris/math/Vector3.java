package hr.fer.zemris.math;

/** Models an <b>immutable</b> 3-component vector. */
@SuppressWarnings("WeakerAccess")
public class Vector3 {

    /** The x component. */
    private double x;

    /** The y component. */
    private double y;

    /** The z component. */
    private double z;

    /**
     * Creates a new vector with the given parameters.
     *
     * @param x the x component
     * @param y the y component
     * @param z the z component
     */
    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Gets the norm of the vector, its "length".
     *
     * @return the norm of this vector
     */
    public double norm() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Normalizes this vector to a unit vector.
     *
     * @return this vector, normalized
     */
    public Vector3 normalized() {
        return norm() == 0 ? new Vector3(1, 0, 0) : this.scale(1.0 / norm());
    }

    /**
     * Adds another vector to this vector and returns the result.
     *
     * @param other the right operand
     *
     * @return the result of the operation.
     */
    public Vector3 add(Vector3 other) {
        return new Vector3(
            x + other.x,
            y + other.y,
            z + other.z
        );
    }

    /**
     * Subtracts another vector from this vector and returns the result.
     *
     * @param other the right operand
     *
     * @return the result of the operation.
     */
    public Vector3 sub(Vector3 other) {
        return new Vector3(
            x - other.x,
            y - other.y,
            z - other.z
        );
    }

    /**
     * Calculates the dot (scalar) product with another vector and returns the result.
     *
     * @param other the right operand
     *
     * @return the result of the operation
     */
    public double dot(Vector3 other) {
        return this.norm() * other.norm() * this.cosAngle(other);
    }

    /**
     * Calculates the cross (vector) product with another vector and returns the result.
     *
     * @param other the right operand
     *
     * @return the result of the operation
     */
    public Vector3 cross(Vector3 other) {
        return new Vector3(
            this.y * other.z - this.z * other.y,
            this.y * other.x - this.x * other.y,
            this.x * other.y - this.y * other.x
        );
    }

    /**
     * Scales each component of the vector by a given factor.
     * @param s the factor to scale by
     * @return the scaled vector
     */
    public Vector3 scale(double s) {
        return new Vector3(x * s, y * s, z * s);
    }

    /**
     * Calculates the cosine of the angle between this and another vector.
     *
     * @param other the other vector
     *
     * @return the angle between this and other
     */
    public double cosAngle(Vector3 other) {
        return this.dot(other) / (this.norm() * other.norm());
    }

    /**
     * Gets the x component of the vector.
     *
     * @return the x component
     */
    public double getX() {
        return x;
    }

    /**
     * Gets the z component of the vector.
     *
     * @return the z component
     */
    public double getY() {
        return y;
    }

    /**
     * Gets the z component of the vector.
     *
     * @return the z component
     */
    public double getZ() {
        return z;
    }

    /**
     * Converts the vector to a 3-element array, such that:
     * <p>
     * <code>
     * array[0] = getX();<br>
     * array[1] = getY();<br>
     * array[2] = getZ();
     * </code>
     *
     * @return this vector as an array
     */
    public double[] toArray() {
        return new double[] { x, y, z };
    }

    /**
     * Converts the vector to a string in the form {@code "(x, y, z)"}.
     * <p>
     * Each coordinate is rounded to 6 decimal places.
     *
     * @return this vector as a string
     */
    public String toString() {
        return String.format("(%f, %f, %f)", x, y, z);
    }
}
