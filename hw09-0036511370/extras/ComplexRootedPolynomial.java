package hr.fer.zemris.math;

import javax.xml.stream.FactoryConfigurationError;
import java.security.cert.CollectionCertStoreParameters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.lang.Math.*;

/** Models a complex polynomial determined by its roots. */
public class ComplexRootedPolynomial {

    /** Constant of the polynomial. */
    private Complex constant;

    /** Roots of the polynomial. */
    private Complex[] roots;

    /**
     * Creates a new polynomial with the given values.
     *
     * @param constant the constant
     * @param roots    the roots
     *
     * @throws NullPointerException if either of the arguments are null
     */
    public ComplexRootedPolynomial(Complex constant, Complex... roots) {
        this.constant = Objects.requireNonNull(constant, "Polynomial constant can't be null.");
        this.roots = Objects.requireNonNull(roots, "Polynomial roots can't be null.");
    }

    /**
     * Calculates polynomial value at point z.
     *
     * @param z the point to calculate for
     *
     * @return value of the polynomial at (for) z
     */
    public Complex apply(Complex z) {
        Complex result = constant;

        for (Complex root : roots) {
            result = result.multiply(z.sub(root));
        }

        return result;
    }

    /**
     * Converts this root representation to a "regular" (factorized) polynomial.
     *
     * @return this polynomial as a regular polynomial
     */
    public ComplexPolynomial toComplexPolynom() {
        ComplexPolynomial result = new ComplexPolynomial(roots[0].negate(), Complex.ONE);

        for (int i = 1; i < roots.length; i++) {
            result = result.multiply(new ComplexPolynomial(roots[i].negate(), Complex.ONE));
        }

        return result.multiply(new ComplexPolynomial(constant));
    }

    private ComplexPolynomial oldToComplexPolynom() {
        Complex sgn = (roots.length) % 2 == 0 ? Complex.ONE : Complex.ONE_NEG;
        Complex[] factors = new Complex[roots.length + 1];

        for (int i = 0; i < factors.length; i++) {
            factors[i] = getFactorOfExponent(i).multiply(constant).multiply(sgn);
            sgn = sgn.multiply(Complex.ONE_NEG);
        }

        return new ComplexPolynomial(factors);
    }

    /**
     * Gets the factor for an exponent in a standard (factor) polynomial.
     *
     * @param exp the exponent to get the factor for
     *
     * @return the factor for this exponent
     */
    private Complex getFactorOfExponent(int exp) {

        int n = roots.length;

        if (exp == 0) {
            return Stream.of(roots).reduce(Complex.ONE, (acc, curr) -> acc = acc.multiply(curr));
        }

        if (exp == roots.length) {
            return Complex.ONE;
        }

        List<Integer[]> rootCombinations = indexCombinations(n, n - exp);

        return rootCombinations.stream()
            // Map indices to actual roots and reduce each by multiplication
            .map(indices -> Stream.of(indices)
                .map(i -> roots[i])
                .reduce(Complex.ONE, (acc, curr) -> acc = acc.multiply(curr))
            )
            // Reduce multiples by adding them
            .reduce(Complex.ZERO, (acc, curr) -> acc = acc.add(curr));
    }

    /**
     * Computes all r-combinations of indices up to but excluding n (0 <= i < n).
     *
     * @param n the size of the array to calculate indices for
     * @param r the size of the combination, {@code 0 < r <= n}
     *
     * @return a list of all the r-sized index combinations
     */
    private List<Integer[]> indexCombinations(int n, int r) {
        Integer[] curr = new Integer[r];
        List<Integer[]> all = new ArrayList<>();

        for (int i = 0; i < r; i++) {
            curr[i] = i;
        }

        while (curr[r - 1] < n) {
            all.add(Arrays.copyOf(curr, curr.length));

            int t = r - 1;
            while (t != 0 && curr[t] == n - r + t) {
                t--;
            }

            curr[t] += 1;

            for (int i = t + 1; i < r; i++) {
                curr[i] = curr[i - 1] + 1;
            }
        }

        return all;
    }

    /**
     * Returns the string representation of this polynomial in the form:
     * <code>
     * z0*(z-z1)*(z-z2)*(z-z3)*...
     * </code>
     *
     * @return this polynomial as a string
     */
    @Override
    public String toString() {
        return String.format(
            "(%s)*%s",
            constant,
            String.join(
                "*",
                Stream.of(roots)
                    .map(c -> String.format("(z-(%s))", c.toString()))
                    .toArray(String[]::new)
            )
        );
    }

    /**
     * Finds the index of the root closest to the given number, that is within the threshold.
     * <p>
     * If there is no such root, -1 is returned.
     *
     * @param z         the number to check distance to
     * @param threshold the threshold when comparing distance
     *
     * @return a 0-based index, or -1 if no such root was found
     */
    public int indexOfClosestRootFor(Complex z, double threshold) {

        int ind = 0;
        double min = roots[0].sub(z).module();

        for (int i = 1; i < roots.length; i++) {
            var dist = roots[i].sub(z).module();

            if (dist < min && abs(dist - min) > 1e-6) {
                ind = i;
                min = dist;
            }
        }

        return min < threshold ? ind : -1;
    }
}
