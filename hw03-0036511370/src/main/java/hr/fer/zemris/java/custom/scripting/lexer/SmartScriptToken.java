package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

/**
 * Represents a SmartScript lexer token, with a type and value.
 *
 * @author Filip Majetic
 * @see SmartScriptLexer
 */
public class SmartScriptToken {
    private Object value;
    private SmartScriptTokenType type;

    /**
     * Creates a token with the specified type and value.
     *
     * @param type  the token type
     * @param value the token value
     *
     * @see SmartScriptTokenType
     */
    public SmartScriptToken(SmartScriptTokenType type, Object value) {
        this.type = type;
        this.value = value;
    }

    /**
     * Gets the token value.
     *
     * @return the token value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Gets the token type
     *
     * @return the token type
     *
     * @see SmartScriptTokenType
     */
    public SmartScriptTokenType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, type);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SmartScriptToken)) {
            return false;
        }

        SmartScriptToken other = (SmartScriptToken) obj;

        return type == other.getType() && value.equals(other.getValue());
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", type.toString(), value.toString());
    }
}
