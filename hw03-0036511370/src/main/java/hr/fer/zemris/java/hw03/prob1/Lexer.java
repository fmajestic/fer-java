package hr.fer.zemris.java.hw03.prob1;

import hr.fer.zemris.java.hw03.prob1.Token.TokenType;

import java.util.Objects;

/**
 * A simple example lexer.
 */
public class Lexer {
    private char[] data;
    private Token token;
    private int currentIndex;
    private LexerState currentState = LexerState.BASIC;

    /**
     * Creates a lexer with an input string to tokenize.
     *
     * @param text the text to process
     *
     * @throws NullPointerException if {@code text} is null
     */
    public Lexer(String text) {
        data = Objects.requireNonNull(text, "Lexer input can't be null.").toCharArray();
    }

    /**
     * Generates and returns the next token.
     *
     * @return the next token
     *
     * @throws LexerException if an error occurs
     */
    public Token nextToken() {
        generateNextToken();
        return getToken();
    }

    /**
     * Gets the last generated token.
     * <p>
     * This method can be called multiple times,
     * and does not generate any new tokens.
     *
     * @return the last generated token
     */
    public Token getToken() {
        return token;
    }

    /**
     * Sets the lexer state.
     *
     * @param state the new lexer state
     *
     * @throws NullPointerException if {@code state} is null
     */
    public void setState(LexerState state) {
        currentState = Objects.requireNonNull(state, "Lexer state can't be null.");
    }

    private void generateNextToken() {
        if (token != null && token.getType() == TokenType.EOF) {
            throw new LexerException("No more tokens");
        }

        moveToNextToken();

        if (currentIndex == data.length) {
            token = new Token(TokenType.EOF, null);
            return;
        }

        Object value;
        TokenType type;
        char ch = data[currentIndex];

        if (currentState == LexerState.BASIC) {
            if (Character.isLetter(ch) || ch == '\\') {
                value = extractWord();
                type = TokenType.WORD;
            } else if (Character.isDigit(ch)) {
                value = extractNumber();
                type = TokenType.NUMBER;
            } else {
                value = ch;
                type = TokenType.SYMBOL;

                currentIndex += 1;
            }
        } else {
            if (ch == '#') {
                type = TokenType.SYMBOL;
                value = ch;

                currentIndex += 1;
            } else {
                type = TokenType.WORD;
                value = extractAnything();
            }
        }

        token = new Token(type, value);
    }

    private Object extractAnything() {
        var sb = new StringBuilder();

        for (; currentIndex < data.length; currentIndex++) {
            if (notWhitespace(data[currentIndex]) && data[currentIndex] != '#') {
                sb.append(data[currentIndex]);
            } else {
                break;
            }
        }

        return sb.toString();
    }

    private Long extractNumber() {
        var sb = new StringBuilder();

        for (; currentIndex < data.length; currentIndex++) {
            if (Character.isDigit(data[currentIndex])) {
                sb.append(data[currentIndex]);
            } else {
                break;
            }
        }

        try {
            return Long.parseLong(sb.toString());
        } catch (NumberFormatException ex) {
            throw new LexerException("Number too large.");
        }
    }

    private String extractWord() {
        var sb = new StringBuilder();

        for (; currentIndex < data.length; currentIndex++) {
            var ch = data[currentIndex];

            // If it isn't a letter or a backslash, end word capture
            if (!Character.isLetter(ch) && ch != '\\') {
                break;
            }

            if (ch == '\\') {
                if (currentIndex + 1 >= data.length) {
                    throw new LexerException("Invalid escape sequence.");
                }

                char nextCh = data[currentIndex + 1]; // Now we know this won't throw IndexOutOfBounds

                if (nextCh != '\\' && !Character.isDigit(nextCh)) {
                    throw new LexerException("Invalid escape sequence");
                }

                // All checks passed, the escape is valid
                sb.append(nextCh);

                // Skip over the escaped character
                currentIndex++;
            } else {
                sb.append(ch);
            }
        }

        return sb.toString();
    }

    private void moveToNextToken() {
        for (; currentIndex < data.length; currentIndex++) {
            if (notWhitespace(data[currentIndex])) {
                break;
            }
        }
    }

    private boolean notWhitespace(char c) {
        return c != ' ' && c != '\t' && c != '\n' && c != '\r';
    }

    public enum LexerState {
        BASIC, EXTENDED
    }
}
