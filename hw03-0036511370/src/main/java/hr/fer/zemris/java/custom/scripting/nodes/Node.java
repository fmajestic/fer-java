package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

/**
 * A base class for all Nodes.
 *
 * @author Filip Majetic
 */
public class Node {
    private ArrayIndexedCollection children;

    /**
     * Adds a node to the list of children.
     *
     * @param child the node to be added
     */
    public void addChildNode(Node child) {
        if (children == null) {
            children = new ArrayIndexedCollection();
        }

        children.add(child);
    }

    /**
     * Gets the number of children this node has.
     *
     * @return the number of children
     */
    public int numberOfChildren() {
        return children == null ? 0 : children.size();
    }

    /**
     * Gets the child at the specified index.
     *
     * @param index the index to get from
     *
     * @return the child at the {@code index}
     *
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    public Node getChild(int index) {
        return (Node) children.get(index);
    }

    /**
     * Returns the text representation of the node's children
     *
     * @return the text
     */
    @Override
    public String toString() {
        var sb = new StringBuilder();

        if (children != null) {
            for (Object o : children.toArray()) {
                sb.append(o.toString());
            }
        }

        return sb.toString();
    }

    /**
     * General tree generating method, displays all children as indented
     * and adds an extra level of indenting for each sub-element.
     *
     * @return all the children as a tree
     */
    public String toTree() {
        var sb = new StringBuilder();

        if (children != null) {

            for (int i = 0; i < children.size() - 1; i++) {
                sb.append("├───");
                sb.append(((Node) children.get(i)).toTree().replace("\n", "\n│   "));
                sb.append("\n");
            }

            sb.append("└───");
            sb.append(((Node) children.get(children.size() - 1)).toTree().replace("\n", "\n    "));
            // sb.append("\n");
        }

        return sb.toString();
    }
}
