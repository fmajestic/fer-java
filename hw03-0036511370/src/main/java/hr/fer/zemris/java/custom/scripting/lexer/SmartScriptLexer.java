package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

import static hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType.*;

/**
 * A lexer that generates tokens for the SmartScrtipt language
 */
public class SmartScriptLexer {

    private char[] data;
    private SmartScriptToken token;
    private int currentIndex;
    private SmartScriptLexerState currentState = SmartScriptLexerState.TEXT;

    /**
     * Creates a lexer with an input string to tokenize.
     *
     * @param text the text to process
     *
     * @throws NullPointerException if {@code text} is null
     */
    public SmartScriptLexer(String text) {
        data = Objects.requireNonNull(text, "Lexer input can't be null.").toCharArray();
    }

    /**
     * Generates and returns the next token.
     *
     * @return the next token
     *
     * @throws SmartScriptLexerException if an error occurs
     */
    public SmartScriptToken nextToken() {
        generateNextToken();
        return getToken();
    }

    /**
     * Gets the last generated token.
     * <p>
     * This method can be called multiple times, and does not generate any new
     * tokens.
     *
     * @return the last generated token
     */
    public SmartScriptToken getToken() {
        return token;
    }

    /**
     * Sets the lexer state.
     *
     * @param state the new lexer state
     *
     * @throws NullPointerException if {@code state} is null
     */
    public void setState(SmartScriptLexerState state) {
        currentState = Objects.requireNonNull(state, "Lexer state can't be set to null.");
    }

    private void generateNextToken() {
        if (token != null && token.getType() == EOF) {
            throw new SmartScriptLexerException("End of file reached.");
        }

        if (currentState != SmartScriptLexerState.TEXT) {
            skipWhitespace();
        }

        if (currentIndex == data.length) {
            token = new SmartScriptToken(EOF, null);
            return;
        }

        Object value;
        SmartScriptTokenType type;

        if (haveTagStart() || haveTagEnd()) {
            value = String.format("%c%c", data[currentIndex], data[currentIndex + 1]);
            type = value.equals("{$") ? TAG_OPEN : TAG_CLOSE;

            currentIndex += 2;
        } else if (currentState == SmartScriptLexerState.TEXT) {
            value = extractAllText();
            type = TEXT;
        } else {
            char ch = data[currentIndex];

            // Htio sam ovo riješiti puno kraće preko HashMap, ali zabranjeno je u uputi
            switch (ch) {
            case '-':
                if (haveMoreData() && Character.isDigit(data[currentIndex + 1])) {
                    currentIndex++;
                    value = extractNumber();
                    type = getNumericTokenType(value);

                    value = -1 * (type == CONST_DOUBLE ? (Double) value : (Integer) value);

                    token = new SmartScriptToken(type, value);
                } else {
                    currentIndex++;
                    token = new SmartScriptToken(OP_SUB, "-");
                }
                return;

            case '+':
                currentIndex++;
                token = new SmartScriptToken(OP_ADD, "+");
                return;

            case '*':
                currentIndex++;
                token = new SmartScriptToken(OP_MUL, "*");
                return;

            case '/':
                currentIndex++;
                token = new SmartScriptToken(OP_DIV, "/");
                return;

            case '^':
                currentIndex++;
                token = new SmartScriptToken(OP_POW, "^");
                return;

            case '=':
                currentIndex++;
                token = new SmartScriptToken(ECHO_TAG, "=");
                return;

            default:
                break;
            }

            if (Character.isLetter(ch)) { // Check for an identifier (variable or tag)
                value = extractIdentifier();
                type = IDENTIFIER;
            } else if (ch == '"') { // Check for a string literal
                value = exactStringLiteral();
                type = STRING;
            } else if (Character.isDigit(ch)) { // Check for an integer/double literal
                value = extractNumber();
                type = getNumericTokenType(value);
            } else if (ch == '@') {
                currentIndex++;
                value = extractIdentifier();
                type = FUNCTION;
            } else {
                throw new SmartScriptLexerException("Invalid tag expression.");
            }
        }

        token = new SmartScriptToken(type, value);

    }

    /**
     * Checks which numeric type {@code value} is and returns an appropriate
     * {@code TokenType}, or throws an exception if no matching type exists.
     *
     * <p>
     * Currently, {@code value} can only be {@code Integer} or {@code Double}
     *
     * @param value the value to check
     *
     * @return the TokenType of this value
     *
     * @throws IllegalArgumentException if there is no matching TokenType for {@value}
     * @see SmartScriptTokenType
     */
    private SmartScriptTokenType getNumericTokenType(Object value) {
        if (value instanceof Integer) {
            return CONST_INTEGER;
        }

        if (value instanceof Double) {
            return CONST_DOUBLE;
        }

        throw new IllegalArgumentException("Invalid value for numeric type.");
    }

    private String extractIdentifier() {
        int startIndex = currentIndex;

        if (!Character.isLetter(data[currentIndex])) {
            throw new SmartScriptLexerException("Illegal identifier start.");
        }

        for (; currentIndex < data.length; currentIndex++) {
            if (!Character.toString(data[currentIndex]).matches("[a-zA-Z0-9_]")) {
                break;
            }
        }

        return new String(data, startIndex, currentIndex - startIndex);
    }

    private String exactStringLiteral() {
        // Moves startingIndex away from opening quote
        int startIndex = ++currentIndex;

        for (; currentIndex < data.length; currentIndex++) {
            if (data[currentIndex] == '"' && data[currentIndex - 1] != '\\') {
                break;
            }
        }

        // Move current index away from the quote
        currentIndex++;

        // Because we moved the index, now length needs to be -1
        var str = new String(data, startIndex, currentIndex - startIndex - 1);

        // Check for any invalid escapes.
        // Regex visualized: https://regex101.com/r/tBB2Ep/1/
        if (str.matches("[^\\\\]\\\\[^ntr\"\\\\]")) {
            throw new SmartScriptLexerException("Invalid escape sequence.");
        }

        str = str.replace("\\\\", "\\")
                 // Escape quote
                 .replace("\\\"", "\"")
                 // Escape newline
                 .replace("\\n", Character.toString((char) 10))
                 // Escape carriage return
                 .replace("\\r", Character.toString((char) 13))
                 // Escape tab
                 .replace("\\t", Character.toString((char) 9));

        return str;
    }

    private boolean haveTagStart() {
        return data[currentIndex] == '{' && haveMoreData() && data[currentIndex + 1] == '$';
    }

    private boolean haveTagEnd() {
        return data[currentIndex] == '$' && haveMoreData() && data[currentIndex + 1] == '}';
    }

    /**
     * Extracts all text as-is, until it encounters a tag start.
     */
    private String extractAllText() {
        var sb = new StringBuilder();

        for (; currentIndex < data.length; currentIndex++) {
            if (data[currentIndex] == '\\') {
                if (haveMoreData() && (data[currentIndex + 1] == '{' || data[currentIndex + 1] == '\\')) {
                    sb.append(data[currentIndex + 1]);
                    currentIndex++;
                } else {
                    throw new SmartScriptLexerException("Illegal escape sequence.");
                }
            } else if (haveTagStart()) {
                break;
            } else {
                sb.append(data[currentIndex]);
            }
        }

        return sb.toString();
    }

    private Object extractNumber() {
        boolean isDouble = false;
        int startIndex = currentIndex;

        for (; currentIndex < data.length; currentIndex++) {
            if (!Character.isDigit(data[currentIndex])) {
                if (!isDouble && data[currentIndex] == '.') {
                    isDouble = true;

                    if (haveMoreData() && !Character.isDigit(data[currentIndex + 1])) {
                        throw new SmartScriptLexerException("Invalid number format");
                    }
                } else {
                    break;
                }
            }
        }

        try {
            var str = new String(data, startIndex, currentIndex - startIndex);

            if (isDouble) {
                return Double.parseDouble(str);
            } else {
                return Integer.parseInt(str);
            }
        } catch (NumberFormatException ex) {
            throw new SmartScriptLexerException("Invalid number");
        }
    }

    private void skipWhitespace() {
        for (; currentIndex < data.length; currentIndex++) {
            if (!isWhitespace(data[currentIndex])) {
                break;
            }
        }
    }

    private boolean isWhitespace(char c) {
        return c == ' ' || c == '\t' || (currentState == SmartScriptLexerState.TAG && (c == '\n' || c == '\r'));
    }

    private boolean haveMoreData() {
        return currentIndex + 1 < data.length;
    }
}
