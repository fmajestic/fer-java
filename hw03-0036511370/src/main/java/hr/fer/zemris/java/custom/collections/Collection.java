package hr.fer.zemris.java.custom.collections;

/**
 * The base interface for all custom collections.
 * 
 * @author Filip Majetic
 */
public interface Collection {

    /**
     * Checks whether the collection is empty.
     * 
     * @return {@code false} if the collection contains no elements, {@code false}
     *         otherwise
     */
    default boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Returns the size of the collection.
     * 
     * @return the number of elements in the collection
     */
    int size();

    /**
     * Adds a value to the list.
     * 
     * @param value the value to be added.
     */
    void add(Object value);

    /**
     * Checks whether the collections contains a value.
     * 
     * @param value the value to search for
     * 
     * @return {@code true} if the value is found, {@code false} otherwise
     */
    boolean contains(Object value);

    /**
     * Removes the first occurence of {@code value} from the collection.
     * 
     * @param value the value to be removed
     * 
     * @return {@code true} if the value was removed, {@code false} otherwise
     */
    boolean remove(Object value);

    /**
     * Returns an array containing all the elements of this collection.
     * 
     * @return the array
     */
    Object[] toArray();

    /**
     * Applies the {@code Processor.process} method to every element of the
     * collection.
     * 
     * @param processor the processor containing the action to be performed
     * 
     * @see Processor
     */
    default void forEach(Processor processor) {
        this.createElementsGetter().processRemaining(processor);
    }

    /**
     * Adds all the elements of another {@code Collection} to this one.
     * 
     * @param other the collection containing the elemtns to be added
     * 
     * @see #addAllSatisfying(Collection, Tester)
     */
    default void addAll(Collection other) {
        other.forEach(this::add);
    }

    /**
     * Adds all the elements of another {@code Collection} satisfying a test to this
     * collection.
     * 
     * @param other  the other collection to add to this one
     * @param tester the test to perform on each value in {@code other}
     * 
     * @see #addAll(Collection)
     */
    default void addAllSatisfying(Collection other, Tester tester) {
        other.forEach(el -> {
            if (tester.test(el))
                this.add(el);
        });
    }

    /**
     * Removes all the elements from this collection.
     */
    void clear();

    /**
     * Creates an element getter for this collection.
     * 
     * @return the new element getter.
     */
    ElementsGetter createElementsGetter();
}
