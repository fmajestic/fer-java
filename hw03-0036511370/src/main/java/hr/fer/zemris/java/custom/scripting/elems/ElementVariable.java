package hr.fer.zemris.java.custom.scripting.elems;

public class ElementVariable extends Element {
    private final String name;

    /**
     * Creates a new variable with the given name.
     * 
     * @param name the variable name
     */
    public ElementVariable(String name) {
        this.name = name;
    }

    /**
     * Returns the text representation of this variable.
     * 
     * @return the variable name as a string
     */
    @Override
    public String asText() {
        return name;
    }

    /**
     * Delegated to {@link #asText()}
     * 
     * @return the variable name as a string
     */
    @Override
    public String toString() {
        return asText();
    }
}
