package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * Represents a node containing some text.
 *
 * @author Filip Majetic
 */
public class TextNode extends Node {
    /** The node's text */
    private final String text;

    /**
     * Creates a new node with the given text.
     *
     * @param text the node text
     */
    public TextNode(String text) {
        this.text = text;
    }

    /**
     * Gets the node text as-is, with no escaping.
     *
     * @return the node text
     */
    public String getText() {
        return this.text;
    }

    /**
     * Returns the string representation of the node's text, such that it can be
     * parsed back by a {@link SmartScriptParser}.
     * <p>
     * Properly escapes all the {@code \} and <code>{$</code> so they don't break further reparsing.
     *
     * @return the escaped text
     */
    @Override
    public String toString() {
        return getText()
            .replace("\\", "\\\\")
            .replace("{$", "\\{$");
    }

    /**
     * Returns escaped text.
     *
     * @return the text
     */
    @Override
    public String toTree() {
        return "Text: " + getText()
            .replace("\\", "\\\\")
            .replace("{$", "\\{$")
            .replace("\n", "\\n")
            .replace("\t", "\\t")
            .replace("\r", "\\r");
    }
}
