package hr.fer.zemris.java.hw03.prob1;

/**
 * Represents a lexer token, with a type and a value.
 *
 * @author Filip Majetic
 */
public class Token {
    private Object value;
    private TokenType type;

    /**
     * Creates a token with the specified type and value.
     *
     * @param type  the token type
     * @param value the token value
     *
     * @see TokenType
     */
    public Token(TokenType type, Object value) {
        this.type = type;
        this.value = value;
    }

    /**
     * Gets the token value.
     *
     * @return the token value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Gets the token type
     *
     * @return the token type
     *
     * @see TokenType
     */
    public TokenType getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", type.toString(), value.toString());
    }

    /**
     * Defines all possible token types.
     */
    public enum TokenType {
        EOF, WORD, NUMBER, SYMBOL
    }
}
