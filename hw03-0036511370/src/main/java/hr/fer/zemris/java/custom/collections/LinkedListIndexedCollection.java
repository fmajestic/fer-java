package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Represents an indexed collection that uses a doubly linked list as a backing
 * field.
 * 
 * @author Filip Majetic
 * 
 * @see ArrayIndexedCollection
 */
public class LinkedListIndexedCollection implements List {
    private static final class ListNode {
        private ListNode previous;
        private ListNode next;
        private Object value;

        /**
         * Initializes a new node with the given value
         * 
         * @param value inital value of this node
         */
        public ListNode(Object value) {
            this.value = value;
            previous = null;
            next = null;
        }
    }

    private static class LinkedListElementsGetter implements ElementsGetter {
        private LinkedListIndexedCollection parent;
        private ListNode getterHead;
        private final long savedModificationCount;

        /**
         * Creates a new ElementsGetter for this list.
         * 
         * @param parent            the parent collection of the getter
         * @param modificationCount the modification count at creation of getter
         */
        public LinkedListElementsGetter(LinkedListIndexedCollection parent, long modificationCount) {
            this.parent = parent;
            this.getterHead = parent.first;
            this.savedModificationCount = modificationCount;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNextElement() {
            requireNoModifications();

            return getterHead != null;
        }

        private void requireNoModifications() {
            if (savedModificationCount != parent.modificationCount) {
                throw new ConcurrentModificationException();
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object getNextElement() {

            if (!hasNextElement()) {
                throw new NoSuchElementException("Returned all elements.");
            }

            var ret = getterHead.value;

            getterHead = getterHead.next;

            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ElementsGetter createElementsGetter() {
        return new LinkedListElementsGetter(this, modificationCount);
    }

    private ListNode first;
    private ListNode last;
    private int size;
    private long modificationCount;

    /**
     * Defaul constructor. Initializes an empty list.
     */
    public LinkedListIndexedCollection() {
        first = last = null;
        size = 0;
        modificationCount = 0;
    }

    /**
     * A constructor that takes another {@code Collection} to be added as inital
     * elements of the list.
     * 
     * @param other the inital collection to add to the list
     */
    public LinkedListIndexedCollection(Collection other) {
        this();
        this.addAll(other);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return first == null && last == null && size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(int index) {
        Objects.checkIndex(index, size);

        ListNode node = walkToIndex(index);

        return node.value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object value) {
        int index = 0;

        var walk = first;

        while (walk != null) {
            if (walk.value.equals(value)) {
                return index;
            }

            walk = walk.next;
            index += 1;
        }

        return -1;
    }

    /**
     * Inserts the given non-null {@code value} at the position, shifting the
     * remaining elements to the right.
     * 
     * @param value    the value to be inserted, not null
     * @param position the position at which to insert, valid range is
     *                 {@code [0, size]}
     *
     * @throws NullPointerException      if {@code value} is {@code null}
     * @throws IndexOutOfBoundsException if {@code position} is negative, or greater
     *                                   than {@code size}
     * 
     * @see #add(Object)
     */
    public void insert(Object value, int position) {
        Objects.requireNonNull(value);

        Objects.checkIndex(position, size + 1);

        // Special case: inserting into an empty list.
        if (this.isEmpty()) {
            add(value);
            return;
        }

        var node = new ListNode(value);

        // Special cases: inserting to the beginning or end
        if (position == 0) {
            node.next = first;
            first.previous = node;
            first = node;
        } else if (position == size) {
            last.next = node;
            node.previous = last;
            last = node;
        } else {
            ListNode insertAfter = walkToIndex(position - 1);

            // Since the previous case handles inserting to the end,
            // we don't need to worry about insertAfter.next.previous
            // throwing a NullPointerException
            node.next = insertAfter.next;
            insertAfter.next.previous = node;

            node.previous = insertAfter;
            insertAfter.next = node;
        }

        updateModificationCount();

        size += 1;
    }

    /**
     * Adds the non-null value to the end of the list.
     * 
     * @param value the value to be added, not null
     * 
     * @throws NullPointerException if value is null
     * 
     * @see #insert(Object, int)
     * @see #addAll(Collection)
     */
    @Override
    public void add(Object value) {
        Objects.requireNonNull(value);

        var node = new ListNode(value);

        if (this.isEmpty()) {
            first = last = node;
        } else {
            node.previous = last;
            last.next = node;
            last = node;
        }

        this.size += 1;

        updateModificationCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return indexOf(value) >= 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(Object value) {
        boolean nodeRemoved = false;

        if (first.value.equals(value)) {
            var oldFirst = first;

            first = oldFirst.next;
            first.previous = null;

            oldFirst.next = null;

            nodeRemoved = true;
        } else if (last.value.equals(value)) {
            var oldLast = last;

            last = oldLast.previous;
            last.next = null;

            oldLast.previous = null;

            nodeRemoved = true;
        } else {
            var toRemove = first;

            while (toRemove != null) {
                if (toRemove.value.equals(value)) {
                    toRemove.previous.next = toRemove.next;
                    toRemove.next.previous = toRemove.previous;

                    nodeRemoved = true;
                    break;
                }

                toRemove = toRemove.next;
            }
        }

        if (nodeRemoved) {
            size -= 1;
            updateModificationCount();
        }

        return nodeRemoved;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(int index) {
        Objects.checkIndex(index, size);

        ListNode toRemove = walkToIndex(index);

        if (size == 1) {
            clear();
            return;
        }

        if (toRemove == first) {
            toRemove.next.previous = null;
            first = toRemove.next;
        } else if (toRemove == last) {
            toRemove.previous.next = null;
            last = toRemove.previous;
        } else {
            toRemove.previous.next = toRemove.next;
            toRemove.next.previous = toRemove.previous;
        }

        size -= 1;
        updateModificationCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] toArray() {
        var elements = new Object[size];

        var walk = first;

        // This could be done with a while loop,
        // but that would unnecessarily expose the counter variable
        for (int i = 0; i < size; i++) {
            elements[i] = walk.value;
            walk = walk.next;
        }

        return elements;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        while (first != null) {
            var next = first.next;

            first.previous = null;
            first.next = null;

            first = next;
        }

        first = last = null;
        size = 0;
        updateModificationCount();
    }

    /**
     * Gets the {@code ListNode} at the specified index.
     * 
     * @param index the index to get from
     * @return the node
     */
    private ListNode walkToIndex(int index) {
        ListNode walk;

        // Optimizacija hodanja po listi:
        // Ako se nalazi u prvoj polovici, krećemo od početka.
        // Ako se nalazi u drugoj polovici, krećemo od kraja.
        if (index <= (size - 1) / 2) {
            walk = first;

            for (int i = 0; i < index; i++) {
                walk = walk.next;
            }
        } else {
            walk = last;

            for (int i = 0; i < size - index - 1; i++) {
                walk = walk.previous;
            }
        }

        return walk;
    }

    /**
     * Increments modificationCount by 1
     */
    private void updateModificationCount() {
        this.modificationCount += 1;
    }
}
