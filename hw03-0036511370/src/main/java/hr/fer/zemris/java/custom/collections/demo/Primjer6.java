package hr.fer.zemris.java.custom.collections.demo;

import java.util.ConcurrentModificationException;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.Collection;
import hr.fer.zemris.java.custom.collections.ElementsGetter;

/**
 * Primjer6
 */
public class Primjer6 {

    public static void main(String[] args) {
        Collection col = new ArrayIndexedCollection();
        col.add("Ivo");
        col.add("Ana");
        col.add("Jasna");
        ElementsGetter getter = col.createElementsGetter();
        System.out.println("Jedan element: " + getter.getNextElement());
        System.out.println("Jedan element: " + getter.getNextElement());
        col.clear();
        try {
            System.out.println("Jedan element: " + getter.getNextElement());
        } catch (ConcurrentModificationException ex) {
            System.out.println("Program se očekivano raspada uz ConcurrentModificationException.");
        }
    }
}
