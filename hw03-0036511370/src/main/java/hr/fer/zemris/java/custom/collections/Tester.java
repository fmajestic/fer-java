package hr.fer.zemris.java.custom.collections;

/**
 * Tests a single value for a contidion.
 */
public interface Tester {

    /**
     * The test to perform on a value.
     * 
     * @param obj the value to be tested.
     * 
     * @return {@code true} if the condition was satisfied,
     *         {@code false} otherwise
     */
    boolean test(Object obj);
}
