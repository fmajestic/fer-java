package hr.fer.zemris.java.hw03.prob1;

/**
 * LexerException
 */
public class LexerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LexerException() {
        super();
    }

    public LexerException(String message) {
        super(message);
    }
}
