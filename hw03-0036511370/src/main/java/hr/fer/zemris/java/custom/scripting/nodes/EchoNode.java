package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

import java.util.Objects;

/**
 * Represents a node that contains an expression whose result will be echoed.
 * <p>
 * The expression must be valid for the node to be echoed. Each expression
 * element (operator, function, argument) is stored as an appropriate subtype of
 * {@link Element}, in the order they were input.
 *
 * @author Filip Majetic
 */
public class EchoNode extends Node {
    private final Element[] elements;

    /**
     * Creates a new echo node with the specified elements.
     *
     * @param elements the elements of this node, not null
     */
    public EchoNode(Element[] elements) {
        this.elements = Objects.requireNonNull(elements, "Elements can't be null");
    }

    /**
     * Gets the elements of this echo node.
     *
     * @return the elements
     */
    public Element[] getElements() {
        return this.elements;
    }

    /**
     * Returns the string representation of the node's elements, such that it can be
     * parsed back by a {@link SmartScriptParser}.
     *
     * @return the escaped text
     */
    @Override
    public String toString() {
        var sb = new StringBuilder();

        sb.append("{$= ");

        for (var el : elements) {
            sb.append(String.format("%s ", el));
        }

        sb.append("$}");

        return sb.toString();
    }

    @Override
    public String toTree() {
        var sb = new StringBuilder();

        sb.append("Echo: { ");

        for (var el : elements) {
            sb.append(String.format("%s ", el.asText()));
        }

        sb.append("}");

        return sb.toString();
    }
}
