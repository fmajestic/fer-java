package hr.fer.zemris.java.custom.collections;

/**
 * Occurs when the stack is empty and either pop() or peek() is called.
 * 
 * @author Filip Majetic
 */
public class EmptyStackException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor. Delegated to {@code super();}
     */
    public EmptyStackException() {
        super();
    }

    /**
     * Constructor with a message. Delegated to {@code super(message);}
     */
    public EmptyStackException(String message) {
        super(message);
    }
}
