package hr.fer.zemris.java.custom.scripting.elems;

public class ElementOperator extends Element {
    private final String symbol;

    /**
     * Creates a new operator with the given symbol.
     * 
     * @param symbol the operator symbol
     */
    public ElementOperator(String symbol) {
        this.symbol = symbol;
    }

    /**
     * Returns a text representation of this symbol.
     * 
     * @return the symbol as a string
     */
    @Override
    public String asText() {
        return symbol;
    }

    /**
     * Delegated to {@link #asText()}
     * 
     * @return the symbol as a string
     */
    @Override
    public String toString() {
        return asText();
    }

}
