package hr.fer.zemris.java.custom.collections;

/**
 * The base interface for all custom Processors
 * 
 * @author Filip Majetic
 */
public interface Processor {
	/**
	 * Performs an action on the {@code value}.
	 * 
	 * @param value the value the action will be applied to
	 */
	void process(Object value);
}
