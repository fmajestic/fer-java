package hr.fer.zemris.java.hw03;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Tests the functionality of {@link SmartScriptParser} using example inputs.
 */
public class SmartScriptTester {
    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Program requires exactly 1 argument.");
            System.exit(-1);
        }

        String docBody;
        SmartScriptParser parser = null;

        try {
            docBody = Files.readString(Paths.get(args[0]));
            parser = new SmartScriptParser(docBody);
        } catch (SmartScriptParserException e) {
            System.out.println("Unable to parse document!");
            System.exit(-1);
        } catch (IOException ex) { // Samo zbog Files.readAllBytes
            System.out.println("Can't find file");
            System.exit(-1);
        } catch (Exception e) {
            System.out.println("If this line ever executes, you have failed this class!");
            System.exit(-1);
        }

        DocumentNode document = parser.getDocumentNode();
        String originalDocumentBody = createOriginalDocumentBody(document);

        // should write something like original content of docBody
        System.out.println(originalDocumentBody);
        System.out.println();

        // Should write document tree structure
        System.out.println(document.toTree());
    }

    /**
     * Generates a textual document body.
     * <p>
     * Implemented via overriding toString
     *
     * @param document the document to generate a body from
     *
     * @return the body
     */
    private static String createOriginalDocumentBody(DocumentNode document) {
        return document.toString();
    }
}
