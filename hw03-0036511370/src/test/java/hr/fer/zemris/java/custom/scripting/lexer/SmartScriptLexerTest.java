package hr.fer.zemris.java.custom.scripting.lexer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SmartScriptLexerTest {
    @Test
    public void testNotNull() {
        SmartScriptLexer lexer = new SmartScriptLexer("");

        assertNotNull(lexer.nextToken(), "Token was expected but null was returned.");
    }

    @Test
    public void testNullInput() {
        // must throw!
        assertThrows(NullPointerException.class, () -> new SmartScriptLexer(null));
    }

    @Test
    public void testEmpty() {
        SmartScriptLexer lexer = new SmartScriptLexer("");

        assertEquals(SmartScriptTokenType.EOF, lexer.nextToken().getType(),
            "Empty input must generate only EOF token.");
    }

    @Test
    public void testGetReturnsLastNext() {
        // Calling getToken once or several times after calling nextToken
        // must return each time what nextToken returned...
        SmartScriptLexer lexer = new SmartScriptLexer("");

        SmartScriptToken token = lexer.nextToken();
        assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
        assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
    }

    @Test
    public void testRadAfterEOF() {
        SmartScriptLexer lexer = new SmartScriptLexer("");

        // will obtain EOF
        lexer.nextToken();
        // will throw!
        assertThrows(SmartScriptLexerException.class, lexer::nextToken);
    }

    @Test
    public void testNoActualContent() {
        // When input is only of spaces, tabs, newlines, etc...
        String text = "   \r\n\t    ";
        SmartScriptLexer lexer = new SmartScriptLexer(text);

        assertEquals(SmartScriptTokenType.TEXT, lexer.nextToken().getType(),
            "Input was whitespace. Lexer should return a string.");
        assertEquals(text, lexer.getToken().getValue(),
            "Input was whitespace. Lexer should pick up whitespace in text mode.");
    }

    @Test
    public void testSeveralWords() {
        // Lets check for several words...
        SmartScriptLexer lexer = new SmartScriptLexer("This is sample text.\n");

        // We expect the following stream of tokens
        SmartScriptToken[] correctData = {
            new SmartScriptToken(SmartScriptTokenType.TEXT, "This is sample text.\n"),
            new SmartScriptToken(SmartScriptTokenType.EOF, null)
        };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testTag() {
        String text = "This is sample text.\n{$";

        var lexer = new SmartScriptLexer(text);

        SmartScriptToken[] correctData = {
            new SmartScriptToken(SmartScriptTokenType.TEXT, "This is sample text.\n"),
            new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"),
            new SmartScriptToken(SmartScriptTokenType.EOF, null)
        };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testNumbers() {
        String text = "16 12.";

        var lexer = new SmartScriptLexer(text);
        lexer.setState(SmartScriptLexerState.TAG);

        SmartScriptToken[] correctData = {
            new SmartScriptToken(SmartScriptTokenType.CONST_INTEGER, 16),
            new SmartScriptToken(SmartScriptTokenType.CONST_DOUBLE, 12.),
            new SmartScriptToken(SmartScriptTokenType.EOF, null)
        };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testEscapedTag() {
        String text = "  \\{$1st  \r\n\t   ";
        String expected = "  {$1st  \r\n\t   ";
        SmartScriptLexer lexer = new SmartScriptLexer(text);

        // We expect the following stream of tokens
        SmartScriptToken[] correctData = {
            new SmartScriptToken(SmartScriptTokenType.TEXT, expected),
            new SmartScriptToken(SmartScriptTokenType.EOF, null)
        };

        checkTokenStream(lexer, correctData);
    }

    @Test
    public void testInvalidEscapeEnding() {
        // this is three spaces and a single backslash -- 4 letters string
        SmartScriptLexer lexer = new SmartScriptLexer("   \\");

        // will throw!
        assertThrows(SmartScriptLexerException.class, lexer::nextToken);
    }

    @Test
    public void testInvalidEscape() {
        SmartScriptLexer lexer = new SmartScriptLexer("   \\a    ");

        // will throw!
        assertThrows(SmartScriptLexerException.class, lexer::nextToken);
    }

    // Helper method for checking if lexer generates the same stream of tokens
    // as the given stream.
    private void checkTokenStream(SmartScriptLexer lexer, SmartScriptToken[] correctData) {
        int counter = 0;
        for (SmartScriptToken expected : correctData) {
            SmartScriptToken actual = lexer.nextToken();
            String msg = "Checking token " + counter + ":";
            assertEquals(expected.getType(), actual.getType(), msg);
            assertEquals(expected.getValue(), actual.getValue(), msg);
            counter++;
        }
    }

    @Test
    public void testNullState() {
        assertThrows(NullPointerException.class, () -> new SmartScriptLexer("").setState(null));
    }

    @Test
    public void testNotNullInExtended() {
        SmartScriptLexer lexer = new SmartScriptLexer("");
        lexer.setState(SmartScriptLexerState.TAG);

        assertNotNull(lexer.nextToken(), "Token was expected but null was returned.");
    }

    @Test
    public void testEmptyInExtended() {
        SmartScriptLexer lexer = new SmartScriptLexer("");
        lexer.setState(SmartScriptLexerState.TAG);

        assertEquals(SmartScriptTokenType.EOF, lexer.nextToken().getType(),
            "Empty input must generate only EOF token.");
    }

    @Test
    public void testGetReturnsLastNextInExtended() {
        // Calling getToken once or several times after calling nextToken must return
        // each time what nextToken returned...
        SmartScriptLexer lexer = new SmartScriptLexer("");
        lexer.setState(SmartScriptLexerState.TAG);

        SmartScriptToken token = lexer.nextToken();
        assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
        assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
    }

    @Test
    public void testRadAfterEOFInExtended() {
        SmartScriptLexer lexer = new SmartScriptLexer("");
        lexer.setState(SmartScriptLexerState.TAG);

        // will obtain EOF
        lexer.nextToken();
        // will throw!
        assertThrows(SmartScriptLexerException.class, lexer::nextToken);
    }

    @Test
    public void testNoActualContentInExtended() {
        // When input is only of spaces, tabs, newlines, etc...
        SmartScriptLexer lexer = new SmartScriptLexer("   \r\n\t    ");
        lexer.setState(SmartScriptLexerState.TAG);

        assertEquals(SmartScriptTokenType.EOF, lexer.nextToken().getType(),
            "Input was whitespace in TAG mode. Lexer should have generated only EOF token.");
    }

    /**
     * The entire test example, as a string with \n and all whitespace. Lexer state
     * is manually changed.
     */
    @Test
    public void testExampleInput() {
        SmartScriptLexer lexer = new SmartScriptLexer(
            "This is sample text.\n{$ FOR i 1 10 1 $}\n  This is {$= i $}-th time this message is generated" +
            ".\n{$END$}\n{$FOR i 0 10 2 $}\n  sin({$=i$}^2) = {$= i i * @sin \"0.000\" @decfmt $}\n{$END$}\n");

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "This is sample text.\n"));

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"));
        lexer.setState(SmartScriptLexerState.TAG);
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "FOR"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "i"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CONST_INTEGER, 1));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CONST_INTEGER, 10));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CONST_INTEGER, 1));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_CLOSE, "$}"));
        lexer.setState(SmartScriptLexerState.TEXT);

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "\n  This is "));

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"));
        lexer.setState(SmartScriptLexerState.TAG);
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.ECHO_TAG, "="));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "i"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_CLOSE, "$}"));
        lexer.setState(SmartScriptLexerState.TEXT);

        checkToken(lexer.nextToken(),
            new SmartScriptToken(SmartScriptTokenType.TEXT, "-th time this message is generated.\n"));

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"));
        lexer.setState(SmartScriptLexerState.TAG);
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "END"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_CLOSE, "$}"));
        lexer.setState(SmartScriptLexerState.TEXT);

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "\n"));

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"));
        lexer.setState(SmartScriptLexerState.TAG);
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "FOR"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "i"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CONST_INTEGER, 0));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CONST_INTEGER, 10));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CONST_INTEGER, 2));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_CLOSE, "$}"));
        lexer.setState(SmartScriptLexerState.TEXT);

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "\n  sin("));

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"));
        lexer.setState(SmartScriptLexerState.TAG);
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.ECHO_TAG, "="));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "i"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_CLOSE, "$}"));
        lexer.setState(SmartScriptLexerState.TEXT);

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "^2) = "));

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"));
        lexer.setState(SmartScriptLexerState.TAG);
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.ECHO_TAG, "="));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "i"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "i"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OP_MUL, "*"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.FUNCTION, "sin"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.STRING, "0.000"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.FUNCTION, "decfmt"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_CLOSE, "$}"));
        lexer.setState(SmartScriptLexerState.TEXT);

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "\n"));

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_OPEN, "{$"));
        lexer.setState(SmartScriptLexerState.TAG);
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.IDENTIFIER, "END"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG_CLOSE, "$}"));
        lexer.setState(SmartScriptLexerState.TEXT);

        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "\n"));
        checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.EOF, null));
    }

    private void checkToken(SmartScriptToken actual, SmartScriptToken expected) {
        assertEquals(expected.getType(), actual.getType(), "Token types are not equal.");
        assertEquals(expected.getValue(), actual.getValue(), "Token values are not equal");
    }
}
