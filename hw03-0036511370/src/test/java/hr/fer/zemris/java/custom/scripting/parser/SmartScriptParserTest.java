package hr.fer.zemris.java.custom.scripting.parser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartScriptParserTest {

    @Test
    public void nullInput() {
        assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(null));
    }

    @Test
    public void emptyInput() {
        var reconstructed = new SmartScriptParser("").getDocumentNode().toString();
        assertEquals("", reconstructed);
    }

    @Test
    public void whitespaceInput() {
        var reconstructed = new SmartScriptParser("  \r\n\t  ").getDocumentNode().toString();
        assertEquals("  \r\n\t  ", reconstructed);
    }

    @Test
    public void invalidEscape() {
        assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser("\\1").getDocumentNode());
    }
}
