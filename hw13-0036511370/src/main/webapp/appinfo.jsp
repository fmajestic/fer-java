<%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>App info</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body-dynamic.jsp">
</head>
<body>
<h1>Webapp info</h1>
<p>The app has been running for:
    <%
        long uptime = System.currentTimeMillis() - (Long) pageContext.getServletContext().getAttribute("init_time");
        long d = TimeUnit.MILLISECONDS.toDays(uptime);
        long h = TimeUnit.MILLISECONDS.toHours(uptime);
        long m = TimeUnit.MILLISECONDS.toMinutes(uptime);
        long s = TimeUnit.MILLISECONDS.toSeconds(uptime);

        out.print(String.format("%d days %d hours %d minutes %d seconds", d, h, m, s));
    %>
</p>
</body>
</html>
