<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Power calculator error</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body-dynamic.jsp">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/table.css">
</head>
<body>
<h1>Power calculator error</h1>
<p>Invalid input:</p>
<table>
    <thead>
    <tr>
        <th>Parameter</th>
        <th>Value</th>
        <th>Parsed</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>a</td>
        <td>${requestScope.a != null ? requestScope.a : "none"}</td>
        <td>${requestScope.valA}</td>
    </tr>
    <tr>
        <td>b</td>
        <td>${requestScope.b != null ? requestScope.a : "none"}</td>
        <td>${requestScope.valB}</td>
    </tr>
    <tr>
        <td>n</td>
        <td>${requestScope.n != null ? requestScope.a : "none"}</td>
        <td>${requestScope.valN}</td>
    </tr>
    </tbody>
</table>
</body>
</html>
