<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Voting</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body-dynamic.jsp">
</head>
<body>
<h1>Vote for your favorite</h1>
<p>Which one of these bands is your favorite? Click a link to vote!</p>
<ol>
    <c:forEach var="e" items="${requestScope.bands}">
        <li><a href="glasanje-glasaj?id=${e.key}">${e.value.name}</a></li>
    </c:forEach>
</ol>
</body>
</html>
