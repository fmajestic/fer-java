<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <title>Trigonometry</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body-dynamic.jsp">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/table.css">
</head>

<body>
    <h1>Trigonometric data</h1>
    <table>
        <thead>
        <tr>
            <th>x</th>
            <th>sin(x)</th>
            <th>cos(x)</th>
        </tr>
        </thead>
        <tbody style="font-family: Consolas,monospace;">
            <c:forEach var="item" items="${requestScope.trig}">
                <tr>
                    <td>${item.angle}</td>
                    <td>${item.sin}</td>
                    <td>${item.cos}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>

</html>
