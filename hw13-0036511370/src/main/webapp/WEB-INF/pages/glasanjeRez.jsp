<%--suppress unchecked --%>
<%@ page import="hr.fer.zemris.java.hw13.servlets.glasanje.BandInfo" %>
<%@ page import="hr.fer.zemris.java.hw13.servlets.glasanje.VoteData" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Results</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body-dynamic.jsp">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/table.css">
</head>
<body>
<h1>Vote results</h1>
<p>The vote results are:</p>
<table>
    <thead>
    <tr>
        <th>Band</th>
        <th>No. of votes</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="vote" items="${votes}">
        <tr>
            <td>${bands[vote.id].name}</td>
            <td>${vote.votes}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<h2>Graphical display</h2>
<img alt="Pie-chart" src="${pageContext.request.contextPath}/glasanje-grafika"/>

<h2>XLS table results</h2>
<p>An Excel table with the results is available <a href="${pageContext.request.contextPath}/glasanje-xls">here</a></p>

<h2>Misc</h2>
<p>Song examples of winning band(s):</p>
<ul>
    <%
        Map<String, BandInfo> bands = (Map<String, BandInfo>) request.getAttribute("bands");
        List<VoteData> votes = (List<VoteData>) request.getAttribute("votes");

        int maxVotes = votes.stream().mapToInt(VoteData::getVotes).max().orElse(-1);

        for (VoteData v : votes) {
            if (v.getVotes() == maxVotes) {
                BandInfo b = bands.get(v.getId());
                out.print(String.format("<li><a href=\"%s\" target=\"_blank\">%s</a></li>", b.getSong(), b.getName()));
            }
        }
    %>
</ul>

</body>
</html>
