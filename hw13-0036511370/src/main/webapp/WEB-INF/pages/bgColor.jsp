<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Background color changed</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body.css">
</head>
<body>
<h1>Background color changed</h1>
<p>Background color changed to: ${sessionScope.pickedBgCol}</p>
<c:if test="${!sessionScope.pickedBgCol.equals(\"#FFFFFF\")}">
    <svg>
        <rect width="5em" height="5em" fill="${sessionScope.pickedBgCol}"></rect>
    </svg>
</c:if>
<a href="${pageContext.request.contextPath}/index.jsp">Back to homepage</a>
</body>
</html>
