<%--suppress ELValidationInJSP --%>
<%@ page contentType="text/css; charset=UTF-8"  pageEncoding="UTF-8" %>
body {
    width: 35em;
    margin: 0 auto;
    font-family: Tahoma, Verdana, Arial, sans-serif;
    background-color: ${sessionScope.pickedBgCol};
}

body h2 {
    padding-top: 20px;
}
