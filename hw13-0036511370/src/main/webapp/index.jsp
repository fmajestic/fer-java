<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body-dynamic.jsp">
    <style>
        h2 {
            padding-top: 20px;
        }
    </style>
</head>
<body>
<h1>Home</h1>

<h2>Problem 1 - Background color</h2>
<p><a href="colors.jsp">Chooser</a></p>

<h2>Problem 2 - Trigonometry</h2>
<p>Use default values: <a href="trigonometric?a=0&b=90">0 to 90</a></p>
<form action="trigonometric" method="GET">
    Početni kut:<br><input type="number" name="a" min="0" max="360" step="1" value="0"><br>
    Završni kut:<br><input type="number" name="b" min="0" max="360" step="1" value="360"><br>
    <input type="submit" value="Tabeliraj"><input type="reset" value="Reset">
</form>

<h2>Problem 3 - Stories</h2>
<p><a href="stories/funny.jsp">A short, funny story</a></p>

<h2>Problem 4 - Survey</h2>
<p><a href="report.jsp">OS usage</a></p>

<h2>Problem 5 - Powers</h2>
<p><a href="powers?a=1&b=100&n=3">Generate .xls table of powers</a></p>

<h2>Problem 6 - App info</h2>
<p><a href="appinfo.jsp">Uptime</a></p>

<h2>Problem 7 - Voting</h2>
<p><a href="glasanje">Voting</a></p>

</body>
</html>
