<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>OS usage report</title>
    <link rel="stylesheet" type="text/css" href="resources/css/body-dynamic.jsp">
</head>
<body>
    <h1>OS usage</h1>
    <p>Here are the results of OS usage in survey that we completed.</p>
    <img src="${pageContext.request.contextPath}/reportImage" alt="OS usage info">
</body>
</html>
