<!DOCTYPE html>
<html lang="en">
<head>
    <title>Color chooser</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/body.css">
</head>
<body>
    <h1>Pick a color</h1>
    <h2><a href="${pageContext.request.contextPath}/setcolor?color=white">WHITE</a></h2>
    <h2><a href="${pageContext.request.contextPath}/setcolor?color=red">RED</a></h2>
    <h2><a href="${pageContext.request.contextPath}/setcolor?color=green">GREEN</a></h2>
    <h2><a href="${pageContext.request.contextPath}/setcolor?color=cyan">CYAN</a></h2>
</body>
</html>