package hr.fer.zemris.java.hw13.servlets;

/**
 * Stores trigonometry calculation data.
 */
public class TrigData {

    /** The angle. */
    private int angle;

    /** The sine of the angle. */
    private double sin;

    /** The cosine of the angle. */
    private double cos;

    /**
     * Creates a new data store.
     *
     * @param angle the angle
     * @param sin   its sine
     * @param cos   its cosine
     */
    public TrigData(int angle, double sin, double cos) {
        this.angle = angle;
        this.sin = sin;
        this.cos = cos;
    }

    /**
     * Gets the angle.
     *
     * @return the angle
     */
    public int getAngle() {
        return angle;
    }

    /**
     * Gets the sine.
     *
     * @return the sine
     */
    public double getSin() {
        return sin;
    }

    /**
     * Gets the cosine.
     *
     * @return the cosine
     */
    public double getCos() {
        return cos;
    }
}
