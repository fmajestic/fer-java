package hr.fer.zemris.java.hw13.servlets.glasanje;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Generates an Excel table with the results.
 */
@WebServlet(name = "resultTable", urlPatterns = { "/glasanje-xls" })
public class GlasanjeXlsServlet extends HttpServlet {

    /**
     * Generates an Excel table (.xls) and writes it directly to the response.
     * The appropriate Content-Disposition and Content-Type are set.
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        HSSFWorkbook hwb = new HSSFWorkbook();
        String bandfile = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
        String resultfile = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
        Map<String, BandInfo> bands = Util.getBands(bandfile);
        List<VoteData> votes = Util.getVotes(resultfile);

        HSSFSheet sheet = hwb.createSheet("Votes");
        HSSFRow header = sheet.createRow(0);
        header.createCell(0).setCellValue("Name");
        header.createCell(1).setCellValue("No. of votes");
        header.createCell(2).setCellValue("Song");

        for (int i = 0; i < votes.size(); i++) {
            HSSFRow row = sheet.createRow(i + 1);

            BandInfo band = bands.get(votes.get(i).getId());
            row.createCell(0).setCellValue(band.getName());
            row.createCell(1).setCellValue(votes.get(i).getVotes());
            row.createCell(2).setCellValue(band.getSong());
        }

        resp.setContentType("application/vnd.ms-excel");
        resp.setHeader("Content-Disposition", "attachment; filename=results.xls");
        hwb.write(resp.getOutputStream());
    }
}
