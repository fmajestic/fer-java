package hr.fer.zemris.java.hw13.servlets.glasanje;

/**
 * Stores band information.
 */
public class BandInfo {

    /** The band name. */
    private String name;

    /** Their song. */
    private String song;

    /**
     * Creates a new band info.
     *
     * @param name the name
     * @param song the song
     */
    public BandInfo(String name, String song) {
        this.name = name;
        this.song = song;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the song.
     *
     * @return the song
     */
    public String getSong() {
        return song;
    }

    /**
     * Sets the song.
     *
     * @param song the song
     */
    public void setSong(String song) {
        this.song = song;
    }
}
