package hr.fer.zemris.java.hw13.servlets.glasanje;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/** Utility methods. */
public class Util {

    /**
     * Loads band definitions from a file.
     *
     * @param file the file to read
     *
     * @return the id -> band info map
     *
     * @throws IOException if an IO error occurs
     */
    public static Map<String, BandInfo> getBands(String file) throws IOException {
        return new TreeMap<>(
            Files.lines(Paths.get(file))
                .filter(line -> !line.isEmpty())
                .map(line -> line.split("\t"))
                .collect(Collectors.toMap(
                    parts -> parts[0],
                    parts -> new BandInfo(parts[1], parts[2])
                ))
        );
    }

    /**
     * Loads vote data from a file.
     *
     * @param file the file to read
     *
     * @return all vote data from the file
     *
     * @throws IOException if an IO error occurs
     */
    public static List<VoteData> getVotes(String file) throws IOException {
        return Files.lines(Paths.get(file))
            .map(line -> line.split("\t"))
            .map(parts -> new VoteData(parts[0], Integer.valueOf(parts[1])))
            .sorted(Comparator.comparingInt(VoteData::getVotes).reversed())
            .collect(Collectors.toList());
    }
}
