package hr.fer.zemris.java.hw13.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Stores the background color for this session.
 */
@WebServlet(name = "s", urlPatterns = { "/setcolor" })
public class BackgroundColorServlet extends HttpServlet {

    /** All available colors. */
    private static Map<String, String> COLORS = new HashMap<>();

    static {
        COLORS.put("red", "#FD1D1D");
        COLORS.put("cyan", "#0D98BA");
        COLORS.put("green", "#4CBB17");
        COLORS.put("white", "#FFFFFF");
    }

    /**
     * Stores the chosen color into the session attributes.
     * Uses default color if no color was chosen or an unsupported color was chosen.
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String picked = COLORS.getOrDefault(req.getParameter("color"), "#FFFFFF");

        req.getSession().setAttribute("pickedBgCol", picked);
        req.getRequestDispatcher("/WEB-INF/pages/bgColor.jsp").forward(req, resp);
    }
}
