package hr.fer.zemris.java.hw13.servlets;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Math.pow;

/**
 * Generates an Excel table with powers of numbers.
 */
@WebServlet(name = "powers", urlPatterns = { "/powers" })
public class PowersTableServlet extends HttpServlet {

    /**
     * Generates the table and writes it to the response output stream.
     * <p>
     * THe proper Content-Disposition and Content-Type are set.
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String strA = req.getParameter("a");
        String strB = req.getParameter("b");
        String strN = req.getParameter("n");

        Integer a = null, b = null, n = null;

        try {
            a = Integer.valueOf(strA);
            b = Integer.valueOf(strB);
            n = Integer.valueOf(strN);
        } catch (Exception ex) {
            displayError(req, resp, a, b, n);
            return;
        }

        if (a < -100 || a > 100 || b < -100 || b > 100 || n < 1 || n > 5) {
            displayError(req, resp, a, b, n);
            return;
        }

        HSSFWorkbook hwb = new HSSFWorkbook();

        if (a < b) {
            for (int i = 1; i <= n; i++) {
                HSSFSheet sheet = hwb.createSheet("Power " + i);

                for (int j = 0; j <= b - a; j++) {
                    HSSFRow row = sheet.createRow(j);
                    int num = a + j;

                    row.createCell(0).setCellValue(num);
                    row.createCell(1).setCellValue(pow(num, i));

                    sheet.setAutobreaks(true);
                }
            }
        } else {
            for (int i = 1; i <= n; i++) {
                HSSFSheet sheet = hwb.createSheet("Power " + i);

                for (int j = 0; j <= a - b; j++) {
                    HSSFRow row = sheet.createRow(j);
                    int num = a - j;

                    row.createCell(0).setCellValue(num);
                    row.createCell(1).setCellValue(pow(num, i));

                    sheet.setAutobreaks(true);
                }
            }
        }

        resp.setContentType("application/vnd.ms-excel");
        resp.setHeader("Content-Disposition", "attachment; filename=powers.xls");
        hwb.write(resp.getOutputStream());
    }

    /**
     * Forward the request to the error page.
     *
     * @param req  the request
     * @param resp the response
     * @param a    the parsed a value
     * @param b    the parse b value
     * @param n    the parsed n value
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    private void displayError(HttpServletRequest req, HttpServletResponse resp, Integer a, Integer b, Integer n)
        throws ServletException, IOException {
        req.setAttribute("valA", a);
        req.setAttribute("valB", b);
        req.setAttribute("valN", n);
        req.getRequestDispatcher("./WEB-INF/pages/powerError.jsp").forward(req, resp);
    }
}
