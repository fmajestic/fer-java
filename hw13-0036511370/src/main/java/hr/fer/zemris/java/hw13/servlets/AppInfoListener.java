package hr.fer.zemris.java.hw13.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Listens for app startup and shutdown.
 * Stores system time (in milliseconds) when the app is started.
 * The data is stored in the servlet context under the key {@code "init_time"}.
 */
@WebListener
public class AppInfoListener implements ServletContextListener {

    /**
     * Stores the system time in the servlet context under {@code "init_time"}.
     *
     * @param servletContextEvent the initialized context
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().setAttribute("init_time", System.currentTimeMillis());
    }

    /**
     * Removes the stored time.
     *
     * @param servletContextEvent the destroyed context
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().removeAttribute("init_time");
    }
}
