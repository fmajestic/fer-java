package hr.fer.zemris.java.hw13.servlets.glasanje;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Generates a pie chart with the results.
 */
@WebServlet(name = "grafika", urlPatterns = { "/glasanje-grafika" })
public class GlasanjeGrafikaServlet extends HttpServlet {

    /**
     * Generates a pie chart with the vote results an prints it directly to the response.
     * The appropriate Content-Type is set (image/png).
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws IOException if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        DefaultPieDataset dataset = new DefaultPieDataset();
        String bandfile = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
        String resultfile = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
        Map<String, BandInfo> bands = Util.getBands(bandfile);
        List<VoteData> votes = Util.getVotes(resultfile);

        for (VoteData v : votes) {
            dataset.setValue(bands.get(v.getId()).getName(), v.getVotes());
        }

        JFreeChart chart = ChartFactory.createPieChart3D(
            "Band voting results",
            dataset,
            true,
            true,
            false
        );

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);

        resp.setContentType("image/png");

        ChartUtils.writeChartAsPNG(
            resp.getOutputStream(),
            chart,
            500,
            350
        );
    }
}

