package hr.fer.zemris.java.hw13.servlets.glasanje;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 * Displays the results page.
 */
@WebServlet(name = "rez", urlPatterns = "/glasanje-rezultati")
public class GlasanjeRezultatiServlet extends HttpServlet {

    /**
     * Sets up all the data and forward the request to glasanjeRez.jsp
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bandFile = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
        String resultFile = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

        Path resultPath = Paths.get(resultFile);
        Path bandPath = Paths.get(bandFile);

        if (!Files.exists(resultPath)) {
            Files.createFile(resultPath);
        }

        if (!Files.exists(bandPath)) {
            Files.createFile(bandPath);
        }

        List<VoteData> votes = Util.getVotes(resultFile);
        Map<String, BandInfo> bands = Util.getBands(bandFile);

        req.setAttribute("votes", votes);
        req.setAttribute("bands", bands);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
    }
}
