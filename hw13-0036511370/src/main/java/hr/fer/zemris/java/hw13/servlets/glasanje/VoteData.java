package hr.fer.zemris.java.hw13.servlets.glasanje;

public class VoteData {

    private String id;
    private int votes;

    public VoteData(String id, int votes) {
        this.id = id;
        this.votes = votes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    @Override
    public String toString() {
        return String.format("%s\t%d", id, votes);
    }
}
