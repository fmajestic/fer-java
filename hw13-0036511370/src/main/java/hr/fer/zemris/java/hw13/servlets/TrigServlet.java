package hr.fer.zemris.java.hw13.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;

/**
 * Calculates trigonometry data for an angle range.
 */
@WebServlet(name = "trig", urlPatterns = { "/trigonometric" })
public class TrigServlet extends HttpServlet {

    /**
     * Calculates sine and cosine of angle range (in degrees)
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String strA = req.getParameter("a");
        String strB = req.getParameter("b");

        int a = 0;
        int b = 360;

        try {
            if (strA != null) {
                a = Integer.valueOf(strA);
            }

            if (strB != null) {
                b = Integer.valueOf(strB);
            }
        } catch (NumberFormatException ignored) {
        }

        if (a > b) {
            int tmp = a;
            a = b;
            b = tmp;
        }

        if (b > a + 720) {
            b = a + 720;
        }

        List<TrigData> data = new ArrayList<>();

        for (int i = a; i <= b; i++) {
            double rad = toRadians(i);
            data.add(new TrigData(i, sin(rad), cos(rad)));
        }

        req.setAttribute("ang_start", Integer.toString(a));
        req.setAttribute("ang_end", Integer.toString(b));
        req.setAttribute("trig", data);
        req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
    }
}
