package hr.fer.zemris.java.hw13.servlets;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Creates a survey report pie chart.
 */
@WebServlet(name = "report", urlPatterns = { "/reportImage" })
public class ReportImageServlet extends HttpServlet {

    /**
     * Generates a survey report pie chart and writes it directly to the response stream.
     * The proper Content-Type is set (image/png).
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws IOException if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        DefaultPieDataset dataset = new DefaultPieDataset();

        dataset.setValue("Windows", 59);
        dataset.setValue("MacOS", 21);
        dataset.setValue("Linux", 20);

        JFreeChart chart = ChartFactory.createPieChart3D(
            "Desktop OS usage",
            dataset,
            true,
            true,
            false
        );

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);

        resp.setContentType("image/png");

        ChartUtils.writeChartAsPNG(
            resp.getOutputStream(),
            chart,
            500,
            350
        );
    }
}

