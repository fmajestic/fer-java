package hr.fer.zemris.java.hw13.servlets.glasanje;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Displays the voting page.
 */
@WebServlet(name = "glasanje", urlPatterns = "/glasanje")
public class GlasanjeServlet extends HttpServlet {

    /**
     * Loads the band info and forwards request to glasanjeIndex.jsp
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String file = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");

        Map<String, BandInfo> bands = Util.getBands(file);

        req.setAttribute("bands", bands);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
    }
}
