package hr.fer.zemris.java.hw13.servlets.glasanje;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Stores the vote.
 */
@WebServlet(name = "glasaj", urlPatterns = "/glasanje-glasaj")
public class GlasanjeGlasajServlet extends HttpServlet {

    /**
     * Stores the vote for a given id into the results file.
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws IOException if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
        Path path = Paths.get(fileName);
        String id = req.getParameter("id");

        if (!Files.exists(path)) {
            Files.createFile(path);
        }

        boolean found = false;
        List<VoteData> votes = Util.getVotes(fileName);

        for (VoteData data : votes) {
            if (data.getId().equals(id)) {
                data.setVotes(data.getVotes() + 1);
                found = true;
                break;
            }
        }

        if (!found) {
            votes.add(new VoteData(id, 1));
        }

        Files.write(path, votes.stream().map(VoteData::toString).collect(Collectors.toList()));
        resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
    }
}
