$(document).ready(() => {

  $.getJSON('rest/tags', data => displayTags(data.tags));

  // Necessary because buttons and thumbnails are dynamically generated
  let body = $('body');
  body.on('click', '.btn-tag', selectTag);
  body.on('click', '.thumbnail', selectImage);

  $('#btn-clear').on('click', clearSelection);
});


function displayTags(tags) {
  let tagList = $('#tags');

  if (tags.length === 0) {
    tagList.prepend(document.createTextNode('No tags'));
    return;
  }

  for (let i = 0; i < tags.length; i++) {
    let btn = $(document.createElement('button'));
    let tag = htmlEscape(tags[i]);

    btn.text(tag);
    btn.attr('data-tag', tag);
    btn.addClass('btn-tag');

    tagList.append(btn);
  }
}


let lastTag;

function selectTag() {

  if (lastTag !== this)
    lastTag = this;
  else
    return;

  $(this).removeClass('unfocused');
  $(this).siblings().addClass('unfocused');

  let thumbList = $('#thumbs');

  $('#image').empty();
  thumbList.empty();

  $.getJSON(
    `rest/thumbs/${$(this).attr('data-tag')}`,
    data => {
      let thumbs = data.thumbs;

      if (thumbs.length === 0) {
        thumbList.append(document.createTextNode('No images.'));
        return;
      }

      for (let i = 0; i < thumbs.length; i++) {
        let thumb = thumbs[i];
        let elem = $(document.createElement('div'));
        let img = $(document.createElement('img'));

        img.attr('src', thumb.url);
        img.attr('alt', thumb.name);
        elem.attr('data-img-info', thumb.info);
        elem.addClass('thumbnail');

        elem.append(img);
        thumbList.append(elem);
      }
    }
  );
}

function selectImage() {
  let infoUrl = $(this).attr('data-img-info');

  $.getJSON(infoUrl, async image => {

    let display = $('#image');

    let img = $(document.createElement('img'));
    img.attr('src', image.url);
    img.attr('alt', image.description);

    let name = makeParagraph(`Name: ${image.name}`);
    let tags = makeParagraph(`Tags: ${image.tags.join(', ')}`);
    let description = makeParagraph(`Description: ${image.description}`);

    display.empty();
    display.append('<hr>', img, name, description, tags);

    // Wait for the image to load...
    await new Promise(r => setTimeout(r, 500));

    // Scroll to image
    $('html, body').animate({
      scrollTop: display.offset().top
    }, 300);
  });
}

function makeParagraph(text) {
  let paragraph = $(document.createElement('p'));
  paragraph.text(htmlEscape(text));
  return paragraph;
}

function clearSelection() {
  $(this).siblings().removeClass('unfocused');
  $('#thumbs').empty();
  $('#image').empty();
}
