package hr.fer.zemris.java.hw16.rest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import hr.fer.zemris.java.hw16.data.ImageInfo;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

/**
 * Writes image info as JSON.
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ImageInfoWriter implements MessageBodyWriter<ImageInfo> {

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == ImageInfo.class;
    }

    @Override
    public void writeTo(
        ImageInfo image,
        Class<?> type,
        Type genericType,
        Annotation[] annotations,
        MediaType mediaType,
        MultivaluedMap<String, Object> httpHeaders,
        OutputStream entityStream
    ) throws IOException, WebApplicationException {

        entityStream.write(toData(image));
    }

    /**
     * Gets JSON message as bytes.
     *
     * @param image the info to convert to JSON
     *
     * @return the bytes
     */
    private byte[] toData(ImageInfo image) {

        if (image == null) {
            return new byte[0];
        }

        JsonObject data = new JsonObject();
        JsonArray tags = new JsonArray();

        image.getTags().forEach(tags::add);

        data.addProperty("name", image.getName());
        data.addProperty("description", image.getDescription());
        data.addProperty("url", String.format("rest/images/%s", image.getName()));
        data.add("tags", tags);

        return data.toString().getBytes(StandardCharsets.UTF_8);
    }
}
