package hr.fer.zemris.java.hw16.data;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * A simple image info data store.
 */
@WebListener
public class ImageDB implements ServletContextListener {

    /** All available images. */
    private static List<ImageInfo> images = new ArrayList<>();

    /** All available tags. Pre-generated. */
    private static Set<String> tags = new TreeSet<>();

    /**
     * Gets all images.
     *
     * @return the images
     */
    public static List<ImageInfo> getImages() {
        return images;
    }

    /**
     * Gets all tags, sorted alphabetically.
     *
     * @return the tags
     */
    public static Set<String> getAllTags() {
        return tags;
    }

    /**
     * Reads the descriptor file and populates the DB.
     *
     * @param sce the initialized context
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Path dbFile = Paths.get(sce.getServletContext().getRealPath("/WEB-INF/opisnik.txt"));
        List<String> lines = new ArrayList<>();

        try {
            lines = Files.readAllLines(dbFile, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < lines.size(); i += 3) {
            String name = lines.get(i);
            String desc = lines.get(i + 1);
            String[] tags =
                Arrays.stream(lines.get(i + 2).split(","))
                      .map(String::trim)
                      .toArray(String[]::new);

            images.add(new ImageInfo(name, desc, tags));
        }

        images.stream()
              .map(ImageInfo::getTags)
              .forEach(tags::addAll);
    }

    /**
     * Does nothing.
     *
     * @param sce the destroyed context
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
