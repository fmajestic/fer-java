package hr.fer.zemris.java.hw16.data;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * Stores relevant image information.
 */
public class ImageInfo {

    /** Image filename. */
    private String name;

    /** Image description. */
    private String description;

    /** Image tags. */
    private Set<String> tags;

    /**
     * Creates a new image info
     *
     * @param name        the name
     * @param description the description
     * @param tags        the tags
     */
    public ImageInfo(String name, String description, String... tags) {
        this.name = name;
        this.description = description;
        this.tags = new TreeSet<>();

        this.tags.addAll(Arrays.asList(tags));
    }

    /**
     * Gets the filename.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the tags, sorted alphabetically.
     *
     * @return the tags
     */
    public Set<String> getTags() {
        return tags;
    }
}
