package hr.fer.zemris.java.hw16.rest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import hr.fer.zemris.java.hw16.data.ImageDB;
import hr.fer.zemris.java.hw16.data.ImageInfo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

/**
 * General path for getting thumbnails
 */
@Path("/thumbs")
public class ThumbJSON {

    /**
     * Gets thumbnail info for all images with tag.
     * @param tag the tag
     * @return the response
     */
    @GET
    @Path("/{tag}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getThumbs(@PathParam("tag") String tag) {

        if (!ImageDB.getAllTags().contains(tag)) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        List<ImageInfo> images = ImageDB
            .getImages()
            .stream()
            .filter(img -> img.getTags().contains(tag))
            .collect(Collectors.toList());

        return makeThumbs(images);
    }

    /**
     * Gets thumbnail info for all available tags
     * @return the response
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {

        List<ImageInfo> images = ImageDB.getImages();

        return makeThumbs(images);
    }

    /**
     * Makes a JSON array of thumbnail info.
     *
     * @param images the images to make thumbnail info for
     *
     * @return the array
     */
    private Response makeThumbs(List<ImageInfo> images) {
        JsonObject result = new JsonObject();
        JsonArray thumbs = new JsonArray();

        for (ImageInfo img : images) {
            JsonObject thumb = new JsonObject();
            thumb.addProperty("name", img.getName());
            thumb.addProperty("url", String.format("thumb?name=%s", img.getName()));
            thumb.addProperty("info", String.format("rest/images/%s/info", img.getName()));

            thumbs.add(thumb);
        }

        result.add("thumbs", thumbs);

        return Response.ok(result.toString()).build();
    }

}
