package hr.fer.zemris.java.hw16.rest;

import hr.fer.zemris.java.hw16.data.ImageDB;
import hr.fer.zemris.java.hw16.data.ImageInfo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Optional;

@Path("/images")
public class ImageJSON {


    /**
     * Gets info for image.
     *
     * @param name the image filename
     *
     * @return the info
     */
    @GET
    @Path("/{name}/info")
    @Produces(MediaType.APPLICATION_JSON)
    public ImageInfo getImageInfo(@PathParam("name") String name) {

        Optional<ImageInfo> img = ImageDB
            .getImages()
            .stream()
            .filter(i -> i.getName().equals(name))
            .findFirst();

        return img.orElse(null);
    }


    /**
     * Reiirects to actual image.
     *
     * @param name the image filename
     *
     * @return the response
     */
    @GET
    @Path("/{name}")
    public Response getImage(@PathParam("name") String name) {

        URI uri = UriBuilder.fromUri("img?name={name}").build(name);
        return Response.temporaryRedirect(uri).build();
    }
}
