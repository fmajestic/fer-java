package hr.fer.zemris.java.hw16.rest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import hr.fer.zemris.java.hw16.data.ImageDB;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * A list of all tags
 */
@Path("/tags")
public class TagsJSON {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTagList() {

        JsonObject result = new JsonObject();
        JsonArray tags = new JsonArray();

        for (String tag : ImageDB.getAllTags()) {
            tags.add(tag);
        }

        result.add("tags", tags);
        return Response.status(Status.OK).entity(result.toString()).build();
    }
}
