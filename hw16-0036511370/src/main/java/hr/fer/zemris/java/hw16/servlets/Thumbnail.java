package hr.fer.zemris.java.hw16.servlets;

import javax.imageio.ImageIO;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * Generates an image thumbnail if it doesn't exist, and returns it.
 */
@WebServlet(name = "thumbnail", urlPatterns = "/thumb")
public class Thumbnail extends HttpServlet {

    /** Thumbnail width. */
    private static final int WIDTH = 150;

    /** Thumbnail height. */
    private static final int HEIGHT = 150;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Path imgDir = Paths.get(req.getServletContext().getRealPath("/WEB-INF/slike"));
        Path thumbDir = Paths.get(req.getServletContext().getRealPath("/WEB-INF/thumbs"));

        if (!Files.exists(thumbDir)) {
            Files.createDirectory(thumbDir);
        }

        String name = req.getParameter("name");
        Path img = imgDir.resolve(name);
        Path thumb = thumbDir.resolve(getThumbName(name));

        if (!Files.exists(img)) {
            System.out.println("File doesn't exist (thumb): " + img);
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Image not found");
            return;
        }

        if (!Files.exists(thumb)) {
            try (InputStream is = new BufferedInputStream(Files.newInputStream(img));
                 OutputStream os = new BufferedOutputStream(Files.newOutputStream(thumb))
            ) {
                BufferedImage original = ImageIO.read(is);
                BufferedImage small = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_4BYTE_ABGR);

                Graphics2D g2d = small.createGraphics();
                g2d.drawImage(original, 0, 0, WIDTH, HEIGHT, null);
                g2d.dispose();

                // Better quality resizing
                g2d.setComposite(AlphaComposite.Src);
                g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

                ImageIO.write(small, "png", os);
            }
        }

        try (InputStream is = new BufferedInputStream(Files.newInputStream(thumb))) {
            resp.setContentType("image/png");
            BufferedImage thumbImage = ImageIO.read(is);
            ImageIO.write(thumbImage, "png", resp.getOutputStream());
        }
    }

    private String getThumbName(String name) {
        return String.format("thumb-%s.png", name);
    }
}
