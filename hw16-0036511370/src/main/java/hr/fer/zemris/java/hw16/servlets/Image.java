package hr.fer.zemris.java.hw16.servlets;

import javax.imageio.ImageIO;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Returns the requested image.
 */
@WebServlet(name = "image", urlPatterns = "/rest/img")
public class Image extends HttpServlet {

    /**
     * Known image types.
     */
    private static final Set<String> formats = new HashSet<>(List.of("jpg", "jpeg", "png", "gif"));

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Path imgDir = Paths.get(req.getServletContext().getRealPath("/WEB-INF/slike"));
        String name = req.getParameter("name");
        Path img = imgDir.resolve(name);

        int index = name.lastIndexOf('.');
        String ext = index != -1 ? name.substring(index + 1) : "";


        if (!Files.exists(img)) {
            System.out.println("File doesn't exist (img): " + img);
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Image not found: " + name + ", " + img);
            return;
        }

        String type = formats.contains(ext) ? String.format("image/%s", ext) : "image/*";

        resp.setContentType(type);

        try (InputStream is = new BufferedInputStream(Files.newInputStream(img))) {
            ImageIO.write(
                ImageIO.read(is),
                formats.contains(ext) ? ext : "png",
                resp.getOutputStream()
            );
        }
    }
}
