package hr.fer.zemris.java.hw06.crypto;

import org.junit.jupiter.api.Test;

import static hr.fer.zemris.java.hw06.crypto.Util.bytetohex;
import static hr.fer.zemris.java.hw06.crypto.Util.hextobyte;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilTest {

    private String hexString = "01aE22";
    private byte[] bytes = new byte[] { 1, -82, 34};

    @Test
    void testHextobyte() {
        assertArrayEquals(bytes, hextobyte(hexString));
    }

    @Test
    void testBytetohex() {
        assertEquals(hexString.toLowerCase(), bytetohex(bytes));
    }
}