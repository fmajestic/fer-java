package hr.fer.zemris.java.hw06.shell.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommandUtilTest {

    @Test
    void getArguments() {
        String[] expected = new String[] {
            "a.txt", "C:\\Program Files\\Eclipse", "..\\review.txt"
        };

        String[] actual = CommandUtil.getArguments(
            "a.txt \"C:\\Program Files\\Eclipse\" \"..\\review.txt\"",
            true
        ).toArray(new String[3]);

        assertArrayEquals(expected, actual);
    }
}