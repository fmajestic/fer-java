package hr.fer.zemris.java.hw06.crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Scanner;

import static hr.fer.zemris.java.hw06.crypto.Util.bytetohex;
import static hr.fer.zemris.java.hw06.crypto.Util.hextobyte;

/**
 * Provides simple digest checking and file encryption/decryption.
 * <p>
 * Valid commands are: checksha, encrypt, decrypt.
 */
public class Crypto {

    /**
     * Entry point of the program.
     *
     * @param args command, file name(s)
     */
    public static void main(String[] args) {

        if (args.length == 0
            || args[0].equals("checksha") && args.length != 2
            || args[0].matches("encrypt|decrypt") && args.length != 3) {

            System.out.println("Invalid command. Usage:");
            System.out.println("\tchecksha [file]");
            System.out.println("\tencrypt  [original file]  [encrypted file]");
            System.out.println("\tdecrypt  [encrypted file] [decrypted file]");
            System.exit(-1);
        }

        String command = args[0];
        Path inputFile = Paths.get(args[1]);
        Scanner sc = new Scanner(System.in);

        if (command.equals("checksha")) {
            checkShaDigest(inputFile, sc);
        } else {
            Path outputFile = Paths.get(args[2]);
            cipherFile(command, inputFile, outputFile, sc);
        }
    }

    /** Performs a cipher on a file. */
    private static void cipherFile(String command, Path inputFile, Path outputFile, Scanner sc) {

        String keyText =
            readInput("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):", sc);
        String ivText =
            readInput("Please provide initialization vector as hex-encoded text (32 hex-digits):", sc);


        boolean encrypt = command.equals("encrypt");

        SecretKeySpec keySpec = new SecretKeySpec(hextobyte(keyText), "AES");
        AlgorithmParameterSpec paramSpec = new IvParameterSpec(hextobyte(ivText));

        try (InputStream is = new BufferedInputStream(Files.newInputStream(inputFile));
             OutputStream os = new BufferedOutputStream(Files.newOutputStream(outputFile))) {
            doFullCipher(keySpec, paramSpec, encrypt, is, os);
        } catch (IOException ex) {
            System.out.println("IO error: " + ex.getMessage());
            return;
        }

        System.out.println("Decryption completed. Generated file " + outputFile.toString() + " based on file " + inputFile.toString());
    }

    /**
     * Reads an expected digest from the users and compares it to the actual digest.
     *
     * @param inputFile the file to calculate the digest for
     * @param sc        the scanner to read user input from
     */
    private static void checkShaDigest(Path inputFile, Scanner sc) {
        byte[] inputBytes;
        MessageDigest shaDigest;

        try (var is = new BufferedInputStream(Files.newInputStream(inputFile))) {
            inputBytes = hextobyte(readInput(
                String.format("Please provide expected sha-256 digest for %s:", inputFile.toString()), sc
            ));

            shaDigest = MessageDigest.getInstance("SHA-256");

            int nread;
            byte[] buf = new byte[1024];

            while ((nread = is.read(buf)) > 0) {
                shaDigest.update(buf, 0, nread);
            }

        } catch (IllegalArgumentException ex) {
            System.out.println("Invalid input digest: " + ex.getMessage());
            return;
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Error getting SHA-256 algorithm instance: " + ex.getMessage());
            return;
        } catch (IOException ex) {
            System.out.println("Error reading input file '" + inputFile.toString() + "': " + ex.getMessage());
            return;
        }

        byte[] actualBytes = shaDigest.digest();

        System.out.print("Digesting completed. ");

        if (Arrays.equals(inputBytes, actualBytes)) {
            System.out.printf("Digest of %s matches expected digest.%n", inputFile.toString());
        } else {
            System.out.printf("Digest of %s does not match expected digest.%n", inputFile.toString());
            System.out.println("Digest was: " + bytetohex(actualBytes));
        }
    }

    /**
     * Prompts the user each time until non-empty input is entered.
     *
     * @param prompt the prompt to display
     * @param sc     the scanner to read input from
     *
     * @return the non-empty user input
     */
    private static String readInput(String prompt, Scanner sc) {
        System.out.println(prompt);

        String ret;

        do {
            System.out.print("> ");
        } while ((ret = sc.nextLine()).isEmpty());

        return ret;
    }

    /**
     * Performs a full cipher on bytes from the input stream and writes them to the output stream.
     *
     * @param keySpec   the key spec to use
     * @param paramSpec the parameter spec to use
     * @param encrypt   true for encryption, false for decryption
     * @param is        the stream of input bytes
     * @param os        the stream to output bytes to
     */
    private static void doFullCipher(SecretKeySpec keySpec, AlgorithmParameterSpec paramSpec, boolean encrypt,
                                     InputStream is, OutputStream os) {

        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);

            int nread;
            byte[] buf = new byte[1024];

            while ((nread = is.read(buf)) > 0) {
                os.write(cipher.update(buf, 0, nread));
            }

            os.write(cipher.doFinal());

        } catch (NoSuchPaddingException | NoSuchAlgorithmException ex) {
            System.out.println("Error getting AES/CBC/PKCS5Padding cipher: " + ex.getMessage());
        } catch (InvalidAlgorithmParameterException | InvalidKeyException ex) {
            System.out.println("Error during cipher initializing cipher: " + ex.getMessage());
        } catch (IllegalBlockSizeException | BadPaddingException ex) {
            System.out.println("Cipher error: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("IO error: " + ex.getMessage());
        }
    }
}
