package hr.fer.zemris.java.hw06.shell;

import hr.fer.zemris.java.hw06.shell.commands.*;
import hr.fer.zemris.java.hw06.shell.util.Console;

import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;

/** The implementation of {@link Environment} used by {@link MyShell}. */
public class ShellEnvironment implements Environment {

    private SortedMap<String, ShellCommand> commands = new TreeMap<>();

    /** The prompt character. */
    private char prompt;

    /** The more-lines character */

    private char morelines;

    /** The line continuation character. */
    private char multiline;

    /** The current console we are reading from / writing to. */
    private Console console;

    /**
     * Creates a new shell environment with the given console for IO.
     *
     * @param console the console to use for keyboard IO
     */
    public ShellEnvironment(Console console) {
        this();
        this.console = console;
    }

    public ShellEnvironment() {
        prompt = '>';
        multiline = '|';
        morelines = '\\';

        commands.put(LsCommand.NAME, new LsCommand());
        commands.put(CatCommand.NAME, new CatCommand());
        commands.put(CopyCommand.NAME, new CopyCommand());
        commands.put(ExitCommand.NAME, new ExitCommand());
        commands.put(HelpCommand.NAME, new HelpCommand());
        commands.put(TreeCommand.NAME, new TreeCommand());
        commands.put(MkdirCommand.NAME, new MkdirCommand());
        commands.put(SymbolCommand.NAME, new SymbolCommand());
        commands.put(HexdumpCommand.NAME, new HexdumpCommand());
        commands.put(CharsetsCommand.NAME, new CharsetsCommand());
    }

    /** {@inheritDoc} */
    @Override
    public String readLine() throws ShellIOException {
        return console.nextLine();
    }

    /** {@inheritDoc} */
    @Override
    public void write(String text) throws ShellIOException {
        console.print(text);
    }

    /** {@inheritDoc} */
    @Override
    public void writeln(String text) throws ShellIOException {
        console.println(text);
    }

    /** {@inheritDoc} */
    @Override
    public SortedMap<String, ShellCommand> commands() {
        return Collections.unmodifiableSortedMap(commands);
    }

    /** {@inheritDoc} */
    @Override
    public Character getMultilineSymbol() {
        return multiline;
    }

    /** {@inheritDoc} */
    @Override
    public void setMultilineSymbol(Character symbol) {
        this.multiline = symbol;
    }

    /** {@inheritDoc} */
    @Override
    public Character getPromptSymbol() {
        return prompt;
    }

    /** {@inheritDoc} */
    @Override
    public void setPromptSymbol(Character symbol) {
        this.prompt = symbol;
    }

    /** {@inheritDoc} */
    @Override
    public Character getMorelinesSymbol() {
        return morelines;
    }

    /** {@inheritDoc} */
    @Override
    public void setMorelinesSymbol(Character symbol) {
        this.morelines = symbol;
    }
}
