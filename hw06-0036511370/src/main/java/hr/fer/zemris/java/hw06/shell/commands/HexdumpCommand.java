package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * hexdump
 * <p>
 * Prints a hexdump of a file.
 */
public class HexdumpCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "hexdump";

    /** The description of this command, a list of lines. */
    private static List<String> description = new ArrayList<>();

    static {
        description.add("Usage: hexdump FILE");
        description.add("Prints the hexdump of a file, 16 bytes per line.");
        description.add("Bytes that are valid 7-bit ASCII characters are printed,");
        description.add("all others are replaced with a dot - '.'");
    }

    /**
     * Prints the hexdump of a file, 16 bytes per line.
     * <p>
     * The entire file is read into memory, then printed line by line.
     *
     * @param env       the environment to execute in
     * @param arguments a single path to a file
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args;
        try {
            args = CommandUtil.getArguments(arguments, true);
        } catch (IllegalArgumentException ex) {
            env.writeln("Invalid argument(s): " + ex.getMessage());
            return ShellStatus.CONTINUE;
        }

        if (args.size() != 1) {
            env.writeln("Invalid number of arguments. Expected: 1, actual: " + args.size());
            return ShellStatus.CONTINUE;
        }

        Path file;

        try {
            file = Paths.get(args.get(0));
        } catch (InvalidPathException ex) {
            env.writeln("Invalid path: " + args.get(0));
            return ShellStatus.CONTINUE;
        }

        if (Files.notExists(file)) {
            env.writeln(String.format("File '%s' not found.", file.toString()));
            return ShellStatus.CONTINUE;
        }


        byte[] bytes;

        try {
            bytes = Files.readAllBytes(file);
        } catch (IOException ex) {
            env.writeln("Error reading file " + ex.getMessage());
            return ShellStatus.CONTINUE;
        }

        for (int i = 0; i < bytes.length + 15; i += 16) {
            env.writeln(String.format(
                "%08x: %s|%s | %s",
                i,
                bytesToHex(bytes, i, 8),
                bytesToHex(bytes, i + 8, 8),
                bytesToAscii(bytes, i, 16)
            ));
        }


        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(description);
    }

    /**
     * Formats a segment of an array, padding with whitespace if there are not enough elements in the array.
     *
     * @param bytes the array to read values from
     * @param index the starting index
     * @param count the number of bytes to read
     *
     * @return the bytes from the array as hex strings, with spaces between
     */
    private String bytesToHex(byte[] bytes, int index, int count) {

        String[] strings = new String[count];

        int i;

        // Add as much from the array as possible
        for (i = 0; i < count && index + i < bytes.length; i++) {
            strings[i] = String.format("%02X", bytes[index + i]);
        }

        // If it didn't reach the end, that means we ran out of numbers and should pad with whitespace
        for (; i < count; i++) {
            strings[i] = "  ";
        }

        return String.join(" ", strings);
    }

    /**
     * Converts a segment of an array to a string, converting any printable byte to a corresponding character,
     * otherwise replacing it with a . (dot).
     *
     * @param bytes the array to read values from
     * @param index the starting index
     * @param count the number of bytes to read
     *
     * @return the segment as a string
     */
    @SuppressWarnings("SameParameterValue")
    private String bytesToAscii(byte[] bytes, int index, int count) {

        var sb = new StringBuilder();

        // Add as much from the array as possible.
        // It is ok if we don't read all 'count' bytes, since this will be the end of the output
        for (int i = 0; i < count && index + i < bytes.length; i++) {

            if (bytes[index + i] >= 32) {
                sb.append((char) bytes[index + i]);
            } else {
                sb.append('.');
            }
        }

        return sb.toString();
    }
}