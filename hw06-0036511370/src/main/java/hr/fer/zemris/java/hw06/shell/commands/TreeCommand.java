package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * tree
 * <p>
 * Prints the folder structure of a given directory as a tree.
 */
public class TreeCommand implements ShellCommand {

    /** Name of the command. */
    public static String NAME = "tree";

    /** The description of this command, a list of lines. */
    private static List<String> description = new ArrayList<>();

    static {
        description.add("Usage: tree [DIRECTORY] [-f]");
        description.add("\t-f\tFancy: use extended ASCII to print an actual tree.");
        description.add("Graphically displays the folder structure of a drive or path.");
        description.add("If the -f flag is passed, the output is formatted using extended ASCII characters");
        description.add("If no path is given, the current directory is used as a default.");
    }

    /**
     * Prints the structure of a given directory and subdirectories as a tree.
     *
     * @param env       the environment to execute in
     * @param arguments empty, or a single path to a directory
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> args = CommandUtil.getArguments(arguments, true);

        int size = args.size();

        if (size > 2) {
            env.writeln("Invalid number of arguments. Expected: 1 or 2, actual: " + size);
            return ShellStatus.CONTINUE;
        }

        Path src;

        try {
            if (size == 0 || (size == 1 && args.get(0).equals("-f"))) {
                src = Paths.get(".");
            } else {
                src = Paths.get(args.get(0));
            }
        } catch (InvalidPathException ex) {
            env.writeln(String.format("Invalid path: %s", ex.getMessage()));
            return ShellStatus.CONTINUE;
        }

        if (Files.isRegularFile(src)) {
            env.writeln(String.format("'%s' is a file.", src.toString()));
            return ShellStatus.CONTINUE;
        }

        if (Files.notExists(src)) {
            env.writeln(String.format("Directory '%s' does not exist.", src.toString()));
            return ShellStatus.CONTINUE;
        }

        if (args.contains("-f")) {
            env.writeln(fancyTreeStringRecursive(src, true));
        } else {
            try {
                Files.walkFileTree(src, new TreePrinter(env));
            } catch (IOException ex) {
                env.writeln("Error reading file: " + ex.getMessage());
            }
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(description);
    }

    /**
     * Recursively builds the tree as a single string.
     * <p>
     * Files and subdirectories are sorted alphabetically.
     * All children indented with 8-bit ASCII characters.
     *
     * @param path   the path to start at
     * @param isRoot is {@code path} the root of the tree or not
     *
     * @return the entire tree as a printable string.
     */
    private String fancyTreeStringRecursive(Path path, boolean isRoot) {
        String name =
            isRoot ? path.toString()
                   : path.getName(path.getNameCount() - 1).toString();

        try {
            if (Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS)) {
                return name;
            }
        } catch (SecurityException ex) {
            return String.format("ERROR: access denied to: '%s'", name);
        }

        StringBuilder sb = new StringBuilder(name + "\n");

        try (DirectoryStream<Path> files = Files.newDirectoryStream(path)) {

            // Using custom iterator because the last line must be formatted differently
            Iterator<Path> iter = files.iterator();

            while (iter.hasNext()) {

                Path child = iter.next();

                // Different format for last line
                if (!iter.hasNext()) {
                    sb.append("└───");
                    // Replace adds padding for each level of recursion
                    sb.append(fancyTreeStringRecursive(child, false).replace("\n", "\n    "));
                    continue;
                }

                sb.append("├───");
                // Replace adds padding for each level of recursion
                sb.append(fancyTreeStringRecursive(child, false).replace("\n", "\n│   "));
                sb.append('\n');
            }
        } catch (IOException ex) {
            return String.format("ERROR while reading '%s': %s", name, ex.toString());
        }

        return sb.toString();
    }

    /** Prints each visited file and folder as a tree. */
    private class TreePrinter extends SimpleFileVisitor<Path> {
        private int level = 0;
        private int scaling = 2;
        private Environment env;

        /**
         * Creates a new tree printer with an environment.
         *
         * @param env the environment to print in
         */
        TreePrinter(Environment env) {
            this.env = env;
        }

        /** {@inheritDoc} */
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            writeIndented(dir.getFileName().toString());
            level++;
            return FileVisitResult.CONTINUE;
        }

        /** {@inheritDoc} */
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            writeIndented(file.getFileName().toString());
            return FileVisitResult.CONTINUE;
        }

        /** {@inheritDoc} */
        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) {
            return FileVisitResult.CONTINUE;
        }

        /** {@inheritDoc} */
        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
            level--;
            return FileVisitResult.CONTINUE;
        }

        /**
         * Indents the text and prints it using {@link Environment#writeln(String)}.
         * The text is indented by {@code scaling * level} spaces.
         *
         * @param text the text to print
         */
        private void writeIndented(String text) {
            env.writeln(" ".repeat(scaling * level) + text);
        }
    }
}
