package hr.fer.zemris.java.hw06.shell.util;

/**
 * Represents a console for user IO.
 */
public interface Console {

    /**
     * The default console, varies on availability of {@code System.console()}.
     *
     * @see System#console()
     */
    Console DEFAULT = System.console() == null
                      ? new StreamConsole(System.in, System.out)
                      : new SystemConsoleWrapper(System.console());

    /**
     * Prints text to the console's output stream.
     *
     * @param text the text to print
     */
    void print(String text);

    /**
     * Prints text to the console's output stream, followed by a newline.
     *
     * @param text the text to print
     */
    void println(String text);

    /**
     * Reads the next line from the user.
     * <p>
     * The terminating newline symbol is not included.
     *
     * @return the line
     */
    String nextLine();

}
