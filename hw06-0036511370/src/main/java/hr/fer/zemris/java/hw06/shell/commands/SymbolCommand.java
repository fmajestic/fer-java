package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;
import hr.fer.zemris.java.hw06.shell.util.SymbolGetters;
import hr.fer.zemris.java.hw06.shell.util.SymbolGetters.ISymbolGetter;
import hr.fer.zemris.java.hw06.shell.util.SymbolSetters;
import hr.fer.zemris.java.hw06.shell.util.SymbolSetters.ISymbolSetter;

import java.util.*;

/**
 * Used to display or change the shell symbols.
 * <p>
 * The valid symbols are PROMPT, MULTILINE, MORELINES.
 */
public class SymbolCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "symbol";

    /** The description of this command, a list of lines. */
    private static List<String> description = new ArrayList<>();
    private static Map<String, ISymbolGetter> symbolGetters = new HashMap<>();
    private static Map<String, ISymbolSetter> symbolSetters = new HashMap<>();

    static {
        description.add("Usage: symbol SYMBOL [CHARACTER]");
        description.add("Displays or changes the SYMBOL. Valid symbols are: PROMPT, MULTILINE, MORELINES.");
        description.add("When called with only one argument, prints the currently used symbol.");
        description.add("When called with two arguments, the specified symbol is updated to be the given character.");

        symbolGetters.put("PROMPT", SymbolGetters.PROMPT);
        symbolGetters.put("MORELINES", SymbolGetters.MORELINES);
        symbolGetters.put("MULTILINE", SymbolGetters.MULTILINE);

        symbolSetters.put("PROMPT", SymbolSetters.PROMPT);
        symbolSetters.put("MORELINES", SymbolSetters.MORELINES);
        symbolSetters.put("MULTILINE", SymbolSetters.MULTILINE);
    }

    /**
     * Displays or changes an environment symbol.
     *
     * Valid symbols are PROMPT, MULTILINE, MORELINES.
     *
     * @param env       the environment to execute in
     * @param arguments a valid symbol name, optionally followed by a new character
     *
     * @return {@link ShellStatus#CONTINUE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        List<String> args = CommandUtil.getArguments(arguments);

        if (args.size() == 1) {
            displaySymbol(args.get(0), env);
        } else if (args.size() == 2) {
            updateSymbol(args.get(0), args.get(1), env);
        } else {
            env.writeln("Invalid number of arguments. Expected: 1 or 2, actual: " + args.size());
        }

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(description);
    }

    private void displaySymbol(String name, Environment env) {

        ISymbolGetter getter = symbolGetters.get(name);

        if (getter != null) {
            env.writeln(String.format("Symbol for %s is '%c'", name, getter.get(env)));
        } else {
            env.writeln(String.format("Unknown symbol: %s", name));
        }
    }

    /**
     * For the given environment, updates a symbol.
     *
     * @param name      the name of the symbol
     * @param newSymbol the new symbol
     * @param env       the environment to update
     */
    private void updateSymbol(String name, String newSymbol, Environment env) {

        if (newSymbol.length() != 1) {
            env.writeln(String.format("New symbol must be a single character, got: %s", newSymbol));
            return;
        }

        ISymbolSetter setter = symbolSetters.get(name);

        if (setter != null) {
            char oldChar = symbolGetters.get(name).get(env);
            char newChar = newSymbol.charAt(0);

            env.writeln(String.format("Symbol for %s changed from '%c' to '%c'", name, oldChar, newChar));
            setter.set(newChar, env);
        } else {
            env.writeln(String.format("Unknown symbol: %s", name));
        }
    }
}
