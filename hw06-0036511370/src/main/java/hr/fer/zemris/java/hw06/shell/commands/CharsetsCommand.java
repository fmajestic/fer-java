package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.util.CommandUtil;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * charsets
 * <p>
 * Displays all available charsets.
 */
public class CharsetsCommand implements ShellCommand {

    /** The name of the command, as it should be entered in the shell. */
    public static final String NAME = "charsets";

    /** The description of this command, a list of lines. */
    private static List<String> description = new ArrayList<>();

    static {
        description.add("Displays all available charsets.");
    }

    /**
     * Prints all charsets available on the system.
     *
     * @param env       the environment to execute in
     * @param arguments must be empty
     *
     * @return the shell status after execution (should the shell continue or not)
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        if (CommandUtil.getArguments(arguments).size() != 0) {
            env.writeln("Invalid number of arguments. Expected: 0.");
            return ShellStatus.CONTINUE;
        }

        Charset.availableCharsets().keySet().forEach(env::writeln);

        return ShellStatus.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(description);
    }
}
