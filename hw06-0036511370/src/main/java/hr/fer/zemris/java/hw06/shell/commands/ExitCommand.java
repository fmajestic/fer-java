package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static hr.fer.zemris.java.hw06.shell.ShellStatus.TERMINATE;

/**
 * exit
 * <p>
 * Exits the shell.
 */
public class ExitCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "exit";

    /** The description of the command, a list of lines. */
    private static final List<String> description = new ArrayList<>();

    static {
        description.add("Exits the shell. Any passed arguments are ignored.");
    }

    /**
     * Sends a {@code TERMINATE} signal to the shell.
     *
     * @param env       the environment to execute in
     * @param arguments ignored
     *
     * @return {@link ShellStatus#TERMINATE}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        return TERMINATE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(description);
    }
}
