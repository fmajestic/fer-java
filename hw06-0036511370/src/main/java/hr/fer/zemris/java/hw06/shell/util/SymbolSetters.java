package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.Environment;

/**
 * Shell symbol setters.
 * <p>
 * Allows for easy addition of shell symbols without having to modify existing code in the SET function.
 */
public class SymbolSetters {

    /** The prompt symbol setter. */
    public static final ISymbolSetter PROMPT = (c, env) -> env.setPromptSymbol(c);

    /** The morelines symbol setter. */
    public static final ISymbolSetter MORELINES = (c, env) -> env.setMorelinesSymbol(c);

    /** The multiline symbol setter. */
    public static final ISymbolSetter MULTILINE = (c, env) -> env.setMultilineSymbol(c);

    /** The setter interface. */
    public interface ISymbolSetter {

        /**
         * Sets a symbol in an environment.
         *
         * @param symbol      the new symbol
         * @param environment the environment
         */
        void set(char symbol, Environment environment);
    }
}
