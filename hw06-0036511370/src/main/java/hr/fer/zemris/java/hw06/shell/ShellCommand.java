package hr.fer.zemris.java.hw06.shell;

import java.util.Collections;
import java.util.List;

/** Models a shell command. */
public interface ShellCommand {

    /**
     * Executes the command within an environment, using the given arguments.
     *
     * @param env       the environment to execute in
     * @param arguments the command arguments
     *
     * @return {@link ShellStatus#CONTINUE} if the command executed successfully,
     *     {@link ShellStatus#TERMINATE} if a fatal error occurs
     */
    ShellStatus executeCommand(Environment env, String arguments);

    /**
     * Gets the command name.
     *
     * @return the command name
     */
    String getCommandName();

    /**
     * Gets the command description.
     * <p>
     * Each string is a line of output.
     * <p>
     * The returned list is unmodifiable.
     *
     * @return the description
     *
     * @see Collections#unmodifiableList(List)
     */
    List<String> getCommandDescription();
}
