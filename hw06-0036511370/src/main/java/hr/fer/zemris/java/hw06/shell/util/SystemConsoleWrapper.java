package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.ShellIOException;

import java.io.IOError;
import java.util.Objects;

/**
 * A wrapper around the system console.
 */
public class SystemConsoleWrapper implements Console {

    /** The backing console. */
    private java.io.Console console;

    /**
     * Creates a new system console wrapper.
     * @param console the system console
     */
    public SystemConsoleWrapper(java.io.Console console) {
        this.console = Objects.requireNonNull(console);
    }

    /** {@inheritDoc} */
    @Override
    public void print(String text) {
        console.printf("%s", text);
    }

    /** {@inheritDoc} */
    @Override
    public void println(String text) {
        console.printf("%s%n", text);
    }

    /** {@inheritDoc} */
    @Override
    public String nextLine() {
        try {
            return console.readLine();
        } catch (IOError ex) {
            throw new ShellIOException("Error reading from stdin.");
        }
    }
}
