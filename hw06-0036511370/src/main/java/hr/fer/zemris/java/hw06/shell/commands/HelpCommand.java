package hr.fer.zemris.java.hw06.shell.commands;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static hr.fer.zemris.java.hw06.shell.ShellStatus.CONTINUE;
import static hr.fer.zemris.java.hw06.shell.util.CommandUtil.getArguments;

/**
 * help
 * <p>
 * Shows available commands, or the description of a specific command.
 */
public class HelpCommand implements ShellCommand {

    /** Name of the command. */
    public static final String NAME = "help";

    /** The description of this command, a list of lines. */
    private static List<String> description = new ArrayList<>();

    static {
        description.add("Usage: help [COMMAND]");
        description.add("Lists all available commands when called with no arguments,");
        description.add("or help for a specific command.");
    }


    /** {@inheritDoc} */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {

        if (arguments.length() == 0) {

            env.writeln("Use help [COMMAND] to get help for individual commands.");
            env.writeln("Available commands:");

            for (var name : env.commands().keySet()) {
                env.writeln(name);
            }

        } else {
            var args = getArguments(arguments);

            if (args.size() != 1) {
                env.writeln("Invalid number of arguments.");
                return CONTINUE;
            }

            ShellCommand command = env.commands().get(args.get(0));

            if (command != null) {
                command.getCommandDescription().forEach(env::writeln);
            } else {
                env.writeln("Unknown command.");
            }

        }

        return CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public String getCommandName() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getCommandDescription() {
        return Collections.unmodifiableList(description);
    }
}
