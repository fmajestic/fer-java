package hr.fer.zemris.java.hw06.shell.util;

import hr.fer.zemris.java.hw06.shell.Environment;

/**
 * Shell symbol getters.
 * <p>
 * Allows for easy addition of shell symbols without having to modify existing code in the SET function.
 */
public class SymbolGetters {

    /** The prompt symbol getter. */
    public static final ISymbolGetter PROMPT = Environment::getPromptSymbol;

    /** The morelines symbol getter. */
    public static final ISymbolGetter MORELINES = Environment::getMorelinesSymbol;

    /** The multiline symbol getter. */
    public static final ISymbolGetter MULTILINE = Environment::getMultilineSymbol;

    /** The getter interface. */
    public interface ISymbolGetter {

        /**
         * Gets a symbol from the environment.
         *
         * @param env the environment to get from
         *
         * @return the symbol
         */
        char get(Environment env);
    }
}
