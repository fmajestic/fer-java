package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ArrayIndexedCollectionTest {
    private static String[] strings = new String[] { "one", "two", "three", "four", "five" };
    private static LinkedListIndexedCollection initialTestData;
    private static ArrayIndexedCollection list;

    @BeforeAll
    public static void initTestData() {
        initialTestData = new LinkedListIndexedCollection();

        for (var str : strings) {
            initialTestData.add(str);
        }
    }

    @BeforeEach()
    public void initList() {
        list = new ArrayIndexedCollection();
    }

    @Test
    public void testingIsEmpty() {
        assertTrue(list.isEmpty(), "List initialized with empty constructor should be empty.");

        list.add(new Object());

        assertFalse(list.isEmpty(), "A list containing at least 1 element should not be empty.");
    }

    @Test
    public void testingSize() {
        assertEquals(0, list.size(), "List initialized with empty constructor should have size 0.");

        list.add(new Object());
        list.add(new Object());

        assertEquals(2, list.size(), "List size should be exactly 2 after adding 2 elements.");
    }

    @Test
    public void testingValidGet() {
        list.addAll(initialTestData);

        for (int i = 0; i < strings.length; i++) {
            assertEquals(strings[i], list.get(i),
                    String.format("List does not match at index %d: [%s, %s]", i, strings[i], list.get(i)));
        }
    }

    @Test
    public void testingInvalidGet() {
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(0));

        list.add(new Object());

        assertThrows(IndexOutOfBoundsException.class, () -> list.get(2));
    }

    @Test
    public void defaultConstructor() {
        var constructorList = new ArrayIndexedCollection();

        assertEquals(0, constructorList.size());
        assertTrue(constructorList.isEmpty());
    }

    @Test
    public void initialCollectionConstructor() {
        list = new ArrayIndexedCollection(initialTestData);

        assertFalse(list.isEmpty(), "List initialized with non-empty collection should not be empty.");
        assertEquals(initialTestData.size(), list.size());
    }

    @Test
    public void testingIndexOf() {
        list.addAll(initialTestData);

        assertEquals(-1, list.indexOf("seven"));
        assertEquals(0, list.indexOf("one"));
        assertEquals(4, list.indexOf("five"));
    }

    @Test
    public void testingAdd() {
        list.add("one");
        list.add("two");

        assertEquals(2, list.size());
        assertEquals(1, list.indexOf("two"));
    }

    @Test
    public void testingInsert() {
        list.addAll(initialTestData);

        assertThrows(NullPointerException.class, () -> list.insert(null, 2),
                "Inserting null value should be forbidden.");
        assertThrows(IndexOutOfBoundsException.class, () -> list.insert("", -1),
                "Inserting at negative index should be forbidden");
        assertThrows(IndexOutOfBoundsException.class, () -> list.insert("", 10),
                "Inserting past the end should be forbidden.");

        list.insert("zero", 0);

        list.insert("six", 6);

        assertEquals(7, list.size());
        assertEquals(0, list.indexOf("zero"));
        assertEquals(6, list.indexOf("six"));
    }

    @Test
    public void testingContains() {
        list.addAll(initialTestData);

        assertTrue(list.contains("three"));
        assertFalse(list.contains("ten"));
    }

    @Test
    public void testingRemove() {
        list.addAll(initialTestData);

        String toRemove = "one";

        assertTrue(list.contains(toRemove));
        assertTrue(list.remove(toRemove));
        assertFalse(list.contains(toRemove));

        assertFalse(list.remove("ten"));
    }

    @Test
    public void testingToArray() {
        list.addAll(initialTestData);

        assertArrayEquals(strings, list.toArray());
    }

    @Test
    public void testingClear() {
        list.addAll(initialTestData);
        list.clear();

        assertEquals(0, list.size());
    }

    @Test
    public void testingForEach() {
        final class MyProcessor extends Processor {
            @Override
            public void process(Object value) {
                System.out.print(value);
            }
        }

        var list = new LinkedListIndexedCollection();
        var redirectedOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(redirectedOut));

        list.addAll(initialTestData);
        list.forEach(new MyProcessor());

        var sb = new StringBuilder();

        for (var str : strings) {
            sb.append(str);
        }

        assertEquals(sb.toString(), redirectedOut.toString());
    }
}
