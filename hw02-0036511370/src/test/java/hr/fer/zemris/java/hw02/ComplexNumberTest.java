package hr.fer.zemris.java.hw02;

import static hr.fer.zemris.java.hw02.ComplexNumber.fromImaginary;
import static hr.fer.zemris.java.hw02.ComplexNumber.fromMagnitudeAndAngle;
import static hr.fer.zemris.java.hw02.ComplexNumber.fromReal;
import static hr.fer.zemris.java.hw02.ComplexNumber.normalizeAngle;
import static hr.fer.zemris.java.hw02.ComplexNumber.parse;
import static java.lang.Math.PI;
import static java.lang.Math.atan2;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ComplexNumberTest {
    private static String[] validParserInputs;
    private static String[] invalidParserInputs;
    private static ComplexNumber[] validParserOutputs;
    private static final double DELTA_TOLERANCE = 1e-8;

    @BeforeAll
    public static void initTestData() {
        validParserInputs = new String[] { "2+3i", "2", "i", "-3i", "2.17-6.12i", "-3.14", "-44i" };

        validParserOutputs = new ComplexNumber[] { new ComplexNumber(2, 3), new ComplexNumber(2, 0),
                new ComplexNumber(0, 1), new ComplexNumber(0, -3), new ComplexNumber(2.17, -6.12),
                new ComplexNumber(-3.14, 0), new ComplexNumber(0, -44) };

        invalidParserInputs = new String[] { "++2+3i", "2+-3i", "2-+6i", "2i+3i", "2i--6", "3.14+2,16i", "-3,15i",
                "3+i2", "-i65", "a+bi" };
    }

    @Test
    public void validParserInput() {
        for (int i = 0; i < validParserInputs.length; i++) {
            var input = validParserInputs[i];

            var z = assertDoesNotThrow(() -> parse(input), "Exception thrown for valid parser input: " + input);

            assertEquals(validParserOutputs[i], z,
                    String.format("Parse result %s does not match valid output %s.", z, validParserOutputs[i]));
        }
    }

    @Test
    public void invalidParserInput() {
        for (var input : invalidParserInputs) {
            assertThrows(NumberFormatException.class, () -> parse(input),
                    "No exception thrown for invalid parser input: " + input);
        }
    }

    @Test
    public void constructor() {
        var z = new ComplexNumber(3, 4);
        assertEquals(3d, z.getReal(), DELTA_TOLERANCE, "Constructor failed to set real part.");
        assertEquals(4d, z.getImaginary(), DELTA_TOLERANCE, "Constructor failed to set imaginary part.");
    }

    @Test
    public void testingFromReal() {
        var z = fromReal(2d);

        assertEquals(2d, z.getReal(), DELTA_TOLERANCE, "Factory method failed to set real part.");
        assertEquals(0d, z.getImaginary(), DELTA_TOLERANCE, "Factory method modified wrong property.");
    }

    @Test
    public void testingFromImaginary() {
        var z = fromImaginary(2d);

        assertEquals(0d, z.getReal(), DELTA_TOLERANCE, "Factory method modified wrong property.");
        assertEquals(2d, z.getImaginary(), DELTA_TOLERANCE, "Factory method failed to set imaginary part.");
    }

    @Test
    public void testingMagnitudeAngle() {
        var z = fromMagnitudeAndAngle(sqrt(2), PI / 4);

        assertEquals(1d, z.getReal(), DELTA_TOLERANCE, "Factory method failed to set proper real part.");
        assertEquals(1d, z.getImaginary(), DELTA_TOLERANCE, "Factory method failed to set proper imaginary part.");
    }

    @Test
    public void testingGetters() {
        var z = new ComplexNumber(3, 4);

        assertEquals(3d, z.getReal(), DELTA_TOLERANCE, "Getter returned wrong real part.");
        assertEquals(4d, z.getImaginary(), DELTA_TOLERANCE, "Getter returned wrong imaginary part.");
        assertEquals(5d, z.getMagnitude(), DELTA_TOLERANCE, "Getter returned wrong magnitude.");
        assertEquals(atan2(4d, 3d), z.getAngle(), DELTA_TOLERANCE, "Getter returned wrong angle.");
    }

    @Test
    public static void testingOperators() {
        var z = new ComplexNumber(1, 2);
        var w = new ComplexNumber(3, 4);

        assertEquals(new ComplexNumber(4, 6), z.add(w), "Add operation failed.");
        assertEquals(new ComplexNumber(-2, -2), z.sub(w), "Subtract operation failed.");
        assertEquals(new ComplexNumber(-5, 10), z.mul(w), "Multiply operation failed.");
        assertEquals(new ComplexNumber(11 / 25, 2 / 25), z.div(w), "Divide operation failed.");

        assertThrows(IllegalArgumentException.class, () -> z.div(fromMagnitudeAndAngle(0, 3.14)),
                "Division by 0 allowed.");
    }

    @Test
    public void testingPower() {
        var z = new ComplexNumber(2, 3);

        assertEquals(new ComplexNumber(2035, -828), z.power(6), "Raising to valid power failed.");
        assertThrows(IllegalArgumentException.class, () -> z.power(-2), "Raising to negative power allowed.");
    }

    @Test
    public void testingRoot() {
        var z = fromReal(6);

        var expectedRoots = new ComplexNumber[6];
        var expectedMagnitude = Math.pow(6, 1d / 6);

        for (int i = 0; i < 6; i++) {
            expectedRoots[i] = fromMagnitudeAndAngle(expectedMagnitude, i * PI / 3);
        }

        assertArrayEquals(expectedRoots, z.root(6), "Actual roots do not match expected roots.");

        assertThrows(IllegalArgumentException.class, () -> z.root(-2), "Negative root allowed.");
    }

    @Test
    public void testingNormalization() {
        var input = new double[] { PI / 2, -PI / 3, -9 * PI, 13 * PI / 2, 2 * PI, 0 };
        var expected = new double[] { PI / 2, 5 * PI / 3, PI, PI / 2, 0, 0 };

        for (int i = 0; i < input.length; i++) {
            var actual = normalizeAngle(input[i]);

            assertEquals(expected[i], actual, DELTA_TOLERANCE,
                    String.format("Failed to normalize %.2f to %.2f. Actual: %.2f", input[i], expected[i], actual));
        }
    }
}
