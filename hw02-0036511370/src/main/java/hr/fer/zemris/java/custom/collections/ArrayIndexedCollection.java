package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents an indexed collection that uses an array as a backing field.
 * 
 * @author Filip Majetic
 */
public class ArrayIndexedCollection extends Collection {
    /**
     * The default collection capacity
     */
    public static final int DEFAULT_CAPACITY = 16;

    /** Current size of the collection */
    private int size;

    /** Elements of the collection */
    private Object[] elements;

    /**
     * Default constructor. Creates an empty collection with a default capacity.
     * 
     * @see ArrayIndexedCollection#DEFAULT_CAPACITY
     */
    public ArrayIndexedCollection() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Creates an empty collection with an inital capacity.
     * 
     * @param initialCapacity the initial capacity
     * 
     * @throws IllegalArgumentException if {@code initialCapacity} is less than 1
     */
    public ArrayIndexedCollection(int initialCapacity) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("Initial size must be greater than 1");
        }

        this.size = 0;
        this.elements = new Object[initialCapacity];
    }

    /**
     * Creates a new collection with an initial capacity using {@code source} for
     * initial data.
     * <p>
     * Note that if {@code initialCapacity} is smaller than {@code source.size()},
     * the source's size will be used instead.
     * 
     * @param source          the source collection
     * @param initialCapactiy the initial capacity of this collection
     * 
     * @throws NullPointerException if source is null
     */
    public ArrayIndexedCollection(Collection source, int initialCapacity) {

        this(Integer.max(initialCapacity, Objects.requireNonNull(source).size()));

        this.addAll(source);
    }

    /**
     * Creates a new collection using {@code source} for initial data.
     * 
     * @param source the source collection
     * 
     * @throws NullPointerException if source is null
     * 
     * @see #ArrayIndexedCollection(Collection, int)
     */
    public ArrayIndexedCollection(Collection source) {
        this(source, 1);
    }

    /**
     * Gets the element at specified index.
     * 
     * @param index the index to get from
     * 
     * @return the element at the index
     * 
     * @throws IndexOutOfBoundsException if index is negative or greater than
     *                                   {@code size-1}
     */
    public Object get(int index) {
        validateIndex(index, 0, size - 1);

        return elements[index];
    }

    /**
     * Returns the index of the first element that matches {@code value}.
     * 
     * @param value the value to search for
     * 
     * @return the index of the element, or -1 if nothing was found
     */
    public int indexOf(Object value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Checks whether this collection is empty.
     * 
     * @return true if the collection contains no elements, false otherwise
     */
    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    /**
     * Gets the size of this collection.
     * 
     * @return the size of the collection
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * Adds the non-null {@code value} to the collection.
     * 
     * @param value the value to be added, not null
     * 
     * @throws NullPointerException if value is null
     * @throws OutOfMemoryError     if the collection has reached max size
     * 
     * @see #insert(Object, int)
     */
    @Override
    public void add(Object value) {
        if (value == null) {
            throw new NullPointerException("Value cannot be null.");
        }

        ensureCapacity();

        elements[size] = value;
        size += 1;
    }

    /**
     * Inserts the given non-null {@code value} at the position, shifting the
     * remaining elements to the right.
     * 
     * @param value    the value to be inserted, not null
     * @param position the position at which to insert, valid range is
     *                 {@code [0, size]}
     *
     * @throws NullPointerException      if {@code value} is null
     * @throws IndexOutOfBoundsException if {@code position} is negative, or greater
     *                                   than {@code size}
     * 
     * @see #add(Object)
     */
    public void insert(Object value, int index) {
        validateValue(value);

        validateIndex(index, 0, size);

        ensureCapacity();

        // Čitaj sve od 'index' na dalje, a počni zapisivati na jednom mjestu dalje.
        // Ovime je ostvareno stvaranje mjesta za novi element i pomak ostatka udesno.
        // arraycopy je siguran iako su source i destination isti objekti, pogledati
        // javadoc.
        System.arraycopy(elements, index, elements, index + 1, size - index);
        elements[index] = value;
        size += 1;
    }

    /**
     * Checks whether the collections contains a value.
     * 
     * @param value the value to search for
     * @return true if the value was found, false otherwise
     */
    @Override
    public boolean contains(Object value) {
        return indexOf(value) >= 0;
    }

    /**
     * Removes the first occurence of {@code value} from the collection.
     * 
     * @param value the value to remove
     * @return true if a value was removed, false otherwise
     */
    @Override
    public boolean remove(Object value) {
        var index = indexOf(value);

        if (index < 0) {
            return false;
        }

        this.remove(index);

        return true;
    }

    /**
     * Removes an element from the specified index.
     * 
     * @param index the index to remove from
     * 
     * @throws IndexOutOfBoundsException if index is negative or greater than
     *                                   {@code size-1 }
     */
    public void remove(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException();
        }

        // Čitaj sve elemente desno od indeksa, a počni ih zapisivati od indeksa.
        // Ovime se ostvaruje prebrisavanje elementa na 'index' i pomak ostatka ulijevo.
        System.arraycopy(elements, index + 1, elements, index, size - index - 1);

        size -= 1;
    }

    /**
     * Removes all elements of this collection.
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }

        size = 0;
    }

    /**
     * Returns an array representation of this collection.
     * 
     * @return an array containing all elements of this collection
     */
    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elements, size);
    }

    /**
     * Applies the action contained in {@code Processor} to every element of the
     * collection.
     * 
     * @param processor the overriden {@code Processor} containing the action to be
     *                  performed
     */
    @Override
    public void forEach(Processor processor) {
        for (int i = 0; i < size; i++) {
            processor.process(elements[i]);
        }
    }

    /**
     * Ensures that the array is expanded once full.
     * <p>
     * Doubles the array capacity if it is possible to do so without overflowing.
     * <p>
     * Expands to {@code Integer.MAX_VALUE} otherwise.
     * 
     * @throws OutOfMemoryError value is attempted to be added after the collection
     *                          size reaches {@code Integer.MAX_VALUE}
     */
    private void ensureCapacity() {
        if (size == elements.length) {
            if (elements.length < Integer.MAX_VALUE) {
                int newCapacity = elements.length * 2;

                // Check for overflow from multiplication
                newCapacity = newCapacity > 0 ? newCapacity : Integer.MAX_VALUE;

                // Expand the array to the new capacity
                elements = Arrays.copyOf(elements, newCapacity);
            } else {
                throw new OutOfMemoryError("Maximum list size reached.");
            }
        }
    }

    /**
     * Makes sure that an index is within the given bounds. Both bounds are
     * inclusive.
     * 
     * @param index      the index to check
     * @param lowerBound the lower bound, inclusive
     * @param upperBound the upper bound, inclusive
     * 
     * @throws IndexOutOfBoundsException if the index is not within given bounds.
     */
    private void validateIndex(int index, int lowerBound, int upperBound) {
        if (index < lowerBound || index > upperBound) {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Makes sure the value is not null.
     * 
     * @param value the value to check
     */
    private void validateValue(Object value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }

}
