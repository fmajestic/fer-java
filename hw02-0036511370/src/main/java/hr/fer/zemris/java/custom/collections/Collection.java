package hr.fer.zemris.java.custom.collections;

/**
 * A base class for all inheriting collections.
 * <p>
 * Contains empty method definitions. It is expected for this class to be
 * extended, and empty methods overriden with the proper functionatily.
 * 
 * @author Filip Majetic
 */
public class Collection {

    /**
     * Protected constructor. Removes the ability to make an instance of this class.
     */
    protected Collection() {

    }

    /**
     * Checks whether the collection is empty.
     * 
     * @return false if the collection contains no elements, true otherwise
     */
    public boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Gets the size of the collection.
     * <p>
     * Implemented here to always return 0.
     * 
     * @return the size of the collection
     */
    public int size() {
        return 0;
    }

    /**
     * Adds an object to the collection.
     * <p>
     * Implemented here as an empty method.
     * 
     * @param value the object to add
     */
    public void add(Object value) {

    }

    /**
     * Checks whether the collection contains an object.
     * <p>
     * Implemented here to always return false.
     * 
     * @param value the value to search for
     * @return true if the value was found, false otherwise
     */
    public boolean contains(Object value) {
        return false;
    }

    /**
     * Removes the first occurence of value from the collection. Returns the succes
     * of removing the value.
     * <p>
     * Implemented here to always return false.
     * 
     * @param value the value to remove
     * @return true if the value was removed, false otherwise
     */
    public boolean remove(Object value) {
        return false;
    }

    /**
     * Returns an array representation of this collection.
     * <p>
     * Implemented here to throw UnsupportedOperationException
     * 
     * @return the array containing all the elements of this collection
     */
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    /**
     * Applies the action contained in {@code Processor} to every element of the
     * collection.
     * <p>
     * Implemented here as an empty method.
     * 
     * @param processor the overriden {@code Processor} containing the action to be
     *                  performed
     */
    public void forEach(Processor processor) {

    }

    /**
     * Adds all the elements of another collection to this collection.
     * <p>
     * Uses an overriden {@link Processor} to to apply {@link #add(Object)} to every
     * element of {@code other}.
     * 
     * @param other the collection to add elements from
     */
    public void addAll(Collection other) {
        final class AddProcessor extends Processor {
            @Override
            public void process(Object value) {
                add(value);
            }
        }

        other.forEach(new AddProcessor());
    }

    /**
     * Removes all elements from this collection.
     * <p>
     * Implemented here as an empty method.
     */
    public void clear() {

    }
}
