package hr.fer.zemris.java.custom.collections;

/**
 * Represents an indexed collection that uses a doubly linked list as a backing
 * field.
 * 
 * @author Filip Majetic
 * 
 * @see ArrayIndexedCollection
 */
public class LinkedListIndexedCollection extends Collection {
    private static final class ListNode {
        private ListNode previous;
        private ListNode next;
        private Object value;

        /**
         * Initializes a new node with the given value
         * 
         * @param value inital value of this node
         */
        public ListNode(Object value) {
            this.value = value;
            previous = null;
            next = null;
        }
    }

    /** Current size of the collection */
    private int size;

    /** The first node of the list, head */
    private ListNode first;

    /** The last node of the list, tail */
    private ListNode last;

    /**
     * Defaul constructor. Initializes an empty list.
     */
    public LinkedListIndexedCollection() {
        first = last = null;
        size = 0;
    }

    /**
     * A constructor that takes another {@code Collection} to be added as inital
     * elements of the list.
     * 
     * @param other the inital collection to add to the list
     */
    public LinkedListIndexedCollection(Collection other) {
        this();
        this.addAll(other);
    }

    /**
     * Gets the list element at the specified index.
     * 
     * @param index the index of the element to be retrieved
     * 
     * @throws IndexOutOfBoundsException if {@code index} is negative, or greater
     *                                   than {@code size - 1}
     */
    public Object get(int index) {
        validateIndex(index, 0, size - 1);

        var node = walkToIndex(index);

        return node.value;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return first == null && last == null && size == 0;
    }

    /**
     * Gets the index of the first element that matches {@code value} in the list.
     * 
     * @param value the value of the element whose index is to be returned
     * 
     * @return the index of the element in the list, or {@code -1} if it is not
     *         present.
     */
    public int indexOf(Object value) {
        int index = 0;

        var walk = first;

        while (walk != null) {
            if (walk.value.equals(value)) {
                return index;
            }

            walk = walk.next;
            index += 1;
        }

        return -1;
    }

    /**
     * Inserts the given non-null {@code value} at the position, shifting the
     * remaining elements to the right.
     * 
     * @param value    the value to be inserted, not null
     * @param position the position at which to insert, valid range is
     *                 {@code [0, size]}
     *
     * @throws NullPointerException      if {@code value} is {@code null}
     * @throws IndexOutOfBoundsException if {@code position} is negative, or greater
     *                                   than {@code size}
     * 
     * @see #add(Object)
     */
    public void insert(Object value, int position) {
        validateValue(value);

        validateIndex(position, 0, size);

        // Special case: inserting into an empty list.
        if (this.isEmpty()) {
            add(value);
            return;
        }

        var node = new ListNode(value);

        // Special cases: inserting to the beginning or end
        if (position == 0) {
            node.next = first;
            first.previous = node;
            first = node;
        } else if (position == size) {
            last.next = node;
            node.previous = last;
            last = node;
        } else {
            var insertAfter = walkToIndex(position - 1);

            // Since the previous case handles inserting to the end,
            // we don't need to worry about insertAfter.next.previous
            // throwing a NullPointerException
            node.next = insertAfter.next;
            insertAfter.next.previous = node;

            node.previous = insertAfter;
            insertAfter.next = node;
        }

        size += 1;
    }

    /**
     * Adds the value to the end of the list.
     * 
     * @param value the value to be added, not null
     * 
     * @throws NullPointerException if value is null
     * 
     * @see #insert(Object, int)
     * @see #addAll(Collection)
     */
    @Override
    public void add(Object value) {
        validateValue(value);

        var node = new ListNode(value);

        if (this.isEmpty()) {
            first = last = node;
        } else {
            node.previous = last;
            last.next = node;
            last = node;
        }

        this.size += 1;
    }

    /**
     * Checks whether the list contains the given value.
     * 
     * @param value the value to search for
     * 
     * @return {@code true} if the value was found, {@code false} otherwise
     */
    @Override
    public boolean contains(Object value) {
        return indexOf(value) >= 0;
    }

    /**
     * Removes the first occurence of {@code value} from the list.
     * 
     * @param value the value to be removed
     * 
     * @return {@code true} if an element was removed, {@code false} otherwise
     */
    @Override
    public boolean remove(Object value) {
        boolean nodeRemoved = false;

        if (first.value.equals(value)) {
            var oldFirst = first;

            first = oldFirst.next;
            first.previous = null;

            oldFirst.next = null;

            nodeRemoved = true;
        } else if (last.value.equals(value)) {
            var oldLast = last;

            last = oldLast.previous;
            last.next = null;

            oldLast.previous = null;

            nodeRemoved = true;
        } else {
            var toRemove = first;

            while (toRemove != null) {
                if (toRemove.value.equals(value)) {
                    toRemove.previous.next = toRemove.next;
                    toRemove.next.previous = toRemove.previous;

                    nodeRemoved = true;
                    break;
                }

                toRemove = toRemove.next;
            }
        }

        if (nodeRemoved) {
            size -= 1;
        }

        return nodeRemoved;
    }

    /**
     * Removes an element from the specified index.
     * 
     * @param index the index to remove from
     * 
     * @throws IndexOutOfBoundsException if index is negative or greater than
     *                                   {@code size-1 }
     */
    public void remove(int index) {
        validateIndex(index, 0, size - 1);

        var toRemove = walkToIndex(index);

        if (size == 1) {
            clear();
            return;
        }

        if (toRemove == first) {
            toRemove.next.previous = null;
            first = toRemove.next;
        } else if (toRemove == last) {
            toRemove.previous.next = null;
            last = toRemove.previous;
        } else {
            toRemove.previous.next = toRemove.next;
            toRemove.next.previous = toRemove.previous;
        }

        size -= 1;
    }

    /**
     * Returns an array containing the elements of the list.
     * 
     * @return an array with all the elements of the list
     */
    @Override
    public Object[] toArray() {
        var elements = new Object[size];

        var walk = first;

        // This could be done with a while loop,
        // but that would unnecessarily expose the counter variable
        for (int i = 0; i < size; i++) {
            elements[i] = walk.value;
            walk = walk.next;
        }

        return elements;
    }

    /**
     * Executes {@code processor.process} on all the elements of the list.
     * <p>
     * The intended use is to provide your own class that extends {@link Processor}
     * and overrides {@link Processor#process} with the desired behavior.
     * 
     * @param processor a processor containing the action to be done
     * 
     * @see Processor
     */
    @Override
    public void forEach(Processor processor) {
        var walk = first;

        while (walk != null) {
            processor.process(walk.value);
            walk = walk.next;
        }
    }

    /**
     * Removes all elements from the list.
     */
    @Override
    public void clear() {
        while (first != null) {
            var next = first.next;

            first.previous = null;
            first.next = null;

            first = next;
        }

        first = last = null;
        size = 0;
    }

    /**
     * Makes sure the value is not null.
     * 
     * @param value the value to check
     */
    private void validateValue(Object value) {
        if (value == null) {
            throw new NullPointerException("Value can't be null.");
        }
    }

    /**
     * Makes sure that an index is within the given bounds. Both bounds are
     * inclusive.
     * 
     * @param index      the index to check
     * @param lowerBound the lower bound, inclusive
     * @param upperBound the upper bound, inclusive
     * 
     * @throws IndexOutOfBoundsException if the index is not within given bounds.
     */
    private void validateIndex(int index, int lowerBound, int upperBound) {
        if (index < lowerBound || index > upperBound) {
            throw new IndexOutOfBoundsException();
        }
    }

    /** Utility method. Returns the list node at the given index. */
    private ListNode walkToIndex(int index) {
        ListNode walk;

        // Optimizacija hodanja po listi:
        // Ako se nalazi u prvoj polovici, krećemo od početka.
        // Ako se nalazi u drugoj polovici, krećemo od kraja.
        if (index <= (size - 1) / 2) {
            walk = first;

            for (int i = 0; i < index; i++) {
                walk = walk.next;
            }
        } else {
            walk = last;

            for (int i = 0; i < size - index - 1; i++) {
                walk = walk.previous;
            }
        }

        return walk;
    }

}
