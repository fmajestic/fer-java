package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * A demo program for the {@code ObjectStack} class.
 * <p>
 * Calculates the result of a given postfix mathematical expression.
 * <p>
 * Supported operators: +, -, *, /, %
 * 
 * @author Filip Majetic
 * 
 * @see ObjectStack
 */
public class StackDemo {

    /**
     * Main method of the program
     * 
     * @param args a single postfix mathematical expression, space-separated. Make
     *             sure to quote it, so it is passed as a single arguments
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Invalid number of arguments.");
            System.exit(1);
        }

        var stack = new ObjectStack();

        for (var token : args[0].split("\\s+")) {
            if (token.matches("-?\\d+")) {
                stack.push(Integer.parseInt(token));
                continue;
            }

            if (token.matches("[+\\-/*%]")) {
                try {
                    var rightOperand = (int) stack.pop();
                    var leftOperand = (int) stack.pop();

                    switch (token) {
                    case "+":
                        stack.push(leftOperand + rightOperand);
                        break;

                    case "-":
                        stack.push(leftOperand - rightOperand);
                        break;

                    case "*":
                        stack.push(leftOperand * rightOperand);
                        break;

                    case "/":
                        stack.push(leftOperand / rightOperand);
                        break;

                    case "%":
                        stack.push(leftOperand % rightOperand);
                        break;
                    }
                } catch (ArithmeticException ex) {
                    System.out.println("Dijeljenje s nulom nije dozvoljeno. Kraj programa.");
                    System.exit(1);
                } catch (EmptyStackException ex) {
                    System.out.println("Neispravan izraz. Kraj programa.");
                    System.exit(1);
                }
            } else {
                System.out.println("Neispravan izraz. Kraj programa.");
                System.exit(1);
            }
        }

        if (stack.size() != 1) {
            System.out.println("Invalid expression.");
        } else {
            System.out.println("Expression evaluates to " + stack.pop());
        }
    }
}
