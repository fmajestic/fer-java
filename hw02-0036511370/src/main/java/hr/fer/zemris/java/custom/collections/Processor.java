package hr.fer.zemris.java.custom.collections;

/**
 * A basic element processor.
 * 
 * @author Filip Majetic
 */
public class Processor {
	/**
	 * An empty method, indended for overriding.
	 * 
	 * @param value the value to be processed
	 */
	public void process(Object value) {
	}
}
