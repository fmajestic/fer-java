package hr.fer.zemris.java.hw02;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import java.util.regex.Pattern;

/**
 * Represents an immutable complex number.
 * 
 * @author Filip Majetic
 */
public class ComplexNumber {
    /**
     * The regular expression used to parse complex numbers.
     * 
     * @see Visualization: <a href=
     *      "https://regex101.com/r/YlvIFX/6">https://regex101.com/r/YlvIFX/6</a>
     */
    public static final String REGEX_PATTERN = "^(?!\\s)(?<real>[+-]?\\d+(?:\\.\\d+)?(?![i.\\d]))?(?<imaginary>[+-]?(?:\\d+(?:\\.\\d+)?)?[i])?$";

    /**
     * The double-precision floating point tolerance that is used for comparing two
     * complex numbers.
     */
    public static final double DELTA_TOLERANCE = 1e-8;

    private final double real;
    private final double imaginary;

    /**
     * Creates a complex number.
     * 
     * @param real      the real part
     * @param imaginary the imainary part
     */
    public ComplexNumber(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    /**
     * Creates a complex number without an imaginary part.
     * 
     * @param real the real part
     * 
     * @return a new complex number with imaginary part 0
     */
    public static ComplexNumber fromReal(double real) {
        return new ComplexNumber(real, 0d);
    }

    /**
     * Creates a complex number without a real part.
     * 
     * @param imaginary the imaginary part
     *
     * @return a new complex number with real part 0
     */
    public static ComplexNumber fromImaginary(double imaginary) {
        return new ComplexNumber(0d, imaginary);
    }

    /**
     * Creates a complex number from a magnitude and an angle.
     * 
     * @param magnitude the magnitude
     * @param angle     the angle, in radians
     * 
     * @return a new complex number
     */
    public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
        if (magnitude < 0) {
            throw new IllegalArgumentException("Magnitude cannot be nagative.");
        }

        return new ComplexNumber(magnitude * cos(angle), magnitude * sin(angle));
    }

    /**
     * Gets the real part of this complex number.
     * 
     * @return the real part
     */
    public double getReal() {
        return real;
    }

    /**
     * Gets the imaginary part of this complex number.
     * 
     * @return the imaginary part
     */
    public double getImaginary() {
        return imaginary;
    }

    /**
     * Gets the magnitidue of this complex number.
     * 
     * @return the magnitude
     */
    public double getMagnitude() {
        return sqrt(real * real + imaginary * imaginary);
    }

    /**
     * Gets the angle of this complex number.
     * 
     * @return the angle, normalized to the interval {@code [0, 2pi]}
     */
    public double getAngle() {
        var radians = atan2(imaginary, real);
        return normalizeAngle(radians);
    }

    /**
     * Adds another complex number to this number.
     * 
     * @param c the number to add
     * 
     * @return a new complex number representing the result of the operation
     */
    public ComplexNumber add(ComplexNumber c) {
        return new ComplexNumber(real + c.getReal(), imaginary + c.getImaginary());
    }

    /**
     * Subtracts another complex number from this number.
     * 
     * @param c the number to subtract
     * 
     * @return a new complex number representing the result of the operation
     */
    public ComplexNumber sub(ComplexNumber c) {
        return new ComplexNumber(real - c.getReal(), imaginary - c.getImaginary());
    }

    /**
     * Multiplies this number by another complex number.
     * 
     * @param c the number to multiply by
     * 
     * @return a new complex number representing the result of the operation
     */
    public ComplexNumber mul(ComplexNumber c) {
        var magnitude = this.getMagnitude() * c.getMagnitude();
        var angle = this.getAngle() + c.getAngle();

        return ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
    }

    /**
     * Divides this number by another complex number.
     * 
     * @param c the number to divide by
     * 
     * @return a new complex number representing the result of the operation
     * 
     * @throws IllegalArgumentException if the magnitude of {@code c} is zero.
     */
    public ComplexNumber div(ComplexNumber c) {
        // 2020-05-01 why tf did I write abs() for magintude lmao
        if (abs(c.getMagnitude()) < DELTA_TOLERANCE) {
            throw new IllegalArgumentException("Cannot divide by zero.");
        }

        var magnitude = this.getMagnitude() / c.getMagnitude();
        var angle = this.getAngle() - c.getAngle();

        return ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
    }

    /**
     * Calculates the n-th power of this complex number.
     * 
     * @param n the power to calculate
     * 
     * @return a new complex number represeting the result of the operation
     * 
     * @throws IllegalArgumentException if n is negative
     */
    public ComplexNumber power(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

        return ComplexNumber.fromMagnitudeAndAngle(pow(getMagnitude(), n), getAngle() * n);
    }

    /**
     * Calculates all of the n-th roots of this number.
     * 
     * @param n the root to calculate
     * 
     * @throws IllegalArgumentException if n is negative or 0
     * 
     * @return a new array containing all of the n-th roots
     */
    public ComplexNumber[] root(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        }

        var roots = new ComplexNumber[n];
        var angle = getAngle();
        var magnitude = Math.pow(getMagnitude(), 1d / n);

        for (int k = 0; k < n; k++) {
            roots[k] = ComplexNumber.fromMagnitudeAndAngle(magnitude, (angle + 2 * k * PI) / n);
        }

        return roots;
    }

    /**
     * Normalizes the given angle to [0, 2pi].
     * 
     * @param radians the angle to normalize, in radians
     * 
     * @return the given angle, normalized to the interval [0, 2pi] radians
     */
    public static double normalizeAngle(double radians) {
        if (abs(radians) >= 2 * PI) {
            radians -= 2 * PI * (int) (radians / (2 * PI));
        }

        if (radians < 0) {
            radians += 2 * PI;
        }

        return radians;
    }

    /**
     * Parses a string into a complex number.
     * 
     * @param s the string to parse
     * 
     * @return the parsed number
     * 
     * @throws NumberFormatException if the given string isn't a valid complex
     *                               number
     * 
     * @see ComplexNumber#REGEX_PATTERN
     */
    public static ComplexNumber parse(String s) {
        double real = 0d, imaginary = 0d;

        // 2020-05-01 why tf do I compile this every time lmao
        var pattern = Pattern.compile(REGEX_PATTERN);
        var matcher = pattern.matcher(s.replaceAll(" ", "")); // just to make sure there is no whitespace

        // Attempts to match the string against the regex
        if (matcher.matches() == false) {
            throw new NumberFormatException("Can't parse complex number: " + s);
        }

        var strRe = matcher.group("real");
        var strIm = matcher.group("imaginary");

        if (strRe != null) {
            real = Double.parseDouble(strRe);
        }

        if (strIm != null) {
            // special case: i without number
            if (strIm.matches("[+-]?i")) {
                imaginary = Double.parseDouble(strIm.replace("i", "1"));
            } else {
                // remove 'i' from end of string and parse
                imaginary = Double.parseDouble(strIm.replace("i", ""));
            }
        }

        return new ComplexNumber(real, imaginary);
    }

    /**
     * A string representation of this complex number.
     * 
     * @return a complex number string in the form {@code a+bi}, rounded to 2
     *         decimal places
     */
    @Override
    public String toString() {
        return String.format("%.2f%+.2fi", real, imaginary);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(imaginary);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(real);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null || !(obj instanceof ComplexNumber)) {
            return false;
        }

        ComplexNumber other = (ComplexNumber) obj;

        return abs(real - other.getReal()) < DELTA_TOLERANCE && abs(imaginary - other.getImaginary()) < DELTA_TOLERANCE;
    }
}
