<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Slika</title>
</head>
<body>

  <h1>Name: ${name}</h1>
  <p>Lines: ${lines}</p>
  <p>Circles: ${circles}</p>
  <p>Filled circles: ${fcircles}</p>
  <p>Triangles: ${triangles}</p>

  <img src="${pageContext.request.contextPath}/image?name=${name}" alt="${name}"/>
  <br>
  <a href="${pageContext.request.contextPath}/">Back to homepage</a>
</body>
</html>
