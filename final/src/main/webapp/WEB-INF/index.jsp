<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Grafika</title>
</head>
<body>

  <h1>Grafika</h1>

  <h2>Dostupne slike:</h2>

  <ul>
    <c:forEach var="file" items="${files}">
      <li><a href="${pageContext.request.contextPath}/info?name=${file.getFileName().toString()}">
          ${file.getFileName().toString()}
      </a></li>
    </c:forEach>
  </ul>

  <h2>Nova slika:</h2>
  <form id="new" method="post" action="${pageContext.request.contextPath}/create">
    <label for="name">Name:</label>
    <input id="name" name="name"/>
    <br>
    <label for="content">Content:</label><br>
    <textarea id="content" name="content" form="new"></textarea>
    <br><br>
    <button type="submit">Create new file</button>
  </form>
</body>
</html>
