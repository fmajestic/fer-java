package hr.fer.zemris.java.hw17.jvdraw.visitors;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledTriangle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Line;

import java.awt.*;
import java.util.Objects;

/** The bounding box caluclator. */
public class GeometricalObjectBBCalculator implements GeometricalObjectVisitor {

    private Rectangle bb;

    /**
     * Gets the bounding box.
     * <p>
     * if no objects have been visited, returns the null rectangle (width and height 0).
     *
     * @return the bounding box.
     */
    public Rectangle getBoundingBox() {
        return Objects.requireNonNullElseGet(bb, Rectangle::new);
    }

    @Override
    public void visit(Line line) {
        if (bb == null) {
            bb = new Rectangle(line.getStart());
        }

        bb.add(line.getStart());
        bb.add(line.getEnd());
    }

    @Override
    public void visit(Circle circle) {
        int r = circle.getRadius();
        Point p1 = new Point(circle.getCenter());
        Point p2 = new Point(circle.getCenter());

        p1.translate(-r, -r);
        p2.translate(r, r);

        if (bb == null) {
            bb = new Rectangle(circle.getCenter());
        }

        bb.add(p1);
        bb.add(p2);
    }

    @Override
    public void visit(FilledCircle filledCircle) {
        visit((Circle) filledCircle);
    }

    @Override
    public void visit(FilledTriangle ft) {
        if (bb == null) {
            bb = new Rectangle(ft.getPt1());
        }

        bb.add(ft.getPt1());
        bb.add(ft.getPt2());
        bb.add(ft.getPt3());
    }
}
