package hr.fer.zemris.java.web;

import hr.fer.zemris.java.hw17.jvdraw.Util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@WebListener
public class FileDB implements ServletContextListener {

    private static final DirectoryStream.Filter<Path> JVDFilter =
        f -> Files.isRegularFile(f) && Util.ext(f).equals("jvd");
    private static List<Path> files = new ArrayList<>();

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        Path folder = Paths.get(sce.getServletContext().getRealPath("/WEB-INF/images"));
        System.out.println("Folder path:" + folder.toString());

        if (!Files.exists(folder)) {
            try {
                Files.createDirectory(folder);
            } catch (IOException ignored) {
                System.out.println("Folder error");
            }
        }

        try (DirectoryStream<Path> ds = Files.newDirectoryStream(folder, JVDFilter)) {
            ds.forEach(files::add);
        } catch (IOException ignored) {
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    public static List<Path> getFiles() {
        return files;
    }

    public static void createFile(Path file, String content) throws IOException {
        Files.writeString(file, content);
        files.add(file);
    }
}
