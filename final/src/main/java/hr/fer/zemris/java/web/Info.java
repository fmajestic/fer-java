package hr.fer.zemris.java.web;

import hr.fer.zemris.java.hw17.jvdraw.GeometricalObjectParser;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.*;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "info", urlPatterns = "/info")
public class Info extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");

        if (name == null) {
            resp.sendError(403, "No name provided");
            return;
        }

        Path file = Paths.get(req.getServletContext().getRealPath("/WEB-INF/images")).resolve(name);

        if (!Files.exists(file)) {
            resp.sendError(404);
            return;
        }


        List<GeometricalObject> objects;
        try {
            objects = Files
                .lines(file)
                .map(GeometricalObjectParser::getObject)
                .collect(Collectors.toList());
        } catch (IllegalArgumentException ex) {
            resp.sendError(403, "Bad file");
            return;
        }

        long lines = objects.stream().filter(o -> o instanceof Line).count();
        long circles = objects.stream().filter(o -> o instanceof Circle && !(o instanceof FilledCircle)).count();
        long fcircles = objects.stream().filter(o -> o instanceof FilledCircle).count();
        long triangles = objects.stream().filter(object -> object instanceof FilledTriangle).count();

        req.setAttribute("name", name);
        req.setAttribute("lines", lines);
        req.setAttribute("circles", circles);
        req.setAttribute("fcircles", fcircles);
        req.setAttribute("triangles", triangles);

        req.getRequestDispatcher("/WEB-INF/info.jsp").forward(req, resp);
    }
}
