package hr.fer.zemris.java.hw17.jvdraw.drawing.tools;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * A base class for all tools, contains empty implementations of the Tool interface methods.
 *
 * @see Tool
 */
public abstract class AbstractTool implements Tool {

    /**
     * Does nothing.
     *
     * @param e ignored
     */
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    /**
     * Does nothing.
     *
     * @param e ignored
     */
    @Override
    public void mouseMoved(MouseEvent e) {

    }

    /**
     * Does nothing.
     *
     * @param g2d ignored
     */
    @Override
    public void paint(Graphics2D g2d) {

    }

    /**
     * Does nothing.
     *
     * @param e ignored
     */
    @Override
    public void mousePressed(MouseEvent e) {

    }

    /**
     * Does nothing.
     *
     * @param e ignored
     */
    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Does nothing.
     *
     * @param e ignored
     */
    @Override
    public void mouseDragged(MouseEvent e) {

    }
}
