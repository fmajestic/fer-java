package hr.fer.zemris.java.hw17.jvdraw.drawing.tools;

import java.awt.*;
import java.awt.event.MouseEvent;

/** Models a tool that draws a certain object */
public interface Tool {

    /**
     * Starts/stops the drawing.
     *
     * @param e the click event
     */
    void mouseClicked(MouseEvent e);

    /**
     * Updates the drawing.
     *
     * @param e the move event
     */
    void mouseMoved(MouseEvent e);

    /**
     * Paints the current shape on the graphics.
     *
     * @param g2d the graphics
     */
    void paint(Graphics2D g2d);

    /**
     * Unused.
     *
     * @param e unused
     */
    void mousePressed(MouseEvent e);

    /**
     * Unused.
     *
     * @param e unused
     */
    void mouseReleased(MouseEvent e);

    /**
     * Unused.
     *
     * @param e unused
     */
    void mouseDragged(MouseEvent e);
}