package hr.fer.zemris.java.hw17.jvdraw.actions;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw;
import hr.fer.zemris.java.hw17.jvdraw.Util;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectPainter;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectVisitor;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

/** The export action. */
public class Export extends JVDrawAction {

    private static final String NAME = "Export";

    public Export(JVDraw app, DrawingModel model) {
        super(NAME, app, model);
    }

    /**
     * Prompts the user for a file and writes the image
     *
     * @param e the event
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        Path file = Util.promptSavePath(app, "Image file", "jpg", "png", "gif");

        if (file == null) {
            return;
        }

        BufferedImage bim = makeFullImage();

        try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(file))) {
            ImageIO.write(bim, Util.ext(file), os);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(
                app, String.format("Error writing image: %s", ex.getMessage()), "IO error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(app, "Image exported successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Creates and populates the image via observer.
     *
     * @return the finished image
     */
    private BufferedImage makeFullImage() {
        GeometricalObjectBBCalculator calc = new GeometricalObjectBBCalculator();
        visitAll(calc);
        Rectangle bb = calc.getBoundingBox();

        // System.out.println("Created bounding box: " + bb);

        BufferedImage bim = new BufferedImage(bb.width, bb.height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = bim.createGraphics();
        g2d.translate(-bb.x, -bb.y);

        GeometricalObjectPainter painter = new GeometricalObjectPainter(g2d);
        visitAll(painter);
        g2d.dispose();

        return bim;
    }

    /**
     * Asks each of the model's objects to accept the visitor.
     *
     * @param visitor the visitor to accept
     */
    private void visitAll(GeometricalObjectVisitor visitor) {
        for (int i = 0; i < model.getSize(); i++) {
            model.getObject(i).accept(visitor);
        }
    }
}
