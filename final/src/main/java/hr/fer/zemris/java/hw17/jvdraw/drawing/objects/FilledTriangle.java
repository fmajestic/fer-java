package hr.fer.zemris.java.hw17.jvdraw.drawing.objects;

import hr.fer.zemris.java.hw17.jvdraw.editors.FilledTriangleEditor;
import hr.fer.zemris.java.hw17.jvdraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectVisitor;

import java.awt.*;

public class FilledTriangle extends GeometricalObject {

    private Color bgColor;
    private Point pt1;
    private Point pt2;
    private Point pt3;

    public FilledTriangle(Point pt1, Point pt2, Point pt3, Color fg, Color bgColor) {
        super(fg);
        this.pt1 = pt1;
        this.pt2 = pt2;
        this.pt3 = pt3;
        this.bgColor = bgColor;
    }

    public Color getBgColor() {
        return bgColor;
    }

    public void setBgColor(Color bg) {
        this.bgColor = bg;
        notifyObservers();
    }

    public Point getPt1() {
        return pt1;
    }

    public void setPt1(Point pt1) {
        this.pt1 = pt1;
        notifyObservers();
    }

    public Point getPt2() {
        return pt2;
    }

    public void setPt2(Point pt2) {
        this.pt2 = pt2;
        notifyObservers();
    }

    public Point getPt3() {
        return pt3;
    }

    public void setPt3(Point pt3) {
        this.pt3 = pt3;
        notifyObservers();
    }

    @Override
    public void accept(GeometricalObjectVisitor v) {
        v.visit(this);
    }

    @Override
    public GeometricalObjectEditor createGeometricalObjectEditor() {
        return new FilledTriangleEditor(this);
    }

    @Override
    public String toString() {
        return String.format("Triangle %s %s %s %s", string(pt1), string(pt2), string(pt3), string(bgColor));
    }
}
