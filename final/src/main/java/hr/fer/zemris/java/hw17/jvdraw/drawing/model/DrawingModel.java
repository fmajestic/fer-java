package hr.fer.zemris.java.hw17.jvdraw.drawing.model;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;

/** Manages geometrical objects in the drawing. */
public interface DrawingModel {

    /**
     * Gets the number of elements in the drawing.
     *
     * @return the number of elements
     */
    int getSize();

    /**
     * Gets an object at an index.
     *
     * @param index the index to get from
     *
     * @return the object
     */
    GeometricalObject getObject(int index);

    /**
     * Adds an object to the model.
     *
     * @param object the new object
     */
    void add(GeometricalObject object);

    /**
     * Removes an object from the model.
     *
     * @param object the object to remove
     */
    void remove(GeometricalObject object);

    /**
     * Changes the order of the* selected element and an element at a specified offset.
     *
     * @param object the object
     * @param offset the offset
     */
    void changeOrder(GeometricalObject object, int offset);

    /**
     * Gets an index of an object, or -1.
     *
     * @param object the object
     *
     * @return the index
     */
    int indexOf(GeometricalObject object);

    /**
     * Removes all elements from the model
     */
    void clear();

    /**
     * Clears the modification status of the document.
     */
    void clearModifiedFlag();

    /**
     * Checks if the modle has been modifed.
     *
     * @return true/false
     */
    boolean isModified();

    /**
     * Adds a listener.
     *
     * @param l the listener
     */
    void addDrawingModelListener(DrawingModelListener l);

    /**
     * Removes a listener.
     *
     * @param l the listenr
     */
    void removeDrawingModelListener(DrawingModelListener l);
}
