package hr.fer.zemris.java.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Paths;

@WebServlet(name = "newFile", urlPatterns = "/create")
public class NewFile extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");
        String content = req.getParameter("content");

        if (name == null || content == null) {
            resp.sendError(403, "Null parameter.");
            return;
        }

        if (!name.matches("[a-zA-Z0-9.]+\\.jvd")) {
            resp.sendError(403, "Invalid file name: " + name);
            return;
        }

        FileDB.createFile(
            Paths.get(req.getServletContext().getRealPath("/WEB-INF/images")).resolve(name),
            content
        );

        resp.sendRedirect(req.getContextPath());
    }
}
