package hr.fer.zemris.java.hw17.jvdraw.drawing;

import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModelListener;
import hr.fer.zemris.java.hw17.jvdraw.drawing.tools.Tool;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectPainter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Supplier;

/** The custom canvas. */
public class JDrawingCanvas extends JComponent implements DrawingModelListener {

    /** The model it is attached to. */
    private final DrawingModel model;

    /** The tool supplier, from the app. */
    private final Supplier<Tool> toolSupplier;

    /** The painter. */
    private final GeometricalObjectPainter painter;

    public JDrawingCanvas(DrawingModel model, Supplier<Tool> toolSupplier) {
        this.model = model;
        this.toolSupplier = toolSupplier;
        this.painter = new GeometricalObjectPainter();

        MouseForwarder mf = new MouseForwarder();
        addMouseListener(mf);
        addMouseMotionListener(mf);
    }

    /**
     * Schedules a repaint.
     *
     * @param source ignored
     * @param start  ignored
     * @param end    ignored
     */
    @Override
    public void objectsAdded(DrawingModel source, int start, int end) {
        repaint();
    }

    /**
     * Schedules a repaint.
     *
     * @param source ignored
     * @param start  ignored
     * @param end    ignored
     */
    @Override
    public void objectsRemoved(DrawingModel source, int start, int end) {
        repaint();
    }

    /**
     * Schedules a repaint.
     *
     * @param source ignored
     * @param start  ignored
     * @param end    ignored
     */
    @Override
    public void objectsChanged(DrawingModel source, int start, int end) {
        repaint();
    }

    /**
     * Paints the model's components, then forwards the call to the currently selected tool.
     *
     * @param g the graphics to paint on
     */
    @Override
    public void paintComponent(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        painter.setG2d(g2d);
        for (int i = 0; i < model.getSize(); i++) {
            model.getObject(i).accept(painter);
        }

        toolSupplier.get().paint(g2d);
    }

    /**
     * Forwards mouse movement and clicks to the current tool
     */
    private class MouseForwarder extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            JDrawingCanvas.this.toolSupplier.get().mouseClicked(e);
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            JDrawingCanvas.this.toolSupplier.get().mouseMoved(e);
            repaint();
        }
    }
}
