package hr.fer.zemris.java.hw17.jvdraw.editors;

import hr.fer.zemris.java.hw17.jvdraw.color.JColorArea;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledCircle;

import javax.swing.*;
import java.awt.*;

/** The filled circle editor. Adds background color editing to the base circle editor. */
public class FilledCircleEditor extends CircleEditor {

    /** The background color. */
    private final JColorArea bgArea;
    /** The circle being edited. */
    private final FilledCircle filledCircle;

    public FilledCircleEditor(FilledCircle filledCircle) {
        super(filledCircle);

        ((GridLayout) getLayout()).setRows(5);

        this.bgArea = new JColorArea("Background", filledCircle.getBgColor());
        this.filledCircle = filledCircle;

        add(new JLabel("Background"));
        add(bgArea);
    }

    /**
     * Calls super and sets the background color.
     */
    @Override
    public void acceptEditing() {
        super.acceptEditing();
        filledCircle.setBgColor(bgArea.getSelectedColor());
    }
}
