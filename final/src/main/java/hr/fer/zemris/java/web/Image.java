package hr.fer.zemris.java.web;

import hr.fer.zemris.java.hw17.jvdraw.GeometricalObjectParser;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectPainter;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "image", urlPatterns = "/image")
public class Image extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");

        if (name == null) {
            resp.sendError(403, "No name provided");
            return;
        }

        Path file = Paths.get(req.getServletContext().getRealPath("/WEB-INF/images")).resolve(name);

        if (!Files.exists(file)) {
            resp.sendError(404);
            return;
        }

        List<GeometricalObject> objects;
        try {
            objects = Files
                .lines(file)
                .map(GeometricalObjectParser::getObject)
                .collect(Collectors.toList());
        } catch (IllegalArgumentException ex) {
            resp.sendError(403, "Bad file");
            return;
        }


        GeometricalObjectBBCalculator calculator = new GeometricalObjectBBCalculator();
        objects.forEach(o -> o.accept(calculator));
        Rectangle bb = calculator.getBoundingBox();


        BufferedImage bim = new BufferedImage(bb.width, bb.height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = bim.createGraphics();
        g2d.translate(-bb.x, -bb.y);

        GeometricalObjectPainter painter = new GeometricalObjectPainter(g2d);
        objects.forEach(o -> o.accept(painter));
        g2d.dispose();

        resp.setContentType("img/png");
        ImageIO.write(bim, "png", resp.getOutputStream());
    }
}
