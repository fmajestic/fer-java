package hr.fer.zemris.java.hw17.jvdraw.drawing.tools;

import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Line;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectPainter;

import java.awt.*;
import java.awt.event.MouseEvent;

/** The line tool. */
public class LineTool extends AbstractTool {

    /** The model to add the final line to. */
    private final DrawingModel model;

    /** The foreground color provider. */
    private final IColorProvider fg;

    /** The painter. */
    private final GeometricalObjectPainter painter;


    /** The current line. */
    private Line line;

    /** Is the tool in edit mode? */
    private boolean editMode = false;

    public LineTool(DrawingModel model, IColorProvider fg) {
        this.model = model;
        this.fg = fg;
        this.painter = new GeometricalObjectPainter();
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() != 1) {
            return;
        }

        if (!editMode) {
            line = new Line(e.getPoint(), e.getPoint(), fg.getSelectedColor());
            editMode = true;
        } else {
            line.setEnd(e.getPoint());
            model.add(line);
            editMode = false;
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (editMode) {
            line.setEnd(e.getPoint());
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        if (editMode) {
            painter.setG2d(g2d).visit(line);
        }
    }
}
