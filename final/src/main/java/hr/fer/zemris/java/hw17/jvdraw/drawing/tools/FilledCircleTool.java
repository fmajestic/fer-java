package hr.fer.zemris.java.hw17.jvdraw.drawing.tools;

import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectPainter;

import java.awt.*;
import java.awt.event.MouseEvent;

/** The filled circle tool. */
public class FilledCircleTool extends AbstractTool {

    /** The model to add the final circle to. */
    private final DrawingModel model;

    /** The foreground color provider. */
    private final IColorProvider fg;

    /** The background color provider. */
    private final IColorProvider bg;

    /** The painter. */
    private final GeometricalObjectPainter painter;


    /** The current citcle. */
    private FilledCircle fCircle;

    /** Is the tool in edit mode= */
    private boolean editMode = false;

    public FilledCircleTool(DrawingModel model, IColorProvider fg, IColorProvider bg) {
        this.fg = fg;
        this.bg = bg;
        this.model = model;
        this.painter = new GeometricalObjectPainter();
    }


    /**
     * Same as regulat circle tool, but with a different stored type.
     *
     * @param e the click location
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() != 1) {
            return;
        }

        if (!editMode) {
            fCircle = new FilledCircle(e.getPoint(), 0, fg.getSelectedColor(), bg.getSelectedColor());
            editMode = true;
        } else {
            fCircle.setRadius((int) Math.round(e.getPoint().distance(fCircle.getCenter())));
            model.add(fCircle);
            editMode = false;
        }
    }

    /**
     * Same as regular circle tool.
     *
     * @param e the move point
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        if (editMode) {
            fCircle.setRadius((int) Math.round(e.getPoint().distance(fCircle.getCenter())));
        }
    }

    /**
     * Paints the circle using the visitor.
     *
     * @param g2d the graphics to paint on
     */
    @Override
    public void paint(Graphics2D g2d) {
        if (editMode) {
            painter.setG2d(g2d).visit(fCircle);
        }
    }
}
