package hr.fer.zemris.java.hw17.jvdraw;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.*;

import java.awt.*;

public class GeometricalObjectParser {

    /**
     * Gets an object from a string definition.
     * <p>
     * The supported definitions are:
     * <pre>
     *     LINE    x1 y1 x2 y2 r g b
     *     CIRCLE  x y rad r g b
     *     FCIRCLE x y rad r g b r g b
     * </pre>
     *
     * @param def the string definition as given above
     *
     * @return the object
     */
    public static GeometricalObject getObject(String def) {

        String[] parts = def.split("\\s+");

        if (parts.length < 6) {
            throw new IllegalArgumentException("Invalid object definition in file: " + def);
        }

        String name = parts[0];

        try {
            if (name.equals("LINE")) {
                return new Line(
                    new Point(getInt(parts[1]), getInt(parts[2])),
                    new Point(getInt(parts[3]), getInt(parts[4])),
                    getColor(parts[5], parts[6], parts[7])
                );
            } else if (name.matches("F?CIRCLE")) {

                Point center = new Point(getInt(parts[1]), getInt(parts[2]));
                int radius = getInt(parts[3]);
                Color color = getColor(parts[4], parts[5], parts[6]);

                if (name.equals("CIRCLE")) {
                    return new Circle(center, radius, color);
                } else {
                    return new FilledCircle(center, radius, color, getColor(parts[7], parts[8], parts[9]));
                }
            } else if (name.equals("FTRIANGLE")) {
                Point pt1 = new Point(getInt(parts[1]), getInt(parts[2]));
                Point pt2 = new Point(getInt(parts[3]), getInt(parts[4]));
                Point pt3 = new Point(getInt(parts[5]), getInt(parts[6]));
                Color fg = getColor(parts[7], parts[8], parts[9]);
                Color bg = getColor(parts[10], parts[11], parts[12]);

                return new FilledTriangle(pt1, pt2, pt3, fg, bg);
            } else {
                throw new IllegalArgumentException("Unknown object name: " + name);
            }
        } catch (IndexOutOfBoundsException ex) {
            throw new IllegalArgumentException("Not enough parameters in definition: " + def, ex);
        }
    }

    /**
     * Gets a color from string parts.
     *
     * @param r the red
     * @param g the green
     * @param b the blue
     *
     * @return the color
     */
    private static Color getColor(String r, String g, String b) {
        return new Color(getInt(r), getInt(g), getInt(b));
    }

    /**
     * Parses an int.
     *
     * @param s the string to parse
     *
     * @return the parsed number
     */
    private static int getInt(String s) {
        return Integer.parseInt(s);
    }
}
