package hr.fer.zemris.java.hw17.jvdraw.visitors;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledTriangle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Line;

/**
 * Models a visitor that can perform actions on geometric objects.
 */
public interface GeometricalObjectVisitor {

    void visit(Line line);

    void visit(Circle circle);

    void visit(FilledCircle filledCircle);

    void visit(FilledTriangle filledTriangle);
}