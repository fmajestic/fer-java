package hr.fer.zemris.java.hw17.jvdraw.actions;

import hr.fer.zemris.java.hw17.jvdraw.GeometricalObjectParser;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/** The open action. */
public class Open extends JVDrawAction {

    private static final String NAME = "Open";

    public Open(JVDraw app, DrawingModel model) {
        super(NAME, app, model);
    }

    /**
     * Attempts to open and read JVD file.
     *
     * @param e the event
     */
    @Override
    public void actionPerformed(ActionEvent e) {


        JFileChooser jfc = new JFileChooser(".");
        jfc.setAcceptAllFileFilterUsed(false);
        jfc.setFileFilter(new FileNameExtensionFilter("JVD file, *.jvd", "jvd"));

        if (jfc.showSaveDialog(app) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        Path path = jfc.getSelectedFile().toPath();
        List<GeometricalObject> objects;

        try {
            objects = Files
                .lines(path)
                .filter(str -> !str.isEmpty() && !str.isBlank())
                .map(GeometricalObjectParser::getObject)
                .collect(Collectors.toList());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(app, "IO error: " + ex.getMessage(), "IO error", JOptionPane.ERROR_MESSAGE);
            return;
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(app, "Format error: " + ex.getMessage(), "Format error",
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        model.clear();
        objects.forEach(model::add);
        model.clearModifiedFlag();
        app.setFilePath(path);
    }
}