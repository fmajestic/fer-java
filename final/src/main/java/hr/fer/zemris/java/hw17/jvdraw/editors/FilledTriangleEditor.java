package hr.fer.zemris.java.hw17.jvdraw.editors;

import hr.fer.zemris.java.hw17.jvdraw.color.JColorArea;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledTriangle;

import javax.swing.*;
import java.awt.*;

public class FilledTriangleEditor extends GeometricalObjectEditor {

    private final JColorArea fg;
    private final JColorArea bg;
    private final FilledTriangle triangle;

    private final JTextField pt1x;
    private final JTextField pt1y;
    private final JTextField pt2x;
    private final JTextField pt2y;
    private final JTextField pt3x;
    private final JTextField pt3y;

    public FilledTriangleEditor(FilledTriangle triangle) {
        this.triangle = triangle;
        this.fg = new JColorArea("Foreground", triangle.getColor());
        this.bg = new JColorArea("Background", triangle.getBgColor());

        setLayout(new GridLayout(5, 4));

        add(new JLabel("Pt1x"));
        add(pt1x = new JTextField(String.valueOf(triangle.getPt1().x)));

        add(new JLabel("Pt1x"));
        add(pt1y = new JTextField(String.valueOf(triangle.getPt1().y)));

        add(new JLabel("Pt1x"));
        add(pt2x = new JTextField(String.valueOf(triangle.getPt2().x)));

        add(new JLabel("Pt1x"));
        add(pt2y = new JTextField(String.valueOf(triangle.getPt2().y)));

        add(new JLabel("Pt1x"));
        add(pt3x = new JTextField(String.valueOf(triangle.getPt3().x)));

        add(new JLabel("Pt1x"));
        add(pt3y = new JTextField(String.valueOf(triangle.getPt3().y)));

        add(new JLabel("Foreground"));
        add(fg);

        add(new JLabel("Background"));
        add(bg);
    }

    @Override
    public void checkEditing() {
        try {
            Integer.parseInt(pt1x.getText());
            Integer.parseInt(pt1y.getText());
            Integer.parseInt(pt2x.getText());
            Integer.parseInt(pt2y.getText());
            Integer.parseInt(pt3x.getText());
            Integer.parseInt(pt3y.getText());
        } catch (NumberFormatException ex) {
            throw new IllegalStateException("Invalid number:" + ex.getMessage());
        }
    }

    @Override
    public void acceptEditing() {
        checkEditing();

        triangle.setPt1(new Point(Integer.parseInt(pt1x.getText()), Integer.parseInt(pt1y.getText())));
        triangle.setPt2(new Point(Integer.parseInt(pt2x.getText()), Integer.parseInt(pt2y.getText())));
        triangle.setPt3(new Point(Integer.parseInt(pt3x.getText()), Integer.parseInt(pt3y.getText())));

        triangle.setFgColor(fg.getSelectedColor());
        triangle.setBgColor(bg.getSelectedColor());
    }
}
