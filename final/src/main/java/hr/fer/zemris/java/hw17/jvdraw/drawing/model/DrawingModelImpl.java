package hr.fer.zemris.java.hw17.jvdraw.drawing.model;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObjectListener;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/** The model implementation. */
public class DrawingModelImpl implements DrawingModel {

    /** The objects in the model. */
    private final List<GeometricalObject> objects = new CopyOnWriteArrayList<>();

    /** The listeners. */
    private final List<DrawingModelListener> listeners = new CopyOnWriteArrayList<>();

    /** The listener attached to each added object. */
    private final GeometricalObjectListener objListener =
        o -> listeners.forEach(l -> {
            int oi = indexOf(o);
            l.objectsChanged(this, oi, oi);
        });

    /** Stores the modification state. */
    private boolean modified;

    /** {@inheritDoc} */
    @Override
    public int getSize() {
        return objects.size();
    }

    /** {@inheritDoc} */
    @Override
    public GeometricalObject getObject(int index) {
        return objects.get(index);
    }

    /** {@inheritDoc} */
    @Override
    public void add(GeometricalObject object) {
        modified = true;

        objects.add(object);
        object.addGeometricalObjectListener(objListener);

        int index = objects.size() - 1;
        listeners.forEach(l -> l.objectsAdded(this, index, index));
    }

    /** {@inheritDoc} */
    @Override
    public void remove(GeometricalObject object) {
        int index = objects.indexOf(object);

        if (index != -1) {
            modified = true;

            objects.remove(index);
            object.removeGeometricalObjectListener(objListener);

            listeners.forEach(l -> l.objectsRemoved(this, 0, getSize() - 1));
        }
    }

    /** {@inheritDoc} */
    @Override
    public void changeOrder(GeometricalObject object, int offset) {
        int index = objects.indexOf(object);

        if (index == -1) {
            throw new IllegalArgumentException(
                String.format("Model doesn't contain object: [%s].", String.valueOf(object))
            );
        }

        if (index + offset >= objects.size()) {
            throw new IndexOutOfBoundsException(
                String.format("Offset %d out of bounds for length %d.", offset, objects.size())
            );
        }

        Collections.swap(objects, index, index + offset);

        modified = true;
        listeners.forEach(l -> l.objectsChanged(this, index, index + offset));
    }

    /** {@inheritDoc} */
    @Override
    public int indexOf(GeometricalObject object) {
        return objects.indexOf(object);
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        modified = true;
        objects.clear();
        listeners.forEach(l -> l.objectsChanged(this, 0, getSize() - 1));
    }

    /** {@inheritDoc} */
    @Override
    public void clearModifiedFlag() {
        modified = false;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isModified() {
        return modified;
    }

    /** {@inheritDoc} */
    @Override
    public void addDrawingModelListener(DrawingModelListener l) {
        listeners.add(l);
    }

    /** {@inheritDoc} */
    @Override
    public void removeDrawingModelListener(DrawingModelListener l) {
        listeners.remove(l);
    }
}
