package hr.fer.zemris.java.hw17.jvdraw.color;

import javax.swing.*;
import java.awt.*;

/** A label displaying the current colors. */
public class JColorLabel extends JLabel implements ColorChangeListener {

    /** The foreground color provider. */
    private final IColorProvider fg;

    /** The background color provider. */
    private final IColorProvider bg;

    public JColorLabel(IColorProvider fg, IColorProvider bg) {
        this.fg = fg;
        this.bg = bg;

        fg.addColorChangeListener(this);
        bg.addColorChangeListener(this);

        newColorSelected(null, null, null);
    }

    @Override
    public void newColorSelected(IColorProvider source, Color oldColor, Color newColor) {
        setText(String.format(
            "Foreground color: %s, background color: %s",
            string(fg.getSelectedColor()),
            string(bg.getSelectedColor())
        ));
    }

    /**
     * Gets a color as a string in the form of (r, g, b).
     *
     * @param c the color
     *
     * @return the string
     */
    private String string(Color c) {
        return String.format("(%d, %d, %d)", c.getRed(), c.getGreen(), c.getBlue());
    }
}
