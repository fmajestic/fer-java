package hr.fer.zemris.java.hw17.jvdraw;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * Utility class with a few static methods.
 */
public class Util {

    /**
     * Gets the extension from a file.
     *
     * @param file the file
     *
     * @return the extension
     */
    public static String ext(Path file) {

        String name = file.getFileName().toString();
        int i = name.lastIndexOf('.');

        if (i > 0 && i < name.length() - 1) {
            return name.substring(i + 1);
        }

        return "";
    }

    /**
     * Prompts the user for a file overwrite.
     *
     * @param app the parent container
     *
     * @return true if the old file can be overwrittten, false otherwise
     */
    public static boolean promptOverwrite(JFrame app) {
        return JOptionPane.showConfirmDialog(
            app,
            "File already exists. Replace with the new file?",
            "File exists",
            JOptionPane.YES_NO_OPTION
        ) == JOptionPane.YES_OPTION;
    }

    /**
     * Prompts the user for a file save path tha has the required extension.
     *
     * @param app        the parent component
     * @param filterName the filter name
     * @param extensions the filter extensions
     *
     * @return the selected path, or null on a cancel
     */
    public static Path promptSavePath(JFrame app, String filterName, String... extensions) {

        Path file;
        JFileChooser jfc = new JFileChooser(".");
        jfc.setAcceptAllFileFilterUsed(false);
        jfc.addChoosableFileFilter(new FileNameExtensionFilter(filterName, extensions));

        while (true) {
            if (jfc.showSaveDialog(app) != JFileChooser.APPROVE_OPTION) {
                file = null;
                break;
            }

            file = jfc.getSelectedFile().toPath();
            String ext = ext(file);

            if (!Arrays.asList(extensions).contains(ext) || Files.exists(file) && !promptOverwrite(app)) {
                JOptionPane.showMessageDialog(
                    app,
                    "Invalid extension. Supported: " + Arrays.toString(extensions),
                    "Extension error",
                    JOptionPane.ERROR_MESSAGE
                );
            } else {
                break;
            }
        }

        return file;
    }
}
