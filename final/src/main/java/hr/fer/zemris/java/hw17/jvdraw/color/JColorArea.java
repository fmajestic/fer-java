package hr.fer.zemris.java.hw17.jvdraw.color;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/** A color area with an on-click picker. */
public class JColorArea extends JComponent implements IColorProvider {

    /** The name of the color being picked. */
    private final String name;

    /** The liseners. */
    private final List<ColorChangeListener> listeners = new CopyOnWriteArrayList<>();

    /** The currently selected color. */
    private Color selectedColor;


    public JColorArea(String name, Color initial) {
        this.name = name;
        this.selectedColor = initial;
        addMouseListener(new ChooseColor());
    }

    /**
     * Paints a 15x15 square of the currently selected color.
     *
     * @param g the graphics to paint on
     */
    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(selectedColor);
        g.fillRect(0, 0, getWidth(), getHeight());
    }


    /**
     * Returns a fixed 15x15.
     *
     * @return 15x15
     */
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(15, 15);
    }

    /**
     * Returns {@link #getPreferredSize()}.
     *
     * @return the <i>preferred</i> size
     */
    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    /**
     * Returns {@link #getPreferredSize()}.
     *
     * @return the <i>preferred</i> size
     */
    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }


    /** {@inheritDoc} */
    @Override
    public Color getSelectedColor() {
        return selectedColor;
    }

    /** {@inheritDoc} */
    @Override
    public void addColorChangeListener(ColorChangeListener l) {
        listeners.add(l);
    }

    /** {@inheritDoc} */
    @Override
    public void removeColorChangeListener(ColorChangeListener l) {
        listeners.remove(l);
    }

    /** Creates a color picker on click. */
    private class ChooseColor extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            Color oldColor = selectedColor;
            Color newColor = JColorChooser.showDialog(
                JColorArea.this,
                String.format("Choose a %s color", name),
                selectedColor
            );

            if (newColor != null && !oldColor.equals(newColor)) {
                selectedColor = newColor;
                listeners.forEach(l -> l.newColorSelected(JColorArea.this, oldColor, newColor));
                repaint();
            }
        }
    }
}
