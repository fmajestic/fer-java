package hr.fer.zemris.java.hw17.jvdraw.drawing.objects;

/** A listener for geometrical object changes. */
public interface GeometricalObjectListener {

    /**
     * Fired when an object's property changes.
     *
     * @param o the changed object
     */
    void geometricalObjectChanged(GeometricalObject o);
}
