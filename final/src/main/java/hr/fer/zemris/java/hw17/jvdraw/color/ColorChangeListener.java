package hr.fer.zemris.java.hw17.jvdraw.color;

import java.awt.*;

/**
 * A listener for color provider changes.
 *
 * @see IColorProvider
 */
public interface ColorChangeListener {

    /**
     * Fired when a new color is selected.
     *
     * @param source   the source
     * @param oldColor the old color
     * @param newColor the new color
     */
    void newColorSelected(IColorProvider source, Color oldColor, Color newColor);
}
