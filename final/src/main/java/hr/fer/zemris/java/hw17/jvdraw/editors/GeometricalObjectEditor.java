package hr.fer.zemris.java.hw17.jvdraw.editors;

import javax.swing.*;

/** Base class for all object editors. */
public abstract class GeometricalObjectEditor extends JPanel {

    /**
     * Checks if all the input values are legal.
     *
     * @throws IllegalStateException if any argument is invalid
     */
    public abstract void checkEditing();

    /**
     * Calls {@link #checkEditing()} and stores the data in the edited object.
     */
    public abstract void acceptEditing();
}
