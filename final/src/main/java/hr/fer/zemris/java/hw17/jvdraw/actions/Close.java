package hr.fer.zemris.java.hw17.jvdraw.actions;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

/** The close action. */
public class Close extends JVDrawAction {

    private static final String NAME = "Close";

    public Close(JVDraw app, DrawingModel model) {
        super(NAME, app, model);
    }

    /**
     * Dispatches a new {@link WindowEvent#WINDOW_CLOSING} event to the main app.
     *
     * @param e the event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        app.dispatchEvent(new WindowEvent(app, WindowEvent.WINDOW_CLOSING));
    }
}
