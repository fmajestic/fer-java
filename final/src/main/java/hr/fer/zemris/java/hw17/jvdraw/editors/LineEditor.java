package hr.fer.zemris.java.hw17.jvdraw.editors;

import hr.fer.zemris.java.hw17.jvdraw.color.JColorArea;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Line;

import javax.swing.*;
import java.awt.*;

/** The line editor. */
public class LineEditor extends GeometricalObjectEditor {

    private final JTextField originX;
    private final JTextField originY;
    private final JTextField endX;
    private final JTextField endY;
    private final JColorArea fgArea;

    private final Line line;
    private final Point newStart;
    private final Point newEnd;

    public LineEditor(Line line) {
        this.line = line;
        this.fgArea = new JColorArea("Foreground", line.getColor());
        this.newStart = new Point(line.getStart());
        this.newEnd = new Point(line.getEnd());

        setLayout(new GridLayout(5, 2));

        add(new JLabel("Origin x"));
        add(originX = new JTextField(String.valueOf(line.getStart().x)));

        add(new JLabel("Origin y"));
        add(originY = new JTextField(String.valueOf(line.getStart().y)));

        add(new JLabel("End x"));
        add(endX = new JTextField(String.valueOf(line.getEnd().x)));

        add(new JLabel("End y"));
        add(endY = new JTextField(String.valueOf(line.getEnd().y)));

        add(new JLabel("Foreground"));
        add(this.fgArea);
    }

    @Override
    public void checkEditing() {
        try {
            newStart.x = Integer.parseInt(originX.getText().trim());
            newStart.y = Integer.parseInt(originY.getText().trim());
            newEnd.x = Integer.parseInt(endX.getText().trim());
            newEnd.y = Integer.parseInt(endY.getText().trim());
        } catch (NumberFormatException ex) {
            throw new IllegalStateException("Invalid number; " + ex.getMessage(), ex);
        }

        if (newStart.equals(newEnd)) {
            throw new IllegalStateException("Line can't begin and end in the same point");
        }
    }

    @Override
    public void acceptEditing() {
        checkEditing(); // Safeguard if someone tries to accept without checking.

        line.setFgColor(fgArea.getSelectedColor());
        line.setStart(newStart);
        line.setEnd(newEnd);
    }
}
