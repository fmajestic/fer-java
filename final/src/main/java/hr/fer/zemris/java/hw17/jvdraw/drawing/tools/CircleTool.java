package hr.fer.zemris.java.hw17.jvdraw.drawing.tools;

import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectPainter;

import java.awt.*;
import java.awt.event.MouseEvent;

/** The circle drawing tool. */
public class CircleTool extends AbstractTool {

    /** The model to add the final circle to. */
    private final DrawingModel model;

    /** The foreground color provider. */
    private final IColorProvider fg;

    /** The circle painter for the duration of the drawing. */
    private final GeometricalObjectPainter painter;


    /** The current circle. */
    private Circle circle;

    /** Is the tool in edit mode (between two clicks?) */
    private boolean editMode = false;

    public CircleTool(DrawingModel model, IColorProvider fg) {
        this.model = model;
        this.fg = fg;
        this.painter = new GeometricalObjectPainter();
    }

    /**
     * Stores a new circle at the clicked location.
     *
     * @param e the click location
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() != 1) {
            return;
        }

        if (!editMode) {
            circle = new Circle(e.getPoint(), 0, fg.getSelectedColor());
            editMode = true;
        } else {
            circle.setRadius((int) Math.round(e.getPoint().distance(circle.getCenter())));
            model.add(circle);
            editMode = false;
        }
    }

    /**
     * Stores a new radius for the circle based on the distance from the first click.
     *
     * @param e the move point
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        if (editMode) {
            circle.setRadius((int) Math.round(e.getPoint().distance(circle.getCenter())));
        }
    }

    /**
     * Paints the circle on a graphics
     *
     * @param g2d
     */
    @Override
    public void paint(Graphics2D g2d) {
        if (editMode) {
            painter.setG2d(g2d).visit(circle);
        }
    }
}
