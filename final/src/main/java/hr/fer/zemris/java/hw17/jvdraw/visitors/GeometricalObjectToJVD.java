package hr.fer.zemris.java.hw17.jvdraw.visitors;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledTriangle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Line;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The JVD visitor.
 */
public class GeometricalObjectToJVD implements GeometricalObjectVisitor {

    private final List<String> lines = new ArrayList<>();

    public List<String> getLines() {
        return lines;
    }

    @Override
    public void visit(Line line) {
        lines.add(String.format(
            "LINE %s %s %s",
            string(line.getStart()),
            string(line.getEnd()),
            string(line.getColor())
        ));
    }

    @Override
    public void visit(Circle circle) {
        lines.add(String.format(
            "CIRCLE %s %d %s",
            string(circle.getCenter()),
            circle.getRadius(),
            string(circle.getColor())
        ));
    }

    @Override
    public void visit(FilledCircle filledCircle) {
        lines.add(String.format(
            "FCIRCLE %s %d %s %s",
            string(filledCircle.getCenter()),
            filledCircle.getRadius(),
            string(filledCircle.getColor()),
            string(filledCircle.getBgColor())
        ));
    }

    @Override
    public void visit(FilledTriangle ft) {
        lines.add(String.format(
           "FTRIANGLE %s %s %s %s %s",
           string(ft.getPt1()),
           string(ft.getPt2()),
           string(ft.getPt3()),
           string(ft.getColor()),
           string(ft.getBgColor())
        ));
    }

    private static String string(Point p) {
        return String.format("%d %d", p.x, p.y);
    }

    private static String string(Color c) {
        return String.format("%d %d %d", c.getRed(), c.getGreen(), c.getBlue());
    }
}
