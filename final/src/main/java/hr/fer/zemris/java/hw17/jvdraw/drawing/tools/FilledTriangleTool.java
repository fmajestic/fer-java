package hr.fer.zemris.java.hw17.jvdraw.drawing.tools;

import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledTriangle;

import java.awt.*;
import java.awt.event.MouseEvent;

public class FilledTriangleTool extends AbstractTool {

    private final DrawingModel model;
    private final IColorProvider fg;
    private final IColorProvider bg;
    private Point pt1;
    private Point pt2;
    private Point pt3;
    private int clickCount = 0;

    public FilledTriangleTool(DrawingModel model, IColorProvider fg, IColorProvider bg) {
        this.model = model;
        this.fg = fg;
        this.bg = bg;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getClickCount() != 1) {
            return;
        }

        clickCount++;

        switch (clickCount) {
        case 1:
            pt1 = e.getPoint();
            break;

        case 2:
            pt2 = e.getPoint();
            break;

        case 3:
            pt3 = e.getPoint();
            model.add(new FilledTriangle(pt1, pt2, pt3, fg.getSelectedColor(), bg.getSelectedColor()));
            clickCount = 0;
            pt1 = null;
            pt2 = null;
            pt3 = null;
            break;

        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        switch (clickCount) {
        case 1:
            pt2 = e.getPoint();
            break;

        case 2:
            pt3 = e.getPoint();
            break;

        default:
            break;
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        switch (clickCount) {
        case 1:
            if (pt2 != null) {
                g2d.setColor(fg.getSelectedColor());
                g2d.drawLine(pt1.x, pt1.y, pt2.x, pt2.y);
            }
            break;

        case 2:
            if (pt3 == null) {
                g2d.setColor(fg.getSelectedColor());
                g2d.drawLine(pt1.x, pt1.y, pt2.x, pt2.y);

            } else {
                int[] xPoints = { pt1.x, pt2.x, pt3.x };
                int[] yPoints = { pt1.y, pt2.y, pt3.y };
                g2d.setColor(bg.getSelectedColor());
                g2d.fillPolygon(xPoints, yPoints, 3);
                g2d.setColor(fg.getSelectedColor());
                g2d.drawPolygon(xPoints, yPoints, 3);
            }
            break;

        default:
            break;
        }
    }
}
