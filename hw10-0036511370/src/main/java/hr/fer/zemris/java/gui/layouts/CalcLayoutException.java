package hr.fer.zemris.java.gui.layouts;


public class CalcLayoutException extends RuntimeException {

    static final long serialVersionUID = 1L;

    public CalcLayoutException() {
        super();
    }

    public CalcLayoutException(String message) {
        super(message);
    }

    public CalcLayoutException(String message, Throwable cause) {
        super(message, cause);
    }

    public CalcLayoutException(Throwable cause) {
        super(cause);
    }
}
