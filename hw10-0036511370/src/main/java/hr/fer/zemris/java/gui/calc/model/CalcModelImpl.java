package hr.fer.zemris.java.gui.calc.model;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.DoubleBinaryOperator;

import static java.lang.Math.abs;

/** Implementation of the simple calculator model. */
public class CalcModelImpl implements CalcModel {

    /** All listeners to notify of a value change. */
    private List<CalcValueListener> listeners;


    /** The stored value. */
    private double value;

    /** The stored digits. */
    private String digits;

    /** Stored value is positive. */
    private boolean positive;

    /** Model is editable. */
    private boolean editable;


    /** Currently active operand. */
    private Double activeOperand;

    /** Operation waiting for another operand. */
    private DoubleBinaryOperator pendingOperation;

    /**
     * Initializes a new calculator model.
     */
    public CalcModelImpl() {
        value = 0;
        digits = "";
        positive = true;
        editable = true;
        listeners = new CopyOnWriteArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    public void addCalcValueListener(CalcValueListener l) {
        listeners.add(Objects.requireNonNull(l));
    }

    /** {@inheritDoc} */
    @Override
    public void removeCalcValueListener(CalcValueListener l) {
        listeners.remove(Objects.requireNonNull(l));
    }

    /** {@inheritDoc} */
    @Override
    public double getValue() {
        return value;
    }

    /** {@inheritDoc} */
    @Override
    public void setValue(double value) {
        editable = false;

        this.value = value;
        this.positive = value >= 0;
        this.digits = String.format(Locale.ROOT, "%s", abs(value));

        notifyObservers();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEditable() {
        return editable;
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        value = 0;
        digits = "";
        positive = true;
        editable = true;

        notifyObservers();
    }

    /** {@inheritDoc} */
    @Override
    public void clearAll() {
        clear();
        activeOperand = null;
        pendingOperation = null;
    }

    /** {@inheritDoc} */
    @Override
    public void swapSign() throws CalculatorInputException {
        requireEditable();

        value = -value;
        positive = !positive;

        notifyObservers();
    }

    /** {@inheritDoc} */
    @Override
    public void insertDecimalPoint() throws CalculatorInputException {
        requireEditable();

        if (digits.isEmpty() || digits.contains(".")) {
            throw new CalculatorInputException("Value already contains a decimal point.");
        }

        digits = digits + ".";

        notifyObservers();
    }

    /** {@inheritDoc} */
    @Override
    public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
        requireEditable();

        try {
            value = Double.parseDouble((positive ? "+" : "-") + digits + digit);

            if (Double.isInfinite(value)) {
                throw new CalculatorInputException("Value would become too large to store.");
            }

            if (digits.equals("0")) {
                if (digit != 0) {
                    digits = String.valueOf(digit);
                }
            } else {
                digits += digit;
            }

        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid digit.");
        }

        notifyObservers();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isActiveOperandSet() {
        return activeOperand != null;
    }

    /** {@inheritDoc} */
    @Override
    public double getActiveOperand() throws IllegalStateException {
        if (activeOperand == null) {
            throw new IllegalStateException("Active operand not set.");
        }

        return activeOperand;
    }

    /** {@inheritDoc} */
    @Override
    public void setActiveOperand(double activeOperand) {
        this.activeOperand = activeOperand;
    }

    /** {@inheritDoc} */
    @Override
    public void clearActiveOperand() {
        activeOperand = null;
    }

    /** {@inheritDoc} */
    @Override
    public DoubleBinaryOperator getPendingBinaryOperation() {
        return pendingOperation;
    }

    /** {@inheritDoc} */
    @Override
    public void setPendingBinaryOperation(DoubleBinaryOperator op) {
        pendingOperation = op;
    }

    /**
     * Checks if the calculator is editable and throws an exception if it isn't.
     *
     * @throws CalculatorInputException if the calculator is not editable
     * @see #editable
     * @see #setValue(double)
     */
    private void requireEditable() {
        if (!editable) {
            throw new CalculatorInputException("Calculator is not editable.");
        }
    }

    /**
     * Notifies all listeners of a value change.
     *
     * @see CalcValueListener
     */
    private void notifyObservers() {
        listeners.forEach(l -> l.valueChanged(this));
    }

    /**
     * Returns the string representation of the currently stored digits
     *
     * @return the currently stored digits as a string
     */
    @Override
    public String toString() {
        return String.format(
            "%s%s",
            positive ? "" : "-",
            digits.length() == 0 ? "0" : digits
        );
    }
}
