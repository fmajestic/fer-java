package hr.fer.zemris.java.gui.charts;

/** Stores two integer values. */
public class XYValue {

    /** The x value. */
    private int x;

    /** The y value. */
    private int y;

    /**
     * Constructor
     *
     * @param x the x value
     * @param y the y value
     */
    public XYValue(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets the x value
     *
     * @return the x value
     */
    public int getX() {
        return x;
    }

    /**
     * Gets the y value
     *
     * @return the y value
     */
    public int getY() {
        return y;
    }
}
