package hr.fer.zemris.java.gui.layouts;

/** Models a calculator layout grid position. */
public class RCPosition {

    /** The x coordinate. */
    private int x;

    /** The y coordinate. */
    private int y;

    /**
     * Creates a new position.
     *
     * @param row the row
     * @param col the column
     */
    public RCPosition(int row, int col) {
        this.x = col;
        this.y = row;
    }

    /**
     * Gets the x coordinate
     *
     * @return the x coordinate
     */
    public int getX() {
        return x;
    }

    /**
     * Gets the y coordinate
     *
     * @return the y coordinate
     */
    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return String.format("RCPosition [x=%d,y=%d]", x, y);
    }
}
