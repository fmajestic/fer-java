package hr.fer.zemris.java.gui.charts;

import java.util.List;

public class BarChart {

    /** Values to be displayed in the chart. */
    private List<XYValue> values;

    /** Description of the x axis. */
    private String descriptionX;

    /** Description of the y axis. */
    private String descriptionY;

    /** First y value. */
    private int yMin;

    /** Last y value. */
    private int yMax;

    /** Y value step. */
    private int yStep;

    /**
     * Creates a new bar chart.
     * <p>
     * If any of the values contain a y-value smaller than the minimum, an exception is thrown.
     *
     * @param values       the values to draw
     * @param descriptionX description of the x axis
     * @param descriptionY description of the y axis
     * @param yMin         first y value
     * @param yMax         last y value
     * @param yStep        y value step
     *
     * @throws IllegalArgumentException if the values contain an illegal value
     */
    public BarChart(List<XYValue> values, String descriptionX, String descriptionY, int yMin, int yMax, int yStep) {

        if (values.stream().anyMatch(value -> value.getY() < yMin)) {
            throw new IllegalArgumentException("Values contain a y-value below the specified minimum.");
        }

        this.values = values;
        this.yMin = yMin;
        this.yMax = yMax;
        this.yStep = yStep;
        this.descriptionX = descriptionX;
        this.descriptionY = descriptionY;
    }

    public List<XYValue> getValues() {
        return values;
    }

    public String getDescriptionX() {
        return descriptionX;
    }

    public String getDescriptionY() {
        return descriptionY;
    }

    public int getyMin() {
        return yMin;
    }

    public int getyMax() {
        return yMax;
    }

    public int getyStep() {
        return yStep;
    }
}
