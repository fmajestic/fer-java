package hr.fer.zemris.java.gui.calc.model;

import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

import javax.swing.*;
import java.awt.*;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import static hr.fer.zemris.java.gui.calc.model.CalculatorUtil.*;

/** A simple GUI calculator. */
public class Calculator extends JFrame {

    /** Maps buttons to regular and inverted names. */
    private Map<JButton, String[]> functionButtons;

    /** Is the inverted checkbox selected? */
    private boolean inverted;

    /** The calculator display. */
    private JLabel display;

    /** The model. */
    private CalcModel model;

    /** The stack. */
    private Stack<Double> stack;

    /** Initializes collections and GUI. */
    private Calculator() {
        stack = new Stack<>();
        functionButtons = new HashMap<>();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setSize(850, 400);
        initGUI();
        pack();
    }

    /**
     * Entry point of the program.
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Calculator calculator = new Calculator();
            calculator.setTitle("Java Calculator v1.0");
            calculator.setVisible(true);
        });
    }

    /** Creates and adds all of the components to the app. */
    private void initGUI() {
        model = new CalcModelImpl();
        Container pane = getContentPane();
        pane.setLayout(new CalcLayout(5));

        // Everything else
        addMiscComponents(pane, model);

        // Number buttons
        addNumberButtons(pane, model);

        // Binary operators
        addMainButtons(pane, model);

        // Functions
        addFunctionButtons(pane, model);
    }

    /**
     * Adds all the components that are not part of a "group":
     * display, equals, clear, reset, push, pop, invert.
     *
     * @param parent the parent to add to
     * @param model  the model to use
     */
    private void addMiscComponents(Container parent, CalcModel model) {
        // Calculator display
        display = new JLabel("0");
        display.setFont(display.getFont().deriveFont(30f));
        display.setHorizontalAlignment(SwingConstants.RIGHT);
        display.setBackground(Color.GREEN);
        display.setOpaque(true);
        display.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        this.model.addCalcValueListener(m -> display.setText(m.toString()));

        JButton equals = new JButton("=");
        enlargeFont(equals);

        JButton clr = new JButton("clr");
        JButton reset = new JButton("reset");
        JButton push = new JButton("push");
        JButton pop = new JButton("pop");
        JCheckBox invert = new JCheckBox("Inv");

        clr.addActionListener(ev -> model.clear());
        push.addActionListener(ev -> stack.push(model.getValue()));
        reset.addActionListener(ev -> model.clearAll());
        equals.addActionListener(ev -> executePendingOperation(this.model));
        pop.addActionListener(ev -> {
            try {
                this.model.setValue(stack.pop());
            } catch (EmptyStackException ignored) {
                JOptionPane.showMessageDialog(null, "The stack is empty.");
            }
        });
        invert.addActionListener(ev -> {
            inverted = !inverted;
            for (var kvp : functionButtons.entrySet()) {
                JButton btn = kvp.getKey();
                btn.setText(kvp.getValue()[inverted ? 1 : 0]);
            }
        });

        parent.add(display, "1,1");
        parent.add(equals, "1,6");
        parent.add(clr, "1,7");
        parent.add(reset, "2,7");
        parent.add(push, "3,7");
        parent.add(pop, "4,7");
        parent.add(invert, "5,7");
    }

    /**
     * Adds all of the number buttons 0-9.
     *
     * @param parent the parent to add to
     * @param model  the model to use
     */
    private void addNumberButtons(Container parent, CalcModel model) {
        int n = 9;

        for (int row = 2; row <= 4; row++) {
            for (int col = 5; col >= 3; col--) {
                int nn = n;
                JButton digit = new JButton(String.valueOf(n));
                enlargeFont(digit);
                digit.addActionListener(ev -> {
                    try {
                        model.insertDigit(nn);
                    } catch (CalculatorInputException ignored) {
                        JOptionPane.showMessageDialog(null, "Content not editable.");
                    }
                });
                parent.add(digit, new RCPosition(row, col));
                n--;
            }
        }

        JButton zero = new JButton(String.valueOf(0));
        enlargeFont(zero);
        zero.addActionListener(ev -> model.insertDigit(0));
        parent.add(zero, "5,3");
    }

    /**
     * Adds all of the "main" operation buttons: + - * / +- .
     *
     * @param parent the parent to add to
     * @param model  the model to use
     */
    private void addMainButtons(Container parent, CalcModel model) {

        JButton add = new JButton("+");
        enlargeFont(add);
        JButton sub = new JButton("-");
        enlargeFont(sub);
        JButton mul = new JButton("×");
        enlargeFont(mul);
        JButton div = new JButton("÷");
        enlargeFont(div);
        JButton negate = new JButton("+/-");
        enlargeFont(negate);
        JButton decimal = new JButton(".");
        enlargeFont(decimal);

        add.addActionListener(binaryOperation(Double::sum, model, display));
        sub.addActionListener(binaryOperation((a, b) -> a - b, model, display));
        mul.addActionListener(binaryOperation((a, b) -> a * b, model, display));
        div.addActionListener(binaryOperation((a, b) -> a / b, model, display));
        negate.addActionListener(ev -> {
            try {
                model.swapSign();
            } catch (CalculatorInputException ignored) {
                JOptionPane.showMessageDialog(null, "Content not editable.");
            }
        });
        decimal.addActionListener(ev -> {
            try {
                model.insertDecimalPoint();
            } catch (CalculatorInputException ex) {
                display.setText("ERROR");
            }
        });

        parent.add(add, "5,6");
        parent.add(sub, "4,6");
        parent.add(mul, "3,6");
        parent.add(div, "2,6");
        parent.add(negate, "5,4");
        parent.add(decimal, "5,5");
    }

    /**
     * Adds all of the function buttons.
     *
     * @param parent the parent to add to
     * @param model  the model to use
     */
    private void addFunctionButtons(Container parent, CalcModel model) {

        JButton reciprocal = new JButton("1/x");
        JButton log = new JButton("log");
        JButton ln = new JButton("log");
        JButton xn = new JButton("log");
        JButton sin = new JButton("sin");
        JButton cos = new JButton("cos");
        JButton tan = new JButton("tan");
        JButton ctg = new JButton("ctg");

        parent.add(reciprocal, "2,1");
        parent.add(xn, "5,1");
        parent.add(ln, "4,1");
        parent.add(log, "3,1");
        parent.add(sin, "2,2");
        parent.add(cos, "3,2");
        parent.add(tan, "4,2");
        parent.add(ctg, "5,2");

        functionButtons.put(ln, new String[] { "ln", "e^x" });
        functionButtons.put(xn, new String[] { "x^n", "x^1/n" });
        functionButtons.put(log, new String[] { "log", "10^x" });
        functionButtons.put(sin, new String[] { "sin", "arcsin" });
        functionButtons.put(cos, new String[] { "cos", "arccos" });
        functionButtons.put(tan, new String[] { "tan", "arctan" });
        functionButtons.put(ctg, new String[] { "ctg", "arcctg" });

        reciprocal.addActionListener(unaryOperation(
            a -> 1 / a,
            model, display
        ));

        log.addActionListener(invertibleUnaryOperation(
            Math::log10,
            a -> Math.pow(10, a),
            model, display, inverted
        ));
        ln.addActionListener(invertibleUnaryOperation(
            Math::log,
            Math::exp,
            model, display, inverted
        ));
        xn.addActionListener(invertibleBinaryOperation(
            Math::pow,
            (a, b) -> Math.pow(a, 1 / b),
            model, display, inverted
        ));
        sin.addActionListener(invertibleUnaryOperation(
            Math::sin,
            Math::asin,
            model, display, inverted
        ));
        cos.addActionListener(invertibleUnaryOperation(
            Math::cos,
            Math::acos,
            model, display, inverted
        ));
        tan.addActionListener(invertibleUnaryOperation(
            Math::tan,
            Math::atan,
            model, display, inverted
        ));
        ctg.addActionListener(invertibleUnaryOperation(
            a -> 1 / Math.tan(a),
            a -> Math.PI / 2 - Math.atan(a),
            model, display, inverted
        ));
    }
}
