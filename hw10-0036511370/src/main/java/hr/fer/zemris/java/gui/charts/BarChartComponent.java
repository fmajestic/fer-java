package hr.fer.zemris.java.gui.charts;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Math.PI;

/** A component that displays a {@link BarChart} */
@SuppressWarnings("FieldCanBeLocal")
public class BarChartComponent extends JComponent {

    /** The chart to display. */
    private BarChart chart;

    /** Width of the component. */
    private int width;

    /** Height of the component. */
    private int height;

    /** Left padding. */
    private int padLeft = 20;

    /** Right padding. */
    private int padRight = 20;

    /** Top padding. */
    private int padTop = 20;

    /** Bottom padding. */
    private int padBottom = 20;

    /**
     * Creates a new component that displays the chart
     * @param chart the chart to display
     */
    public BarChartComponent(BarChart chart) {
        super();
        this.chart = chart;
    }


    /**
     * Paints the chart
     * @param g the graphics to paint on
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        width = getSize().width;
        height = getSize().height;

        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fm = g2d.getFontMetrics();
        g2d.setFont(g2d.getFont().deriveFont(14f));

        paintDescriptions(g2d, padLeft, padBottom);

        paintGraph(
            g2d,
            padLeft + fm.getHeight() + 20,
            width - padRight,
            padTop,
            height - padBottom - fm.getHeight() - 20
        );
    }

    /** {@inheritDoc} */
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    /**
     * Paints the graph: the axes and columns
     * @param g2d the graphics to paint on
     * @param xmin the minimum x position
     * @param xmax the maximum x position
     * @param ymin the minimum y position
     * @param ymax the maximum y position
     */
    private void paintGraph(Graphics2D g2d, int xmin, int xmax, int ymin, int ymax) {

        FontMetrics fm = g2d.getFontMetrics();

        int min = chart.getyMin();
        int max = chart.getyMax();
        int step = chart.getyStep();
        String[] yMarks = makePoints(min, max, step);
        int maxMarkWidth = Arrays.stream(yMarks).mapToInt(fm::stringWidth).max().orElse(0);

        ymax -= 20;
        ymin += 10;
        int fullHeight = ymax - ymin + 1;
        int fullWidth = xmax - xmin + 1;
        int heightStep = fullHeight / (yMarks.length - 1);
        int barMin = 0, barMax = 0;


        for (int i = 0; i < yMarks.length; i++) {
            int mid = fm.getAscent() / 2;

            if (i == 0) {
                barMax = ymax - i * heightStep - mid;

                int x = xmax;
                int y = barMax + 1;
                g2d.fill(new Polygon(
                    new int[] { x, x + 12, x },
                    new int[] { y - 5, y, y + 5 },
                    3
                ));
            }

            g2d.drawString(
                yMarks[i],
                xmin + (maxMarkWidth - fm.stringWidth(yMarks[i])),
                ymax - i * heightStep
            );

            g2d.fill(new Rectangle(
                xmin + maxMarkWidth + 5,
                ymax - i * heightStep - mid,
                fullWidth - padRight,
                2
            ));

            barMin = ymax - i * heightStep - mid;
        }

        g2d.fill(new Rectangle(
            xmin + maxMarkWidth + 10,
            ymin - 20,
            2,
            fullHeight + 20
        ));

        int x = xmin + maxMarkWidth + 10;
        int y = ymin - 20;
        g2d.fill(new Polygon(
            new int[] { x - 5, x, x + 5 },
            new int[] { y, y - 12, y },
            3
        ));


        drawColumns(g2d, xmin + maxMarkWidth + 11, xmax - 10, barMin + 1, barMax);
    }

    /**
     * Paints the columns
     * @param g2d the graphics to paint on
     * @param xmin the minimum x position
     * @param xmax the maximum x position
     * @param ymin the minimum y position
     * @param ymax the maximum y position
     */
    private void drawColumns(Graphics2D g2d, int xmin, int xmax, int ymin, int ymax) {
        List<XYValue> values = chart
            .getValues()
            .stream()
            .sorted(Comparator.comparingInt(XYValue::getX))
            .collect(Collectors.toList());

        xmin += 1;
        int columnWidth = (xmax - xmin + 1) / values.size();
        int fullHeight = ymax - ymin + 1;

        for (int i = 0; i < values.size(); i++) {
            var xyv = values.get(i);
            var xx = xyv.getX();
            var yy = xyv.getY();

            int hh = (int) (((double) yy / chart.getyMax()) * fullHeight);

            int xc = xmin + i * columnWidth;
            int yc = ymin + (fullHeight - hh);
            g2d.setPaint(new GradientPaint(xc, yc, new Color(36,59,85), xc, yc + hh, new Color(20,30,48)));
            g2d.fill(new Rectangle(xc, yc, columnWidth, hh));

            g2d.setColor(Color.gray);
            g2d.fill(new Rectangle(xmin + (i + 1) * columnWidth - 1, ymin, 1, ymax - ymin + 1));
            g2d.setColor(Color.black);

            String strx = String.valueOf(xx);
            FontMetrics fm = g2d.getFontMetrics();
            g2d.drawString(
                strx,
                xmin + i * columnWidth + (columnWidth - fm.stringWidth(strx)) / 2,
                ymax + 5 + fm.getAscent()
            );
        }
    }

    /**
     * Paints the axis descriptions
     * @param g2d the graphics to paint on
     * @param padStart the left padding
     * @param padBottom the bottom padding
     */
    private void paintDescriptions(Graphics2D g2d, int padStart, int padBottom) {

        FontMetrics fm = g2d.getFontMetrics();

        String dx = chart.getDescriptionX();
        String dy = chart.getDescriptionY();

        g2d.drawString(dx, (width - fm.stringWidth(dx)) / 2, height - padBottom);

        AffineTransform save = g2d.getTransform();
        AffineTransform vertical = new AffineTransform();

        vertical.rotate(-PI / 2);
        g2d.setTransform(vertical);
        g2d.drawString(dy, -(height - fm.stringWidth(dy)) / 2, padStart);
        g2d.setTransform(save);
    }

    /**
     * Generates all points of the y axis as strings
     * @param min the minimum value
     * @param max the maximum value
     * @param step the step between values
     * @return all values as string
     */
    private String[] makePoints(int min, int max, int step) {
        String[] ret = new String[getCount(min, max, step)];

        for (int i = 0; i < ret.length; i++) {
            ret[i] = String.valueOf(i * step);
        }

        return ret;
    }

    /**
     * Calculates the number of values in the range
     * @param min the minimum value, inclusive
     * @param max the maximum value, inclusive
     * @param step the step between values
     * @return the number of values in the range
     */
    private int getCount(int min, int max, double step) {
        return (int) Math.ceil((max - min + 1) / step);
    }
}
