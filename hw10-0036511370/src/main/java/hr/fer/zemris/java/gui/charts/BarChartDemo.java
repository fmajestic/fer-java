package hr.fer.zemris.java.gui.charts;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BarChartDemo extends JFrame {

    private BarChartDemo(Path path, List<XYValue> values, String xDesc, String yDesc, int ymin, int ymax, int ystep) {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setSize(800, 600);

        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());

        JLabel file = new JLabel(path.toString());
        file.setHorizontalAlignment(SwingConstants.CENTER);
        pane.add(file, BorderLayout.NORTH);

        BarChartComponent chartComponent = new BarChartComponent(new BarChart(values, xDesc, yDesc, ymin, ymax, ystep));
        pane.add(chartComponent, BorderLayout.CENTER);

        pack();
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Expected 1 argument, got: " + args.length);
            return;
        }

        Path file;
        List<String> lines;

        try {
            file = Paths.get(args[0]);
            lines = Files.lines(file).limit(6).collect(Collectors.toUnmodifiableList());
        } catch (InvalidPathException ex) {
            System.out.println("Invalid path: " + ex.getMessage());
            return;
        } catch (IOException ex) {
            System.out.println("Error opening file: " + ex.getMessage());
            return;
        }

        if (lines.size() != 6) {
            System.out.println("Not enough lines. Expected at least 5, got: " + lines.size());
            return;
        }

        String descX = lines.get(0);
        String descY = lines.get(1);
        List<XYValue> values;
        int yMin;
        int yMax;
        int dy;

        try {
            values = Arrays
                .stream(lines.get(2).split("\\s+"))
                .map(str -> str.split(","))
                .map(parts -> new XYValue(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])))
                .collect(Collectors.toList());

            yMin = Integer.parseInt(lines.get(3));
            yMax = Integer.parseInt(lines.get(4));
            dy = Integer.parseInt(lines.get(5));

        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Input file contains invalid value.");
            return;
        } catch (NumberFormatException ex) {
            System.out.println("Invalid number: " + ex.getMessage());
            return;
        }

        SwingUtilities.invokeLater(() -> {
            BarChartDemo demo = new BarChartDemo(file, values, descX, descY, yMin, yMax, dy);
            demo.setTitle("BarChart viewer v1.0");
            demo.setVisible(true);
        });
    }
}
