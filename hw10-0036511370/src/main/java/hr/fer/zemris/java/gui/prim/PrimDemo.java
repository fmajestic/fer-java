package hr.fer.zemris.java.gui.prim;

import javax.swing.*;
import java.awt.*;

public class PrimDemo extends JFrame {

    public PrimDemo() {

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("Prim demo");
        setLocation(20, 20);
        setSize(500, 500);
        GUI();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new PrimDemo().setVisible(true));
    }

    private void GUI() {

        Container pane = getContentPane();

        pane.setLayout(new BorderLayout());

        PrimListModel model = new PrimListModel();
        JList<Integer> first = new JList<>();
        JList<Integer> second = new JList<>();
        JButton next = new JButton("Sljedeći");

        first.setModel(model);
        second.setModel(model);

        JPanel lists = new JPanel(new GridLayout(1, 0));

        JScrollPane scrollFirst = new JScrollPane(first);
        JScrollPane scrollSecond = new JScrollPane(second);

        lists.add(scrollFirst);
        lists.add(scrollSecond);

        pane.add(lists, BorderLayout.CENTER);
        pane.add(next, BorderLayout.SOUTH);

        // Auto-scroll to bottom
        scrollFirst.getVerticalScrollBar().addAdjustmentListener(
            e -> e.getAdjustable().setValue(e.getAdjustable().getMaximum())
        );
        scrollSecond.getVerticalScrollBar().addAdjustmentListener(
            e -> e.getAdjustable().setValue(e.getAdjustable().getMaximum())
        );

        next.addActionListener(e -> model.next());
    }
}
