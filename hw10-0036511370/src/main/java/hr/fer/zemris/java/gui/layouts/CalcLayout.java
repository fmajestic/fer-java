package hr.fer.zemris.java.gui.layouts;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@SuppressWarnings({ "unused", "WeakerAccess", "FieldCanBeLocal" })
public class CalcLayout implements LayoutManager2 {

    private final int rows = 5;
    private final int cols = 7;

    private int gap;
    private Map<RCPosition, Component> grid;

    public CalcLayout() {
        this(0);
    }

    public CalcLayout(int gap) {
        if (gap < 0) {
            throw new IllegalArgumentException("Negative pixel gap.");
        }

        this.gap = gap;
        grid = new HashMap<>();
    }

    @Override
    public void addLayoutComponent(Component comp, Object constraints) {

        RCPosition pos;

        if (constraints instanceof RCPosition) {
            pos = (RCPosition) constraints;
        } else if (constraints instanceof String) {
            String str = (String) constraints;

            if (!str.matches("\\d+,\\d+")) {
                throw new IllegalArgumentException("Invalid position string: " + str);
            }

            String[] parts = str.split(",");

            pos = new RCPosition(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
        } else {
            throw new CalcLayoutException("Component constraints must be an RCPosition or a string.");
        }

        if (!isValidPosition(pos)) {
            throw new IllegalArgumentException("Invalid position: " + pos);
        }

        if (grid.containsKey(pos)) {
            throw new IllegalArgumentException("Component already present at " + pos);
        }

        grid.put(pos, comp);
    }

    /** {@inheritDoc} */
    @Override
    public Dimension maximumLayoutSize(Container target) {
        return getMaxDimension(target, Component::getMaximumSize, (a, b) -> b - a);
    }

    /** {@inheritDoc} */
    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0;
    }

    /** {@inheritDoc} */
    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0;
    }

    /** {@inheritDoc} */
    @Override
    public void invalidateLayout(Container target) {

    }

    /**
     * Checks if a position is valid.
     *
     * @param pos the position to check
     *
     * @return true if the position is valid
     */
    private boolean isValidPosition(RCPosition pos) {
        int x = pos.getX();
        int y = pos.getY();

        if (y == 1) {
            return x == 1 || x > 5 && x <= 7;
        }

        return y >= 1 && y <= 7 && x >= 1 && x <= 7;
    }

    /**
     * Unsupported operation.
     *
     * @param name ignored
     * @param comp ignored
     *
     * @throws UnsupportedOperationException always
     */
    @Override
    public void addLayoutComponent(String name, Component comp) {
        throw new UnsupportedOperationException();
    }

    /** {@inheritDoc} */
    @Override
    public void removeLayoutComponent(Component comp) {

        if (comp == null) {
            return;
        }

        for (var kvp : grid.entrySet()) {
            if (kvp.getValue() == comp) {
                grid.remove(kvp.getKey());
                break;
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public Dimension preferredLayoutSize(Container target) {
        return getMaxDimension(target, Component::getPreferredSize, Comparator.comparingInt(a -> a));
    }

    /** {@inheritDoc} */
    @Override
    public Dimension minimumLayoutSize(Container target) {
        return getMaxDimension(target, Component::getMinimumSize, Comparator.comparingInt(a -> a));
    }

    /**
     * Lays out all components of the parent.
     *
     * @param target the container to lay out
     */
    @Override
    public void layoutContainer(Container target) {

        Insets insets = target.getInsets();
        int availableWidth = target.getSize().width - 6 * gap - insets.left - insets.right;
        int availableHeight = target.getSize().height - 4 * gap - insets.top - insets.bottom;

        int[] distWidth = calculateNormalizedSizes(availableWidth, 7);
        int[] distHeight = calculateNormalizedSizes(availableHeight, 5);

        for (var kvp : grid.entrySet()) {
            var pos = kvp.getKey();
            var comp = kvp.getValue();

            int x = pos.getX() - 1;
            int y = pos.getY() - 1;

            comp.setBounds(
                insets.left + calc(distWidth, gap, x),
                insets.right + calc(distHeight, gap, y),
                x == 0 && y == 0 ? calc(distWidth, gap, 5) - gap : distWidth[x],
                distHeight[y]
            );

        }
    }

    /**
     * Gets the largest dimension of the target's components according to the comparator.
     * Width and height are compared separately, so each will be the largest.
     *
     * @param target     the target container
     * @param dimension  which dimension to get (minimum, preferred, maximum)
     * @param comparator how to compare the sizes
     *
     * @return the largest dimension
     */
    private Dimension getMaxDimension(Container target,
                                      Function<Component, Dimension> dimension,
                                      Comparator<Integer> comparator) {

        Dimension ret = grid.entrySet().stream()
            .map(kvp -> {
                Dimension dim = dimension.apply(kvp.getValue());

                if (kvp.getKey().getX() == 1 && kvp.getKey().getY() == 1) {
                    dim.width -= 4 * gap;
                    dim.width /= 5;
                    dim.height /= 5;
                }

                return dim;
            })
            .reduce(
                new Dimension(),
                (acc, curr) -> {
                    if (curr != null) {
                        if (comparator.compare(acc.width, curr.width) < 0) {
                            acc.width = curr.width;
                        }
                        if (comparator.compare(acc.height, curr.height) < 0) {
                            acc.height = curr.height;
                        }
                    }

                    return acc;
                }
            );

        ret.width = ret.width * 7 + gap * 6;
        ret.height = ret.height * 5 + gap * 4;

        return ret;
    }

    /**
     * Calculates the size of all components that came before the n-th.
     *
     * @param dist all component sizes
     * @param gap  gap between the components
     * @param n    the current component
     *
     * @return size of all components before n-th
     */
    private int calc(int[] dist, int gap, int n) {
        return Arrays.stream(dist).limit(n).reduce(0, Integer::sum) + n * gap;
    }

    /**
     * Calculates sizes so that they fit exactly into the available size,
     * adjusting them by 1 as needed.
     *
     * @param availableSize the maximum available size
     * @param n             the number of places
     *
     * @return the normalized sizes
     */
    private int[] calculateNormalizedSizes(int availableSize, int n) {

        int[] ret = new int[n];

        int rounded = (int) Math.round(availableSize / (double) n);

        int extra = n * rounded - availableSize;
        int toDistribute = extra <= 3 ? extra : extra - 3;

        if (extra > 0) {
            for (int i = 0; i < ret.length; i++) {
                ret[i] = rounded - (extra-- > 0 ? 1 : 0);
            }
        } else {
            for (int i = ret.length - 1; i >= 0; i--) {
                ret[i] = rounded + (extra++ < 0 ? 1 : 0);
            }
        }

        distributeFirst(ret, toDistribute);

        return ret;
    }

    /**
     * Evenly distributes the first n elements of the array.
     *
     * @param arr the array to distribute in
     * @param n   the number of elements
     */
    private void distributeFirst(int[] arr, int n) {
        if (arr.length == 7) {
            for (int i = 1; i <= n; i++) {
                swap(arr, n - i, n + (4 - 2 * i));
            }
        } else {
            for (int i = 1; i <= n; i++) {
                swap(arr, n - i, n + (2 - i));
            }
        }
    }

    /**
     * Swaps two elements of an array.
     *
     * @param arr the array to swap in
     * @param i1  the first element
     * @param i2  the second element
     */
    void swap(int[] arr, int i1, int i2) {
        int tmp = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = tmp;
    }
}