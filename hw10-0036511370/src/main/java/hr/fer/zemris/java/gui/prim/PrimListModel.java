package hr.fer.zemris.java.gui.prim;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/** Models a list of primes. */
public class PrimListModel implements ListModel<Integer> {

    /** All currently stored primes */
    private List<Integer> primes;

    /** Listeners */
    private List<ListDataListener> listeners;

    /** Creates a new list model. */
    public PrimListModel() {
        listeners = new CopyOnWriteArrayList<>();
        primes = new ArrayList<>();
        primes.add(1);
    }

    /** {@inheritDoc} */
    @Override
    public int getSize() {
        return primes.size();
    }

    /** {@inheritDoc} */
    @Override
    public Integer getElementAt(int index) {
        return primes.get(index);
    }

    /** {@inheritDoc} */
    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.add(l);
    }

    /** {@inheritDoc} */
    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.remove(l);
    }

    /**
     * Generates the next prime and stores it in the list.
     */
    public void next() {
        int last = primes.size() == 0 ? 1 : primes.get(primes.size() - 1);

        primes.add(primeAfter(last));
        notifyObservers();
    }

    /**
     * Calculates the first prime that comes after n.
     * <p>
     * Even if n already is a prime, it is ignored and the next prime after n is still calculated.
     *
     * @param n the number to check from
     *
     * @return the next prime after n
     *
     * @throws IllegalStateException if n is {@link Integer#MAX_VALUE} (the biggest prime
     *                               that fits into a 32-bit signed integer)
     */
    private int primeAfter(int n) {
        if (n == Integer.MAX_VALUE) {
            throw new IllegalStateException("No more primes fit into a 32-bit unsigned integer.");
        }

        n++;

        while (!isPrime(n)) {
            n++;
        }

        return n;
    }

    /**
     * Checks if n is a prime number.
     *
     * @param n the number to check
     *
     * @return true if n is prime
     */
    private boolean isPrime(int n) {

        if (n == 2) {
            return true;
        }

        if (n < 2 || n % 2 == 0) {
            return false;
        }

        int bound = (int) Math.sqrt(n);

        for (int i = 3; i <= bound; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    /** Notifies all observers of change. */
    private void notifyObservers() {
        ListDataEvent event = new ListDataEvent(
            this,
            ListDataEvent.INTERVAL_ADDED,
            primes.size() - 1,
            primes.size() - 1
        );

        for (ListDataListener l : listeners) {
            l.intervalAdded(event);
        }
    }
}
