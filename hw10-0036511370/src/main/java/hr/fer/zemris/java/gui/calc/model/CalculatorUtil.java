package hr.fer.zemris.java.gui.calc.model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

/** A collection of static utility methods. */
class CalculatorUtil {

    /**
     * Creates a listener that executes an unary operation.
     * @param operator the operation to apply
     * @param model the model to use
     * @param display the display to output to
     * @return the listener
     */
    static ActionListener unaryOperation(DoubleUnaryOperator operator, CalcModel model, JLabel display) {
        return ev -> {
            if (noPendingOperation(model, display)) {
                model.setValue(operator.applyAsDouble(model.getValue()));
            }
        };
    }

    /**
     * Creates a listener that executes a binary operation.
     * @param operator the operation to apply
     * @param model the model to use
     * @param display the display to use
     * @return the listener
     */
    static ActionListener binaryOperation(DoubleBinaryOperator operator, CalcModel model, JLabel display) {
        return ev -> {
            if (!noPendingOperation(model, display)) {
                executePendingOperation(model);
            }

            model.setActiveOperand(model.getValue());
            model.setPendingBinaryOperation(operator);

            String save = display.getText();

            model.clear();

            display.setText(save);
        };
    }

    /**
     * Creates a listener that executes a regular or inverted unary operation.
     * @param regular the regular operation
     * @param inverted the inverted operation
     * @param model the model to use
     * @param display the display to use
     * @param isInverted should the inverted operation be performed
     * @return the listener
     */
    static ActionListener invertibleUnaryOperation(DoubleUnaryOperator regular,
                                                   DoubleUnaryOperator inverted,
                                                   CalcModel model,
                                                   JLabel display,
                                                   boolean isInverted) {
        return ev -> {
            if (noPendingOperation(model, display)) {
                DoubleUnaryOperator op = isInverted ? inverted : regular;
                model.setValue(op.applyAsDouble(model.getValue()));
            }
        };
    }

    /**
     * Creates a listener that executes a regular or inverted binary operation.
     * @param regular the regular operation
     * @param inverted the inverted operation
     * @param model the model to use
     * @param display the display to use
     * @param isInverted should the inverted operation be performed
     * @return the listener
     */
    static ActionListener invertibleBinaryOperation(DoubleBinaryOperator regular,
                                                    DoubleBinaryOperator inverted,
                                                    CalcModel model,
                                                    JLabel display,
                                                    boolean isInverted) {
        return ev -> {
            if (noPendingOperation(model, display)) {
                DoubleBinaryOperator op = isInverted ? inverted : regular;
                model.setValue(op.applyAsDouble(model.getActiveOperand(), model.getValue()));
            }
        };
    }

    /**
     * Checks if the model has a pending operations, and displays an error if it does.
     * @param model the model to use
     * @param display the display to use
     * @return true if there is no pending operation or operand, false otherwise
     */
    private static boolean noPendingOperation(CalcModel model, JLabel display) {
         if (model.isActiveOperandSet() || model.getPendingBinaryOperation() != null) {
             model.clear();
             display.setText("ERROR");
             display.setBackground(Color.RED);
             return false;
         }

        return true;
    }

    /**
     * Executes a pending calculator operation
     *
     * @param model the calculator to work on
     */
    static void executePendingOperation(CalcModel model) {
        try {
            double lhs = model.getActiveOperand();
            double rhs = model.getValue();
            double r = model.getPendingBinaryOperation().applyAsDouble(lhs, rhs);
            model.setValue(r);
            model.clearActiveOperand();
            model.setPendingBinaryOperation(null);
        } catch (IllegalStateException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }

    /**
     * Makes the component font larger by deriving a 30-size font
     * from the component's current one.
     *
     * @param comp the component
     */
    static void enlargeFont(JComponent comp) {
        comp.setFont(comp.getFont().deriveFont(30f));
    }
}
