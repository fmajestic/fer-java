package hr.fer.zemris.java.gui.prim;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimListModelTest {

    private PrimListModel model;

    @BeforeEach
    void setUp() {
        model = new PrimListModel();
    }

    @Test
    void getSize() {
        assertEquals(1, model.getSize());

        model.next();
        model.next();
        model.next();

        assertEquals(4, model.getSize());
    }

    @Test
    void getElementAt() {
        assertEquals(1, model.getElementAt(0));

        model.next();
        model.next();
        model.next();
        model.next();
        model.next();
        model.next();
        model.next();
        model.next();

        assertEquals(19, model.getElementAt(8));
    }

    @Test
    void next() {
        model.next();
        model.next();
        model.next();

        assertEquals(5, model.getElementAt(3));

        model.next();
        model.next();
        model.next();

        assertEquals(13, model.getElementAt(6));
    }
}