<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/WEB-INF/style.css">
  <title>Register</title>
</head>
<body>
  <h1>New user</h1>

  <form action="${pageContext.request.contextPath}/servleti/register" method="post">
    <label for="firstName">First name:</label>
    <input type="text" name="fn" id="firstName"><br>

    <label for="lastName">Last name:</label>
    <input type="text" name="ln" id="lastName"><br>

    <label for="email">E-mail:</label>
    <input type="text" name="email" id="email"><br>

    <label for="nickname">Nickname:</label>
    <input type="text" name="nick" id="nickname">
    <c:if test="${invalidNickname eq true}">
      <span style="color: red;">Nickname already taken! Pick another one.</span>
    </c:if>
    <br>

    <label for="password">Password:</label>
    <input type="password" name="pass" id="password">

    <br><br>
    <input type="submit" value="Register">
  </form>
</body>
</html>
