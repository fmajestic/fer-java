<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Error</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/WEB-INF/style.css">

</head>
<body>
  <h1>404 - Not found</h1>
  <p>${message}</p>
  <a href="${pageContext.request.contextPath}/">Back to homepage</a>
</body>
</html>
