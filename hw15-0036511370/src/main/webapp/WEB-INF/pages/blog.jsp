<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Blog</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/WEB-INF/style.css">

</head>
<body>
  <h1>${entry.title}</h1>

  <p>Posted by ${entry.creator.nick} on ${blog.createdOn}</p>

  <div>
    ${blog.text}
  </div>

  <hr>

  <c:choose>
    <c:when test="${not empty loginId}">
      <form method="post" action="${pageContext.request.contextPath}/servleti/comment?id=${entry.id}" id="comment">
        <p>Posting as: ${loginNick}</p>
        <label for="msg">Message:</label>
        <textarea id="msg" name="message" form="comment">Enter message...</textarea><br>
        <input type="submit" value="Comment">
      </form>
    </c:when>
    <c:otherwise>
      <form method="post" action="${pageContext.request.contextPath}/servleti/comment?id=${entry.id}" id="comment">
        <label for="mail">E-mail:</label>
        <input type="text" id="mail" name="email"><br>

        <label for="message">Message:</label>
        <textarea id="message" name="message" form="comment">Enter message...</textarea><br>
        <input type="submit" value="Comment">
      </form>
    </c:otherwise>
  </c:choose>
  <hr>

  <c:forEach var="comment" items="${entry.comments}">
    <div id="comment" style="padding-bottom: 20px">
      <p>Comment by ${comment.userEMail} on ${comment.postedOn}</p>
      <p><c:out value="${comment.message}"/></p>
    </div>
  </c:forEach>

</body>
</html>
