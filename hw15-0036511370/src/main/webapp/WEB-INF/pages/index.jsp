<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Iterator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/WEB-INF/style.css">
  <title>Home</title>
</head>
<body>

  <h1>Blog</h1>

  <c:if test="${not empty sessionScope[\"current.user.id\"]}">
    <a href="${pageContext.request.contextPath}/servleti/logout">
      Logout for ${sessionScope["current.user.nick"]}
    </a>
  </c:if>

  <h2>User login</h2>

  <c:if test="${sessionScope.invalidLogin eq true}">
    <c:set var="invalidLogin" scope="session" value="${false}"/>
    <p style="color: red;">Invalid nickname and/or password!</p>
  </c:if>

  <form method="post" action="${pageContext.request.contextPath}/servleti/login">
    <label for="nickname">Nickname:</label>
    <input type="text" name="nick" id="nickname"><br>
    <label for="pass">Password:</label>
    <input type="password" name="pass" id="pass"><br><br>
    <input type="submit" value="Log in">
  </form>
  <br><br>

  <p>New user? Register <a href="${pageContext.request.contextPath}/servleti/register">here</a>.</p>

  <h2>Registered authors</h2>
  <ul>
    <jsp:useBean id="authors" scope="request" type="java.util.List"/>
    <c:if test="${not empty authors}">
      <c:forEach var="author" items="${authors}">
        <li><a
            href="${pageContext.request.contextPath}/servleti/author/${author.nick}">${author.firstName} ${author.lastName}</a>
        </li>
      </c:forEach>
    </c:if>
  </ul>
</body>
</html>
