<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Edit</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/WEB-INF/style.css">

</head>
<body>
  <c:choose>
    <c:when test="${empty requestScope.id}">
      <h1>New blog post</h1>
    </c:when>
    <c:otherwise>
      <h1>Edit blog post</h1>
    </c:otherwise>
  </c:choose>

  <c:choose>
  <c:when test="${empty requestScope.id}">
  <form action="${pageContext.request.contextPath}/servleti/edit" method="post" id="blogpost">
    </c:when>
    <c:otherwise>
    <form action="${pageContext.request.contextPath}/servleti/edit?id=${requestScope.id}" method="post" id="blogpost">
      </c:otherwise>
      </c:choose>
      <label for="title">Title:</label>
      <input type="text" name="title" id="title"><br>

      <label for="text">Text:</label><br>
      <textarea name="content" form="blogpost" id="text">Enter text</textarea>
      <br><br>
      <c:choose>
        <c:when test="${empty requestScope.id}">
          <button type="submit">Create</button>
        </c:when>
        <c:otherwise>
          <button type="submit">Save changes</button>
        </c:otherwise>
      </c:choose>
    </form>
</body>
</html>
