<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="author" scope="request" type="hr.fer.zemris.java.hw15.model.BlogUser"/>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/WEB-INF/style.css">
  <title>Author</title>
</head>
<body>
  <h1>All blog entries by ${author.nick}</h1>

  <c:choose>
    <c:when test="${empty entries}">
      <p>No entries!</p>
    </c:when>
    <c:otherwise>
      <ul>
        <c:forEach var="entry" items="${entries}">
          <li>
            <a href="/servleti/author/${author.nick}/${entry.id}">${entry.title}</a>
          </li>
        </c:forEach>
      </ul>
    </c:otherwise>
  </c:choose>

  <c:if test="${not empty sessionScope[\"current.user.id\"]}">
    <a href="${pageContext.request.contextPath}/servleti/author/${author.nick}/new">Create new blog post</a>
  </c:if>
</body>
</html>
