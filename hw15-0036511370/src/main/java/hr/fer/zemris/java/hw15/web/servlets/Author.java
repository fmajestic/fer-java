package hr.fer.zemris.java.hw15.web.servlets;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "author", urlPatterns = "/servleti/author/*")
public class Author extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String info = req.getPathInfo().substring(1);
        String[] parts = info.split("/");
        DAO dao = DAOProvider.getDAO();

        if (parts.length == 1) {
            BlogUser author = dao.getBlogUser(parts[0]);

            if (author == null) {
                req.setAttribute("message", "Nickname doesn't exist");
                req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
            } else {
                req.setAttribute("author", author);
                req.setAttribute("entries", dao.getEntries(author.getNick()));
                req.getRequestDispatcher("/WEB-INF/pages/author.jsp").forward(req, resp);
            }
        } else if (parts.length == 2) {
            if (parts[1].matches("new|edit")) {

                BlogUser author = dao.getBlogUser(parts[0]);
                req.setAttribute("author", author);

                if (!req.getSession().getAttribute("current.user.id").equals(author.getId())) {
                    resp.sendError(403, "Error");
                    return;
                }

                if (parts[1].equals("edit")) {
                    String editId = req.getParameter("id");

                    try {
                        BlogEntry entry = dao.getBlogEntry(Long.valueOf(editId));
                        if (entry != null) {
                            req.setAttribute("entry", entry);
                        }
                    } catch (NumberFormatException ex) {
                        req.setAttribute("message", "Bad entry id.");
                        req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
                        return;
                    }
                }

                req.getRequestDispatcher("/WEB-INF/pages/edit.jsp").forward(req, resp);
            } else {
                BlogEntry entry = null;
                try {
                    entry = dao.getBlogEntry(Long.valueOf(parts[1]));
                } catch (NumberFormatException ex) {
                    req.setAttribute("message", "Invalid entry id.");
                    req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
                }

                if (entry == null) {
                    req.setAttribute("message", "Entry not found.");
                    req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
                    return;
                }

                req.setAttribute("entry", entry);
                req.setAttribute("author", entry.getCreator());

                req.getRequestDispatcher("/WEB-INF/pages/blog.jsp").forward(req, resp);
            }
        }
    }
}
