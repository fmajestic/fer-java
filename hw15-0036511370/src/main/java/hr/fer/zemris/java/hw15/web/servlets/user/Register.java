package hr.fer.zemris.java.hw15.web.servlets.user;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "register", urlPatterns = "/servleti/register")
public class Register extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        DAO dao = DAOProvider.getDAO();

        String fn = req.getParameter("fn");
        String ln = req.getParameter("ln");
        String email = req.getParameter("email");
        String nick = req.getParameter("nick");
        String pass = req.getParameter("pass");

        dao.addUser(fn, ln, nick, email, pass);

        // TODO: log in user automatically?

        resp.sendRedirect(req.getContextPath() + "/servleti/main");
    }
}
