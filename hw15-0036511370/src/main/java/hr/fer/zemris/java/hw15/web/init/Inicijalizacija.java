package hr.fer.zemris.java.hw15.web.init;

import hr.fer.zemris.java.hw15.dao.jpa.JPAEMFProvider;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Manages the entity manager factory.
 */
@WebListener
public class Inicijalizacija implements ServletContextListener {

    /**
     * Creates an entity manager factory and stores it in the servlet context under {@code blog.app.emf}
     * <p>
     * Trying to create it currently throws an exception, so this method does nothing.
     *
     * @param sce the context
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
          EntityManagerFactory emf = Persistence.createEntityManagerFactory("baza.podataka.za.blog");
          sce.getServletContext().setAttribute("blog.app.emf", emf);
          JPAEMFProvider.setEmf(emf);
    }

    /**
     * Closes the entity manager factory.
     *
     * @param sce the context
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        JPAEMFProvider.setEmf(null);

        EntityManagerFactory emf = (EntityManagerFactory) sce.getServletContext().getAttribute("blog.app.emf");

        if (emf != null) {
            emf.close();
        }
    }
}