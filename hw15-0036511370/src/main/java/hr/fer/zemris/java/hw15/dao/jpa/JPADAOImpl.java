package hr.fer.zemris.java.hw15.dao.jpa;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOException;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.web.Util;
import org.apache.derby.client.am.DateTime;

import javax.persistence.EntityManager;
import java.sql.DatabaseMetaData;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static hr.fer.zemris.java.hw15.dao.jpa.JPAEMProvider.getEntityManager;

public class JPADAOImpl implements DAO {

    @Override
    public BlogEntry getBlogEntry(Long id) throws DAOException {
        return getEntityManager().find(BlogEntry.class, id);
    }

    @Override
    public BlogUser getBlogUser(String nickname) throws DAOException {
        List<BlogUser> users = JPAEMProvider
            .getEntityManager()
            .createNamedQuery("BlogUser.byNick", BlogUser.class)
            .setParameter("nick", nickname)
            .getResultList();

        if (users.size() != 1) {
            return null;
        }

        return users.get(0);
    }

    @Override
    public BlogUser addUser(String firstName, String lastName, String nickname, String email, String password) {

        EntityManager em = getEntityManager();

        if (em.createNamedQuery("BlogUser.countNick", Long.class)
              .setParameter("nick", nickname)
              .getSingleResult() != 0L
        ) {
            return null;
        }

        BlogUser user = new BlogUser();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setNick(nickname);
        user.setEmail(email);
        user.setPasswordHash(Util.passwordHash(password));

        em.persist(user);

        return user;
    }

    @Override
    public List<BlogUser> getAllUsers() {
        return getEntityManager()
            .createNamedQuery("BlogUser.all", BlogUser.class)
            .getResultList();
    }

    @Override
    public List<BlogEntry> getEntries(String nick) {
        return JPAEMProvider
            .getEntityManager()
            .createNamedQuery("BlogEntry.byAuthor", BlogEntry.class)
            .setParameter("creator", nick)
            .getResultList();
    }

    @Override
    public BlogComment addComment(String email, String message, Long entryId) {
        EntityManager em = JPAEMProvider.getEntityManager();

        BlogComment comment = new BlogComment();
        comment.setBlogEntry(getBlogEntry(entryId));
        comment.setMessage(message);
        comment.setPostedOn(new Date());
        comment.setUsersEMail(email);

        em.persist(comment);

        return comment;
    }

    @Override
    public void addBlogEntry(BlogUser creator, String title, String content) {
        EntityManager em = JPAEMProvider.getEntityManager();

        BlogEntry entry = new BlogEntry();
        entry.setCreator(creator);
        entry.setTitle(title);
        entry.setText(content);
        entry.setCreatedAt(new Date());
        entry.setComments(new ArrayList<>());

        em.persist(entry);
    }
}