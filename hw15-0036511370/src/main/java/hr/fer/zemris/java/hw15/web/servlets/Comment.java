package hr.fer.zemris.java.hw15.web.servlets;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "comment", urlPatterns = "/servleti/comment")
public class Comment extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        DAO dao = DAOProvider.getDAO();
        String email = req.getParameter("email");
        String message = req.getParameter("message");

        Long lid;

        try {
            lid = Long.valueOf(req.getParameter("id"));
        } catch (NumberFormatException ex) {
            req.setAttribute("message", "Bad entry id.");
            req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
            return;
        }

        if (email == null) {
            email = dao.getBlogUser((String) req.getSession().getAttribute("loginNick")).getEmail();
        }

        dao.addComment(email, message, lid);

        resp.sendRedirect(req.getRequestURI());
    }
}
