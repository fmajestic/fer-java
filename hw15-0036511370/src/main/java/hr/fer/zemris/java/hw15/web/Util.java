package hr.fer.zemris.java.hw15.web;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * A utility class for text-byte conversions.
 */
public class Util {

    private static final String ALGORITHM = "SHA-1";

    /**
     * Parses a hex string to an array of bytes. The text must be a valid hex string.
     * <p>
     * A valid hex string has an even length and only consists of the characters
     * {@code a-f}, {@code A-F}, and {@code 0-9}.
     * <p>
     * {@code "010F20FF"} would become {@code { 1, 15, 32, 255 }}.
     * <p>
     * {@code "01aE22"} would become {@code { 1, -82, 34}}.
     *
     * @param text the text to convert
     *
     * @return the parsed bytes
     *
     * @throws IllegalArgumentException if {@code text} is not a valid hex string
     */
    public static byte[] hextobyte(String text) {
        if (text.length() % 2 != 0 || !text.matches("[a-fA-F0-9]*")) {
            throw new IllegalArgumentException("Invalid hex string: " + text);
        }

        String[] parts = text.split("(?<=\\G.{2})");
        byte[] hex = new byte[parts.length];

        for (int i = 0; i < parts.length; i++) {
            hex[i] = (byte) Integer.parseInt(parts[i], 16);
        }

        return hex;
    }

    /**
     * Converts a series of bytes to a capitalized hex string. Each byte is displayed as 2 hex digits.
     * <p>
     * {@code { 1, 15, 32, 255 }} would become {@code "010F20FF"}.
     * <p>
     * {@code { 1, -82, 34}}  would become  {@code "01AE22"}.
     *
     * @param hex the bytes to convert
     *
     * @return the bytes as a string
     */
    public static String bytetohex(byte[] hex) {
        var sb = new StringBuilder();

        for (byte b : hex) {
            sb.append(String.format("%02x", b));
        }

        return sb.toString();
    }

    public static String passwordHash(String password) {
        MessageDigest md = null;

        try {
            md = MessageDigest.getInstance(ALGORITHM);
            return bytetohex(md.digest(password.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }
}