package hr.fer.zemris.java.hw15.dao;

import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

import java.util.List;

public interface DAO {

    /**
     * Dohvaća entry sa zadanim <code>id</code>-em. Ako takav entry ne postoji,
     * vraća <code>null</code>.
     *
     * @param id ključ zapisa
     * @return entry ili <code>null</code> ako entry ne postoji
     * @throws DAOException ako dođe do pogreške pri dohvatu podataka
     */
    BlogEntry getBlogEntry(Long id) throws DAOException;

    BlogUser getBlogUser(String nickname) throws DAOException;

    BlogUser addUser(String firstName, String lastName, String nickname, String email, String password);

    List<BlogUser> getAllUsers();

    List<BlogEntry> getEntries(String nick);

    BlogComment addComment(String email, String message, Long entryId);

    void addBlogEntry(BlogUser creator, String title, String content);
}