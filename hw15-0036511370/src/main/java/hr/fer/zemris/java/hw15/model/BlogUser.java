package hr.fer.zemris.java.hw15.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "blog_users")
@NamedQueries({
    @NamedQuery(name = "BlogUser.all", query = "select b from BlogUser b"),
    @NamedQuery(name = "BlogUser.byNick", query = "select b from BlogUser b where b.nick = :nick"),
    @NamedQuery(name = "BlogUser.countNick", query = "select count(b) from BlogUser b where b.nick = :nick")
})
public class BlogUser {

    private Long id;
    private String firstName;
    private String lastName;
    private String nick;
    private String email;
    private String passwordHash;
    private List<BlogEntry> entries = new ArrayList<>();

    @Id
    @GeneratedValue
    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    @Column(length = 100, nullable = false)
    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    @Column(length = 100, nullable = false)
    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    @Column(length = 20, nullable = false, unique = true)
    public String getNick() { return nick; }

    public void setNick(String nick) { this.nick = nick; }

    @Column(length = 100, nullable = false)
    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    @Column(length = 40, nullable = false)
    public String getPasswordHash() { return passwordHash; }

    public void setPasswordHash(String passwordHash) { this.passwordHash = passwordHash; }

    @OrderBy("createdAt")
    @OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
    public List<BlogEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<BlogEntry> entries) {
        this.entries = entries;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Objects.hashCode(id);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof BlogUser)) {
            return false;
        }

        BlogUser blogUser = (BlogUser) o;

        return id.equals(blogUser.id);
    }
}
