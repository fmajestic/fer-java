package hr.fer.zemris.java.hw15.web.servlets.user;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.web.Util;
import org.apache.taglibs.standard.tag.common.fmt.RequestEncodingSupport;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "login", urlPatterns = "/servleti/login")
public class Login extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        String nick = req.getParameter("nick");
        String password = req.getParameter("pass");
        String passHash = Util.passwordHash(password);

        DAO dao = DAOProvider.getDAO();

        BlogUser user = dao.getBlogUser(nick);

        if (user == null || !user.getPasswordHash().equals(passHash)) {
            req.getSession().setAttribute("invalidLogin", true);
            resp.sendRedirect(req.getContextPath() + "/");
            return;
        }

        req.getSession().setAttribute("loginId", user.getId());
        req.getSession().setAttribute("loginNick", user.getNick());
        req.getSession().setAttribute("loginFn", user.getFirstName());

        req.getRequestDispatcher("/servleti/main").forward(req, resp);
    }
}
