package hr.fer.zemris.java.hw15.web.servlets.editing;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "edit", urlPatterns = "/servleti/edit")
public class Edit extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        DAO dao = DAOProvider.getDAO();
        String idStr = req.getParameter("id");

        String title = req.getParameter("title");
        String content = req.getParameter("content");

        if (idStr == null) {
            BlogUser user = dao.getBlogUser((String) req.getSession().getAttribute("current.user.nick"));
            dao.addBlogEntry(user, title, content);
        } else {
            Long id = Long.valueOf(idStr);
            BlogEntry entry = dao.getBlogEntry(id);
            entry.setTitle(title);
            entry.setText(content);
        }
    }
}
