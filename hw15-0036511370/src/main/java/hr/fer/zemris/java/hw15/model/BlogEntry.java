package hr.fer.zemris.java.hw15.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("DefaultAnnotationParam")
@NamedQueries({
    @NamedQuery(
        name = "BlogEntry.commentsSince",
        query = "select b from BlogComment as b where b.blogEntry=:be and b.postedOn>:when"
    ),
    @NamedQuery(
        name = "BlogEntry.byAuthor",
        query = "select b from BlogEntry b where b.creator = :creator"
    )
})
@Entity
@Table(name = "blog_entries")
@Cacheable(true)
public class BlogEntry {

    private Long id;
    private List<BlogComment> comments = new ArrayList<>();

    private Date createdAt;
    private Date lastModifiedAt;
    private String title;
    private String text;

    private BlogUser creator;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OrderBy("postedOn")
    @OneToMany(mappedBy = "blogEntry", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
    public List<BlogComment> getComments() {
        return comments;
    }

    public void setComments(List<BlogComment> comments) {
        this.comments = comments;
    }

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(Date lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    @Column(length = 200, nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 4096, nullable = false)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Objects.hashCode(id);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        BlogEntry other = (BlogEntry) obj;

        return Objects.equals(id, other.id);
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public BlogUser getCreator() { return creator; }

    public void setCreator(BlogUser creator) { this.creator = creator; }
}