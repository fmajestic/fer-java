package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

public class DocumentNode extends Node {

    /**
     * Generates document body as a text.
     * <p>
     * Since a DocumentNode carries no information such as a name, it is delegated
     * to {@code super.toString()}, which in turn creates a string representation of
     * the node's children.
     * <p>
     * The generated text can be parsed again by a {@link SmartScriptParser}
     * 
     * @return the document body as a string that can be parsed again
     */
    @Override
    public String toString() {
        return super.toString();
    }

    /** {@inheritDoc} */
    @Override
    public void accept(INodeVisitor visitor) {
        visitor.visitDocumentNode(this);
    }
}
