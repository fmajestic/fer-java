package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringJoiner;

/** Reconstructs document text based on the parsed document tree. */
public class TreeWriter {

    /** The visitor that writes all elements. */
    private static INodeVisitor treeWriter = new INodeVisitor() {


        @Override
        public void visitTextNode(TextNode node) {
            System.out.print(node.getText().replace("\\", "\\\\").replace("{$", "\\{$"));
        }

        @Override
        public void visitForLoopNode(ForLoopNode node) {

            System.out.printf(
                "{$ FOR %s %s %s %s $}",
                node.getVariable(),
                node.getStartExpression(),
                node.getEndExpression(),
                node.getStepExpression()
            );

            traverseChildren(node);

            System.out.print("{$ END $}");
        }

        @Override
        public void visitEchoNode(EchoNode node) {
            var sj = new StringJoiner(" ", "{$= ", " $}");

            for (var el : node.getElements()) {
                sj.add(el.toString());
            }

            System.out.print(sj.toString());
        }

        @Override
        public void visitDocumentNode(DocumentNode node) {
            traverseChildren(node);
        }

        private void traverseChildren(Node node) {
            for (int i = 0; i < node.numberOfChildren(); i++) {
                node.getChild(i).accept(this);
            }
        }
    };


    /**
     * Entry point of the program
     *
     * @param args unused
     */
    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Program requires exactly 1 argument.");
            System.exit(-1);
        }

        String docBody;
        SmartScriptParser parser = null;

        try {
            docBody = Files.readString(Paths.get(args[0]));
            parser = new SmartScriptParser(docBody);
        } catch (SmartScriptParserException e) {
            System.out.println("Unable to parse document!");
            System.exit(-1);
        } catch (IOException ex) {
            System.out.println("Can't find file");
            System.exit(-1);
        } catch (Exception e) {
            System.out.println("If this line ever executes, you have failed this class!");
            System.exit(-1);
        }

        DocumentNode document = parser.getDocumentNode();
        document.accept(treeWriter);
    }
}
