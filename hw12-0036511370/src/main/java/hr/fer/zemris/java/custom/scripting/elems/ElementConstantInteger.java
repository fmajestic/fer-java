package hr.fer.zemris.java.custom.scripting.elems;

public class ElementConstantInteger extends Element {
    private final int value;

    /**
     * Creates a new Element with the given value
     * 
     * @param value the integer's value
     */
    public ElementConstantInteger(int value) {
        this.value = value;
    }

    /**
     * Returns the text representation of the integer value
     * 
     * @return the integer as a string
     */
    @Override
    public String asText() {
        return Integer.toString(value);
    }

    /**
     * Delegated to {@link #asText()}.
     * 
     * @retrun the integer as a string
     */
    @Override
    public String toString() {
        return String.format("%d", value);
    }
}
