package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.ArrayList;
import java.util.List;

/**
 * A base class for all Nodes.
 *
 * @author Filip Majetic
 */
public abstract class Node {
    private List<Node> children;

    /**
     * Adds a node to the list of children.
     *
     * @param child the node to be added
     */
    public void addChildNode(Node child) {
        if (children == null) {
            children = new ArrayList<>();
        }

        children.add(child);
    }

    /**
     * Gets the number of children this node has.
     *
     * @return the number of children
     */
    public int numberOfChildren() {
        return children == null ? 0 : children.size();
    }

    /**
     * Gets the child at the specified index.
     *
     * @param index the index to get from
     *
     * @return the child at the {@code index}
     *
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    public Node getChild(int index) {
        return children.get(index);
    }

    /**
     * Returns the text representation of the node's children
     *
     * @return the text
     */
    @Override
    public String toString() {
        var sb = new StringBuilder();

        if (children != null) {
            for (Node o : children) {
                sb.append(o.toString());
            }
        }

        return sb.toString();
    }
    /**
     * Accept a visitor.
     *
     * @param visitor the visitor to run
     */
    public abstract void accept(INodeVisitor visitor);
}
