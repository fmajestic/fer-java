package hr.fer.zemris.java.custom.scripting.nodes;

/** Models a node visitor. */
public interface INodeVisitor {

    /**
     * Performed when visiting a general node.
     *
     * @param node the visited node.
     */
    void visitTextNode(TextNode node);

    /**
     * Performed when visiting a for-loop node.
     *
     * @param node the visited node.
     */
    void visitForLoopNode(ForLoopNode node);

    /**
     * Performed when visiting an echo node.
     *
     * @param node the visited node.
     */
    void visitEchoNode(EchoNode node);

    /**
     * Performed when visiting a document node.
     *
     * @param node the visited node.
     */
    void visitDocumentNode(DocumentNode node);
}

