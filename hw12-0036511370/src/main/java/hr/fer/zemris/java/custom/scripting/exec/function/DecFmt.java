package hr.fer.zemris.java.custom.scripting.exec.function;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/** The decimal format function. */
public class DecFmt extends SmartScriptFunction {

    /**
     * Creates a new decimal format function.
     */
    public DecFmt() {
        super(2);
    }

    /**
     * Formats the number according to the pattern.
     *
     * @param argv exactly 2 arguments, the number and the pattern
     *
     * @return the formatted string
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);

        if (!(argv[0] instanceof Number || argv[0] instanceof String) || !(argv[1] instanceof String)) {
            throw new IllegalArgumentException("Invalid argument type(s)");
        }

        Number num = getNumber(argv[0]);
        String pattern = (String) argv[1];

        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ROOT);
        DecimalFormat df = (DecimalFormat) nf;

        df.applyPattern(pattern);

        return df.format(num);
    }
}
