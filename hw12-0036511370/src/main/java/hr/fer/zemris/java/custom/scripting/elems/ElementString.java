package hr.fer.zemris.java.custom.scripting.elems;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

public class ElementString extends Element {
    private final String value;

    /**
     * Creates a new Element with the given string.
     * 
     * @param value the string to hold
     */
    public ElementString(String value) {
        this.value = value;
    }

    /**
     * Returns the string stored in the Element.
     * 
     * @return the string stored
     */
    @Override
    public String asText() {
        return value;
    }

    /**
     * Returns the string representation of this element, such that it can be parsed
     * again by a {@link SmartScriptParser}
     * <p>
     * This is done by escaping all the {@code "} characters in the string.
     */
    @Override
    public String toString() {
        return String.format("\"%s\"", value.replace("\\", "\\\\")).replace("\"", "\\\"");
    }
}
