package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** A call counter demo in SmartScript. */
public class NumberOfCallsDemo {

    /**
     * Entry point of the program
     *
     * @param args unused
     *
     * @throws IOException if the file can't be read
     */
    public static void main(String[] args) throws IOException {
        String documentBody = Files.readString(Paths.get("./examples/brojPoziva.smscr"));

        Map<String, String> parameters = new HashMap<>();
        Map<String, String> persistentParameters = new HashMap<>();
        List<RequestContext.RCCookie> cookies = new ArrayList<>();

        persistentParameters.put("brojPoziva", "3");

        RequestContext rc = new RequestContext(System.out, parameters, persistentParameters,
            cookies);

        new SmartScriptEngine(
            new SmartScriptParser(documentBody).getDocumentNode(), rc
        ).execute();

        System.out.println("Vrijednost u mapi: " + rc.getPersistentParameter("brojPoziva"));
    }
}
