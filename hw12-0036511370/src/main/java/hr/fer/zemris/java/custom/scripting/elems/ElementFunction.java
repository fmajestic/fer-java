package hr.fer.zemris.java.custom.scripting.elems;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

public class ElementFunction extends Element {
    private final String name;

    /**
     * Creates a new Element with he given name
     * 
     * @param name the function name
     */
    public ElementFunction(String name) {
        this.name = name;
    }

    /**
     * Retuns the text representation of the function name.
     * 
     * @return the function name as a string
     */
    @Override
    public String asText() {
        return "@" + name;
    }

    /**
     * Returns the text representation of the function name, such that it can be
     * parsed again by a {@link SmartScriptParser}
     * <p>
     * This is done by prepending the name with an {@code @} sign.
     */
    @Override
    public String toString() {
        return "@" + name;
    }
}
