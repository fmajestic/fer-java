package hr.fer.zemris.java.webserver;

/** A web worker. */
public interface IWebWorker {

    /**
     * Processes a request using a context.
     *
     * @param context the context to use
     *
     * @throws Exception if any exception occurs
     */
    void processRequest(RequestContext context) throws Exception;
}
