package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Thrown when a lexer error occours.
 * 
 * @author Filip Majetic
 */
public class SmartScriptLexerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor. Delegated to {@cosde super()}.
     */
    public SmartScriptLexerException() {
        super();
    }

    /**
     * Creates an exception with a message. Delegated to {@cosde super(message)}.
     * 
     * @param message the exception message
     */
    public SmartScriptLexerException(String message) {
        super(message);
    }
}
