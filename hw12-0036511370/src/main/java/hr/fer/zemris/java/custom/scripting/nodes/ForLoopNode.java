package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

import java.util.Objects;

/**
 * Represents a for loop with a variable, start, stop and end expressions.
 * 
 * @author Filip Majetic
 */
public class ForLoopNode extends Node {
    private final Element startExpression;
    private final Element endExpression;
    private final Element stepExpression;
    private final ElementVariable variable;

    /**
     * Creates a new for loop node.
     * 
     * @param startExpr the start expression of the loop, not null
     * @param endExpr   the end expression of the loop, not null
     * @param stepExpr  the step expression of the loop
     * @param variable  the counter variable of the loop, not null
     * 
     * @throws NullPointerException if {@code startExpr}, {@code endExpr} or
     *                              {@code variable} is null
     */
    public ForLoopNode(Element startExpr, Element endExpr, Element stepExpr, ElementVariable variable) {
        this.stepExpression = stepExpr;
        this.startExpression = Objects.requireNonNull(startExpr, "Start expression can't be null.");
        this.endExpression = Objects.requireNonNull(endExpr, "End expression can't be null.");
        this.variable = Objects.requireNonNull(variable, "For loop variable can't be null.");
    }

    /**
     * Gets the start expression.
     * 
     * @return the start expression
     */
    public Element getStartExpression() {
        return this.startExpression;
    }

    /**
     * Gets the end expression.
     * 
     * @return the end expression
     */
    public Element getEndExpression() {
        return this.endExpression;
    }

    /**
     * Gets the step expression.
     * 
     * @return the step expression
     */
    public Element getStepExpression() {
        return this.stepExpression;
    }

    /**
     * Gets the counter variable.
     * 
     * @return the counter variable
     */
    public ElementVariable getVariable() {
        return this.variable;
    }

    /**
     * Returns the string representation of the node data and its children, such
     * that it can be parsed back by a {@link SmartScriptParser}.
     * 
     * @return the for loop and all its children
     */
    @Override
    public String toString() {
        return String.format("{$ FOR %s %s %s %s $}", variable, startExpression, endExpression, stepExpression)
                + super.toString() + "{$ END $}";
    }

    /** {@inheritDoc} */
    @Override
    public void accept(INodeVisitor visitor) {
        visitor.visitForLoopNode(this);
    }
}
