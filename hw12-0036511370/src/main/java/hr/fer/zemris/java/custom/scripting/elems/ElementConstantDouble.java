package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Represents a double constant.
 * 
 * @author Filip Majetoc
 */
public class ElementConstantDouble extends Element {
    /** The value of the constant */
    private final double value;

    /**
     * Creates a new Element with the given value
     * 
     * @param value the double's value
     */
    public ElementConstantDouble(double value) {
        this.value = value;
    }

    /**
     * Returns the text representation of the double value
     * 
     * @return the double as a string
     */
    @Override
    public String asText() {
        return Double.toString(value);
    }

    /**
     * Delegated to {@link #asText()}.
     * 
     * @retrun the double as a string
     */
    @Override
    public String toString() {
        return asText();
    }
}
