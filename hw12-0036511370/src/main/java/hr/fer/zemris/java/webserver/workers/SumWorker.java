package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/** A worker that calculates the sum of two numbers. */
public class SumWorker implements IWebWorker {

    /**
     * Calculates the sum of two passed integers, or uses default values if no valid integers were passed.
     * Sets a temporary parameter background image based on whether the sum is even or odd, then
     * delegates page rendering to ./webroot/private/pages/calc.smscr
     *
     * @param context the context to use
     *
     * @throws Exception if an exception occurs
     */
    @Override
    public void processRequest(RequestContext context) throws Exception {

        int a = getInteger(context, "a");
        int b = getInteger(context, "b");
        int sum = a + b;

        context.setTemporaryParameter("varA", String.valueOf(a));
        context.setTemporaryParameter("varB", String.valueOf(b));
        context.setTemporaryParameter("zbroj", String.valueOf(sum));
        context.setTemporaryParameter("imgName", "/images/image" + sum % 2 + ".jpg");

        context.getDispatcher().dispatchRequest("/private/pages/calc.smscr");
    }

    private int getInteger(RequestContext ctx, String name) {

        String value = ctx.getParameter(name);

        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            return name.equals("a") ? 1 : 2;
        }
    }
}
