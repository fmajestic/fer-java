package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Thrown when a parse error occours.
 * 
 * @author Filip Majetic
 */
public class SmartScriptParserException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor. Delegated to {@cosde super()}.
     */
    public SmartScriptParserException() {
        super();
    }

    /**
     * Creates an exception with a message. Delegated to {@cosde super(message)}.
     * 
     * @param message the exception message
     */
    public SmartScriptParserException(String message) {
        super(message);
    }
}
