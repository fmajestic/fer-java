package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/** Displays an alternative homepage. */
public class Home implements IWebWorker {

    /**
     * Sets the stored (persistent) background color, or a default #7B7B7B,
     * into the temporary parameters.
     * <p>
     * Then, delegates page rendering to ./webroot/private/pages/home.smscr
     *
     * @param context the context to use
     *
     * @throws Exception if an exception occurs
     */
    @Override
    public void processRequest(RequestContext context) throws Exception {

        String bgColor = context.getPersistentParameter("bgcolor");

        context.setTemporaryParameter("background", bgColor != null ? bgColor : "7F7F7F");

        context.getDispatcher().dispatchRequest("/private/pages/home.smscr");
    }
}
