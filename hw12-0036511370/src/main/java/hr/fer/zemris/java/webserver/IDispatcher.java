package hr.fer.zemris.java.webserver;

/** A request dispatcher. */
public interface IDispatcher {

    /**
     * Dispatches a request path.
     *
     * @param urlPath the requested path
     *
     * @throws Exception if any exception occurs
     */
    void dispatchRequest(String urlPath) throws Exception;
}
