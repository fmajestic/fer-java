package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.util.StringJoiner;

/** A worker that changes the persistent background color. */
public class BgColorWorker implements IWebWorker {

    /**
     * If there was a parameter named "bgcolor" and it is a valid 6-digit hex number,
     * sets the persistent parameter "bgcolor" to that color.
     *
     * Outputs an HTML page detailing the change.
     *
     * @param context the context to use
     *
     * @throws Exception if an exception occurs
     */
    @Override
    public void processRequest(RequestContext context) throws Exception {

        String hex = context.getParameter("bgcolor");

        if (hex != null && hex.matches("[a-fA-F0-9]{6}")) {
            context.setPersistentParameter("bgcolor", hex);
        } else {
            hex = null;
        }

        StringJoiner sj = new StringJoiner("\r");

        sj.add("<!DOCTYPE html>")
            .add("<html lang=\"en\">")
            .add("<head>")
            .add("    <title>Sample</title>")
            .add("    <style>")
            .add("        body {")
            .add("            width: 35em;")
            .add("            margin: 0 auto;")
            .add("            font-family: Tahoma, Verdana, Arial, sans-serif;")
            .add("        }")
            .add("    </style>")
            .add("</head>")
            .add("<body>")
            .add("    <h1>Background color</h1>")
            .add("    <p>");

        sj.add("        <a href=\"/index2.html\">/index2.html</a> ");

        if (hex != null) {
            sj.add(String.format("background color changed to #%s</p>", hex));
            sj.add(String.format("<p><svg><rect width=\"20em\" height=\"20em\" fill=\"#%s\"/></svg></p>", hex));
        } else {
            sj.add("background color unchanged");
        }

        sj.add("</body></html>");

        context.write(sj.toString());
    }
}
