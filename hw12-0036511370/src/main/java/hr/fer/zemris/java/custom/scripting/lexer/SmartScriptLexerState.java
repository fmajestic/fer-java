package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Holds all possible lexer states.
 * 
 * @author Filip Majetic
 */
public enum SmartScriptLexerState {
    /** Text mode. Read all text until a tag is opened. */
    TEXT,
    /** Tag mode. Read token-by-token, and detect token types. */
    TAG
}
