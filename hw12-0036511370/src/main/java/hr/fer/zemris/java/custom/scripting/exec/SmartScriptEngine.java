package hr.fer.zemris.java.custom.scripting.exec;

import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.exec.function.*;
import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.webserver.RequestContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.function.BiConsumer;

/** The SmartScript execution engine. */
public class SmartScriptEngine {

    /** The document model to execute. */
    private DocumentNode documentNode;

    /** The context to operate on. */
    private RequestContext requestContext;

    /** The multistack. */
    private ObjectMultistack multistack = new ObjectMultistack();


    /** All available functions. */
    private Map<String, SmartScriptFunction> functions = new HashMap<>();

    /** All available operators. */
    private Map<String, BiConsumer<ValueWrapper, Object>> operators = new HashMap<>();

    /** The visitor that executes the script. */
    private INodeVisitor visitor = new INodeVisitor() {

        /**
         * Writes the node's text to the context.
         *
         * @param node the visited node.
         */
        @Override
        public void visitTextNode(TextNode node) {
            write(node.getText());
        }

        /**
         * Executes the for loop.
         *
         * @param node the visited node.
         */
        @Override
        public void visitForLoopNode(ForLoopNode node) {
            String counter = node.getVariable().asText();
            String start = node.getStartExpression().asText();
            String step = Objects.requireNonNullElse(node.getStepExpression(), new ElementConstantInteger(1)).asText();
            String end = node.getEndExpression().asText();

            multistack.push(counter, new ValueWrapper(start));

            while (multistack.peek(counter).numCompare(end) <= 0) {
                acceptChildren(node);
                multistack.peek(counter).add(step);
            }

            multistack.pop(counter);
        }

        /**
         * Calculates all expressions and calls all functions found in the node,
         * then writes what is left to the context.
         *
         * @param node the visited node.
         */
        @Override
        public void visitEchoNode(EchoNode node) {
            Stack<Object> values = new Stack<>();

            // Update swap and dup with the new stack
            functions.put("@swap", new Swap(values));
            functions.put("@dup", new Duplicate(values));

            for (Element el : node.getElements()) {

                if (el instanceof ElementString ||
                    el instanceof ElementConstantDouble ||
                    el instanceof ElementConstantInteger
                ) {
                    values.push(el.asText());
                } else if (el instanceof ElementVariable) {
                    values.push(multistack.peek(el.asText()).getValue());
                } else if (el instanceof ElementOperator) {
                    ValueWrapper lhs = new ValueWrapper(values.pop());
                    Object rhs = values.pop();

                    operators.get(el.asText()).accept(lhs, rhs);
                    values.push(lhs.getValue());
                } else if (el instanceof ElementFunction) {

                    if (!functions.containsKey(el.asText())) {
                        throw new IllegalStateException("Unknown function" + el.asText());
                    }

                    SmartScriptFunction fun = functions.get(el.asText());

                    Object[] argv = new Object[fun.getArgc()];

                    for (int i = argv.length - 1; i >= 0; i--) {
                        argv[i] = values.pop();
                    }

                    Object ret = fun.apply(argv);

                    if (ret != null) {
                        values.push(ret);
                    }

                } else {
                    throw new IllegalStateException("Unknown element type: " + el.getClass());
                }
            }

            // forEach traverses in the order elements were added, not top to bottom!
            values.forEach(s -> write(s.toString()));
        }

        /**
         * Traverses all children of the node.
         * @param node the visited node.
         */
        @Override
        public void visitDocumentNode(DocumentNode node) {
            acceptChildren(node);
        }

        /**
         * Calls {@link Node#accept(INodeVisitor)} with this visitor
         * as the argument on all of the node's children.
         *
         * @param node the node whose children to traverse
         */
        private void acceptChildren(Node node) {
            for (int i = 0; i < node.numberOfChildren(); i++) {
                node.getChild(i).accept(this);
            }
        }
    };

    /**
     * Creates a new execution engine.
     *
     * @param documentNode   the document model to execute
     * @param requestContext the context to use and write to
     */
    public SmartScriptEngine(DocumentNode documentNode, RequestContext requestContext) {
        this.documentNode = documentNode;
        this.requestContext = requestContext;

        operators.put("+", ValueWrapper::add);
        operators.put("-", ValueWrapper::subtract);
        operators.put("*", ValueWrapper::multiply);
        operators.put("/", ValueWrapper::divide);

        functions.put("@sin", new Sin());
        functions.put("@decfmt", new DecFmt());
        functions.put("@dup", new Duplicate(null));
        functions.put("@swap", new Swap(null));
        functions.put("@setMimeType", new SetMimeType(requestContext));
        functions.put("@paramGet", new ParamGet(requestContext));
        functions.put("@pparamGet", new PersistentParamGet(requestContext));
        functions.put("@pparamSet", new PersistentParamSet(requestContext));
        functions.put("@pparamDel", new PersistentParamDel(requestContext));
        functions.put("@tparamGet", new TemporaryParamGet(requestContext));
        functions.put("@tparamSet", new TemporaryParamSet(requestContext));
        functions.put("@tparamDel", new TemporaryParamDel(requestContext));
    }

    /**
     * Tries to write the text to the context.
     * <p>
     * Prints an error to stderr if an IO error occurs.
     *
     * @param text the text to write
     */
    private void write(String text) {
        try {
            requestContext.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Executes the script.
     */
    public void execute() {
        documentNode.accept(visitor);
    }
}
