package hr.fer.zemris.java.custom.scripting.exec.function;

import hr.fer.zemris.java.webserver.RequestContext;

/** The persistent parameter set function. */
public class PersistentParamSet extends SmartScriptFunction{

    /** The context to get parameters from. */
    private final RequestContext ctx;

    /**
     * Creates a new function for a context.
     *
     * @param ctx the context to set persistent parameters to
     */
    public PersistentParamSet(RequestContext ctx) {
        super(2);
        this.ctx = ctx;
    }

    /**
     * Sets a persistent parameter.
     *
     * @param argv exactly two arguments, the parameter value and name
     *
     * @return the mapped value (can be null), or null if no mapping was found
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);
        putMapValue(argv[1], argv[0], ctx::setPersistentParameter);
        return null;
    }
}
