package hr.fer.zemris.java.webserver;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/** An HTTP server with scripting functionality. */
@SuppressWarnings({ "FieldCanBeLocal", "WeakerAccess" })
public class SmartHttpServer {

    /** The default mime-type to use */
    private final String MIME_DEFAULT = "application/octet-stream";

    /** The session random number generator. */
    private final Random sessionRandom = new Random();


    /** The server address. */
    private String address;

    /** The server domain. */
    private String domainName;

    /** The server port. */
    private int port;

    /** The session timeout, in seconds. */
    private int sessionTimeout;

    /** The web document root. */
    private Path documentRoot;


    /** All configured mime types. */
    private Map<String, String> mimeTypes = new HashMap<>();

    /** All configured workers. */
    private Map<String, IWebWorker> workersMap = new HashMap<>();


    /** Number of worker threads to use. */
    private int workerThreads;

    /** The main server thread. */
    private ServerThread serverThread;

    private ScheduledExecutorService cleanupThreads;

    /** The client worker thread pool. */
    private ExecutorService threadPool;


    /** All known sessions. */
    private Map<String, SessionMapEntry> sessions = new HashMap<>();

    /**
     * Initializes the sever.
     *
     * @param fileName the main configuration file
     */
    private SmartHttpServer(String fileName) {

        Properties config = new Properties();
        loadProperties(config, fileName);

        address = config.getProperty("server.address");
        domainName = config.getProperty("server.domainName");
        port = Integer.parseInt(config.getProperty("server.port"));

        documentRoot = Paths.get(config.getProperty("server.documentRoot")).normalize();
        workerThreads = Integer.parseInt(config.getProperty("server.workerThreads"));
        sessionTimeout = Integer.parseInt(config.getProperty("session.timeout"));

        String mimeFile = config.getProperty("server.mimeConfig");
        Properties mimeConfig = new Properties();
        loadProperties(mimeConfig, mimeFile);
        mimeConfig.forEach((k, v) -> mimeTypes.put((String) k, (String) v));

        String workerFile = config.getProperty("server.workers");
        Properties workerConfig = new Properties();
        loadProperties(workerConfig, workerFile);

        for (Map.Entry<Object, Object> entry : workerConfig.entrySet()) {
            String path = (String) entry.getKey();
            String fqcn = (String) entry.getValue();

            try {
                IWebWorker iww = loadWebWorker(fqcn);
                workersMap.put(path, iww);
            } catch (ReflectiveOperationException e) {
                System.err.println("Web worker initialization error: ");
                e.printStackTrace();
            }
        }
    }

    /**
     * Entry point of the program
     *
     * @param args one argument, the server properties file
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Expected 1 argument: path to server properties file");
            return;
        }

        var server = new SmartHttpServer(args[0]);

        server.start();
    }

    /**
     * Loads a web worker instance.
     *
     * @param fqcn the fully qualified class name to load
     *
     * @return an instance of the worker
     *
     * @throws ReflectiveOperationException if a load or cast error occurs
     */
    private IWebWorker loadWebWorker(String fqcn) throws ReflectiveOperationException {

        Class<?> refToClass = this.getClass().getClassLoader().loadClass(fqcn);
        Object obj = refToClass.getDeclaredConstructor().newInstance();
        return (IWebWorker) obj;
    }

    /**
     * Loads a property file into the properties.
     *
     * @param properties the properties to load into
     * @param filePath   the file to load
     */
    private void loadProperties(Properties properties, String filePath) {
        try (InputStream is = Files.newInputStream(Paths.get(filePath))) {
            properties.load(is);
        } catch (InvalidPathException ex) {
            System.out.println("Invalid path given: " + filePath);
            System.exit(-1);
        } catch (IOException e) {
            System.out.printf("IO Error while reading %s: %s%n", filePath, e.getClass().getName());
            System.exit(-1);
        }
    }

    /**
     * Starts the server thread and thread pool.
     */
    protected synchronized void start() {

        threadPool = Executors.newFixedThreadPool(workerThreads);

        cleanupThreads = Executors.newSingleThreadScheduledExecutor(
            r -> {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        );

        serverThread = new ServerThread();

        serverThread.run();

        cleanupThreads.scheduleAtFixedRate(
            () -> sessions.entrySet().removeIf(kvp -> kvp.getValue().validUntil > System.currentTimeMillis()),
            0,
            5,
            TimeUnit.MINUTES
        );
    }

    /**
     * Stops the server thread and thread pool.
     */
    protected synchronized void stop() {
        serverThread.interrupt();
        threadPool.shutdown();
        cleanupThreads.shutdown();
    }


    /** Models a session. */
    private static class SessionMapEntry {

        /** The session id. */
        String sid;

        /** The session host. */
        String host;

        /** Time until session expires, in microseconds from epoch. */
        long validUntil;

        /** The permanent parameter map. */
        Map<String, String> map;

        /**
         * Creates a new session map entry.
         *
         * @param sid        the session id
         * @param host       the session host
         * @param validUntil until when is the session valid
         */
        SessionMapEntry(String sid, String host, long validUntil) {
            this.sid = sid;
            this.host = host;
            this.validUntil = validUntil;
            this.map = new ConcurrentHashMap<>();
        }
    }

    /** The main server thread. */
    protected class ServerThread extends Thread {
        @Override
        public void run() {
            try (ServerSocket serverSocket = new ServerSocket()) {
                serverSocket.bind(new InetSocketAddress(address, port));

                //noinspection InfiniteLoopStatement
                while (true) {
                    Socket client = serverSocket.accept();
                    ClientWorker cw = new ClientWorker(client);
                    threadPool.submit(cw);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    /**
     * The worker that processes client requests.
     */
    @SuppressWarnings({ "FieldCanBeLocal", "WeakerAccess" })
    private class ClientWorker implements Runnable, IDispatcher {

        /** The socket towards the client. */
        private final Socket clientSocket;

        /** The input stream. */
        private PushbackInputStream istream;

        /** The output stream. */
        private OutputStream ostream;


        /** The HTTP version. */
        private String version;

        /** The HTTP method. */
        private String method;

        /** The host. */
        private String host;


        /** The context. */
        private RequestContext context;

        /** The parameter map. */
        private Map<String, String> params = new HashMap<>();

        /** The temporary parameter map. */
        private Map<String, String> tempParams = new HashMap<>();

        /** The permanent parameter map. */
        private Map<String, String> permPrams = new HashMap<>();

        /** The output cookies. */
        private List<RequestContext.RCCookie> outputCookies = new ArrayList<>();

        /** The session id. */
        private String SID;

        /**
         * Creates a new worket that send a response to the socket
         *
         * @param clientSocket the socket to send output to
         */
        public ClientWorker(Socket clientSocket) {
            super();
            this.clientSocket = clientSocket;
        }

        /**
         * Servers the client request and closes the socket.
         */
        @Override
        public void run() {
            try (clientSocket) {
                istream = new PushbackInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                ostream = new BufferedOutputStream(clientSocket.getOutputStream());

                List<String> request = readRequest();
                if (request.size() < 1) {
                    sendBadRequest();
                    return;
                }


                String[] parts = request.get(0).split("\\s+");
                if (parts.length != 3) {
                    sendBadRequest();
                    return;
                }

                method = parts[0].toUpperCase();
                version = parts[2].toUpperCase();

                host = request.stream()
                    .filter(line -> line.startsWith("Host"))
                    .findFirst()
                    .map(line -> line.split("\\s+")[1].split(":")[0])
                    .orElse("ERROR_NO_HOST_HEADER");

                checkSession(request);

                if (!method.equals("GET") || !version.matches("HTTP/1\\.[01]")) {
                    sendBadRequest();
                    return;
                }

                String requestedPath = parts[1];
                String[] pathParts = requestedPath.split("\\?");
                String path = pathParts[0];
                String paramString = pathParts.length == 2 ? pathParts[1] : "";

                parseParameters(paramString);

                internalDispatchRequest(path, true);

                ostream.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        /**
         * Checks if the session is already stored.
         * <p>
         * If it is, refreshes the timeout, otherwise creates a new session map entry.
         *
         * @param request the request to check
         */
        private void checkSession(List<String> request) {
            String candidate = null;

            for (String line : request) {
                if (line.startsWith("Cookie")) {
                    String cookies = line.split("\\s+")[1];

                    candidate = Arrays.stream(cookies.split("; "))
                        .filter(cookie -> cookie.startsWith("sid="))
                        .findFirst()
                        .map(sid -> sid.split("=")[1])
                        .orElse(null);

                    break;
                }
            }

            long validUntil = nowPlusTimeout();
            SessionMapEntry cSession = sessions.get(candidate);

            if (cSession == null || !cSession.host.equals(host) || cSession.validUntil > validUntil) {
                cSession = createNewSession();
            } else {
                cSession.validUntil = validUntil;
            }

            permPrams = cSession.map;
        }

        /**
         * Creates a new session with a random id.
         * <p>
         * The id is composed of 20 random uppercase letters.
         *
         * @return the new map entry.
         */
        private SessionMapEntry createNewSession() {
            StringBuilder sb = new StringBuilder(20);

            sessionRandom
                .ints(20, 65, 91)
                .mapToObj(value -> (char) value)
                .forEach(sb::append);

            SID = sb.toString();
            SessionMapEntry entry = new SessionMapEntry(SID, host, nowPlusTimeout());

            sessions.put(SID, entry);
            outputCookies.add(new RequestContext.RCCookie("sid", SID, null, host, "/", true));

            return entry;
        }

        /**
         * Calculates the current time since epoch, offset by the session timeout.
         *
         * @return the current time offset by the session timeout
         */
        private long nowPlusTimeout() {
            return System.currentTimeMillis() + sessionTimeout * 1000;
        }

        /** {@inheritDoc} */
        @Override
        public void dispatchRequest(String urlPath) throws Exception {
            internalDispatchRequest(urlPath, false);
        }

        /**
         * Internal dispatch method. Checks if the call was internal or external.
         *
         * @param urlPath    the requested path
         * @param directCall is the call internal or through the interface method
         *
         * @throws Exception if an exception occurs
         */
        public void internalDispatchRequest(String urlPath, boolean directCall) throws Exception {

            if (context == null) {
                context = new RequestContext(ostream, params, permPrams, outputCookies, tempParams, this, SID);
            }

            context.setStatusCode(200);
            context.setStatusText("OK");

            if (urlPath.startsWith("/private/") && directCall) {
                sendNotFound();
                return;
            }

            if (urlPath.startsWith("/ext/")) {
                try {
                    String fqcn = "hr.fer.zemris.java.webserver.workers." + urlPath.substring(5);
                    IWebWorker worker = loadWebWorker(fqcn);
                    // System.out.println("Using worker: " + fqcn);
                    worker.processRequest(context);
                } catch (ReflectiveOperationException ex) {
                    ex.printStackTrace();
                }

                return;
            }

            if (workersMap.containsKey(urlPath)) {
                workersMap.get(urlPath).processRequest(context);
                return;
            }

            Path requestedFile = documentRoot.resolve(urlPath.substring(1));

            if (!requestedFile.normalize().startsWith(documentRoot) || !Files.isReadable(requestedFile)) {
                sendNotFound();
                return;
            }

            String ext = getFileExtension(requestedFile.getFileName().toString());

            if (ext.equals("smscr")) {
                SmartScriptParser parser = new SmartScriptParser(Files.readString(requestedFile));
                SmartScriptEngine engine = new SmartScriptEngine(parser.getDocumentNode(), context);
                engine.execute();
            } else {
                context.setMimeType(mimeTypes.getOrDefault(ext, MIME_DEFAULT));
                serveFile(context, requestedFile);
            }
        }

        /**
         * Reads the request into a list of lines.
         *
         * @return the request separated into lines
         */
        private List<String> readRequest() {
            byte[] data;

            try {
                data = internalReadRequest(istream);
            } catch (IOException ex) {
                data = new byte[0];
            }

            if (data == null) {
                data = new byte[0];
            }

            String request = new String(data);
            System.out.println(request);
            return request.lines().collect(Collectors.toList());
        }

        /**
         * A state machine that reads the full request data.
         *
         * @param is the input stream to read from
         *
         * @return the read data
         *
         * @throws IOException if an IO error occurs
         */
        private byte[] internalReadRequest(InputStream is) throws IOException {

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int state = 0;
l:
            while (true) {
                int b = is.read();
                if (b == -1) {
                    return null;
                }
                if (b != 13) {
                    bos.write(b);
                }
                switch (state) {
                case 0:
                    if (b == 13) {
                        state = 1;
                    } else if (b == 10) {
                        state = 4;
                    }
                    break;
                case 1:
                    if (b == 10) {
                        state = 2;
                    } else {
                        state = 0;
                    }
                    break;
                case 2:
                    if (b == 13) {
                        state = 3;
                    } else {
                        state = 0;
                    }
                    break;
                case 3:
                case 4:
                    if (b == 10) {
                        break l;
                    } else {
                        state = 0;
                    }
                    break;
                }
            }
            return bos.toByteArray();
        }

        /**
         * Parses the parameter string into the parameters map
         *
         * @param paramString the parameters to parse
         */
        private void parseParameters(String paramString) {
            String[] parts = paramString.split("&");

            for (String part : parts) {
                String[] split = part.split("=");

                if (split.length == 2) {
                    params.put(split[0], split[1]);
                }
            }
        }

        /**
         * Gets the file extension of a file, without the dot.
         *
         * @param file the file to read the extension from
         *
         * @return the file extension
         */
        private String getFileExtension(String file) {
            return file.contains(".") ? file.substring(file.lastIndexOf('.') + 1) : "";
        }

        /**
         * Serves a file.
         *
         * @param ctx           the context to write to
         * @param requestedFile the requested file
         *
         * @throws IOException if an IO error occurs
         */
        private void serveFile(RequestContext ctx, Path requestedFile) throws IOException {

            try (InputStream is = new BufferedInputStream(Files.newInputStream(requestedFile))) {

                ctx.setContentLength(Files.size(requestedFile));

                byte[] buf = new byte[1024];
                while (true) {
                    int r = is.read(buf);
                    if (r < 1) {
                        break;
                    }
                    ctx.write(buf, 0, r);
                }

            }
        }

        /**
         * Sends a "400 File not found" response.
         */
        private void sendNotFound() {
            sendError(404, "File not found");
        }

        /**
         * Sends a "400 Bad request" response.
         */
        private void sendBadRequest() {
            sendError(400, "Bad request");
        }

        /**
         * Sends an error response.
         *
         * @param status  the error status code
         * @param message the error message
         */
        private void sendError(int status, String message) {
            RequestContext ctx = new RequestContext(ostream, null, null, null);

            ctx.setStatusCode(status);
            ctx.setStatusText(message);

            try {
                ctx.write(new byte[0]);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
