package hr.fer.zemris.java.custom.scripting.exec.function;

import hr.fer.zemris.java.webserver.RequestContext;

/** The temporary parameter set function. */
public class TemporaryParamSet extends SmartScriptFunction{

    /** The context to set temporary parameters to. */
    private final RequestContext ctx;

    /**
     * Creates a new function for a context.
     *
     * @param ctx the context to set temporary parameters to
     */
    public TemporaryParamSet(RequestContext ctx) {
        super(2);
        this.ctx = ctx;
    }

    /**
     * Sets a temporary parameter.
     *
     * @param argv exactly two arguments, the parameter value and name
     *
     * @return {@code null}
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);
        putMapValue(argv[1], argv[0], ctx::setTemporaryParameter);
        return null;
    }
}
