package hr.fer.zemris.java.custom.scripting.exec.function;

import hr.fer.zemris.java.webserver.RequestContext;

/** The temporary parameter get function. */
public class TemporaryParamGet extends SmartScriptFunction{

    /** The context to get parameters from. */
    private final RequestContext ctx;

    /**
     * Creates a new function for a context.
     *
     * @param ctx the context to get temporary parameters from
     */
    public TemporaryParamGet(RequestContext ctx) {
        super(2);
        this.ctx = ctx;
    }

    /**
     * Gets a temporary parameter with a given name.
     *
     * @param argv exactly one argument, the parameter name to get
     *
     * @return the mapped value (can be null), or null if no mapping was found
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);

        return getValueOrDefault(argv[0], argv[1], ctx::getTemporaryParameter);
    }
}
