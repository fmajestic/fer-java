package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.text.SimpleDateFormat;
import java.util.Date;

/** A worker that show a greeting message. */
public class HelloWorker implements IWebWorker {

    /**
     * Displays a greeting message and the number of letters in a person's name,
     * if the name was passed as a parameter "name".
     *
     * @param context the context to use
     *
     * @throws Exception if an exception occurs
     */
    @Override
    public void processRequest(RequestContext context) throws Exception {

        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        context.setMimeType("text/html");
        String name = context.getParameter("name");

        context.write("<html><head>");
        context.write(String.format("<title>Hello %s!</title>", name));
        context.write(
            "<style>body { width: 35em;margin: 0 auto; font-family: Tahoma, Verdana, Arial, sans-serif; }</style>");
        context.write("</head><body>");
        context.write("<h1>Hello!!!</h1>");
        context.write(String.format("<p>Now is: %s</p>", sdf.format(now)));

        if (name == null || name.trim().isEmpty()) {
            context.write("<p>You did not send me your name!</p>");
        } else {
            context.write(String.format("<p>Your name has %d letters.</p>", name.trim().length()));
        }

        context.write("</body></html>");
    }
}
