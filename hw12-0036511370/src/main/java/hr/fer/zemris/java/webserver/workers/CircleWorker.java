package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

/** A worker that displays a circle. */
public class CircleWorker implements IWebWorker {

    /**
     * Outputs a 200x200 image of a circle with a transparent background.
     *
     * @param context the context to use
     *
     * @throws Exception if an exception occurs
     */
    @Override
    public void processRequest(RequestContext context) throws Exception {
        BufferedImage bim = new BufferedImage(200, 200, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = bim.createGraphics();

        g2d.setColor(new Color(0, 0, 0, 0));
        g2d.fillRect(0, 0, 200, 200);
        g2d.setPaint(new GradientPaint(
            0, 0,
            new Color(36, 59, 85),
            199, 199,
            new Color(20, 30, 48))
        );
        g2d.fillOval(0, 0, 199, 199);

        g2d.dispose();

        context.setMimeType("image/png");

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bim, "png", bos);
        context.setContentLength((long) bos.size());
        context.write(bos.toByteArray());
    }
}
