package hr.fer.zemris.java.custom.scripting.exec.function;

import hr.fer.zemris.java.webserver.RequestContext;

/** The set mime type function. */
public class SetMimeType extends SmartScriptFunction {

    /** The context to set mime type for. */
    private final RequestContext ctx;

    /**
     * Creates a new function for the context
     *
     * @param ctx the context to change mime type for
     */
    public SetMimeType(RequestContext ctx) {
        super(1);
        this.ctx = ctx;
    }

    /**
     * Sets the mime type
     *
     * @param argv exactly one argument, the mime type
     *
     * @return {@code null}
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);

        setValue(argv[0], ctx::setMimeType);
        return null;
    }
}
