package hr.fer.zemris.java.custom.scripting.exec.function;

import java.util.EmptyStackException;
import java.util.Stack;

/** The swap function. */
public class Swap extends SmartScriptFunction {

    /** The stack whose elements to swap. */
    private final Stack<Object> stack;

    /**
     * Creates a new swap function for the stack.
     *
     * @param stack the stack whose elements to swap
     */
    public Swap(Stack<Object> stack) {
        super(0);
        this.stack = stack;
    }

    /**
     * Swaps the top two elements of a stack.
     *
     * @param argv none
     *
     * @return null
     *
     * @throws EmptyStackException if the stack contains less than 2 elements
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);

        Object top = stack.pop();
        Object below = stack.pop();

        stack.push(top);
        stack.push(below);

        return null;
    }
}
