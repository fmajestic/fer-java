package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.lexer.*;
import hr.fer.zemris.java.custom.scripting.nodes.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType.*;

/**
 * SmartScriptParser
 */
public class SmartScriptParser {
    private String documentBody;
    private SmartScriptLexer lexer;

    public SmartScriptParser(String docBody) {
        try {
            documentBody = docBody;
            lexer = new SmartScriptLexer(documentBody);
        } catch (SmartScriptLexerException | NullPointerException ex) {
            throw new SmartScriptParserException("Document body can't be null.");
        }
    }

    public DocumentNode getDocumentNode() {
        var document = new DocumentNode();

        try {
            populateDocument(document);
        } catch (SmartScriptLexerException | NullPointerException ex) {
            throw new SmartScriptParserException("Parse error: " + ex.getMessage());
        }

        return document;
    }

    private void populateDocument(DocumentNode document) {
        Stack<Node> nodeStack = new Stack<>();
        nodeStack.push(document);

        for (SmartScriptToken token = lexer.nextToken(); token.getType() != EOF; token = lexer.nextToken()) {

            var type = token.getType();
            var lastOpenNode = (Node) nodeStack.peek();

            // If we get a text token, no further checks are needed
            if (type == TEXT) {
                var text = (String) token.getValue();
                lastOpenNode.addChildNode(new TextNode(text));
                continue;
            }

            // If we get an open tag, advance the lexer by 1 token
            // and check the tag name
            if (type == TAG_OPEN) {
                lexer.setState(SmartScriptLexerState.TAG);

                var tag = lexer.nextToken();
                var tagType = tag.getType();

                if (tagType == IDENTIFIER) {
                    var tagName = ((String) tag.getValue()).toUpperCase();

                    switch (tagName) {
                    case "FOR":
                        // Consume exactly 5 lexer tokens (4 arguments + tag close)
                        ForLoopNode loop = createForLoopNode();
                        lastOpenNode.addChildNode(loop);
                        nodeStack.push(loop);
                        break;

                    case "END":
                        // Consume 1 token, check if the tag is properly closed
                        if (lexer.nextToken().getType() != TAG_CLOSE) {
                            throw new SmartScriptParserException("Parse error: END tag with extra elements.");
                        }

                        nodeStack.pop();

                        if (nodeStack.size() == 0) {
                            throw new SmartScriptParserException("Parse error: Too many END tags.");
                        }

                        break;

                    default:
                        // If the tag type is IDENTIFIER (i.e. not an ECHO tag)
                        // but the name is not FOR or END, it is not supported
                        throw new SmartScriptParserException("Parse error: Invalid tag name <" + tagName + ">.");
                    }
                } else if (tagType == ECHO_TAG) {

                    // Consume lexer tokens until a tag close (including the tag close)
                    EchoNode echo = createEchoNode();
                    lastOpenNode.addChildNode(echo);
                } else {
                    // If a tag was opened, but the next token is not ECHO (=)
                    // or a valid identifier, the tag is invalid
                    throw new SmartScriptParserException("Parse error: Invalid tag name <" + token.getValue() + ">.");
                }

                // If we got to here, one of the branches consumed the tag close
                // and we can switch back to text mode
                lexer.setState(SmartScriptLexerState.TEXT);
            }
        }

        /*
         * If after consuming all of the tokens, the top stack element isn't the
         * DocumentNode, that means we have 1 or more unclosed FOR tags.
         *
         * The only way this check can be skipped is if an exception is thrown eariler,
         * which means the expression is invalid regardless of what the stack looks like
         */
        if (!(nodeStack.peek() instanceof DocumentNode)) {
            throw new SmartScriptParserException("Parse error: Unclosed FOR tag(s).");
        }
    }

    /**
     * Extracts all the elements of a FOR tag to a new {@code ForLoopNode}.
     * <p>
     * Called after entering a FOR tag. This method will consume exactly 5 lexer
     * tokens: 4 FOR elements (variable, start, end, step) and the tag close token.
     * <p>
     * Each element of the for loop is checked for validity: the first must be a
     * valid idenitifier (variable name), the rest must be valid expressions.
     * <p>
     * The variable is validated via {@link #createVariable(SmartScriptToken)}, the
     * expressions are validated via {@link #createExpression(SmartScriptToken)}
     *
     * @return the new {@code ForLoopNode}
     *
     * @throws SmartScriptParserException if any of the elements are invalid, or
     *                                    there are too many/too few elements
     * @see ForLoopNode
     */
    private ForLoopNode createForLoopNode() {
        // Advance lexer by 4 tokens and confirm they are the correct types
        ElementVariable variable = createVariable(lexer.nextToken());
        Element startExpr = createExpression(lexer.nextToken());
        Element endExpr = createExpression(lexer.nextToken());
        Element stepExpr = null;

        var next = lexer.nextToken();

        if (next.getType() != TAG_CLOSE) {
            stepExpr = createExpression(next);

            // If the for tag isn't closed after 4 tokens
            if (lexer.nextToken().getType() != TAG_CLOSE) {
                throw new SmartScriptParserException("Parse error: too many FOR loop arguments.");
            }
        }

        return new ForLoopNode(startExpr, endExpr, stepExpr, variable);
    }

    /**
     * Extracts all the elements of an ECHO (=) tag to a new {@code EchoNode}.
     * <p>
     * Called after entering an ECHO (=) tag. This method will consume lexer tokens
     * until it encounters a tag close token (and will consume it, too).
     * <p>
     * The intermediary list is an {@link java.util.ArrayList}.
     *
     * @throws SmartScriptParserException if the tag contains illegal types
     * @see EchoNode
     */
    private EchoNode createEchoNode() {
        List<Element> elements = new ArrayList<>();

        for (SmartScriptToken next = lexer.nextToken(); next.getType() != TAG_CLOSE; next = lexer.nextToken()) {

            switch (next.getType()) {
            case IDENTIFIER:
                elements.add(new ElementVariable((String) next.getValue()));
                break;

            case CONST_DOUBLE:
                elements.add(new ElementConstantDouble((Double) next.getValue()));
                break;

            case CONST_INTEGER:
                elements.add(new ElementConstantInteger((Integer) next.getValue()));
                break;

            case FUNCTION:
                elements.add(new ElementFunction((String) next.getValue()));
                break;

            case STRING:
                elements.add(new ElementString((String) next.getValue()));
                break;

            case OP_ADD:
            case OP_SUB:
            case OP_MUL:
            case OP_DIV:
            case OP_POW:
                elements.add(new ElementOperator((String) next.getValue()));
                break;

            default:
                throw new SmartScriptParserException("Parse error: Invalid element type in echo tag.");
            }
        }

        // Must use Arrays.copyOf!
        // A simple cast compiles, but throws a ClassCastException at runtime.
        return new EchoNode(Arrays.copyOf(elements.toArray(), elements.size(), Element[].class));
    }

    /**
     * Creates an expression element from a valid token.
     *
     * @param expressionToken the token to make an expression from
     *
     * @return the {@code Element} subclass representing this token
     *
     * @throws SmartScriptParserException if the token doesn't have a valid
     *                                    expression type
     */
    private Element createExpression(SmartScriptToken expressionToken) {
        if (!isValidExpressionType(expressionToken)) {
            throw new SmartScriptParserException("Parse error: Invalid for loop expression.");
        }

        Object value = expressionToken.getValue();
        Element ret = null;

        switch (expressionToken.getType()) {
        case IDENTIFIER:
            ret = new ElementVariable((String) value);
            break;

        case CONST_DOUBLE:
            ret = new ElementConstantDouble((Double) value);
            break;

        case CONST_INTEGER:
            ret = new ElementConstantInteger((Integer) value);
            break;

        case STRING:
            ret = new ElementString((String) value);
            break;

        default:
            break;
        }

        return ret;
    }

    /**
     * Creates a variable element from a valid token.
     *
     * @param variableToken the token to make a variable from
     *
     * @return the {@code ElementVariable} with this token's value
     *
     * @throws SmartScriptParserException if the token type is not
     *                                    {@code TokenType.IDENTIFIER}
     * @see ElementVariable
     * @see SmartScriptTokenType#IDENTIFIER
     */
    private ElementVariable createVariable(SmartScriptToken variableToken) {
        // There is only one valid type for a loop counter
        if (variableToken.getType() != IDENTIFIER) {
            throw new SmartScriptParserException("Parse error: Invalid FOR variable.");
        }

        return new ElementVariable((String) variableToken.getValue());
    }

    /**
     * Checks if an expression token has a valid type.
     * <p>
     * The valid types are:
     * <ul>
     * <li>{@link SmartScriptTokenType#IDENTIFIER}
     * <li>{@link SmartScriptTokenType#CONST_DOUBLE}
     * <li>{@link SmartScriptTokenType#CONST_INTEGER}
     * <li>{@link SmartScriptTokenType#STRING}
     * </ul>
     *
     * @param expressionToken the token to check
     *
     * @return true if the token's type matches at least one of the defined valid
     *     types, false otherwise
     */
    private boolean isValidExpressionType(SmartScriptToken expressionToken) {
        SmartScriptTokenType[] validExpressionTypes = { IDENTIFIER, CONST_DOUBLE, CONST_INTEGER, STRING };

        // Expressions can be of multiple types
        return tokenIsAny(expressionToken, validExpressionTypes);
    }

    /**
     * Checks if the {@code token} type matches any of the given types.
     *
     * @param token the token to compare
     * @param types the types to check
     *
     * @return true if the token's type matches any of the given types, false
     *     otherwise
     */
    private boolean tokenIsAny(SmartScriptToken token, SmartScriptTokenType... types) {
        var tokenType = token.getType();

        for (var type : types) {
            if (tokenType == type) {
                return true;
            }
        }

        return false;
    }
}
