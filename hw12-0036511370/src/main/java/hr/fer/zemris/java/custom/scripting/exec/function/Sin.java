package hr.fer.zemris.java.custom.scripting.exec.function;

/** The sine function. */
public class Sin extends SmartScriptFunction {

    /**
     * Creates a new sine function.
     */
    public Sin() {
        super(1);
    }

    /**
     * Computes the sine of an angle given in degrees.
     *
     * @param argv exactly one argument, the angle in degrees
     *
     * @return the sine
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);

        Number number = getNumber(argv[0]);
        return Math.sin(Math.toRadians(number.doubleValue()));
    }
}
