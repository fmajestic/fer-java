package hr.fer.zemris.java.custom.scripting.exec.function;

import hr.fer.zemris.java.webserver.RequestContext;

/** The persistent parameter delete function. */
public class PersistentParamDel extends SmartScriptFunction{

    /** The context to get parameters from. */
    private final RequestContext ctx;

    /**
     * Creates a new function for a context.
     *
     * @param ctx the context to get parameters from
     */
    public PersistentParamDel(RequestContext ctx) {
        super(1);
        this.ctx = ctx;
    }

    /**
     * Deletes a persistent parameter with a given name.
     *
     * @param argv exactly one argument, the parameter name to delete
     *
     * @return {@code null}
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);

        deleteValue(argv[0], ctx::removePersistentParameter);
        return null;
    }
}
