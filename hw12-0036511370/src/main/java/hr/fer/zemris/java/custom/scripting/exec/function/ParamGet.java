package hr.fer.zemris.java.custom.scripting.exec.function;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Objects;

/** The parameter get function. */
public class ParamGet extends SmartScriptFunction {

    /** The context to get parameters from. */
    private final RequestContext ctx;

    /**
     * Creates a new function for a context.
     *
     * @param ctx the context to get parameters from
     */
    public ParamGet(RequestContext ctx) {
        super(2);
        this.ctx = Objects.requireNonNull(ctx);
    }

    /**
     * Gets a parameter with a given name.
     *
     * @param argv exactly one argument, the parameter name to get
     *
     * @return the mapped value (can be null), or null if no mapping was found
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);
        return getValueOrDefault(argv[0], argv[1], ctx::getParameter);
    }
}
