package hr.fer.zemris.java.custom.scripting.exec.function;

import hr.fer.zemris.java.webserver.RequestContext;

/** The temporary parameter delete function. */
public class TemporaryParamDel extends SmartScriptFunction{

    /** The context to get parameters from. */
    private final RequestContext ctx;

    /**
     * Creates a new function for a context.
     *
     * @param ctx the context to delete temporary parameters from
     */
    public TemporaryParamDel(RequestContext ctx) {
        super(1);
        this.ctx = ctx;
    }

    /**
     * Deletes a temporary parameter with a given name.
     *
     * @param argv exactly one argument, the parameter name to delete
     *
     * @return {@code null}
     */
    @Override
    public Object apply(Object... argv) {
        requireArgumentCount(argv);

        deleteValue(argv[0], ctx::removeTemporaryParameter);
        return null;

    }
}
