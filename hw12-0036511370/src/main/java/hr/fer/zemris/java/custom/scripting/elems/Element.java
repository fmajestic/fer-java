package hr.fer.zemris.java.custom.scripting.elems;

/**
 * A base class for all elements.
 * 
 * @author Filip Majetic
 */
public class Element {
    /**
     * Default implementation. Returns an empty string.
     * 
     * @return an empty string.
     */
    public String asText() {
        return "";
    }

}
