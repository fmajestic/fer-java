package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Set;

/** A worker that simply displays all passed parameters. */
public class EchoParams implements IWebWorker {

    /**
     * Displays all passed parameters in a table.
     *
     * @param context the context to use
     *
     * @throws Exception if an exception occurs
     */
    @Override
    public void processRequest(RequestContext context) throws Exception {

        context.setMimeType("text/html");

        StringBuilder sb = new StringBuilder("<!DOCTYPE html>\r\n" +
            "<html lang=\"en\">\r\n" +
            "<head>\r\n" +
            "    <title>EchoParams</title>\r\n" +
            "    <style>\r\n" +
            "        body {\r\n" +
            "            width: 35em;\r\n" +
            "            margin: 0 auto;\r\n" +
            "            font-family: Tahoma, Verdana, Arial, sans-serif;\r\n" +
            "        }\r\n" +
            "    </style>\r\n" +
            "</head>\r\n" +
            "<body>\r\n" +
            "    <h1>Request parameters</h1>\r\n");


        Set<String> paramNames = context.getParameterNames();

        if (!paramNames.isEmpty()) {
            sb.append("    <table border='1'>\r\n    <tr><td>Name</td><td>Value</td></tr>\r\n");

            for (String name : paramNames) {
                sb.append("    <tr><td>")
                    .append(name)
                    .append("</td><td>")
                    .append(context.getParameter(name))
                    .append("</td></tr>\r\n");
            }

            sb.append("    </table>\n</body>\r\n</html>");
        } else {
            sb.append("<p>No parameters given!</p>");
        }

        context.setContentLength((long) sb.length());
        context.write(sb.toString());
    }
}
