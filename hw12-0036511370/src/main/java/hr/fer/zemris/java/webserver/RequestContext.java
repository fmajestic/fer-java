package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

@SuppressWarnings({ "WeakerAccess", "UnusedReturnValue", "unused" })
public class RequestContext {

    /** Was the header generated. */
    private boolean headerGenerated = false;
    /** Charset for content encoding. */
    private Charset charset;
    /** Request output stream. */
    private OutputStream outputStream;
    /** Length of the content. */
    private Long contentLength = null;
    /** Encoding of the content. */
    private String encoding = "UTF-8";
    /** Mime-type of the content. */
    private String mimeType = "text/html";
    /** Status code. */
    private int statusCode = 200;
    /** Status message. */
    private String statusText = "OK";
    /** Header cookies. */
    private List<RCCookie> outputCookies;
    /** Parameters. */
    private Map<String, String> parameters;
    /** Persistent parameters. */
    private Map<String, String> persistentParameters;
    /** Temporary parameters. */
    private Map<String, String> temporaryParameters = new HashMap<>();

    /** The dispatcher. */
    private IDispatcher dispatcher;

    /** The session ID. */
    private String sid;

    /**
     * Creates a new request context.
     *
     * @param outputStream         the stream to write to
     * @param parameters           the parameters
     * @param persistentParameters the persistent parameters
     * @param outputCookies        the header cookies
     */
    public RequestContext(
        OutputStream outputStream,
        Map<String, String> parameters,
        Map<String, String> persistentParameters,
        List<RCCookie> outputCookies
    ) {
        this.outputStream = Objects.requireNonNull(outputStream);
        this.parameters = parameters != null ? parameters : new HashMap<>();
        this.outputCookies = outputCookies != null ? outputCookies : new ArrayList<>();
        this.persistentParameters = persistentParameters != null ? persistentParameters : new HashMap<>();
    }

    /**
     * Creates a new request context.
     *
     * @param outputStream         the stream to write to
     * @param parameters           the parameters
     * @param persistentParameters the persistent parameters
     * @param outputCookies        the header cookies
     * @param temporaryParameters  the temporary parameters
     * @param dispatcher           the dispatcher
     */
    public RequestContext(
        OutputStream outputStream,
        Map<String, String> parameters,
        Map<String, String> persistentParameters,
        List<RCCookie> outputCookies,
        Map<String, String> temporaryParameters,
        IDispatcher dispatcher,
        String sid
    ) {
        this(outputStream, parameters, persistentParameters, outputCookies);
        this.temporaryParameters = temporaryParameters;
        this.dispatcher = dispatcher;
        this.sid = sid;
    }

    /**
     * Sets the content length.
     *
     * @param contentLength the new content length, can be null
     *
     * @throws RuntimeException if the header has already been generated
     */
    public void setContentLength(Long contentLength) {
        requireHeaderNotGenerated();
        this.contentLength = contentLength;
    }

    /**
     * Sets the encoding.
     *
     * @param encoding the new encoding
     *
     * @throws RuntimeException if the header has already been generated
     */
    public void setEncoding(String encoding) {
        requireHeaderNotGenerated();
        this.encoding = encoding;
    }

    /**
     * Sets the mime-type.
     *
     * @param mimeType the new mime type
     *
     * @throws RuntimeException if the header has already been generated
     */
    public void setMimeType(String mimeType) {
        requireHeaderNotGenerated();
        this.mimeType = mimeType;
    }

    /**
     * Sets the status code
     *
     * @param statusCode the new status code
     *
     * @throws RuntimeException if the header has already been generated
     */
    public void setStatusCode(int statusCode) {
        requireHeaderNotGenerated();
        this.statusCode = statusCode;
    }

    /**
     * Sets the status message.
     *
     * @param statusText the new status message
     *
     * @throws RuntimeException if the header has already been generated
     */
    public void setStatusText(String statusText) {
        requireHeaderNotGenerated();
        this.statusText = statusText;
    }

    /**
     * Adds a non-null cookie to the request header.
     *
     * @param rcCookie the cookie to add, not null
     *
     * @throws NullPointerException if the cookie is null
     */
    public void addRCCookie(RCCookie rcCookie) {
        outputCookies.add(Objects.requireNonNull(rcCookie, "Request cookie can't be null"));
    }

    /**
     * Gets the dispatcher.
     *
     * @return the dispatcher
     */
    public IDispatcher getDispatcher() {
        return dispatcher;
    }

    /**
     * Retrieves a value from the parameters map.
     *
     * @param name name of the value to get
     *
     * @return the associated value, or null if no mapping was found
     */
    public String getParameter(String name) {
        return parameters.get(name);
    }

    /**
     * Retrieves all names in the parameters map.
     * <p>
     * The returned set is read-only.
     *
     * @return all names in the parameters map
     */
    public Set<String> getParameterNames() {
        return Collections.unmodifiableSet(parameters.keySet());
    }

    /**
     * Retrieves a value from the persistent parameters map.
     *
     * @param name name of the value to get
     *
     * @return the associated value, or null if no mapping was found
     */
    public String getPersistentParameter(String name) {
        return persistentParameters.get(name);
    }

    /**
     * Retrieves all names in the persistent parameters map.
     * <p>
     * The returned set is read-only.
     *
     * @return all names in the persistent parameters map
     */
    public Set<String> getPersistentParameterNames() {
        return Collections.unmodifiableSet(persistentParameters.keySet());
    }

    /**
     * Stores a value into the persistent parameters map.
     * <p>
     * Both the map and the key can be null.
     *
     * @param name  the parameter name
     * @param value the parameter value
     */
    public void setPersistentParameter(String name, String value) {
        persistentParameters.put(name, value);
    }

    /**
     * Removes a value from the persistent parameters map.
     * <p>
     * Both the map and the key can be null.
     *
     * @param name the parameter name
     */
    public void removePersistentParameter(String name) {
        persistentParameters.remove(name);
    }

    /**
     * Retrieves a value from the temporary parameters map.
     *
     * @param name name of the value to get
     *
     * @return the associated value, or null if no mapping was found
     */
    public String getTemporaryParameter(String name) {
        return temporaryParameters.get(name);
    }

    /**
     * Retrieves all names in the temporary parameters map.
     * <p>
     * The returned set is read-only.
     *
     * @return all names in the temporary parameters map
     */
    public Set<String> getTemporaryParameterNames() {
        return Collections.unmodifiableSet(temporaryParameters.keySet());
    }

    /**
     * Retrieves an identifier that is unique for each user session.
     * <p>
     * Currently, an empty string
     *
     * @return the unique ID (currently an empty string)
     */
    public String getSessionID() {
        return sid;
    }

    /**
     * Stores a value into the temporary parameters map.
     * <p>
     * Both the map and the key can be null.
     *
     * @param name  the parameter name
     * @param value the parameter value
     */
    public void setTemporaryParameter(String name, String value) {
        temporaryParameters.put(name, value);
    }

    /**
     * Removes a value from the temporary parameters map.
     * <p>
     * Both the map and the key can be null.
     *
     * @param name the parameter name
     */
    public void removeTemporaryParameter(String name) {
        temporaryParameters.remove(name);
    }


    /**
     * Generates and writes header, then writes the data to the output stream.
     *
     * @param data the data to write
     *
     * @return this context
     *
     * @throws IOException      if an IO error occurs
     * @throws RuntimeException if the header was already generated
     */
    public RequestContext write(byte[] data) throws IOException {
        return write(data, 0, data.length);
    }

    /**
     * Generates and writes header, then writes the data to the output stream.
     *
     * @param data   the data to write
     * @param offset the initial offset into the data
     * @param len    the number of bytes to write, from offset
     *
     * @return this context
     *
     * @throws IOException      if an IO error occurs
     * @throws RuntimeException if the header was already generated
     */
    public RequestContext write(byte[] data, int offset, int len) throws IOException {
        if (!headerGenerated) {
            generateAndWriteHeader();
        }

        outputStream.write(data, offset, len);
        return this;
    }

    /**
     * Generates and writes header, then writes the text to the output stream.
     * <p>
     * The text is converted into bytes using the specified encoding (defaults to UTF-8).
     *
     * @param text the text to write
     *
     * @return this context
     *
     * @throws IOException      if an IO error occurs
     * @throws RuntimeException if the header was already generated
     */
    public RequestContext write(String text) throws IOException {
        if (!headerGenerated) {
            generateAndWriteHeader();
        }

        return write(text.getBytes(charset));
    }


    /** Generates and writes header to the output stream. */
    private void generateAndWriteHeader() throws IOException {

        headerGenerated = true;

        charset = Charset.forName(encoding);

        StringJoiner sj = new StringJoiner("\r\n", "", "\r\n\r\n");

        sj.add(String.format("HTTP/1.1 %d %s", statusCode, statusText));

        if (mimeType.startsWith("text/")) {
            sj.add(String.format("Content-Type: %s; charset=%s", mimeType, encoding));
        } else {
            sj.add(String.format("Content-Type: %s", mimeType));
        }

        if (contentLength != null) {
            sj.add(String.format("Content-Length: %d", contentLength));
        }

        for (RCCookie cookie : outputCookies) {
            sj.add(String.format("Set-Cookie: %s", cookie.toString()));
        }

        String header = sj.toString();
        outputStream.write(header.getBytes(StandardCharsets.ISO_8859_1));
    }

    /**
     * Checks if the header has been generated,
     * and throws an exception if it has been.
     * <p>
     * The header is generated on the first call of any write method.
     *
     * @throws RuntimeException if the header has been generated.
     */
    private void requireHeaderNotGenerated() {
        if (headerGenerated) {
            throw new RuntimeException("Message already written to output stream.");
        }
    }


    /** A single cookie with a name, value, domain, path, and an optional maximum age. */
    public static class RCCookie {

        /** The cookie age. */
        private String name;

        /** The cookie value. */
        private String value;

        /** The cookie domain. */
        private String domain;

        /** The cookie path. */
        private String path;

        /** The max cookie age. */
        private Integer maxAge;

        /** Is the cookie http-only. */
        private boolean isHttpOnly;

        /**
         * Creates a new cookie with the given values.
         *
         * @param name   the name
         * @param value  the value
         * @param domain the domain
         * @param path   the path
         * @param maxAge the max age, can be null if there is no max age
         */
        public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
            this.name = name;
            this.value = value;
            this.domain = domain;
            this.path = path;
            this.maxAge = maxAge;
        }

        public RCCookie(String name, String value, Integer maxAge, String domain, String path, boolean httpOnly) {
            this.name = name;
            this.value = value;
            this.domain = domain;
            this.path = path;
            this.maxAge = maxAge;
            this.isHttpOnly = httpOnly;
        }

        /**
         * Gets the cookie name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Gets the cookie value.
         *
         * @return the value
         */
        public String getValue() {
            return value;
        }

        /**
         * Gets the cookie domain.
         *
         * @return the domain
         */
        public String getDomain() {
            return domain;
        }

        /**
         * Gets the cookie path.
         *
         * @return the path
         */
        public String getPath() {
            return path;
        }

        /**
         * Gets the max cookie age.
         *
         * @return the max age, or null if there is no max age
         */
        public Integer getMaxAge() {
            return maxAge;
        }

        /**
         * Gets the http-only cookie property.
         * @return true if the cookie is set as http-only
         */
        public boolean getIsHttpOnly() {
            return isHttpOnly;
        }

        /**
         * Generates a string that can be used in a request header.
         *
         * @return the cookie as a string
         */
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();

            sb.append(name).append("=").append(value);

            if (domain != null) {
                sb.append("; Domain=").append(domain);
            }

            if (path != null) {
                sb.append("; Path=").append(path);
            }

            if (maxAge != null) {
                sb.append("; Max-Age=").append(maxAge);
            }

            if (isHttpOnly) {
                sb.append("; HttpOnly");
            }

            return sb.toString();
        }
    }
}
