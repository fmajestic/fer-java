package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Defines all possible token types.
 * 
 * @author Filip Majetic
 */
public enum SmartScriptTokenType {
    /** Integer constant */
    CONST_INTEGER,
    /** Real constant */
    CONST_DOUBLE,
    /** Function name */
    FUNCTION,
    /** Tag or variable name */
    IDENTIFIER,
    /** String literal */
    STRING,
    /** Regular text outside of tag */
    TEXT,
    /** Tag opened */
    TAG_OPEN,
    /** Tag closed */
    TAG_CLOSE,
    /** Addition operaor (+) */
    OP_ADD,
    /** Subtraction operator (-) */
    OP_SUB,
    /** Multiplication operator (*) */
    OP_MUL,
    /** Division operator (/) */
    OP_DIV,
    /** Power operator (^) */
    OP_POW,
    /** An echo tag (+) */
    ECHO_TAG,
    /** End of input data */
    EOF

    /*
     * In the future, once collections are allowed, we can add things like:
     *
     * public static final EnumSet<TokenType> CHILDLESS = EnumSet.of(TEXT,
     * ECHO_TAG); public static final EnumSet<TokenType> OPERATORS =
     * EnumSet.of(OP_ADD, OP_SUB, OP_MUL, OP_DIV, OP_POW); public static final
     * EnumSet<TokenType> LITERALS = EnumSet.of(CONST_INTEGER, CONST_DOUBLE,
     * STRING);
     */
}
