package hr.fer.zemris.java.custom.scripting.exec.function;

import java.util.EmptyStackException;
import java.util.Stack;

/** The duplicate function. */
public class Duplicate extends SmartScriptFunction {

    /** The stack whose top to duplicate. */
    private final Stack<Object> stack;

    /**
     * Creates a new duplication function for the stack.
     *
     * @param stack the stack whose top to duplicate
     *
     * @throws NullPointerException if the stack is null
     */
    public Duplicate(Stack<Object> stack) {
        super(0);
        this.stack = stack;
    }

    /**
     * Duplicates current top value from the stack.
     *
     * @param argv none
     *
     * @return {@code null}
     *
     * @throws EmptyStackException if the stack is empty
     */
    @Override
    public Object apply(Object... argv) throws EmptyStackException {
        requireArgumentCount(argv);

        stack.push(stack.peek());

        return null;
    }
}
