package hr.fer.zemris.java.custom.scripting.exec.function;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

/** Models a SmartScript function. */
public abstract class SmartScriptFunction {

    /** The argument count. */
    private int argc;

    /**
     * Creates a new function.
     *
     * @param argc the number of arguments the function requires
     *
     * @throws IllegalArgumentException if the number of arguments is negative
     */
    public SmartScriptFunction(int argc) {
        if (argc < 0) {
            throw new IllegalArgumentException("Argument count can't be negative");
        }
        this.argc = argc;
    }

    /**
     * Gets the number from an object that is an instance of {@link Number} or a valid number string.
     *
     * @param o the object to extract a number from
     *
     * @return the number
     *
     * @throws IllegalArgumentException if the object is not a {@link Number}, or a valid number string.
     */
    static Number getNumber(Object o) {
        Number num;

        if (o instanceof Number) {
            num = (Number) o;
        } else {
            String str = (String) o;
            try {
                num = Double.parseDouble(str);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Argument must be an integer, double, or valid number string");
            }
        }

        return num;
    }

    /**
     * Gets a named value from a getter, or the default value if the getter returns {@code null}.
     *
     * @param key          the key to apply to the getter.
     * @param defaultValue the default value to return
     * @param getter       the getter to use
     *
     * @return the getter value, or the default value if getter returned null
     */
    static Object getValueOrDefault(Object key, Object defaultValue, Function<String, String> getter) {
        String ret = getter.apply(asString(key));
        return ret != null ? ret : defaultValue;
    }

    /**
     * Puts a named value into a map.
     *
     * @param name   the name of the value
     * @param value  the value to set
     * @param putter (usually) the map's {@code put} function reference
     */
    static void putMapValue(Object name, Object value, BiConsumer<String, String> putter) {
        String strKey = asString(name);
        putter.accept(strKey, String.valueOf(value));
    }

    /**
     * Sets the value using the consumer.
     *
     * @param value  the value to set
     * @param setter the consumer to apply
     */
    static void setValue(Object value, Consumer<String> setter) {
        String str = asString(value);
        setter.accept(str);
    }

    /**
     * Deletes a value from a map.
     *
     * @param key     the key to delete from
     * @param deleter the consumer to apply
     */
    static void deleteValue(Object key, Consumer<String> deleter) {
        String strKey = asString(key);
        deleter.accept(strKey);
    }

    /**
     * Gets the object as a string, or throws an exception if it is not a string.
     *
     * @param key the object to cast
     *
     * @return the object as a string
     *
     * @throws IllegalArgumentException if the object is not a string
     */
    private static String asString(Object key) {
        if (!(key instanceof String)) {
            throw new IllegalArgumentException("Parameters map uses strings as keys, got: " + key.getClass());
        }

        return (String) key;
    }

    /**
     * Gets the required argument count for this function.
     *
     * @return the argument count
     */
    public int getArgc() {
        return argc;
    }

    /**
     * Applies the function.
     *
     * @param argv the function arguments
     *
     * @return the return value, or null
     */
    public abstract Object apply(Object... argv);

    /**
     * Checks if the given argument array contains exactly {@code argc} elements.
     *
     * @param argv the arguments to check
     *
     * @throws IllegalArgumentException if the argument count is different than the required one
     * @see #getArgc()
     */
    void requireArgumentCount(Object[] argv) {
        if (argv.length != argc) {
            throw new IllegalArgumentException(
                String.format("Invalid number of arguments. Expected: %d. Got: %d.", argc, argv.length));
        }
    }
}
