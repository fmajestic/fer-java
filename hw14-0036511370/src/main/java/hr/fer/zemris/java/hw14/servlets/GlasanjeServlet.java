package hr.fer.zemris.java.hw14.servlets;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Displays the voting page.
 */
@WebServlet(name = "glasanje", urlPatterns = "/servleti/glasanje")
public class GlasanjeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int id;
        String pollID = req.getParameter("pollID");

        try {
            id = Integer.parseInt(pollID);
        } catch (NumberFormatException ex) {
            Util.sendError(req, resp, pollID);
            return;
        }

        DAO dao = DAOProvider.getDao();
        Poll poll = dao.getPoll(id);
        List<PollOption> options = dao.getPollOptions(id, "id", true);

        req.setAttribute("poll", poll);
        req.setAttribute("pollOptions", options);
        req.getRequestDispatcher("/WEB-INF/pages/poll.jsp").forward(req, resp);
    }
}
