package hr.fer.zemris.java.hw14.servlets;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Adds a vote to a passed option.
 */
@WebServlet(name = "glasaj", urlPatterns = "/servleti/glasanje-glasaj")
public class GlasanjeGlasajServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        int id;
        String optionID = req.getParameter("optionID");

        try {
            id = Integer.parseInt(optionID);
        } catch (NumberFormatException ex) {
            Util.sendError(req, resp, optionID);
            return;
        }

        DAO dao = DAOProvider.getDao();

        if (!dao.vote(id, 1)) {
            Util.sendError(req, resp, optionID);
            return;
        }

        req.getRequestDispatcher("/servleti/glasanje-rezultati").forward(req, resp);
    }
}
