package hr.fer.zemris.java.hw14.servlets;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Generates a pie chart with the poll results.
 */
@WebServlet(name = "grafika", urlPatterns = { "/servleti/glasanje-grafika" })
public class GlasanjeGrafikaServlet extends HttpServlet {

    /**
     * Generates a pie chart with the vote results an prints it directly to the response.
     * The appropriate Content-Type is set (image/png).
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws IOException if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        DefaultPieDataset dataset = new DefaultPieDataset();

        int id;
        String pollID = req.getParameter("pollID");

        try {
            id = Integer.parseInt(pollID);
        } catch (NumberFormatException ex) {
            Util.sendError(req, resp, pollID);
            return;
        }

        DAO dao = DAOProvider.getDao();
        Poll poll = dao.getPoll(id);
        List<PollOption> options = dao.getPollOptions(id, "votesCount", false);

        for (PollOption op : options) {
            if (op.getVotes() > 0) {
                dataset.setValue(op.getTitle(), op.getVotes());
            }
        }

        JFreeChart chart = ChartFactory.createPieChart3D(
            poll.getTitle(),
            dataset,
            true,
            true,
            false
        );

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);

        resp.setContentType("image/png");
        resp.setHeader("Content-Disposition", String.format("result-%d.png", poll.getId()));

        ChartUtils.writeChartAsPNG(
            resp.getOutputStream(),
            chart,
            500,
            350
        );
    }
}

