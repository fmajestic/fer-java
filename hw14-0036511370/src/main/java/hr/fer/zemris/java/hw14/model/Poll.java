package hr.fer.zemris.java.hw14.model;

/**
 * Models a poll.
 * <p>
 * Used as a bean for data transfer.
 */
public class Poll {

    /** The poll id. */
    private int id;

    /** The poll title. */
    private String title;

    /** The poll message. */
    private String message;

    /**
     * Creates a new poll.
     *
     * @param id      the id
     * @param title   the title
     * @param message the message
     */
    public Poll(int id, String title, String message) {
        this.id = id;
        this.title = title;
        this.message = message;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the title.
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the message.
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets a string representation of the poll
     * @return the poll as a string
     */
    @Override
    public String toString() {
        return String.format("Poll [id=%d; title=\"%s\"]", id, title);
    }
}
