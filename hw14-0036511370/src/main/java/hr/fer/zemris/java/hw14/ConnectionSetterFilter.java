package hr.fer.zemris.java.hw14;

import hr.fer.zemris.java.hw14.dao.sql.SQLConnectionProvider;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebFilter(filterName = "servletFilter", urlPatterns = { "/servleti/*" })
public class ConnectionSetterFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    /**
     * Initializes a database connection for the servlet.
     *
     * @param request  the request
     * @param response the response
     * @param chain    the chain
     *
     * @throws IOException      if an error occurs
     * @throws ServletException if an error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        DataSource ds = (DataSource) request.getServletContext().getAttribute("hr.fer.zemris.dbpool");

        try (Connection con = ds.getConnection()) {
            SQLConnectionProvider.setConnection(con);
            chain.doFilter(request, response);
        } catch (SQLException e) {
            throw new IOException("Database not available.", e);
        } finally {
            SQLConnectionProvider.setConnection(null);
        }
    }

    @Override
    public void destroy() {
    }
}