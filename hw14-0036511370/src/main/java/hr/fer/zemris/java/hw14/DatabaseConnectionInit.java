package hr.fer.zemris.java.hw14;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

@WebListener
public class DatabaseConnectionInit implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        Properties dbSettings = new Properties();

        try (InputStream inputStream = Files.newInputStream(
            Paths.get(sce.getServletContext().getRealPath("/WEB-INF/dbsettings.properties")))
        ) {
            dbSettings.load(inputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String host = dbSettings.getProperty("host");
        String port = dbSettings.getProperty("port");
        String name = dbSettings.getProperty("name");
        String user = dbSettings.getProperty("user");
        String pass = dbSettings.getProperty("password");

        String connectionURL = String.format(
            "jdbc:derby://%s:%s/%s;user=%s;password=%s",
            host, port, name, user, pass
        );


        ComboPooledDataSource cpds = new ComboPooledDataSource();

        try {
            cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
            cpds.setJdbcUrl(connectionURL);
        } catch (PropertyVetoException ex) {
            throw new RuntimeException("Error initializing data pool.", ex);
        }

        verifyTablesAndData(cpds);

        sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ComboPooledDataSource cpds =
            (ComboPooledDataSource) sce.getServletContext().getAttribute("hr.fer.zemris.dbpool");

        if (cpds != null) {
            try {
                DataSources.destroy(cpds);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Verifies that at least 2 tables and some options exist.
     *
     * @param cpds the data source to get connections from
     */
    private void verifyTablesAndData(ComboPooledDataSource cpds) {
        try (Connection con = cpds.getConnection()) {

            createIfNotExisting(con);

            try (
                PreparedStatement pst = con.prepareStatement("SELECT * FROM POLLS");
                ResultSet rs = pst.executeQuery()
            ) {
                if (!rs.next()) {
                    addPolls(con);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Adds two polls.
     *
     * @param con the database connection
     *
     * @throws SQLException if an error occurs
     */
    private void addPolls(Connection con) throws SQLException {

        int bandID, osID;

        try (PreparedStatement pst = con.prepareStatement(
            "INSERT INTO POLLS (TITLE, MESSAGE) VALUES\n" +
            "('Favorite band poll', 'Which of the following bands is your favorite? Click a link to vote!')",
            Statement.RETURN_GENERATED_KEYS
        )) {
            pst.executeUpdate();

            try (ResultSet rs = pst.getGeneratedKeys()) {
                rs.next();
                bandID = rs.getInt(1);
            }
        }

        try (PreparedStatement pst = con.prepareStatement(
            "INSERT INTO POLLS (TITLE, MESSAGE) VALUES\n" +
            "('OS usage poll', 'Which operating system do you mainly use? Click a link to vote!')",
            Statement.RETURN_GENERATED_KEYS
        )) {
            pst.executeUpdate();

            try (ResultSet rs = pst.getGeneratedKeys()) {
                rs.next();
                osID = rs.getInt(1);
            }
        }

        addOptions(con, bandID, osID);
    }

    /**
     * Adds some options for the two polls.
     *
     * @param con    the database connection
     * @param bandID the band poll ID
     * @param osID   the OS poll ID
     *
     * @throws SQLException if an error occurs
     */
    private void addOptions(Connection con, int bandID, int osID) throws SQLException {
        try (PreparedStatement pollOptions = con.prepareStatement(
            "INSERT INTO PollOptions (POLLID, VOTESCOUNT, OPTIONTITLE, OPTIONLINK) VALUES\n" +
            "  (?, 0, 'The Beatles',             'https://www.youtube.com/watch?v=z9ypq6_5bsg'),\n" +
            "  (?, 0, 'The Marcels',             'https://www.youtube.com/watch?v=qoi3TH59ZEs'),\n" +
            "  (?, 0, 'The Platters',            'https://www.youtube.com/watch?v=H2di83WAOhU'),\n" +
            "  (?, 0, 'The Beach Boys',          'https://www.youtube.com/watch?v=2s4slliAtQU'),\n" +
            "  (?, 0, 'The Four Seasons',        'https://www.youtube.com/watch?v=y8yvnqHmFds'),\n" +
            "  (?, 0, 'The Everly Brothers',     'https://www.youtube.com/watch?v=tbU3zdAgiX8'),\n" +
            "  (?, 0, 'The Mamas And The Papas', 'https://www.youtube.com/watch?v=N-aK6JnyFmk'),\n" +
            "  (?, 0, 'Windows',                 'https://www.microsoft.com/en-us/windows'),\n" +
            "  (?, 0, 'macOS',                   'https://www.apple.com/lae/macos/mojave/'),\n" +
            "  (?, 0, 'Linux',                   'https://www.linux.org/')"
        )) {

            // Znam da nije lijepo...
            for (int i = 1; i <= 10; i++) {
                pollOptions.setInt(i, i <= 7 ? bandID : osID);
            }

            pollOptions.executeUpdate();
        }
    }

    /**
     * Creates the 'Polls' and 'PollOptions' tables if they do not exist.
     *
     * @param con the database connection
     *
     * @throws SQLException if an error occurs
     */
    private void createIfNotExisting(Connection con) throws SQLException {

        DatabaseMetaData dbMeta = con.getMetaData();
        Set<String> tables = new HashSet<>();

        try (ResultSet rs = dbMeta.getTables(null, "IVICA", null, new String[] { "TABLE" })) {
            while (rs.next()) {
                tables.add(rs.getString("TABLE_NAME").toUpperCase());
            }
        }

        if (!tables.contains("POLLS")) {
            try (PreparedStatement create = con.prepareStatement(
                "CREATE TABLE POLLS\n" +
                " (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,\n" +
                " title VARCHAR(150) NOT NULL,\n" +
                " message CLOB(2048) NOT NULL\n)"
            )) {
                create.executeUpdate();
            }
        }

        if (!tables.contains("POLLOPTIONS")) {
            try (PreparedStatement create = con.prepareStatement(
                "CREATE TABLE POLLOPTIONS\n" +
                " (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,\n" +
                " optionTitle VARCHAR(100) NOT NULL,\n" +
                " optionLink VARCHAR(150) NOT NULL,\n" +
                " pollID BIGINT,\n" +
                " votesCount BIGINT,\n" +
                " FOREIGN KEY (pollID) REFERENCES Polls(id)\n)"
            )) {
                create.executeUpdate();
            }
        }
    }
}