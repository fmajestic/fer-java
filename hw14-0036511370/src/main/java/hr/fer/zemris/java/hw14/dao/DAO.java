package hr.fer.zemris.java.hw14.dao;

import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;

import java.util.List;

/**
 * Interface for the persistent data store.
 */
@SuppressWarnings("unused")
public interface DAO {

    /**
     * Gets all polls from the database.
     *
     * @return a list of polls with all data (id, title, message).
     */
    List<Poll> getPolls();

    /**
     * Gets a poll with an id.
     *
     * @param id the id of the poll
     *
     * @return the poll
     */
    Poll getPoll(long id);

    /**
     * Gets all options that belong to a poll.
     *
     * @param pollId  the poll id
     * @param orderBy which column to order by, can be null for no sorting
     * @param asc     is the ordering ascending or descending, ignored if {@code orderBy} is null
     *
     * @return all the poll options with all their data
     */
    List<PollOption> getPollOptions(long pollId, String orderBy, boolean asc);

    /**
     * Gets a poll option with an id.
     *
     * @param id the option id
     *
     * @return the option
     */
    PollOption getPollOption(long id);

    /**
     * Adds votes to an option
     *
     * @param id    the option to vote for
     * @param votes the number of votes to give
     *
     * @return true if the votes were registered, false otherwise
     */
    boolean vote(long id, int votes);

    /**
     * Resets all of the poll's options.
     *
     * @param id the poll to reset
     *
     * @return true if the poll was reset, false otherwise
     */
    boolean resetPoll(long id);

    /**
     * Resets a single option
     *
     * @param id the option to reset
     *
     * @return true if the option was reset, false otherwise
     */
    boolean resetOption(long id);
}
