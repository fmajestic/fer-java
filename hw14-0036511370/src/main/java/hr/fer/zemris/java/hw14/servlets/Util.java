package hr.fer.zemris.java.hw14.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet utility methods.
 */
class Util {

    /**
     * Displays the invalid id error page
     *
     * @param req  the request
     * @param resp the response
     * @param id   the invalid id
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    static void sendError(HttpServletRequest req, HttpServletResponse resp, String id) throws ServletException,
        IOException {
        req.setAttribute("invalidId", id);
        req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
    }
}
