package hr.fer.zemris.java.hw14.servlets;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Generates an Excel table with the results.
 */
@WebServlet(name = "resultTable", urlPatterns = { "/servleti/glasanje-xls" })
public class GlasanjeXlsServlet extends HttpServlet {

    /**
     * Generates an Excel table (.xls) and writes it directly to the response.
     * The appropriate Content-Disposition and Content-Type are set.
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws IOException if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        int id;
        String pollID = req.getParameter("pollID");

        try {
            id = Integer.parseInt(pollID);
        } catch (NumberFormatException ex) {
            Util.sendError(req, resp, pollID);
            return;
        }

        try (HSSFWorkbook hwb = new HSSFWorkbook()) {

            HSSFSheet sheet = hwb.createSheet("Votes");
            HSSFRow header = sheet.createRow(0);
            header.createCell(0).setCellValue("Name");
            header.createCell(1).setCellValue("No. of votes");
            header.createCell(2).setCellValue("Link");

            DAO dao = DAOProvider.getDao();
            Poll poll = dao.getPoll(id);
            List<PollOption> options = dao.getPollOptions(id, "votesCount", false);

            for (int i = 0; i < options.size(); i++) {
                HSSFRow row = sheet.createRow(i + 1);

                PollOption opt = options.get(i);
                row.createCell(0).setCellValue(opt.getTitle());
                row.createCell(1).setCellValue(opt.getVotes());
                row.createCell(2).setCellValue(opt.getLink());
            }

            resp.setContentType("application/vnd.ms-excel");
            resp.setHeader("Content-Disposition", String.format("attachment; filename=results-%d.xls", poll.getId()));
            hwb.write(resp.getOutputStream());
        }
    }
}
