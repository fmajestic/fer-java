package hr.fer.zemris.java.hw14.model;

/**
 * Models a poll option.
 * <p>
 * Used as a bean for data transfer.
 */
public class PollOption {

    /** The id. */
    private int id;

    /** The parent poll id. */
    private int pollId;

    /** The number of votes. */
    private int votes;

    /** The title. */
    private String title;

    /** The link. */
    private String link;

    /**
     * Creates a new option
     *
     * @param id     the id
     * @param pollId the parent poll id
     * @param votes  the number of votes
     * @param title  the title
     * @param link   the link
     */
    public PollOption(int id, int pollId, int votes, String title, String link) {
        this.id = id;
        this.pollId = pollId;
        this.votes = votes;
        this.title = title;
        this.link = link;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the parent poll id.
     *
     * @return the parent poll id
     */
    public int getPollId() {
        return pollId;
    }

    /**
     * Gets the number of votes.
     *
     * @return the number of votes
     */
    public int getVotes() {
        return votes;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the link.
     *
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * Gets the string representation of the option.
     *
     * @return the option as a string
     */
    @Override
    public String toString() {
        return String.format("PollOption [pollId=%d; id=%d; title=\"%s\"; votes=%d]", pollId, id, title, votes);
    }
}
