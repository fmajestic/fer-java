package hr.fer.zemris.java.hw14.servlets;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Displays the results page for a poll.
 */
@WebServlet(name = "rez", urlPatterns = "/servleti/glasanje-rezultati")
public class GlasanjeRezultatiServlet extends HttpServlet {

    /**
     * Sets up all the data and forwards the request to voteResult.jsp
     *
     * @param req  the request
     * @param resp the response
     *
     * @throws ServletException if an error occurs
     * @throws IOException      if an error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int id;
        String pollID = req.getParameter("pollID");

        try {
            id = Integer.parseInt(pollID);
        } catch (NumberFormatException ex) {
            Util.sendError(req, resp, pollID);
            return;
        }

        DAO dao = DAOProvider.getDao();
        Poll poll = dao.getPoll(id);
        List<PollOption> options = dao.getPollOptions(id, "votesCount", false);

        int maxVotes = options.stream().mapToInt(PollOption::getVotes).max().orElse(0);

        List<PollOption> winners = options.stream()
            .filter(opt -> opt.getVotes() == maxVotes)
            .collect(Collectors.toList());

        req.setAttribute("poll", poll);
        req.setAttribute("options", options);
        req.setAttribute("winners", winners);

        req.getRequestDispatcher("/WEB-INF/pages/voteResult.jsp").forward(req, resp);
    }
}
