package hr.fer.zemris.java.hw14.dao.sql;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of the DAO interface using SQL.
 * <p>
 * This implementation expects an already set-up database connection
 * that can be acquired from the {@link SQLConnectionProvider}.
 * <p>
 * There should be a servlet filter that opens and closes database connections
 * before any servlet is allowed to run.
 */
@SuppressWarnings("resource")
public class SQLDAO implements DAO {

    @Override
    public List<Poll> getPolls() {
        List<Poll> polls = new ArrayList<>();

        Connection con = SQLConnectionProvider.getConnection();
        try (
            PreparedStatement pst = con.prepareStatement("SELECT * FROM POLLS");
            ResultSet rs = pst.executeQuery()
        ) {
            while (rs.next()) {
                polls.add(loadPoll(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return polls;
    }

    @Override
    public Poll getPoll(long id) {
        Connection con = SQLConnectionProvider.getConnection();
        try (
            PreparedStatement pst = con.prepareStatement("SELECT * FROM POLLS WHERE ID=?")
        ) {
            pst.setLong(1, id);

            try (ResultSet rs = pst.executeQuery()) {
                return rs.next() ? loadPoll(rs) : null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public PollOption getPollOption(long id) {
        Connection con = SQLConnectionProvider.getConnection();
        try (
            PreparedStatement pst = con.prepareStatement("SELECT * FROM POLLOPTIONS WHERE ID=?")
        ) {
            pst.setLong(1, id);

            try (ResultSet rs = pst.executeQuery()) {
                return rs.next() ? loadOption(rs) : null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean vote(long id, int votes) {
        Connection con = SQLConnectionProvider.getConnection();
        try (
            PreparedStatement pst =
                con.prepareStatement("UPDATE POLLOPTIONS SET VOTESCOUNT = VOTESCOUNT + ? WHERE ID=?")
        ) {
            pst.setInt(1, votes);
            pst.setLong(2, id);

            // Voting should affect only one row
            return pst.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean resetPoll(long id) {
        try (Connection con = SQLConnectionProvider.getConnection();
             PreparedStatement pst =
                 con.prepareStatement("UPDATE POLLOPTIONS SET VOTESCOUNT = 0 WHERE POLLID=?")
        ) {

            pst.setLong(1, id);

            // Resetting an entire poll should affect at least 1 row
            return pst.executeUpdate() > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    @Override
    public boolean resetOption(long id) {
        try (
            Connection con = SQLConnectionProvider.getConnection();
            PreparedStatement pst =
                con.prepareStatement("UPDATE POLLOPTIONS SET VOTESCOUNT = 0 WHERE ID=?")
        ) {
            pst.setLong(1, id);

            // Resetting a single option should affect only one row
            return pst.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    @Override
    public List<PollOption> getPollOptions(long pollId, String orderBy, boolean asc) {
        List<PollOption> options = new ArrayList<>();
        Connection con = SQLConnectionProvider.getConnection();

        String query = "SELECT * FROM POLLOPTIONS WHERE POLLID=?";

        if (orderBy != null) {
            query += String.format(" ORDER BY %s %s", orderBy, asc ? "ASC" : "DESC");
        }

        try (PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, pollId);

            getOptions(options, pst);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return options;
    }

    private void getOptions(List<PollOption> options, PreparedStatement pst) throws SQLException {
        try (ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                options.add(loadOption(rs));
            }
        }
    }

    private Poll loadPoll(ResultSet rs) throws SQLException {
        return new Poll(
            rs.getInt("id"),
            rs.getString("title"),
            rs.getString("message")
        );
    }

    private PollOption loadOption(ResultSet rs) throws SQLException {
        return new PollOption(
            rs.getInt("id"),
            rs.getInt("pollId"),
            rs.getInt("votesCount"),
            rs.getString("optionTitle"),
            rs.getString("optionLink")
        );
    }
}
