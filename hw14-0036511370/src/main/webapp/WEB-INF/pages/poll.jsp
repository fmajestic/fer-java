<jsp:useBean id="poll" scope="request" type="hr.fer.zemris.java.hw14.model.Poll"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Poll vote</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles.css">
</head>
<body>
<h1>${poll.title}</h1>
<p>${poll.message}</p>

<jsp:useBean id="pollOptions" scope="request" type="java.util.List"/>
<c:forEach var="opt" items="${pollOptions}">
    <li>
        <a href="
            <c:url value="/servleti/glasanje-glasaj">
                <c:param name="optionID" value="${opt.id}"/>
                <c:param name="pollID" value="${opt.pollId}"/>
            </c:url>">
                ${opt.title}
        </a>
    </li>
</c:forEach>
</body>
</html>
