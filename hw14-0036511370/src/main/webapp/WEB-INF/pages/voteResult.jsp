<jsp:useBean id="poll" scope="request" type="hr.fer.zemris.java.hw14.model.Poll"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Results</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles.css">
</head>
<body>
<h1>${poll.title}</h1>
<h2>The poll results are:</h2>
<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>No. of votes</th>
    </tr>
    </thead>
    <tbody>
    <jsp:useBean id="options" scope="request" type="java.util.List"/>
    <c:forEach var="opt" items="${options}">
        <tr>
            <td>${opt.title}</td>
            <td>${opt.votes}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<h2>Graphical display</h2>
<img alt="Pie-chart"
     src="<c:url value="/servleti/glasanje-grafika"><c:param name="pollID" value="${poll.id}"/></c:url>"/>

<h2>XLS table results</h2>
<p>An Excel table with the results is available
    <a href="<c:url value="/servleti/glasanje-xls"><c:param  name="pollID" value="${poll.id}"/></c:url>">here</a>
</p>

<h2>Misc</h2>
<p>Links to the winner(s):</p>
<ul>
    <jsp:useBean id="winners" scope="request" type="java.util.List"/>
    <c:forEach var="opt" items="${winners}">
        <li><a href="${opt.link}">${opt.title}</a></li>
    </c:forEach>
</ul>

</body>
</html>
