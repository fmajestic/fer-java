<jsp:useBean id="invalidId" scope="request" type="java.lang.String"/>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Error</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles.css">
</head>
<body>
    <h1>Error: invalid ID</h1>
    <p>The given ID is not valid: ${not empty invalidId ? invalidId : "<none>"}</p>
</body>
</html>
