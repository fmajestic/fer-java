<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Polls</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles.css">
</head>
<body>

<h1>Available polls</h1>
<ol>
    <jsp:useBean id="polls" scope="request" type="java.util.List"/>
    <c:forEach var="poll" items="${polls}">
    <li>
        <a href="<c:url value="/servleti/glasanje"><c:param name="pollID" value="${poll.id}"/></c:url>">
            ${poll.title}
        </a>
    </li>
    </c:forEach>
</ol>
</body>
</html>
