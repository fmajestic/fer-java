package hr.fer.zemris.java.hw05.db;

import org.junit.jupiter.api.Test;

import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;
import static org.junit.jupiter.api.Assertions.*;

class ComparisonOperatorsTest {

    @Test
    void lessOperators() {
        assertFalse(LESS.satisfied("Yes", "YES"));
        assertTrue(LESS.satisfied("Ana", "Ivana"));

        assertFalse(LESS.satisfied("Yes", "Yes"));
        assertFalse(LESS.satisfied("Night", "Day"));

        assertTrue(LESS_OR_EQUALS.satisfied("Ana", "Anamarija"));
        assertFalse(LESS_OR_EQUALS.satisfied("Sun", "Moon"));
    }

    @Test
    void greaterOperators() {
        assertFalse(GREATER.satisfied("YES", "Yes"));
        assertTrue(GREATER.satisfied("Ivana", "Ana"));

        assertFalse(GREATER.satisfied("Yes", "Yes"));
        assertFalse(GREATER.satisfied("Day", "Night"));

        assertFalse(GREATER_OR_EQUALS.satisfied("Ana", "Anamarija"));
        assertTrue(GREATER_OR_EQUALS.satisfied("Sun", "Moon"));
    }

    @Test
    void equalityOperators() {

        assertTrue(EQUALS.satisfied("One", "One"));
        assertFalse(EQUALS.satisfied("OneTwo", "One"));

        assertTrue(NOT_EQUALS.satisfied("Yes", "no"));
        assertFalse(NOT_EQUALS.satisfied("Day","Day"));
    }

    @Test
    void likeOperator() {

        assertTrue(LIKE.satisfied("AAAA", "AA*AA"));
        assertFalse(LIKE.satisfied("AAA", "AA*AA"));

        assertTrue(LIKE.satisfied("ABCDEF", "*EF"));
        assertTrue(LIKE.satisfied("Hello", "He*"));
    }
}