package hr.fer.zemris.java.hw05.db;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class StudentDatabaseTest {

    private static IFilter acceptAll = (ignored) -> true;
    private static IFilter acceptNone = (ignored) -> false;
    private static StudentDatabase database;

    @BeforeAll
    static void setup() {
        try {
            database =
                new StudentDatabase(Files.readAllLines(Paths.get("database.txt"), StandardCharsets.UTF_8));
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    void forJMBAG() {
        var jmbag = "0000000042";
        var expected = new StudentRecord(jmbag, "Nikola", "Palajić", 3);

        assertEquals(expected, database.forJMBAG(jmbag));

        assertNull(database.forJMBAG("12345"));
        assertNull(database.forJMBAG(null));
    }

    @Test
    void acceptNone() {
        assertEquals(0, database.filter(acceptNone).size());
    }

    @Test
    void acceptAll() {
        assertEquals(63, database.filter(acceptAll).size());
    }
}
