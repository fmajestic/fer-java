package hr.fer.zemris.java.hw05.db;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.JMBAG;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.LAST_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class QueryFilterTest {

    private static StudentDatabase database;

    @BeforeAll
    static void setUp() {
        try {
            database = new StudentDatabase(Files.readAllLines(Paths.get("database.txt"), StandardCharsets.UTF_8));
        } catch (IOException ex) {
            fail("Can't read database file.");
        } catch (IllegalArgumentException ex) {
            fail("Error in database file.");
        }
    }

    @Test
    void directQuery() {
        var expected = Collections.singletonList(
            new StudentRecord("0000000003", "Bosnić", "Andrea", 4)
        );

        var filter = new QueryFilter(
            Collections.singletonList(new ConditionalExpression(JMBAG, EQUALS, "0000000003"))
        );

        assertEquals(expected, database.filter(filter));
    }

    @Test
    void compoundQuery() {
        var expected = Arrays.asList(
            new StudentRecord("0000000003", "Bosnić", "Andrea", 4),
            new StudentRecord("0000000004", "Božić", "Marin", 5),
            new StudentRecord("0000000005", "Brezović", "Jusufadis", 2)
        );

        var filter = new QueryFilter(Arrays.asList(
            new ConditionalExpression(JMBAG, GREATER_OR_EQUALS, "0000000003"),
            new ConditionalExpression(LAST_NAME, LIKE, "B*")
        ));

        assertEquals(expected, database.filter(filter));
    }
}
