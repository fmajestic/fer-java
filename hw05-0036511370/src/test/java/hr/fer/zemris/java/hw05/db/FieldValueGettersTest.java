package hr.fer.zemris.java.hw05.db;

import org.junit.jupiter.api.Test;

import static hr.fer.zemris.java.hw05.db.FieldValueGetters.*;
import static org.junit.jupiter.api.Assertions.*;

class FieldValueGettersTest {
    private static final StudentRecord[] records = new StudentRecord[] {
        new StudentRecord("1", "Marko", "Markovic", 5),
        new StudentRecord("2", "Ivan", "Ivanovic", 5),
        new StudentRecord("3", "Petar", "Petrovic", 5),
        new StudentRecord("4", "Luka", "Lukic", 5),
        new StudentRecord("5", "Pero", "Peric", 5)
    };

    @Test
    void jmbagGetter() {
        for (var rec : records) {
            assertEquals(rec.getJmbag(), JMBAG.get(rec));
        }
    }

    @Test
    void firstNameGetter() {
        for (var rec : records) {
            assertEquals(rec.getFirstName(), FIRST_NAME.get(rec));
        }
    }

    @Test
    void lastNameGetter() {
        for (var rec : records) {
            assertEquals(rec.getLastName(), LAST_NAME.get(rec));
        }
    }
}