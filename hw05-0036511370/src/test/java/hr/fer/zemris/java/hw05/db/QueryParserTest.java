package hr.fer.zemris.java.hw05.db;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.*;
import static org.junit.jupiter.api.Assertions.*;

class QueryParserTest {

    @Test
    void directQuery() {
        assertTrue(new QueryParser("   jmbag = \"0036912345\"").isDirectQuery());

        assertFalse(new QueryParser("jmbag LIKE \"1234567890\"").isDirectQuery());
        assertFalse(new QueryParser("jmbag=\"1234567890\" AND lastName LIKE \"A*\"").isDirectQuery());
    }

    @Test
    void queriedJMBAG() {
        assertEquals("0036912345", new QueryParser("   jmbag = \"0036912345\"").getQueriedJMBAG());

        assertThrows(IllegalStateException.class,
            () -> new QueryParser("jmbag LIKE \"1234567890\"").getQueriedJMBAG());

        assertThrows(IllegalStateException.class,
            () -> new QueryParser("jmbag=\"1234567890\" AND lastName LIKE \"A*\"").getQueriedJMBAG());
    }

    @Test
    void getQuery() {
        var expectedExpressions = Arrays.asList(
            new ConditionalExpression(JMBAG, EQUALS, "1234567890"),
            new ConditionalExpression(LAST_NAME, LIKE, "Ba*"),
            new ConditionalExpression(FIRST_NAME, LESS_OR_EQUALS, "Perić")
        );

        var actualExpressions =
            new QueryParser("jmbag=\"1234567890\" AND lastName LIKE \"Ba*\" AND firstName <= \"Perić\"").getQuery();

        assertEquals(expectedExpressions, actualExpressions);
    }
}