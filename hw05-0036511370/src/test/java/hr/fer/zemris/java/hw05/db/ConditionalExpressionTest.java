package hr.fer.zemris.java.hw05.db;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static hr.fer.zemris.java.hw05.db.ComparisonOperators.LIKE;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.JMBAG;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.LAST_NAME;
import static org.junit.jupiter.api.Assertions.*;

class ConditionalExpressionTest {

    static ConditionalExpression expression = new ConditionalExpression(
        LAST_NAME,
        LIKE,
        "AA*AA"
    );

    @BeforeAll
    static void setUp() {
    }

    @Test
    void getFieldGetter() {
        assertEquals(LAST_NAME, expression.getFieldGetter());
    }

    @Test
    void getComparisonOperator() {
        assertEquals(LIKE, expression.getComparisonOperator());
    }

    @Test
    void getStringLiteral() {
        assertEquals("AA*AA", expression.getStringLiteral());
    }
}