package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.LSystem;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LSystemBuilderImplTest {

    private static final LSystem system = new LSystemBuilderImpl()
        .setAxiom("F")
        .registerProduction('F', "F+F--F+F")
        .build();

    @Test
    void generateLevel0() {
        assertEquals("F", system.generate(0));
    }

    @Test
    void generateLevel1() {
        assertEquals("F+F--F+F", system.generate(1));
    }

    @Test
    void generateLevel2() {
        assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", system.generate(2));
    }
}
