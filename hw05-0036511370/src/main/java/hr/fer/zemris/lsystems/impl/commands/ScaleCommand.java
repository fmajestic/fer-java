package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Stores a scale command.
 * <p>
 * This command scales the current line length by the given factor.
 */
public class ScaleCommand implements Command {

    /** The factor to scale by. */
    private double factor;

    /**
     * Creates a new scale command with the given factor.
     *
     * @param factor the factor to scale by
     *
     * @throws IllegalArgumentException if factor is not positive
     */
    public ScaleCommand(double factor) {
        if (factor <= 0) {
            throw new IllegalArgumentException("Scale factor must be positive.");
        }

        this.factor = factor;
    }

    /**
     * Scales the current line length by {@code factor}.
     *
     * @param ctx     the current context
     * @param painter ignored
     */
    @Override
    public void execute(Context ctx, Painter painter) {

        var current = ctx.getCurrentState();

        current.setLineLength(current.getLineLength() * factor);
    }
}