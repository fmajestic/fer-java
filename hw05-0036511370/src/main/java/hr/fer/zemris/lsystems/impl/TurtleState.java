package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.math.Vector2D;

import java.awt.*;
import java.util.Objects;

/**
 * Represents a turtle with a position, direction, line length nad line color.
 */
public class TurtleState {

    /**
     * The turtle's current position on the canvas.
     * <p>
     * The vector is treated as a point.
     */
    private Vector2D currentPosition;

    /**
     * The turtle's current direction on the canvas.
     * <p>
     * The vector is treated as a radium-vector.
     */
    private Vector2D currentDirection;

    /** The turtle's current line color. */
    private Color lineColor;

    /** The turtle's current line length. */
    private double lineLength;

    /**
     * Creates a new turtle state with the given parameters.
     *
     * @param position   the position of the turtle
     * @param direction  the direction of the turtle
     * @param lineColor  the line color for drawing
     * @param lineLength the line length for drawing
     *
     * @throws IllegalArgumentException if {@code lineLength} is not greater than 0
     * @throws NullPointerException if any of the parameters are null
     */
    public TurtleState(Vector2D position, Vector2D direction, Color lineColor, double lineLength) {
        if (lineLength <= 0) {
            throw new IllegalArgumentException("Line length must be greater than 0.");
        }

        this.currentDirection = Objects.requireNonNull(direction, "Turtle parameter can't be null: direction.");
        this.currentPosition = Objects.requireNonNull(position, "Turtle parameter can't be null: position.");
        this.lineColor = Objects.requireNonNull(lineColor, "Turtle parameter can't be null: color.");
        this.lineLength = lineLength;
    }

    /**
     * Creates a copy of the current state.
     * <p>
     * The copy can be freely modified without any changes being visible to this state.
     *
     * @return a copy of the state
     */
    public TurtleState copy() {
        return new TurtleState(currentPosition.copy(), currentDirection.copy(), lineColor, lineLength);
    }

    /**
     * Gets the current position of the turtle.
     *
     * @return the current position
     */
    public Vector2D getCurrentPosition() { return currentPosition; }

    /**
     * Gets the current direction of the turtle.
     *
     * @return the current direction
     */
    public Vector2D getCurrentDirection() {
        return currentDirection;
    }

    /**
     * Gets the current line color for drawing.
     *
     * @return the current line color
     */
    public Color getLineColor() { return lineColor; }

    /**
     * Sets a new line color for drawing.
     *
     * @param lineColor the new line color
     *
     * @throws NullPointerException if {@code lineColor} is null
     */
    public void setLineColor(Color lineColor) {
        this.lineColor = Objects.requireNonNull(lineColor, "Line color can't be null");
    }

    /**
     * Gets the current line length for drawing.
     *
     * @return the current line length
     */
    public double getLineLength() { return lineLength; }

    /**
     * Sets a new line length for drawing.
     *
     * @param lineLength the new line length
     *
     * @throws IllegalArgumentException if {@code lineLength} is negative
     */
    public void setLineLength(double lineLength) {
        if (lineLength <= 0) {
            throw new IllegalArgumentException("Line length can't be negative");
        }

        this.lineLength = lineLength;
    }
}
