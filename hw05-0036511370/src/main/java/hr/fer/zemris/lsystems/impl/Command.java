package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * Represents a command that can be executed on a painter with a given context.
 */
public interface Command {

    /**
     * Exectues the command on the painter.
     *
     * @param ctx     the current context
     * @param painter the painter to paint on
     */
    void execute(Context ctx, Painter painter);
}
