package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Models a draw command
 */
public class DrawCommand implements Command {

    /** The number to scale the line by. */
    private double scale;

    /**
     * Creates a new draw command with a line scale.
     *
     * @param scale the number to scale the line by
     *
     * @throws IllegalArgumentException if scale is not positive
     */
    public DrawCommand(double scale) {
        if (scale <= 0) {
            throw new IllegalArgumentException("Draw scale must be positive.");
        }

        this.scale = scale;
    }

    /**
     * Draws a new line from the current position, in the current direction, using the current color and line length
     * scaled by {@code scale}.
     *
     * @param ctx     the current context
     * @param painter the painter to paint on
     */
    @Override
    public void execute(Context ctx, Painter painter) {
        var current = ctx.getCurrentState();

        var start = current.getCurrentPosition().copy();

        current.getCurrentPosition().translate(
            current.getCurrentDirection().scaled(current.getLineLength() * scale)
        );

        var end = current.getCurrentPosition();

        painter.drawLine(start.getX(), start.getY(), end.getX(), end.getY(), current.getLineColor(), 1);
    }
}
