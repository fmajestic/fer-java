package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

import java.awt.*;
import java.util.Objects;

/**
 * Models a color change command.
 */
public class ColorCommand implements Command {

    /** The new color the turtle will use. */
    private Color newColor;

    /**
     * Creates a new color change command with the specified color.
     *
     * @param color the new color
     *
     * @throws NullPointerException if color is null
     */
    public ColorCommand(Color color) {
        newColor = Objects.requireNonNull(color, "New color can't be null.");
    }

    /**
     * Sets the current state's color to {@code newColor}
     *
     * @param ctx     the current context
     * @param painter ignored
     */
    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().setLineColor(newColor);
    }
}