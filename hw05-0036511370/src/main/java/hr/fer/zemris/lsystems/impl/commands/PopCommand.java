package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;


/**
 * Models a pop command.
 * <p>
 * This command pops a state off the context's stack.
 * <p>
 * The popped value is ignored, which effectively erases it from the context.
 */
public class PopCommand implements Command {

    /**
     * Pops a state off the context's stack.
     * <p>
     * The popped value is ignored, deleting it form the context.
     *
     * @param ctx     the current context
     * @param painter ignored
     */
    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.popState();
    }
}