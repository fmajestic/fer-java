package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Models a skip command.
 * <p>
 * This command moves the turtle, but does not draw a line.
 */
public class SkipCommand implements Command {

    /** The number to scale the line by. */
    private double scale;


    /**
     * Creates a new skip command with a line scale.
     *
     * @param scale the number to scale the line by
     *
     * @throws IllegalArgumentException if scale is not positive
     */
    public SkipCommand(double scale) {
        if (scale <= 0) {
            throw new IllegalArgumentException("Draw scale must be positive.");
        }

        this.scale = scale;
    }


    /**
     * Skips from the current position, in the current direction, using the current line length scaled by {@code scale}.
     *
     * @param ctx     the current context
     * @param painter ignored
     */
    @Override
    public void execute(Context ctx, Painter painter) {
        var current = ctx.getCurrentState();

        current.getCurrentPosition().translate(
            current.getCurrentDirection().scaled(current.getLineLength() * scale)
        );
    }
}