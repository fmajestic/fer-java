package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Models a rotation command.
 */
public class RotateCommand implements Command {
    /** The angle to rotate by. */
    private double angle;

    /**
     * Sets the rotation angle.
     * <p>
     * The provided angle must be in degrees!
     *
     * @param angle the angle to rotate by, in degrees
     */
    public RotateCommand(double angle) {
        this.angle = Math.toRadians(angle);
    }

    /**
     * Rotates the current state's direction by {@code angle}.
     *
     * @param ctx     the current context
     * @param painter ignored
     */
    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().getCurrentDirection().rotate(angle);
    }
}