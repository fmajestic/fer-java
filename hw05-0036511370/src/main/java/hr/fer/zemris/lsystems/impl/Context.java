package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.ObjectStack;

import java.util.Objects;

/**
 * Models a context that stores multiple states.
 * <p>
 * States are maintained in a stack, and can be pushed/popped.
 */
public class Context {

    /** The state stack. */
    private ObjectStack<TurtleState> turtleStates = new ObjectStack<>();

    /**
     * Gets the current state.
     *
     * @return the current state
     */
    public TurtleState getCurrentState() { return turtleStates.peek(); }

    /**
     * Pushes a state onto the stack.
     *
     * @param state the state to push
     *
     * @throws NullPointerException if {@code state} is null
     */
    public void pushState(TurtleState state) {
        turtleStates.push(Objects.requireNonNull(state, "Pushed state can't be null."));
    }

    /**
     * Pops a state off the stack.
     * <p>
     * The popped value is ignored.
     */
    public void popState() {
        turtleStates.pop();
    }

}
