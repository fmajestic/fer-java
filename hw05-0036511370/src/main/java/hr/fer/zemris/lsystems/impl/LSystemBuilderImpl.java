package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.Dictionary;
import hr.fer.zemris.java.custom.collections.List;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.*;
import hr.fer.zemris.math.Vector2D;

import java.awt.*;
import java.util.Objects;
import java.util.function.Function;

import static java.lang.Math.*;

public class LSystemBuilderImpl implements LSystemBuilder {

    /** A static map of all commands that take an argument */
    private static final List<String> nonEmptyCommands = new ArrayIndexedCollection<>();

    /** A static map of constructors for commands that take a number as an argument. */
    private static final Dictionary<String, Function<Double, Command>> numberParamCommands = new Dictionary<>();

    static {
        nonEmptyCommands.add("draw");
        nonEmptyCommands.add("skip");
        nonEmptyCommands.add("scale");
        nonEmptyCommands.add("rotate");
        nonEmptyCommands.add("color");

        numberParamCommands.put("draw", DrawCommand::new);
        numberParamCommands.put("skip", SkipCommand::new);
        numberParamCommands.put("scale", ScaleCommand::new);
        numberParamCommands.put("rotate", RotateCommand::new);
    }


    /** The initial sequence of the system. */
    private String axiom = "";

    /** The initial draw angle. */
    private double angle = 0.0;

    /** The initial unit length */
    private double unitLength = 0.1;

    /** The amount by which to scale the unit length for each level of the system. */
    private double unitLengthDegreeScaler = 1.0;

    /** The origin point for drawing the system. */
    private Vector2D origin = new Vector2D(0, 0);

    /** A list of all registered productions, mapped as symbol -> result. */
    private Dictionary<Character, String> productions = new Dictionary<>();

    /** A list of all registered commands, mapped as symbol -> command. */
    private Dictionary<Character, Command> commands = new Dictionary<>();


    /**
     * Sets a new unit length for drawin the final generated sequence.
     *
     * @param length the new length
     *
     * @return this builder object
     *
     * @throws IllegalArgumentException if {@code length} is {@code null}
     */
    @Override
    public LSystemBuilder setUnitLength(double length) {
        if (length <= 0) {
            throw new IllegalArgumentException("Unit lenght must be greater than 0.");
        }

        unitLength = length;

        return this;
    }

    /**
     * Sets a new origin for drawing the final generated sequence.
     *
     * @param x the x-coordinate
     * @param y the y-coordinate
     *
     * @return this builder object
     */
    @Override
    public LSystemBuilder setOrigin(double x, double y) {
        origin = new Vector2D(x, y);

        return this;
    }

    /**
     * Sets the initial angle for drawing the final generated sequence.
     * <p>
     * The angle specified must be in degrees.
     *
     * @param degrees the new initial angle
     *
     * @return the builder object
     */
    @Override
    public LSystemBuilder setAngle(double degrees) {
        this.angle = Math.toRadians(degrees);

        return this;
    }

    /**
     * The initial sequence of the system.
     *
     * @param axiom the initial sequence
     *
     * @return the builder object
     *
     * @throws IllegalArgumentException if the {@code axiom} is {@code null} or empty
     */
    @Override
    public LSystemBuilder setAxiom(String axiom) {
        if (axiom == null || axiom.isEmpty()) {
            throw new IllegalArgumentException("Axiom can't be null/empty.");
        }

        this.axiom = axiom;

        return this;
    }

    /**
     * Sets the value the unit length will be scaled by.
     * <p>
     * The scaling also depends on the number of steps that are performed.
     *
     * @param scaler the scalar value
     *
     * @return the builder object
     *
     * @throws IllegalArgumentException if the scalar value is 0 or less
     */
    @Override
    public LSystemBuilder setUnitLengthDegreeScaler(double scaler) {
        if (scaler <= 0) {
            throw new IllegalArgumentException("Unit length scalar must be greater than 0.");
        }

        unitLengthDegreeScaler = scaler;

        return this;
    }

    /**
     * Register a new production rule for the system.
     *
     * @param symbol the production start symbol
     * @param result the production result
     *
     * @return the builder object
     *
     * @throws NullPointerException if {@code result} is {@code null}
     */
    @Override
    public LSystemBuilder registerProduction(char symbol, String result) {
        Objects.requireNonNull(result, "Production result can't be null.");

        productions.put(symbol, result);

        return this;
    }

    /**
     * Register a new command for the system.
     * <p>
     * A command is executed for each matching symbol in the final sequence.
     *
     * @param symbol  the symbol to map the action to
     * @param command the action to perform
     *
     * @return the builder object
     *
     * @throws NullPointerException     if {@code result} is {@code null}
     * @throws IllegalArgumentException if the command is not valid
     */
    @Override
    public LSystemBuilder registerCommand(char symbol, String command) {
        Objects.requireNonNull(command, "Production result can't be null.");

        commands.put(symbol, parseCommand(command));

        return this;
    }

    /**
     * Configures a system from a set of parameters.
     *
     * @param strings the
     *
     * @return this builder object
     */
    @Override
    public LSystemBuilder configureFromText(String[] strings) {

        for (var option : strings) {
            if (option.isEmpty()) {
                continue;
            }

            var parts = option.split("\\s+", 2);

            if (parts.length != 2) {
                throw new IllegalArgumentException("Invalid config option: " + option);
            }

            switch (parts[0]) {
            case "command":
                setCommandFromString(parts[1]);
                break;

            case "axiom":
                setAxiom(parts[1]);
                break;

            case "production":
                setProductionFromString(parts[1]);
                break;

            case "angle":
                setAngle(getNumberArgument(parts[1]));
                break;

            case "unitLength":
                setUnitLength(getNumberArgument(parts[1]));
                break;

            case "origin":
                setOriginFromString(parts[1]);
                break;

            case "unitLengthDegreeScaler":
                setUnitLengthDegreeScalerFromString(parts[1]);
                break;

            default:
                throw new IllegalArgumentException("Unrecognized option");
            }
        }

        return this;
    }

    @Override
    public LSystem build() { return new LSystemImpl(); }

    private void setCommandFromString(String part) {
        var cmdParts = part.split("\\s+", 2);

        if (cmdParts.length != 2) {
            throw new IllegalArgumentException("Invalid command.");
        }

        if (cmdParts[0].length() != 1) {
            throw new IllegalArgumentException("Invalid command symbol: must be only one character.");
        }

        registerCommand(cmdParts[0].charAt(0), cmdParts[1]);
    }

    private void setProductionFromString(String part) {
        var prodParts = part.split("\\s+", 2);

        if (prodParts.length != 2) {
            throw new IllegalArgumentException("Invalid production.");
        }

        if (prodParts[0].length() != 1) {
            throw new IllegalArgumentException("Invalid production symbol: must be only one character.");
        }

        registerProduction(prodParts[0].charAt(0), prodParts[1]);
    }

    private void setOriginFromString(String part) {
        var coords = part.split("\\s+", 2);

        if (coords.length != 2) {
            throw new IllegalArgumentException("Invalid number of origin points");
        }

        setOrigin(getNumberArgument(coords[0]), getNumberArgument(coords[1]));
    }

    private void setUnitLengthDegreeScalerFromString(String part) {
        double value;

        if (part.contains("/")) {
            var nums = part.split("/", 2);
            value = getNumberArgument(nums[0]) / getNumberArgument(nums[1]);
        } else {
            value = getNumberArgument(part);
        }

        setUnitLengthDegreeScaler(value);
    }

    /**
     * Parses a command from a string.
     *
     * @param str the string to parse.
     *
     * @return the parsed command
     *
     * @throws IllegalArgumentException if the command is invalid
     */
    private Command parseCommand(String str) {
        Command result;

        var cmdParts = str.split("\\s+", 2);

        if (cmdParts.length < 1 || cmdParts.length > 2) {
            throw new IllegalArgumentException("Invalid command action, in: <" + str + ">");
        }

        if (nonEmptyCommands.contains(cmdParts[0])) {
            return makeArgumentCommand(cmdParts);
        } else {
            switch (cmdParts[0]) {
            case "pop":
                return new PopCommand();

            case "push":
                return new PushCommand();

            default:
                throw new IllegalArgumentException("Invalid command name: " + cmdParts[0]);
            }
        }
    }

    /**
     * Makes a command that takes a parameter from two strings.
     * <p>
     * If the argument is not a valid number, an exception is thrown.
     *
     * @param cmdParts the command parts: name and argument
     *
     * @return the appropriate {@link Command} instance
     *
     * @throws IllegalArgumentException if the command
     */
    private Command makeArgumentCommand(String[] cmdParts) {
        String cmdName = cmdParts[0];

        if (cmdParts.length != 2) {
            throw new IllegalArgumentException("Not enough command arguments, for: <" + cmdParts[0] + ">");
        }

        if (cmdName.equals("color")) {

            var colorStr = cmdParts[1];

            if (!colorStr.matches("[a-fA-F0-9]{6}")) {
                throw new IllegalArgumentException("Invalid color: " + colorStr);
            }

            return new ColorCommand(Color.decode("#" + cmdParts[1]));
        } else {
            double value = getNumberArgument(cmdParts[1]);
            return numberParamCommands.get(cmdName).apply(value);
        }
    }

    /**
     * Gets a command number argument from a string.
     * <p>
     * If the string is not a valid number, an exception is thrown.
     *
     * @param numberStr the string to convert
     *
     * @return the parsed number
     *
     * @throws IllegalArgumentException if the string is not a valid number
     */
    private double getNumberArgument(String numberStr) {
        try {
            return Double.parseDouble(numberStr);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid number: " + numberStr);
        }
    }

    /**
     * A Lindermayer system implementation based on turtle graphics.
     */
    private class LSystemImpl implements LSystem {

        /**
         * Applies productions to te axiom n times to generate a symbol sequence.
         *
         * @param n the number of times to apply productions
         *
         * @return the final sequence
         *
         * @throws IllegalArgumentException if n is negative
         */
        @Override
        public String generate(int n) {
            if (n < 0) {
                throw new IllegalArgumentException();
            }

            var result = axiom;

            for (; n > 0; n--) {
                var next = new StringBuilder();

                for (var ch : result.toCharArray()) {
                    var prod = productions.get(ch);

                    if (prod != null) {
                        next.append(prod);
                    } else {
                        next.append(ch);
                    }
                }

                result = next.toString();
            }

            return result;
        }

        @Override
        public void draw(int level, Painter painter) {

            var symbols = generate(level);

            var turtle = new TurtleState(
                origin.copy(),
                new Vector2D(1,0).rotated(angle),
                Color.BLACK,
                unitLength * pow(unitLengthDegreeScaler, level)
            );

            var ctx = new Context();

            ctx.pushState(turtle);

            for (var symbol : symbols.toCharArray()) {
                var command = commands.get(symbol);

                if (command != null) {
                    command.execute(ctx, painter);
                }
            }
        }
    }
}
