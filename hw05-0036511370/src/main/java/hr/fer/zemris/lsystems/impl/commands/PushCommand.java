package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

/**
 * Models a push command.
 * <p>
 * This command pushes a copy of the current state to the context's stack.
 * <p>
 * Since {@link TurtleState#copy()} is used, changes to the copy do not affect the old state.
 */
public class PushCommand implements Command {

    /**
     * Pushes a copy of the current state to the context's stack.
     *
     * @param ctx     the current context
     * @param painter ignored
     */
    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.pushState(ctx.getCurrentState().copy());
    }
}