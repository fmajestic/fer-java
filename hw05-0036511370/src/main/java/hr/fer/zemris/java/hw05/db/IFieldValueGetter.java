package hr.fer.zemris.java.hw05.db;

/**
 * Defines a function that encapsulates a field getter for a {@link StudentRecord}
 */
public interface IFieldValueGetter {

    /**
     * Gets the field value specified in this strategy.
     *
     * @param record the record from which the value is extracted
     *
     * @return the specified value
     */
    String get(StudentRecord record);
}
