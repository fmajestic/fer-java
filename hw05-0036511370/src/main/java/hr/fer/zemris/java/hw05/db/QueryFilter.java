package hr.fer.zemris.java.hw05.db;

import java.util.List;
import java.util.Objects;

/**
 * Combines an entire query as a single filter.
 */
public class QueryFilter implements IFilter {

    /** All the query expressions. */
    private List<ConditionalExpression> expressions;

    /**
     * Creates a new query filter with the given expressions to execute.
     *
     * @param expressions the list of expressions in the query
     *
     * @throws NullPointerException if {@code expressions} is null
     */
    public QueryFilter(List<ConditionalExpression> expressions) {
        this.expressions = Objects.requireNonNull(expressions);
    }

    /**
     * Checks if each query condition is satisfied.
     *
     * @param record the record to check
     *
     * @return true if all conditions passed, false otherwise
     */
    @Override
    public boolean accepts(StudentRecord record) {

        return expressions.stream()
            .allMatch(expr -> {
                var literal = expr.getStringLiteral();
                var operator = expr.getComparisonOperator();
                var field = expr.getFieldGetter().get(record);

                return operator.satisfied(field, literal);
            });
    }
}
