package hr.fer.zemris.java.hw05.db;

/**
 * Defines all of the query operators as implementations of {@link IComparisonOperator}.
 */
public class ComparisonOperators {

    /** The 'less than' (<) operator. */
    public static final IComparisonOperator LESS = (s1, s2) -> s1.compareTo(s2) < 0;

    /** The 'less than or equal' (<=) operator. */
    public static final IComparisonOperator LESS_OR_EQUALS = (s1, s2) -> s1.compareTo(s2) <= 0;

    /** The 'greater than' (>) operator. */
    public static final IComparisonOperator GREATER = (s1, s2) -> s1.compareTo(s2) > 0;

    /** The 'greater than or equal' (>=) operator. */
    public static final IComparisonOperator GREATER_OR_EQUALS = (s1, s2) -> s1.compareTo(s2) >= 0;

    /** The 'equal' (==) operator. */
    public static final IComparisonOperator EQUALS = (s1, s2) -> s1.compareTo(s2) == 0;

    /** The 'not equal' (!=) operator. */
    public static final IComparisonOperator NOT_EQUALS = (s1, s2) -> s1.compareTo(s2) != 0;

    /** The 'like' (pattern matching) operator. */
    public static final IComparisonOperator LIKE = (s1, s2) -> s1.matches(s2.replace("*", ".*"));
}
