package hr.fer.zemris.java.hw05.db;

/**
 * Defines a filter for a {@link StudentRecord}.
 */
public interface IFilter {

    /**
     * Checks if the record satisfies the filter condition.
     *
     * @param record the record to check
     *
     * @return true if the condition was satisfied, false otherwise
     */
    boolean accepts(StudentRecord record);
}
