package hr.fer.zemris.java.hw05.db;

import hr.fer.zemris.java.hw05.db.lexer.QueryLexer;
import hr.fer.zemris.java.hw05.db.lexer.QueryLexerException;
import hr.fer.zemris.java.hw05.db.lexer.QueryToken;

import java.util.*;

import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.*;
import static hr.fer.zemris.java.hw05.db.lexer.QueryTokenType.*;


/**
 * A query parser for the simple database in hw05.
 */
public class QueryParser {

    /** Set of all valid field names. */
    private static final Set<String> VALID_FIELDS = new HashSet<>();

    /** Map of all operator strategies. */
    private static final Map<String, IComparisonOperator> OPERATORS = new HashMap<>();
    private static final Map<String, IFieldValueGetter> FIELD_GETTERS = new HashMap<>();

    static {
        OPERATORS.put("<", LESS);
        OPERATORS.put("<=", LESS_OR_EQUALS);
        OPERATORS.put(">", GREATER);
        OPERATORS.put(">=", GREATER_OR_EQUALS);
        OPERATORS.put("=", EQUALS);
        OPERATORS.put("!=", NOT_EQUALS);
        OPERATORS.put("LIKE", LIKE);

        FIELD_GETTERS.put("jmbag", JMBAG);
        FIELD_GETTERS.put("firstName", FIRST_NAME);
        FIELD_GETTERS.put("lastName", LAST_NAME);

        VALID_FIELDS.add("jmbag");
        VALID_FIELDS.add("firstName");
        VALID_FIELDS.add("lastName");
    }

    /** The lexer for this query. */
    private QueryLexer lexer;

    /** The list of expressions in this query. */
    private List<ConditionalExpression> expressions = new ArrayList<>();

    /**
     * Creates a new query parser with the given query text.
     *
     * @param query the query to parse.
     *
     * @throws NullPointerException if {@code query} is null
     * @throws QueryLexerException  if the query is invalid or incomplete
     */
    public QueryParser(String query) {
        lexer = new QueryLexer(query);

        try {
            buildExpressions();
        } catch (QueryLexerException ex) {
            throw new QueryParserException("Parse error: incomplete or invalid query.");
        }
    }

    /**
     * Consumes all lexer tokens and constructs the list of expressions.
     *
     * @throws QueryParserException if the query is invalid
     */
    private void buildExpressions() {
        boolean isAnd = false;

        for (QueryToken token = lexer.nextToken(); token.getType() != EOF; token = lexer.nextToken()) {

            if (token.getType() == AND) {
                isAnd = true;
                continue;
            }

            var fieldToken = lexer.getToken();
            var operatorToken = lexer.nextToken();
            var literalToken = lexer.nextToken();


            if (token.getType() != FIELD) {
                throw new QueryParserException("Parse error: expression must begin with field name.");
            } else if (!VALID_FIELDS.contains(token.getValue())) {
                throw new QueryParserException("Parse error: invalid field name <" + token.getValue() + ">");
            }


            if (!OP_TOKENS.contains(operatorToken.getType())) {
                throw new QueryParserException("Parse error: invalid expression operator.");
            }

            if (literalToken.getType() != LITERAL) {
                throw new QueryParserException("Parse error: string literal expected on expression right-hand side.");
            }

            if (operatorToken.getType() == OP_LIKE) {
                String value = literalToken.getValue();

                if (value.indexOf('*') != value.lastIndexOf('*')) {
                    throw new QueryParserException("Parse error: too many wildcard characters.");
                }
            }

            expressions.add(
                new ConditionalExpression(
                    FIELD_GETTERS.get(fieldToken.getValue()),
                    OPERATORS.get(operatorToken.getValue().toUpperCase()),
                    literalToken.getValue()
                )
            );

            isAnd = false;
        }

        if (isAnd) {
            throw new QueryParserException("Parse error: unfinished query.");
        }
    }

    /**
     * Checks if the query is in the form {@code jmbag="xxxxxxxxxx"}.
     *
     * @return true if the query is direct, false otherwise
     */
    public boolean isDirectQuery() {
        return expressions.size() == 1
            && expressions.get(0).getFieldGetter() == JMBAG
            && expressions.get(0).getComparisonOperator() == EQUALS;
    }

    /**
     * Gets the direct query JMBAG.
     * <p>
     * If the query is not a direct one, an exception is thrown.
     *
     * @return the queried JMBAG
     *
     * @throws IllegalStateException if the query is not direct
     */
    public String getQueriedJMBAG() {
        if (!isDirectQuery()) {
            throw new IllegalStateException("Can't get queried JMBAG of indirect query.");
        }

        return expressions.get(0).getStringLiteral();
    }

    /**
     * Gets the list of all expressions in the query.
     *
     * @return list of all expressions
     */
    public List<ConditionalExpression> getQuery() {
        return expressions;
    }
}
