package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * Represents a single student and stores their JMBAG,
 * first name, last name, and final grade.
 */
public class StudentRecord {

    /** The student's jmbag. */
    private final String jmbag;

    /** The student's first name. */
    private final String firstName;

    /** The student's last name. */
    private final String lastName;

    /** The student's final grade. */
    private final int finalGrade;

    /**
     * Creates a new student with the specified information.
     *
     * @param jmbag the student's jmbag
     * @param firstName the student's first name
     * @param lastName the student's last name
     * @param finalGrade the student's final grade
     *
     * @throws NullPointerException if any of the parameters are null
     * @throws IllegalArgumentException if {@code grade} is less than 1 or greater than 5
     */
    public StudentRecord(String jmbag, String firstName, String lastName, int finalGrade) {
        if (finalGrade < 1 || finalGrade > 5) {
            throw new IllegalArgumentException("Final grade must be between 1 and 5.");
        }

        this.jmbag = Objects.requireNonNull(jmbag, "Student data can't be null.");
        this.firstName = Objects.requireNonNull(firstName, "Student data can't be null.");
        this.lastName = Objects.requireNonNull(lastName, "Student data can't be null.");
        this.finalGrade = finalGrade;
    }

    /**
     * Gets the jmbag of this student.
     *
     * @return the student's jmbag
     */
    public String getJmbag() {
        return jmbag;
    }

    /**
     * Gets the first name of this student.
     *
     * @return the student's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets the last name of this student.
     *
     * @return the student's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Gets the final grade of this student.
     *
     * @return the student's final grade
     */
    public int getFinalGrade() {
        return finalGrade;
    }

    /**
     * Gets the hash code of this record.
     * <p>
     * To match {@code equals}, this method returns the hash code of the {@code jmbag} property.
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        return jmbag.hashCode();
    }

    /**
     * Checks if this record is equal to another object.
     * <p>
     * Equality is checked based on the {@code jmbag} property.
     *
     * @param o the object to check
     *
     * @return true if the other object is a StudentRecord, and the jmbag-s match
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StudentRecord)) {
            return false;
        }

        StudentRecord that = (StudentRecord) o;

        return jmbag.equals(that.jmbag);

    }
}
