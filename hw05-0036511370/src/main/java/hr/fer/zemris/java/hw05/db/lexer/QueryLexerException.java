package hr.fer.zemris.java.hw05.db.lexer;

/**
 * Thrown when an error occurs during token creation.
 */
public class QueryLexerException extends RuntimeException {

    /**
     * Delegated to {@code super()}.
     */
    public QueryLexerException() {
        super();
    }

    /**
     * Delegated to {@code super(message)}.
     *
     * @param message the exception message
     */
    public QueryLexerException(String message) {
        super(message);
    }
}
