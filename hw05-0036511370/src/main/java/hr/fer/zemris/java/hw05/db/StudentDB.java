package hr.fer.zemris.java.hw05.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * A program that reads queries from the user and displays the results.
 */
public class StudentDB {

    /**
     * Entry point of the program.
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        StudentDatabase database = null;

        try {
            List<String> input = Files.readAllLines(Paths.get("database.txt"), StandardCharsets.UTF_8);
            database = new StudentDatabase(input);
        } catch (IOException ex) {
            System.out.println("Error reading database file.");
            System.exit(-1);
        } catch (IllegalArgumentException ex) {
            System.out.println("Error in database file: " + ex.getMessage());
            System.exit(-1);
        }

        var sc = new Scanner(System.in);

        while (true) {
            System.out.print("> ");

            var input = sc.nextLine();

            if (input.toLowerCase().equals("exit")) {
                System.out.println("Goodbye!");
                break;
            }

            if (!input.startsWith("query ")) {
                System.out.println("Invalid input. Expected:\n\tquery [expressions...]");
                continue;
            }

            QueryParser parser;

            try {
                parser = new QueryParser(input.replace("query ", ""));
            } catch (QueryParserException ex) {
                System.out.println("Invalid query! " + ex.getMessage());
                continue;
            }

            List<StudentRecord> queryResult;

            if (parser.isDirectQuery()) {
                queryResult = Collections.singletonList(database.forJMBAG(parser.getQueriedJMBAG()));
                System.out.println("Using index for record retrieval.");
            } else {
                var fullFilter = new QueryFilter(parser.getQuery());
                queryResult = database.filter(fullFilter);
            }

            var output = RecordFormatter.format(queryResult);
            output.forEach(System.out::println);
            System.out.println("Records selected: " + queryResult.size() + "\n");
        }

        sc.close();
    }
}
