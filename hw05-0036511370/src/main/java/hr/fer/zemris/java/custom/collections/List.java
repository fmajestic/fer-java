package hr.fer.zemris.java.custom.collections;

/**
 * List
 */
public interface List<T> extends Collection<T> {

    /**
     * Gets the list element at the specified index.
     * 
     * @param index the index of the element to be retrieved
     * 
     * @throws IndexOutOfBoundsException if {@code index} is negative, or greater
     *                                   than {@code size - 1}
     */
    T get(int index);

    /**
     * Inserts the given non-null {@code value} at the position, shifting the
     * remaining elements to the right.
     * 
     * @param value    the value to be inserted, not null
     * @param position the position at which to insert, valid range is
     *                 {@code [0, size]}
     *
     * @throws NullPointerException      if {@code value} is null
     * @throws IndexOutOfBoundsException if {@code position} is negative, or greater
     *                                   than {@code size}
     * 
     * @see #add(Object)
     */
    void insert(T value, int position);

    /**
     * Returns the index of the first element that matches {@code value}.
     * 
     * @param value the value to search for
     * 
     * @return the index of the element, or -1 if nothing was found
     */
    int indexOf(Object value);

    /** 
     * Removes an element from the specified index.
     * 
     * @param index the index to remove from
     * 
     * @throws IndexOutOfBoundsException if index is negative or greater than {@code size-1 }
     */
    void remove(int index);
}
