package hr.fer.zemris.java.hw05.db.lexer;

import java.util.EnumSet;

/** All existing token types. */
public enum QueryTokenType {
    /** A database field name. */
    FIELD,

    /** A string literal. */
    LITERAL,

    /** The 'greater than' (>) operator. */
    OP_GT,

    /** The 'greater than or equal' (>=) operator. */
    OP_GEQ,

    /** The 'less than' (<) operator. */
    OP_LT,

    /** The 'less than or equal' (<=) operator. */
    OP_LEQ,

    /** The 'equal' (=) operator. */
    OP_EQ,

    /** The 'not equal' (!=) operator. */
    OP_NEQ,

    /** The 'like' (pattern matching) operator. */
    OP_LIKE,

    /** Logical AND operator. */
    AND,

    /** End of query */
    EOF;

    /** Valid expression operator tokens. */
    public static final EnumSet<QueryTokenType> OP_TOKENS =
        EnumSet.of(OP_LT, OP_LEQ, OP_GT, OP_GEQ, OP_EQ, OP_NEQ, OP_LIKE);
}
