package hr.fer.zemris.java.hw05.db;

/**
 * Defines getters for all queryable fields of {@link StudentRecord}.
 */
public class FieldValueGetters {

    /** The first name getter. */
    public static final IFieldValueGetter FIRST_NAME = StudentRecord::getFirstName;

    /** The last name getter. */
    public static final IFieldValueGetter LAST_NAME = StudentRecord::getLastName;

    /** The JMBAG getter. */
    public static final IFieldValueGetter JMBAG = StudentRecord::getJmbag;
}
