package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * Contains a single query conditional expression.
 */
public class ConditionalExpression {

    /** The field getter of the expression. */
    private IFieldValueGetter fieldGetter;

    /** The comparison operator of the expression. */
    private IComparisonOperator comparisonOperator;

    /** The right-hand side of the expression. */
    private String literal;

    /**
     * Creates a new conditional expression with the given parameters.
     *
     * @param fieldGetter the field name to check
     * @param operator  the expression operator
     * @param literal   the literal to check the field for
     *
     * @throws NullPointerException if any of the arguments are null
     */
    public ConditionalExpression(IFieldValueGetter fieldGetter, IComparisonOperator operator, String literal) {
        this.fieldGetter = Objects.requireNonNull(fieldGetter);
        this.comparisonOperator = Objects.requireNonNull(operator);
        this.literal = Objects.requireNonNull(literal);
    }

    /**
     * Gets the field value getter for this expression.
     *
     * @return the field value getter
     */
    public IFieldValueGetter getFieldGetter() {
        return fieldGetter;
    }

    /**
     * Gets the comparison operator for this expression.
     *
     * @return the comparison operator
     */
    public IComparisonOperator getComparisonOperator() {
        return comparisonOperator;
    }

    /**
     * Gets the string literal value for this expression.
     *
     * @return the string literal
     */
    public String getStringLiteral() {
        return literal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ConditionalExpression)) {
            return false;
        }

        ConditionalExpression that = (ConditionalExpression) o;

        if (!fieldGetter.equals(that.fieldGetter)) {
            return false;
        }
        if (!comparisonOperator.equals(that.comparisonOperator)) {
            return false;
        }
        return literal.equals(that.literal);

    }

    @Override
    public int hashCode() {
        int result = fieldGetter.hashCode();
        result = 31 * result + comparisonOperator.hashCode();
        result = 31 * result + literal.hashCode();
        return result;
    }
}
