package hr.fer.zemris.java.hw05.db.lexer;

import java.util.Objects;

/** A single database query token. */
public class QueryToken {

    /** The token value. */
    private String value;

    /** The token type. */
    private QueryTokenType type;

    /**
     * Creates a new token with the given values.
     *
     * @param type  the token type
     * @param value the token value
     *
     * @throws NullPointerException if {@code type} is null
     */
    public QueryToken(QueryTokenType type, String value) {
        this.value = value;
        this.type = Objects.requireNonNull(type, "Token type can't be null.");
    }

    /**
     * Gets the token value.
     *
     * @return the token value
     */
    public String getValue() {
        return value;
    }

    /**
     * Gets the token type.
     *
     * @return the token type
     */
    public QueryTokenType getType() {
        return type;
    }
}
