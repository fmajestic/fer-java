package hr.fer.zemris.java.hw05.db.lexer;

import hr.fer.zemris.java.hw05.db.StudentDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static hr.fer.zemris.java.hw05.db.lexer.QueryTokenType.*;

/**
 * A simple, single-state lexer for {@link StudentDatabase} queries.
 */
public class QueryLexer {

    private static final char[] STOP_SYMBOLS = new char[]{ '\n', '\r', '\t', ' ', '\"' };
    private static final char[] OPERATOR_SYMBOLS = new char[]{ '>', '<', '=', '!' };
    private static final Map<String, QueryTokenType> OPERATOR_TYPE_MAP = new HashMap<>();

    static {
        OPERATOR_TYPE_MAP.put("<", OP_LT);
        OPERATOR_TYPE_MAP.put("<=", OP_LEQ);
        OPERATOR_TYPE_MAP.put(">", OP_GT);
        OPERATOR_TYPE_MAP.put(">=", OP_GEQ);
        OPERATOR_TYPE_MAP.put("=", OP_EQ);
        OPERATOR_TYPE_MAP.put("!=", OP_NEQ);
        OPERATOR_TYPE_MAP.put("LIKE", OP_LIKE);
        OPERATOR_TYPE_MAP.put("AND", AND);
    }

    /** The data to extract tokens from. */
    private char[] data;

    /** The current index in the data. */
    private int currentIndex;

    /** The current token. */
    private QueryToken token;

    /**
     * Creates a new lexer that creates tokens from the given text.
     *
     * @param text the text to tokenize
     *
     * @throws NullPointerException if {@code text} is null
     */
    public QueryLexer(String text) {
        data = Objects.requireNonNull(text, "Lexer text can't be null.").toCharArray();
        currentIndex = 0;
    }

    /**
     * Generates and returns the next token.
     *
     * @return the next token
     *
     * @throws QueryLexerException if a new token is requested after EOF
     */
    public QueryToken nextToken() {
        generateNextToken();
        return token;
    }

    /**
     * Generates the next token.
     *
     * @throws QueryLexerException if a new token is requested after EOF
     */
    private void generateNextToken() {
        if (token != null && token.getType() == EOF) {
            throw new QueryLexerException("End of query already reached.");
        }

        skipWhitespace();

        if (currentIndex >= data.length) {
            token = new QueryToken(EOF, null);
            return;
        }


        if (data[currentIndex] == '\"') {
            currentIndex++;
            token = new QueryToken(LITERAL, nextElement());

            if (currentIndex < data.length && data[currentIndex] != '\"') {
                throw new QueryLexerException("Unclosed string literal.");
            }

            currentIndex++;
        } else {
            String value = nextElement();
            QueryTokenType type;

            if ((type = OPERATOR_TYPE_MAP.get(value.toUpperCase())) != null) {
                token = new QueryToken(type, value);
            } else {
                token = new QueryToken(FIELD, value);
            }
        }
    }

    /**
     * Utility method for getting the next element, whatever it is.
     *
     * @return the next query element.
     */
    private String nextElement() {
        int startIndex = currentIndex;
        boolean isOperator = isOperatorSymbol(data[currentIndex]);

        for (; currentIndex < data.length; currentIndex++) {
            if (isStopSymbol(data[currentIndex]) || (isOperator ^ isOperatorSymbol(data[currentIndex]))) {
                break;
            }
        }

        int endIndex = currentIndex;

        return new String(data, startIndex, endIndex - startIndex);
    }

    /**
     * Checks if a character is a stop symbol.
     *
     * @param ch the character to check
     *
     * @return true if the character matches any of the stop symbols, false otherwise
     */
    private boolean isStopSymbol(char ch) {
        for (char symbol : STOP_SYMBOLS) {
            if (ch == symbol) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a character is an operator symbol.
     *
     * @param ch the character to check
     *
     * @return true if the character matches any of the operator symbols, false otherwise
     */
    private boolean isOperatorSymbol(char ch) {
        for (char symbol : OPERATOR_SYMBOLS) {
            if (ch == symbol) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the current token.
     *
     * @return the current token
     */
    public QueryToken getToken() {
        return token;
    }

    /**
     * Skips to next non-whitespace character.
     */
    private void skipWhitespace() {
        for (; currentIndex < data.length; currentIndex++) {
            if (!Character.isWhitespace(data[currentIndex])) {
                break;
            }
        }
    }

    /**
     * Checks if there are any characters left.
     *
     * @return true if there are characters left, false otherwise
     */
    private boolean haveMoreData() {
        return currentIndex + 1 < data.length;
    }

}
