package hr.fer.zemris.java.custom.collections;

import java.util.Objects;


/**
 * Represents a simple dictionary implementation.
 * <p>
 * This dictionary is an adapter around an {@link ArrayIndexedCollection}
 * and performs no actual hashing and "bucket" storage.
 * <p>
 * Value retrieval is in O(n).
 *
 * @param <K> the key type
 * @param <V> the value type
 */
public class Dictionary<K, V> {

    /** The default dictionary table size. */
    private static final int DEFAULT_CAPACITY = 16;

    /** The key-value pairs of the dictionary. */
    private ArrayIndexedCollection<KeyValuePair<K, V>> elements;

    /**
     * Holds a value mapped to a key.
     *
     * @param <K> the key type
     * @param <V> the value type
     */
    private static class KeyValuePair<K, V> {
        private K key;
        private V value;

        KeyValuePair(K key, V value) {
            this.key = Objects.requireNonNull(key, "Dictionary key can't be null.");
            this.value = value;
        }

        public V getValue() {
            return value;
        }

        public K getKey() {
            return key;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            KeyValuePair<?, ?> that = (KeyValuePair<?, ?>) o;

            return key.equals(that.key);

        }

        @Override
        public int hashCode() {
            return key.hashCode();
        }
    }

    /**
     * Default constructor. Creates a dictionary with a default capacity.
     */
    public Dictionary() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Creates a dictionary with an initial capacity.
     *
     * @param initialCapacity the initial capacity
     * @throws IllegalArgumentException if {@code initialCapacity} is less than 1
     */
    public Dictionary(int initialCapacity) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("Initial size can't be less than 1. Got: " + initialCapacity);
        }

        elements = new ArrayIndexedCollection<>(initialCapacity);
    }

    /**
     * Checks if the dictionary is empty.
     *
     * @return true if the dictionary contains no elements, false otherwise.
     */
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    /**
     * Gets the size of this dictionary.
     *
     * @return the size
     */
    public int size() {
        return elements.size();
    }

    /**
     * Removes all elements from this dictionary.
     */
    public void clear() {
        elements.clear();
    }

    /**
     * Adds a new key-value pair to the dictionary, or if the key already exists, updates the value.
     *
     * @param key the key to update/add
     * @param value the new value
     */
    public void put(K key, V value) {
        Objects.requireNonNull(key, "Dictionary key can't ben null.");

        var kvp = new KeyValuePair<>(key, value);

        int address = elements.indexOf(kvp);

        if (address >= 0) {
            elements.insert(kvp, address);
        } else {
            elements.add(kvp);
        }
    }

    /**
     * Gets the value stored at the specified key.
     *
     * @param key the key to get from
     * @return the value stored at the key, or {@code null} if key does not exits in the dictionary.
     */
    public V get(Object key) {
        var getter = elements.createElementsGetter();

        while (getter.hasNextElement()) {
            var kvp = getter.getNextElement();

            if (key.equals(kvp.key)) {
                return kvp.value;
            }
        }

        return null;
    }
}
