package hr.fer.zemris.java.hw05.db;

/**
 * Defines a comparison operator.
 */
public interface IComparisonOperator {

    /**
     * Checks if the given values satisfy the provided comparison.
     *
     * @param value1 the left value of the operator
     * @param value2 the right value of the operator
     *
     * @return true if the operator was satisfied, false otherwise
     */
    boolean satisfied(String value1, String value2);
}
