package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A simple class offering a static method for record formatting.
 */
public class RecordFormatter {

    /**
     * Formats a list of records as a table fit for writing to a file/stdout.
     * <p>
     * Each string in the output list is a line of output.
     *
     * @param records the records to format
     *
     * @return the formatted table rows
     */
    public static List<String> format(List<StudentRecord> records) {
        var result = new ArrayList<String>();

        if (records.size() == 0) {
            return result;
        }

        var firstLength =
            records.stream().collect(Collectors.summarizingInt(rec -> rec.getFirstName().length())).getMax();
        var lastLength =
            records.stream().collect(Collectors.summarizingInt(rec -> rec.getLastName().length())).getMax();

        var separator = String.format(
            "+%s+%s+%s+%s+",
            "=".repeat(12),
            "=".repeat(lastLength + 2),
            "=".repeat(firstLength + 2),
            "=".repeat(3)
        );

        var rowFormat = String.format("| %%s | %%-%ds | %%-%ds | %%d |", lastLength, firstLength);

        result.add(separator);

        for (var rec : records) {
            result.add(String.format(
                rowFormat,
                rec.getJmbag(),
                rec.getLastName(),
                rec.getFirstName(),
                rec.getFinalGrade()
            ));
        }

        result.add(separator);

        return result;
    }
}
