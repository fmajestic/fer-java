package hr.fer.zemris.java.hw05.db;

import java.util.*;

/**
 * Models a simple database.
 * <p>
 * The database maintains a list of records and an index for fast retrieval of direct queries.
 */
public class StudentDatabase {

    /** All the database records. */
    private List<StudentRecord> records = new ArrayList<>();

    /** The index, mapped as JMBAG -> Record. */
    private Map<String, StudentRecord> index = new HashMap<>();

    /**
     * Creates a new database from a list of entries.
     * <p>
     * All final grades must be between 1 and 5 (inlusive).
     * <p>
     * Duplicate JMBAGs are not permitted!
     *
     * @param input the database entries to add.
     *
     * @throws IllegalArgumentException if a duplicate JMBAG exists, or an invalid entry is found
     */
    public StudentDatabase(List<String> input) {

        for (int i = 0, len = input.size(); i < len; i++) {

            if (input.get(i).isEmpty()) {
                continue;
            }

            StudentRecord record;
            var parts = input.get(i).split("\\t");

            try {
                if (index.containsKey(parts[0])) {
                    throw new IllegalArgumentException("Duplicate JMBAG on line: " + (i + 1));
                }

                record = new StudentRecord(parts[0], parts[2], parts[1], Integer.parseInt(parts[3]));
            } catch (NumberFormatException | IndexOutOfBoundsException ex) {
                throw new IllegalArgumentException("Invalid entry on line:" + (i + 1));
            }

            index.put(parts[0], record);
            records.add(record);
        }
    }

    /**
     * Returns a student record associated with the specified jmbag.
     * <p>
     * If no record with such a jmbag exists, this method returns null.
     *
     * @param jmbag the jmbag to search for
     *
     * @return the record with this jmbag, or {@code null} if no record was found
     */
    public StudentRecord forJMBAG(String jmbag) {
        return index.get(jmbag);
    }

    /**
     * Gets all of the records that are accepted by the filter.
     *
     * @param filter the check to apply to each record
     *
     * @return a list of all the records that the filter accepted
     *
     * @throws NullPointerException if {@code filter} is {@code null}
     */
    public List<StudentRecord> filter(IFilter filter) {
        Objects.requireNonNull(filter, "Record filter can't be null.");

        // How this would actually work:
        // return records.stream().filter(filter::accepts).collect(Collectors.toList());

        var accepted = new ArrayList<StudentRecord>();

        for (var record : records) {
            if (filter.accepts(record)) {
                accepted.add(record);
            }
        }

        return accepted;
    }
}
