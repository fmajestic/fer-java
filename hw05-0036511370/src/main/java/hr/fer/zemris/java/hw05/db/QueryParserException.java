package hr.fer.zemris.java.hw05.db;

/** Thrown when any parse error occurs. */
public class QueryParserException extends RuntimeException {

    /**
     * Delegated to {@code super()}.
     */
    public QueryParserException() {
        super();
    }

    /**
     * Delegated to {@code super(message)}.
     *
     * @param message the exception message
     */
    public QueryParserException(String message) {
        super(message);
    }
}
