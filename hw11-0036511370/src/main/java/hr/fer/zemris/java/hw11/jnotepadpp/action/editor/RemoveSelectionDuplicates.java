package hr.fer.zemris.java.hw11.jnotepadpp.action.editor;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.StringJoiner;

/** Removes duplicate lines from selection. */
public class RemoveSelectionDuplicates extends JNotepadPPAction {

    /**
     * Creates a new removing action
     *
     * @param lp    the localization provider
     * @param model the model to get files from
     * @param app   unused
     */
    public RemoveSelectionDuplicates(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("distinct-lines", lp, model, app);
    }

    /**
     * Removes duplicate lines from the selection of the current document's text area.
     *
     * @param e ignored
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent editor = model.getCurrentDocument().getTextComponent();
        Document document = editor.getDocument();
        Element root = document.getDefaultRootElement();

        Element beginRow = root.getElement(root.getElementIndex(editor.getSelectionStart()));
        Element endRow = root.getElement(root.getElementIndex(editor.getSelectionEnd()));

        int beginTake = beginRow.getStartOffset();
        int endTake = endRow.getEndOffset();

        try {
            int len = endTake - beginTake;
            String[] sorted = document.getText(beginTake, len).split("\n");
            StringJoiner joiner = new StringJoiner("\n");

            Arrays.stream(sorted).distinct().forEach(joiner::add);
            String toInsert = joiner.toString();

            document.remove(beginTake, len - 1);
            document.insertString(beginTake, toInsert, null);
        } catch (BadLocationException ignored) {
        }

    }
}
