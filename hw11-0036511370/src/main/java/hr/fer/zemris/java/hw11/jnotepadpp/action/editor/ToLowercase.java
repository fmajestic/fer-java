package hr.fer.zemris.java.hw11.jnotepadpp.action.editor;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;

/** Converts selected text to lowercase. */
public class ToLowercase extends JNotepadPPAction {

    /**
     * Creates a new lowercase action.
     *
     * @param lp    the localization provider
     * @param model the model to get files from
     * @param app   unused
     */
    public ToLowercase(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("lowercase-text", lp, model, app);
    }

    /**
     * Changes selected text to lowercase
     *
     * @param e ignored
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent editor = model.getCurrentDocument().getTextComponent();

        char[] toggled = editor.getSelectedText().toCharArray();

        for (int i = 0; i < toggled.length; i++) {
            char c = toggled[i];

            if (Character.isUpperCase(c)) {
                toggled[i] = Character.toLowerCase(c);
            }
        }

        editor.replaceSelection(new String(toggled));

    }
}
