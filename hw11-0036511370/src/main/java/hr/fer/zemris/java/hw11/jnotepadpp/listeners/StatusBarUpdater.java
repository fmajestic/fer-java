package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;

import static java.lang.Math.abs;

/**
 * Updates the status bar.
 */
public class StatusBarUpdater implements MultipleDocumentListener, CaretListener {

    /** The file size number label. */
    private JLabel len;

    /** The row number label. */
    private JLabel row;

    /** The column number label. */
    private JLabel col;

    /** The selection length label. */
    private JLabel sel;

    /** The editor to watch. */
    private JTextComponent editor;


    /**
     * Creates a new updater for the given labels.
     *
     * @param lengthNum the file size number label
     * @param lineNum   the row number label
     * @param colNum    the the column number label
     * @param selNum    the selection length label
     */
    public StatusBarUpdater(JLabel lengthNum, JLabel lineNum, JLabel colNum, JLabel selNum) {
        this.len = lengthNum;
        this.row = lineNum;
        this.col = colNum;
        this.sel = selNum;
    }

    /**
     * If the previous editor is not null, unregisters self from its editor caret listener.
     * <p>
     * If the current model is not null, stores its editor and registers self as its caret listener.
     * Otherwise, sets the editor to null.
     * <p>
     * Immediately updates the labels.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
        if (previousModel != null) {
            previousModel.getTextComponent().removeCaretListener(this);
        }

        if (currentModel != null) {
            editor = currentModel.getTextComponent();
            editor.addCaretListener(this);
        } else {
            editor = null;
        }

        update(null);
    }

    /**
     * Updates the labels based on the event. If the event is null, clears the labels.
     *
     * @param e the event
     */
    private void update(CaretEvent e) {
        if (editor != null) {
            len.setText(String.valueOf(editor.getText().length()));

            if (e != null) {
                Document doc = editor.getDocument();
                Element root = doc.getDefaultRootElement();

                int rowInd = root.getElementIndex(e.getDot());
                int colInd = e.getDot() - root.getElement(rowInd).getStartOffset();

                row.setText(String.valueOf(rowInd + 1));
                col.setText(String.valueOf(colInd + 1));
                sel.setText(String.valueOf(abs(e.getDot() - e.getMark())));

            } else {
                row.setText("1");
                col.setText("1");
                sel.setText("0");
            }
        } else {
            len.setText("");
            row.setText("");
            col.setText("");
            sel.setText("");
        }
    }

    /**
     * Does nothing
     *
     * @param model unused
     */
    @Override
    public void documentAdded(SingleDocumentModel model) {

    }

    /**
     * Does nothing
     *
     * @param model unused
     */
    @Override
    public void documentRemoved(SingleDocumentModel model) {

    }

    /**
     * Updates the labels.
     *
     * @param e the event
     */
    @Override
    public void caretUpdate(CaretEvent e) {
        update(e);
    }
}
