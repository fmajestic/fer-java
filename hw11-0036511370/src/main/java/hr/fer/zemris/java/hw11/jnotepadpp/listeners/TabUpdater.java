package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.document.impl.DefaultMultipleDocumentModel;

import javax.swing.*;

/** Updates the current tab title and icon. */
public class TabUpdater implements SingleDocumentListener, MultipleDocumentListener {

    /** The model to get documents from. */
    private DefaultMultipleDocumentModel model;

    /** The modified file icon. */
    private final ImageIcon modified;

    /** the non-modified file icon. */
    private final ImageIcon notModified;

    /**
     * Creates a new tab updater
     *
     * @param model       the model to get documents from
     * @param modified    the modified file icon
     * @param notModified the non-modified file icon
     */
    public TabUpdater(DefaultMultipleDocumentModel model, ImageIcon modified, ImageIcon notModified) {
        this.model = model;
        this.modified = modified;
        this.notModified = notModified;
    }

    /**
     * Sets the current tab icon to the modified icon
     *
     * @param doc the document that changed
     */
    @Override
    public void documentModifyStatusUpdated(SingleDocumentModel doc) {
        update(doc, true);
    }

    /**
     * Sets the current tab icon and title.
     *
     * @param doc the document that was updated
     */
    @Override
    public void documentFilePathUpdated(SingleDocumentModel doc) {
        update(doc, false);
    }

    /**
     * Performs the update
     *
     * @param doc      the document that updated
     * @param iconOnly update only icon or title as well
     */
    private void update(SingleDocumentModel doc, boolean iconOnly) {

        int index = -1;

        for (int i = 0; i < model.getNumberOfDocuments(); i++) {
            if (model.getDocument(i) == doc) {
                index = i;
                break;
            }
        }


        if (index != -1) {

            updateIcon(doc, index);

            if (!iconOnly) {
                updateTooltip(doc, index);
                updateTitle(doc, index);
            }
        }
    }

    /**
     * Updates the tooltip at an index
     *
     * @param doc   the document to get the path from
     * @param index the index
     */
    private void updateTooltip(SingleDocumentModel doc, int index) {
        String tip = doc.getFilePath() != null ? doc.getFilePath().toAbsolutePath().toString() : "(unnamed) ";
        model.setToolTipTextAt(index, tip);
    }

    /**
     * Updates the icon at an index
     *
     * @param doc   the document to get the modified status from
     * @param index the index
     */
    private void updateIcon(SingleDocumentModel doc, int index) {
        model.setIconAt(index, doc.isModified() ? modified : notModified);
    }

    /**
     * Updates the title at an index
     *
     * @param doc   the document to get the path from
     * @param index the index
     */
    private void updateTitle(SingleDocumentModel doc, int index) {
        String title = doc.getFilePath() == null
                       ? "(unnamed)"
                       : doc.getFilePath().getFileName().toString();

        model.setTitleAt(index, title);
    }

    /**
     * Does nothing
     *
     * @param previousModel unused
     * @param currentModel  unused
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {

    }

    /**
     * Registers self as listener to the document and immediately updates.
     *
     * @param doc the document that was added
     */
    @Override
    public void documentAdded(SingleDocumentModel doc) {
        update(doc, true);
        doc.addSingleDocumentListener(this);
    }

    /**
     * Unregisters self as listener
     *
     * @param model the document that was removed
     */
    @Override
    public void documentRemoved(SingleDocumentModel model) {
        model.removeSingleDocumentListener(this);
    }
}
