package hr.fer.zemris.java.hw11.jnotepadpp.action.editor;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import java.awt.event.ActionEvent;

/** Cuts selected text. */
public class CutText extends JNotepadPPAction {

    /**
     * Creates a new cut text action.
     *
     * @param lp    the localization provider
     * @param model the model to get files from
     * @param app   unused
     */
    public CutText(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("cut-text", lp, model, app);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        model.getCurrentDocument().getTextComponent().cut();
    }
}
