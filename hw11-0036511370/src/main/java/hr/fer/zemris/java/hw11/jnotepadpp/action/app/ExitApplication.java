package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

/** Exits the application. */
public class ExitApplication extends JNotepadPPAction {

    /**
     * Creates a new exit app action.
     *
     * @param lp    the localization provider
     * @param model ignored
     * @param app   the app to exit
     */
    public ExitApplication(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("exit-app", lp, model, app);
    }

    /**
     * Dispatches a new window closing event in the app.
     *
     * @param ignored ignored
     *
     * @see JFrame#dispatchEvent(AWTEvent)
     * @see WindowEvent
     * @see WindowEvent#WINDOW_CLOSING
     */
    @Override
    public void actionPerformed(ActionEvent ignored) {
        app.dispatchEvent(new WindowEvent(app, WindowEvent.WINDOW_CLOSING));
    }
}
