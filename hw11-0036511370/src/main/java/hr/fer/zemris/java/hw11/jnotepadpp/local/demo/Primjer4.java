package hr.fer.zemris.java.hw11.jnotepadpp.local.demo;

import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Primjer4 extends JFrame {
    private static final long serialVersionUID = 1L;

    private FormLocalizationProvider flp;


    private Primjer4() {
        super();
        flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(0, 0);
        setTitle("Demo");
        initGUI();
        pack();
    }

    private void initGUI() {
        getContentPane().setLayout(new BorderLayout());
        JButton gumb = new JButton(
            flp.getString("login")
        );

        getContentPane().add(gumb, BorderLayout.CENTER);

        JMenuBar mb = new JMenuBar();
        JMenu lang = new JMenu("Language");

        lang.add(new AbstractAction() {

            {
                putValue(NAME, "hr");
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                LocalizationProvider.getInstance().setLanguage("hr");
            }
        });

        lang.add(new AbstractAction() {

            {
                putValue(NAME, "en");
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                LocalizationProvider.getInstance().setLanguage("en");
            }
        });

        lang.add(new AbstractAction() {

            {
                putValue(NAME, "de");
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                LocalizationProvider.getInstance().setLanguage("de");
            }
        });

        mb.add(lang);
        setJMenuBar(mb);

        flp.addLocalizationListener(
            () -> gumb.setText(flp.getString("login"))
        );
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Primjer4().setVisible(true));
    }
}