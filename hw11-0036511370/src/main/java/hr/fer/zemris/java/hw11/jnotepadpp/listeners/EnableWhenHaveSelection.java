package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

/**
 * Enables actions when the current editor has a selection.
 */
public class EnableWhenHaveSelection extends ActionEnabler implements CaretListener {

    /**
     * Delegated to super constructor.
     *
     * @param actions the actions to manage
     */
    public EnableWhenHaveSelection(Action... actions) {
        super(actions);
    }

    /**
     * In addition to calling the super method, if the previous or current models exist,
     * removes or adds self from their caret listeners.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
        super.currentDocumentChanged(previousModel, currentModel);

        if (previousModel != null) {
            previousModel.getTextComponent().removeCaretListener(this);
        }

        if (currentModel != null) {
            currentModel.getTextComponent().addCaretListener(this);
        }
    }

    /**
     * Enables actions if the caret dot is in a different position than the mark,
     * disables them otherwise.
     *
     * @param e the event
     */
    @Override
    public void caretUpdate(CaretEvent e) {
        boolean haveSelection = e.getDot() != e.getMark();
        for (Action action : actions) {
            action.setEnabled(haveSelection);
        }
    }
}
