package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;

import javax.swing.*;

/**
 * Enables or disables actions.
 */
abstract class ActionEnabler implements MultipleDocumentListener, SingleDocumentListener {

    /** The actions to manage. */
    Action[] actions;

    /**
     * Creates a new enabler for the given actions.
     * <p>
     * All the actions are initially set as disabled.
     *
     * @param actions the actions to manage
     */
    ActionEnabler(Action... actions) {
        this.actions = actions;

        for (Action action : actions) {
            action.setEnabled(false);
        }
    }

    /**
     * The default implementation: if there is a previous model - unregisters itself from it, if there is a current
     * model - registers itself to it.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
        if (previousModel != null) {
            previousModel.removeSingleDocumentListener(this);
        }

        if (currentModel != null) {
            currentModel.addSingleDocumentListener(this);
        }
    }

    /**
     * Does nothing
     *
     * @param model the added document
     */
    @Override
    public void documentAdded(SingleDocumentModel model) {

    }

    /**
     * Does nothing
     *
     * @param model the removed document.
     */
    @Override
    public void documentRemoved(SingleDocumentModel model) {

    }

    /**
     * Does nothing
     *
     * @param model the document impl that changed
     */
    @Override
    public void documentModifyStatusUpdated(SingleDocumentModel model) {

    }

    /**
     * Does nothing
     *
     * @param model the document impl that changed
     */
    @Override
    public void documentFilePathUpdated(SingleDocumentModel model) {

    }
}
