package hr.fer.zemris.java.hw11.jnotepadpp.document;

/**
 * A listeners for a multiple-document.
 */
public interface MultipleDocumentListener {

    /**
     * Fired when the current document changes.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    void currentDocumentChanged(SingleDocumentModel previousModel,
                                SingleDocumentModel currentModel);

    /**
     * Fired when a document is added to the.
     *
     * @param model the added document
     */
    void documentAdded(SingleDocumentModel model);

    /**
     * Fired when a document removed from the.
     *
     * @param model the removed document.
     */
    void documentRemoved(SingleDocumentModel model);
}
