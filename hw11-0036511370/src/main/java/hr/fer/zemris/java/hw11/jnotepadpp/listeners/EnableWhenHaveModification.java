package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;

import javax.swing.*;

/**
 * Enables actions when the current document has unsaved changes.
 */
public class EnableWhenHaveModification extends ActionEnabler {

    /**
     * Delegated to super constructor.
     *
     * @param actions the actions to manage
     */
    public EnableWhenHaveModification(Action... actions) {
        super(actions);
    }

    /**
     * In addition to calling the super method, if the current model exists, updates action status immediately.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
        super.currentDocumentChanged(previousModel, currentModel);

        if (currentModel != null) {
            documentModifyStatusUpdated(currentModel);
        }
    }

    /**
     * Enables actions if the model is modified, disables them otherwise.
     *
     * @param model the document impl that changed
     *
     * @see SingleDocumentModel#isModified()
     * @see SingleDocumentModel#setModified(boolean)
     */
    @Override
    public void documentModifyStatusUpdated(SingleDocumentModel model) {
        boolean enabled = model.isModified() || model.getFilePath() == null;

        for (Action action : actions) {
            action.setEnabled(enabled);
        }
    }
}
