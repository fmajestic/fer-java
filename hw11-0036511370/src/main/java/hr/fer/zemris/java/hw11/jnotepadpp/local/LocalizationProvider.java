package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

/** Singleton provider. */
public class LocalizationProvider extends AbstractLocalizationProvider {

    /** The current language tag. */
    private String language;

    /** The current resource bundle. */
    private ResourceBundle bundle;

    /** The single instance. */
    private static LocalizationProvider instance;

    /**
     * Gets the instance.
     * <p>
     * The instance is lazily loaded, so no instance is created if this method is never called.
     *
     * @return the instance
     */
    public static LocalizationProvider getInstance() {
        if (instance == null) {
            instance = new LocalizationProvider();
        }

        return instance;
    }

    /** Initializes the provider to english. */
    private LocalizationProvider() {
        setLanguage("en");
    }

    /**
     * Sets the current language and notifies all listeners.
     *
     * @param language the language tag
     */
    public void setLanguage(String language) {
        this.language = Objects.requireNonNull(language);

        Locale locale = Locale.forLanguageTag(language);

        bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.local.prijevodi", locale);

        fire();
    }

    /** {@inheritDoc} */
    @Override
    public String getCurrentLanguage() {
        return language;
    }

    /** {@inheritDoc} */
    @Override
    public String getString(String key) {
        return bundle.getString(Objects.requireNonNull(key));
    }
}
