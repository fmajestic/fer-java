package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.nio.file.Paths;

/** Opens a file from disk. */
public class OpenFile extends JNotepadPPAction {

    /**
     * Creates a new open file action.
     *
     * @param lp    the localization provider
     * @param model the model to load the file into
     * @param app   the app to choose files from
     */
    public OpenFile(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("open-file", lp, model, app);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JFileChooser jfc = new JFileChooser(Paths.get(".").normalize().toAbsolutePath().toString());

        if (jfc.showOpenDialog(app) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        var discard = model.loadDocument(jfc.getSelectedFile().toPath());
    }
}
