package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import java.awt.event.ActionEvent;

/** Saves a file to disk. */
public class SaveFile extends JNotepadPPAction {

    /**
     * Creates a new file action.
     *
     * @param lp    the localization provider
     * @param model the model to get the file from
     * @param app   the app to use for saving the file
     */
    public SaveFile(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("save-file", lp, model, app);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        app.trySaveFile(model.getCurrentDocument());
    }
}
