package hr.fer.zemris.java.hw11.jnotepadpp.local;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Localization provider for forms.
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

    /**
     * Creates a new provider and registers a new listener to the form.
     * <p>
     * The listener connects/disconnects from the parent when the form opens/closes.
     *
     * @param parent the parent provider
     * @param form   the form to provide for
     */
    public FormLocalizationProvider(ILocalizationProvider parent, JFrame form) {
        super(parent);

        form.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                connect();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                disconnect();
            }
        });
    }
}
