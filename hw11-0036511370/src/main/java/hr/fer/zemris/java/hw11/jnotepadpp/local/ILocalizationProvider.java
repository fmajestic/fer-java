package hr.fer.zemris.java.hw11.jnotepadpp.local;

/** Provides localization functionality. */
public interface ILocalizationProvider {

    /**
     * Gets the current language tag.
     *
     * @return the current language tag
     */
    String getCurrentLanguage();

    /**
     * Gets the string associated with a key.
     *
     * @param key the key
     *
     * @return the string
     */
    String getString(String key);

    /**
     * Adds a listener to the provider
     *
     * @param l the listener
     */
    void addLocalizationListener(ILocalizationListener l);

    /**
     * Removes a listener from the provider.
     *
     * @param l the listener
     */
    void removeLocalizationListener(ILocalizationListener l);
}
