package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Models a provider that keeps a list of listeners to notify.
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {

    /** The localization change listeners. */
    private List<ILocalizationListener> listeners = new CopyOnWriteArrayList<>();

    /**
     * Notifies all listeners of localization change.
     */
    void fire() {
        listeners.forEach(ILocalizationListener::localizationChanged);
    }

    /** {@inheritDoc} */
    @Override
    public void addLocalizationListener(ILocalizationListener l) {
        listeners.add(l);
    }

    /** {@inheritDoc} */
    @Override
    public void removeLocalizationListener(ILocalizationListener l) {
        listeners.remove(l);
    }
}
