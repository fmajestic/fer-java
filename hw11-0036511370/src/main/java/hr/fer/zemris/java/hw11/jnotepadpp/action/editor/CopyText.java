package hr.fer.zemris.java.hw11.jnotepadpp.action.editor;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import java.awt.event.ActionEvent;

/** Copies selected text. */
public class CopyText extends JNotepadPPAction {

    /**
     * Creates a new copy text action.
     *
     * @param lp    the localization provider
     * @param model the model to get documents from
     * @param app   unused
     */
    public CopyText(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("copy-text", lp, model, app);
    }

    /**
     * Copies selected text to the clipboard.
     * @param e ignored
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        model.getCurrentDocument().getTextComponent().copy();
    }
}
