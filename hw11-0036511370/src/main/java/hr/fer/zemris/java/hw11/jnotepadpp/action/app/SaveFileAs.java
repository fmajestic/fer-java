package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.function.ToDoubleBiFunction;

/** Save to a new file. */
public class SaveFileAs extends JNotepadPPAction {

    /**
     * Creates a new save file as action.
     *
     * @param lp    the localization provider
     * @param model the model to get files from
     * @param app   the app to use for saving
     */
    public SaveFileAs(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("save-file-as", lp, model, app);
    }

    /**
     * Prompts the user for a new save path, and if it is valid, saves.
     * <p>
     * Asks to confirm overwrite in case of existing file
     *
     * @param ignored ignored
     */
    @Override
    public void actionPerformed(ActionEvent ignored) {
        JFileChooser jfc;

        if (model.getCurrentDocument().getFilePath() != null) {
            jfc = new JFileChooser(model.getCurrentDocument().getFilePath().getParent().toString());
        } else {
            jfc = new JFileChooser(".");
        }

        // TODO: fix saving as already opened file

        if (jfc.showSaveDialog(app) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        var current = model.getCurrentDocument();
        model.saveDocument(current, jfc.getSelectedFile().toPath());
    }
}
