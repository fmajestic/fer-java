package hr.fer.zemris.java.hw11.jnotepadpp.document;

/**
 * A listeners for single document.
 */
public interface SingleDocumentListener {

    /**
     * Fired when a document's modification status change
     *
     * @param model the document impl that changed
     */
    void documentModifyStatusUpdated(SingleDocumentModel model);

    /**
     * Fired when a document's path changes (saving to a new/different location).
     *
     * @param model the document impl that changed
     */
    void documentFilePathUpdated(SingleDocumentModel model);
}
