package hr.fer.zemris.java.hw11.jnotepadpp.local;

import javax.swing.*;

/** A localizable label. */
public class LJLabel extends JLabel {

    /** The i18n key. */
    private String key;

    /** The provider. */
    private ILocalizationProvider provider;

    /**
     * Creates a new localized label
     *
     * @param key the i18n key
     * @param lp  the provider
     */
    public LJLabel(String key, ILocalizationProvider lp) {
        this.key = key;
        this.provider = lp;

        updateLabel();
        provider.addLocalizationListener(this::updateLabel);
    }

    /** Updates the label text with the new translation. */
    private void updateLabel() {
        setText(provider.getString(key));
    }
}
