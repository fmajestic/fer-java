package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import java.awt.event.ActionEvent;

/** Closes the current file. */
public class CloseFile extends JNotepadPPAction {

    /**
     * Creates a new close file action
     *
     * @param lp    the i18n provider
     * @param model the model to close in
     * @param app   the app to use for saving modifications
     */
    public CloseFile(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("close-file", lp, model, app);
    }

    /**
     * Closes the current document, prompting the user for saving if there are unsaved changes.
     * @param ignored ignored
     */
    @Override
    public void actionPerformed(ActionEvent ignored) {

        var current = model.getCurrentDocument();

        if (app.saveIfModified(current)) {
            model.closeDocument(current);
        }
    }
}
