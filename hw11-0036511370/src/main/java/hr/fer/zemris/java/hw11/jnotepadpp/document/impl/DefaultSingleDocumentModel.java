package hr.fer.zemris.java.hw11.jnotepadpp.document.impl;

import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.ModifiedStatusUpdater;

import javax.swing.*;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;


public class DefaultSingleDocumentModel implements SingleDocumentModel {

    /** The filePath path. */
    private Path filePath;

    /** The editor textArea. */
    private JTextArea textArea;

    /** Does the filePath have unsaved changes. */
    private boolean modified;

    /** Event listeners. */
    private List<SingleDocumentListener> listeners;

    /**
     * Creates a new single document.
     *
     * @param path     the path of the document
     * @param textArea the area used for editing
     */
    public DefaultSingleDocumentModel(Path path, JTextArea textArea) {
        modified = false;
        this.filePath = path;
        this.textArea = Objects.requireNonNull(textArea);
        listeners = new CopyOnWriteArrayList<>();
        textArea.requestFocusInWindow();
    }

    /** {@inheritDoc} */
    @Override
    public JTextArea getTextComponent() {
        return textArea;
    }

    /** {@inheritDoc} */
    @Override
    public Path getFilePath() {
        return filePath;
    }

    /** {@inheritDoc} */
    @Override
    public void setFilePath(Path path) {
        Objects.requireNonNull(path);

        if (!Objects.equals(path, filePath)) {
            this.filePath = path;
            listeners.forEach(l -> l.documentFilePathUpdated(this));
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean isModified() {
        return modified;
    }

    /** {@inheritDoc} */
    @Override
    public void setModified(boolean modified) {
        if (this.modified != modified) {
            this.modified = modified;
            listeners.forEach(l -> l.documentModifyStatusUpdated(this));
        }
    }

    /** {@inheritDoc} */
    @Override
    public void addSingleDocumentListener(SingleDocumentListener l) {
        listeners.add(l);
    }

    /** {@inheritDoc} */
    @Override
    public void removeSingleDocumentListener(SingleDocumentListener l) {
        listeners.remove(l);
    }

}
