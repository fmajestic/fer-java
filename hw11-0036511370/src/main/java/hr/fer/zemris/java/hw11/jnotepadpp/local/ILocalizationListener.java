package hr.fer.zemris.java.hw11.jnotepadpp.local;

/** Listener of localization changes. */
public interface ILocalizationListener {

    /** Called when the current language changes. */
    void localizationChanged();
}
