package hr.fer.zemris.java.hw11.jnotepadpp.local.demo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Primjer1 extends JFrame {
    private static final long serialVersionUID = 1L;
    private String language;

    public Primjer1(String language) throws HeadlessException {
        this.language = language;
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(0, 0);
        setTitle("Demo");
        initGUI();
        pack();
    }

    private void initGUI() {
        getContentPane().setLayout(new BorderLayout());
        JButton gumb = new JButton(
            language.equals("hr") ? "Prijava" : "Login"
        );
        getContentPane().add(gumb, BorderLayout.CENTER);
        gumb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
// Napravi prijavu...
            }
        });
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Očekivao sam oznaku jezika kao argument!");
            System.err.println("Zadajte kao parametar hr ili en.");
            System.exit(-1);
        }
        final String jezik = args[0];
        SwingUtilities.invokeLater(() -> new Primjer1(jezik).setVisible(true));
    }
}