package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.*;
import java.awt.event.ActionEvent;

/** Displays some stats for the current file. */
public class FileStats extends JNotepadPPAction {

    /** The info window title. */
    private String title;

    /** The character count string. */
    private String charCountStr;

    /** The non whitespace character count string. */
    private String nonWhitespaceStr;

    /** The line count string. */
    private String linesStr;


    /**
     * Creates a new file stats action.
     *
     * @param lp    the localization provider
     * @param model the model to get the current file from
     * @param app   the app to display stats in
     */
    public FileStats(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("file-stats", lp, model, app);

        title = lp.getString("file-stats_title");
        charCountStr = lp.getString("file-stats_chars");
        nonWhitespaceStr = lp.getString("file-stats_whitespace");
        linesStr = lp.getString("file-stats_lines");

        lp.addLocalizationListener(() -> {
            title = lp.getString("file-stats_title");
            charCountStr = lp.getString("file-stats_chars");
            nonWhitespaceStr = lp.getString("file-stats_whitespace");
            linesStr = lp.getString("file-stats_lines");
        });
    }

    /**
     * Displays the stats for the current file.
     *
     * @param ignored ignored
     */
    @Override
    public void actionPerformed(ActionEvent ignored) {

        SingleDocumentModel current = model.getCurrentDocument();

        if (current == null) {
            return;
        }

        JTextArea editor = current.getTextComponent();
        String text = editor.getText();

        int nChars = text.length();
        int nonWhitespace = text.replaceAll("\\s+", "").length();
        int nLines = editor.getLineCount();

        JOptionPane.showMessageDialog(
            app,
            new JLabel(String.format(
                "<html>%s: %d<br>%s: %d<br>%s: %d<html>",
                charCountStr, nChars, nonWhitespaceStr, nonWhitespace, linesStr, nLines
            )),
            title,
            JOptionPane.INFORMATION_MESSAGE
        );
    }
}
