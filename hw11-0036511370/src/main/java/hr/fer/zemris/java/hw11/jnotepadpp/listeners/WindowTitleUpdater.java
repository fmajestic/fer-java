package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;

import java.util.Objects;

/**
 * Updates the window title on every document change
 * (another document focused or current document path changed).
 */
public class WindowTitleUpdater implements MultipleDocumentListener, SingleDocumentListener {

    /** The app whose title will be updated. */
    private JNotepadPP app;

    /**
     * Creates a new title updater.
     *
     * @param app the app whose title to update
     */
    public WindowTitleUpdater(JNotepadPP app) { this.app = Objects.requireNonNull(app, "App can't be null."); }

    /**
     * Unregisters self from the previous model's listeners, registers to the new model's listeners,
     * and updates the window title.
     * <p>
     * A null argument is simply ignored.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
        if (previousModel != null) {
            previousModel.removeSingleDocumentListener(this);
        }

        if (currentModel != null) {
            currentModel.addSingleDocumentListener(this);
        }

        updateTitle(currentModel);
    }

    /**
     * Updates the window title with the new filename.
     *
     * @param model the document impl that changed
     */
    @Override
    public void documentFilePathUpdated(SingleDocumentModel model) {
        updateTitle(model);
    }

    /**
     * Ignored.
     *
     * @param model ignored
     */
    @Override
    public void documentRemoved(SingleDocumentModel model) { }

    /**
     * Updates the window title.
     *
     * @param model the document to use
     */
    private void updateTitle(SingleDocumentModel model) {
        if (model != null) {
            if (model.getFilePath() != null) {
                app.setTitle(String.format("%s - JNotepad++", model.getFilePath().toString()));
            } else {
                app.setTitle("(unnamed) - JNotepad++");
            }
        } else {
            app.setTitle("JNotepad++");
        }
    }

    /**
     * Ignored.
     *
     * @param model ignored
     */
    @Override
    public void documentAdded(SingleDocumentModel model) { }

    /**
     * Ignored.
     *
     * @param model ignored
     */
    @Override
    public void documentModifyStatusUpdated(SingleDocumentModel model) { }
}
