package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import java.awt.event.ActionEvent;

/** Creates a new empty file. */
public class NewFile extends JNotepadPPAction {

    /**
     * Creates a new new file action.
     *
     * @param lp    the localization provider
     * @param model the model to create in
     * @param app   unused
     */
    public NewFile(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("new-file", lp, model, app);
    }

    /**
     * Invoked when an action occurs.
     *
     * @param e the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        model.createNewDocument();
    }
}
