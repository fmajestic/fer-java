package hr.fer.zemris.java.hw11.jnotepadpp.document;

import java.nio.file.Path;

/** A model that holds and manages multiple documents. */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {

    /**
     * Creates a new document.
     *
     * @return the created document
     */
    SingleDocumentModel createNewDocument();

    /**
     * Gets the current document.
     *
     * @return the current document, or null if there is no current document
     */
    SingleDocumentModel getCurrentDocument();

    /**
     * Loads a document from a path
     *
     * @param path the path to load from
     *
     * @return the loaded document
     *
     * @throws NullPointerException if the path is null
     */
    SingleDocumentModel loadDocument(Path path);

    /**
     * Saves the document, using an optional new path.
     *
     * @param model   the document to save
     * @param newPath the optional new path, can be null
     */
    void saveDocument(SingleDocumentModel model, Path newPath);

    /**
     * Closes a document.
     *
     * @param model the document to close
     */
    void closeDocument(SingleDocumentModel model);

    /**
     * Registers a listener to the model.
     *
     * @param l the listener
     */
    void addMultipleDocumentListener(MultipleDocumentListener l);

    /**
     * Unregisters a listener from the model
     *
     * @param l the listener
     */
    void removeMultipleDocumentListener(MultipleDocumentListener l);

    /**
     * Gets the number of open documents.
     *
     * @return the number of documents
     */
    int getNumberOfDocuments();

    /**
     * Gets a document from an index.
     *
     * @param index the index to get from
     *
     * @return the document at the index
     *
     * @throws IndexOutOfBoundsException if the index is negative or greater than
     *                                   {@link #getNumberOfDocuments()} {@code - 1}
     */
    SingleDocumentModel getDocument(int index);
}
