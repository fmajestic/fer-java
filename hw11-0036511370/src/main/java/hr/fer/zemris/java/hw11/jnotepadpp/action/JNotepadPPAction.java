package hr.fer.zemris.java.hw11.jnotepadpp.action;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

/**
 * A base class for all JNotepadPP actions.
 * <p>
 * Contains shared localization logic and model/app references.
 */
public abstract class JNotepadPPAction extends LocalizableAction {

    /** The file model. */
    protected MultipleDocumentModel model;

    /** The app. */
    protected JNotepadPP app;

    /**
     * Creates a new notepad action.
     * <p>
     * Registers an additional listener on the provider, which changes the short description on language change.
     *
     * @param key   the base i18n key
     * @param lp    the localization provider to use
     * @param model the document model to use
     * @param app   the app to use
     */
    protected JNotepadPPAction(String key, ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super(key, lp);
        this.app = app;
        this.model = model;

        lp.addLocalizationListener(() -> putValue(SHORT_DESCRIPTION, lp.getString(key + "_short")));
    }
}
