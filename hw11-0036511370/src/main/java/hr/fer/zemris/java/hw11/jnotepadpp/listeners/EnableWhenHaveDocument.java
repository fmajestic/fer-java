package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;

import javax.swing.*;

/**
 * Enables actions when there is an active document.
 */
public class EnableWhenHaveDocument extends ActionEnabler {

    /**
     * Delegated to super constructor.
     *
     * @param actions the actions to manage
     */
    public EnableWhenHaveDocument(Action... actions) {
        super(actions);
    }

    /**
     * If a current document exists, enables all managed actions.
     * <p>
     * Otherwise, disables all managed actions.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
        boolean enabled = currentModel != null;

        for (Action action : actions) {
            action.setEnabled(enabled);
        }
    }
}
