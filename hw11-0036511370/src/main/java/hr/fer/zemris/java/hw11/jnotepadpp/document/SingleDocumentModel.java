package hr.fer.zemris.java.hw11.jnotepadpp.document;

import javax.swing.*;
import java.nio.file.Path;

/** Models a single document. */
public interface SingleDocumentModel {

    /**
     * Gets the text area in which text can be edited.
     * @return the text area
     */
    JTextArea getTextComponent();

    /**
     * Gets the document path.
     * @return the path
     */
    Path getFilePath();

    /**
     * Sets the path
     * @param path the path
     */
    void setFilePath(Path path);

    /**
     * Checks if the document has any unsaved changes.
     * @return true if there are unsaved changes, false otherwise
     */
    boolean isModified();

    /**
     * Sets the modified state for the document.
     * @param modified does the document have unsaved changes
     */
    void setModified(boolean modified);

    /**
     * Registers a listeners.
     * @param l the listeners
     */
    void addSingleDocumentListener(SingleDocumentListener l);

    /**
     * Unregisters a listeners.
     * @param l the listeners.
     */
    void removeSingleDocumentListener(SingleDocumentListener l);
}

