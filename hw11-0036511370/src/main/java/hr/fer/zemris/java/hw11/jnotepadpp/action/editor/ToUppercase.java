package hr.fer.zemris.java.hw11.jnotepadpp.action.editor;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;

/** Converts selected text to uppercase. */
public class ToUppercase extends JNotepadPPAction {

    /**
     * Creates a new uppercase action.
     *
     * @param lp    the localization provider
     * @param model the model to get files from
     * @param app   unused
     */
    public ToUppercase(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("uppercase-text", lp, model, app);
    }

    /**
     * Converts selected text to uppercase.
     *
     * @param e ignored
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent editor = model.getCurrentDocument().getTextComponent();

        char[] toggled = editor.getSelectedText().toCharArray();

        for (int i = 0; i < toggled.length; i++) {
            char c = toggled[i];

            if (Character.isLowerCase(c)) {
                toggled[i] = Character.toUpperCase(c);
            }
        }

        editor.replaceSelection(new String(toggled));

    }
}
