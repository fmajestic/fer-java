package hr.fer.zemris.java.hw11.jnotepadpp.action.editor;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import java.awt.event.ActionEvent;

/** Pastes text into the editor. */
public class PasteText extends JNotepadPPAction {

    /**
     * Creates a new paste text action
     *
     * @param lp    the localization provider
     * @param model the model to get files from
     * @param app   unused
     */
    public PasteText(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app) {
        super("paste-text", lp, model, app);
    }

    /**
     * Pastes text into the current document.
     *
     * @param e ignored
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        model.getCurrentDocument().getTextComponent().paste();
    }
}
