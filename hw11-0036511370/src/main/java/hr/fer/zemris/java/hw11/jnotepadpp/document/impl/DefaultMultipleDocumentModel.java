package hr.fer.zemris.java.hw11.jnotepadpp.document.impl;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.TabUpdater;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {

    private JNotepadPP app;
    private SingleDocumentModel selected;
    private List<SingleDocumentModel> documents;
    private List<MultipleDocumentListener> listeners;
    private long modificationCount;

    /**
     * Creates an empty document model.
     * @param app the app this model is assigned to
     */
    public DefaultMultipleDocumentModel(JNotepadPP app) {
        this.app = app;
        selected = null;
        documents = new ArrayList<>();
        listeners = new CopyOnWriteArrayList<>();

        ImageIcon modified = loadImageFromResources("../../icons/modified.png");
        ImageIcon notModified = loadImageFromResources("../../icons/not-modified.png");
        this.addMultipleDocumentListener(new TabUpdater(this, modified, notModified));

        this.addChangeListener(e -> {
            var nextSelected = getCurrentDocument();
            listeners.forEach(l -> l.currentDocumentChanged(selected, nextSelected));
            selected = nextSelected;
        });
    }

    private ImageIcon loadImageFromResources(String path) {
        InputStream is = this.getClass().getResourceAsStream(path);

        if (is == null) {
            System.out.println("Couldn't find image " + path);
            return null;
        }

        byte[] bytes;

        try {
            bytes = is.readAllBytes();
        } catch (IOException e) {
            System.out.println("IO error while reading " + path);
            return null;
        }

        ImageIcon icon = new ImageIcon(bytes);
        return new ImageIcon(icon.getImage().getScaledInstance(16, 16, Image.SCALE_FAST));
    }

    /** {@inheritDoc} */
    @Override
    public SingleDocumentModel createNewDocument() {
        return internalCreate(null);
    }

    /** {@inheritDoc} */
    @Override
    public SingleDocumentModel getCurrentDocument() {
        int i = getSelectedIndex();
        return i != -1 ? documents.get(i) : null;
    }

    /** {@inheritDoc} */
    @Override
    public SingleDocumentModel loadDocument(Path path) {
        Objects.requireNonNull(path);

        for (int i = 0; i < documents.size(); i++) {
            SingleDocumentModel document = documents.get(i);

            if (document.getFilePath().equals(path)) {
                setSelectedIndex(i);
                return document;
            }
        }

        return internalCreate(path);
    }

    /** {@inheritDoc} */
    @Override
    public void saveDocument(SingleDocumentModel model, Path newPath) {
        Objects.requireNonNull(model);

        Path path = newPath != null ? newPath : model.getFilePath();

        try {
            Files.writeString(path, model.getTextComponent().getText());
        } catch (IOException e) {
            JOptionPane.showMessageDialog(
                app,
                "Error writing file",
                "Error",
                JOptionPane.ERROR_MESSAGE
            );
        }

        model.setFilePath(path);
        model.setModified(false);
    }

    /** {@inheritDoc} */
    @Override
    public void closeDocument(SingleDocumentModel model) {
        Objects.requireNonNull(model);

        int index = documents.indexOf(model);
        removeTabAt(index);
        documents.remove(index);
        modificationCount++;
        listeners.forEach(l -> l.documentRemoved(model));
    }

    /** {@inheritDoc} */
    @Override
    public void addMultipleDocumentListener(MultipleDocumentListener l) {
        listeners.add(l);
    }

    /** {@inheritDoc} */
    @Override
    public void removeMultipleDocumentListener(MultipleDocumentListener l) {
        listeners.remove(l);
    }

    /** {@inheritDoc} */
    @Override
    public int getNumberOfDocuments() {
        return documents.size();
    }

    /** {@inheritDoc} */
    @Override
    public SingleDocumentModel getDocument(int index) {
        Objects.checkIndex(index, documents.size());
        return documents.get(index);
    }

    /**
     * Creates a new single document impl and populates its text area if the path is not null.
     * <p>
     * If an error occurs while reading an existing file, a message is displayed to the user
     * and nothing is added to the text area.
     *
     * @param path the file path, or null if there is no file
     *
     * @return the document impl
     */
    private SingleDocumentModel internalCreate(Path path) {
        JTextArea text = new JTextArea();
        text.setFont(new Font("Consolas", Font.PLAIN, 16));
        JScrollPane scroll = new JScrollPane(text);

        String title = "(unnamed)";
        String tooltip = "(unnamed)";

        if (path != null) {
            title = path.getFileName().toString();
            tooltip = path.toAbsolutePath().toString();

            try {
                text.setText(Files.readString(path));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(
                    null,
                    "Couldn't open file: " + ex.getMessage() + "\n",
                    "Error",
                    JOptionPane.ERROR_MESSAGE
                );
                return null;
            }
        }

        var doc = new DefaultSingleDocumentModel(path, text);

        documents.add(doc);
        addTab(title, null, scroll, tooltip);
        modificationCount++;
        listeners.forEach(l -> l.documentAdded(doc));

        setSelectedIndex(getNumberOfDocuments() - 1);

        return doc;
    }

    /** {@inheritDoc} */
    @Override
    public Iterator<SingleDocumentModel> iterator() {
        return new DocumentIterator(modificationCount);
    }

    /** {@inheritDoc} */
    @Override
    public void forEach(Consumer<? super SingleDocumentModel> action) {
        documents.forEach(action);
    }

    private class DocumentIterator implements Iterator<SingleDocumentModel> {

        int size;
        int currentIndex;
        long savedModificationCount;

        private DocumentIterator(long savedModificationCount) {
            this.size = documents.size();
            this.currentIndex = 0;
            this.savedModificationCount = savedModificationCount;
        }

        @Override
        public boolean hasNext() {
            requireNoModifications();
            return currentIndex < size;
        }

        @Override
        public SingleDocumentModel next() {
            if (!hasNext())
                throw new IllegalStateException("No more elements to get");

            return documents.get(currentIndex++);
        }

        @Override
        public void remove() {
            requireNoModifications();
            if (currentIndex - 1 >= size) {
                throw new IllegalStateException("No element left to remove");
            }

            closeDocument(documents.get(currentIndex - 1));
            savedModificationCount++;
        }

        @Override
        public void forEachRemaining(Consumer<? super SingleDocumentModel> action) {
            documents.stream().skip(currentIndex).forEach(action);
        }

        private void requireNoModifications() {
            if (savedModificationCount != DefaultMultipleDocumentModel.this.modificationCount) {
                throw new ConcurrentModificationException("Document model has been modified");
            }
        }
    }
}
