package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

import java.awt.event.ActionEvent;

/** Changes application language. */
public class ChangeLanguage extends LocalizableAction {

    /** The language to change to. */
    private String langId;

    /**
     * Creates a new language change action.
     *
     * @param baseKey the base i18n key
     * @param lp      the localization provider
     * @param langId  the language to change to
     */
    public ChangeLanguage(String baseKey, ILocalizationProvider lp, String langId) {
        super(baseKey, lp);
        this.langId = langId;
    }

    /**
     * Sets the localization provider language to {@link #langId}.
     * @param ignored ignored
     */
    @Override
    public void actionPerformed(ActionEvent ignored) {
        LocalizationProvider.getInstance().setLanguage(langId);
    }
}
