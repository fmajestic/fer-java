package hr.fer.zemris.java.hw11.jnotepadpp.local;

import javax.swing.*;

/** An abstract action that responds to localization changes. */
public abstract class LocalizableAction extends AbstractAction {

    /** The base name key. */
    private String key;

    /** The provider. */
    protected ILocalizationProvider provider;

    /**
     * Creates a new action.
     * <p>
     * Immediately updates the name for the current lanugage, and registers a listener that does that on the provider.
     *
     * @param baseKey the base i18n key
     * @param lp      the provider
     */
    protected LocalizableAction(String baseKey, ILocalizationProvider lp) {
        this.key = baseKey;
        this.provider = lp;

        putValue(NAME, provider.getString(key));
        provider.addLocalizationListener(() -> putValue(NAME, provider.getString(key)));
    }
}
