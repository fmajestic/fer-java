package hr.fer.zemris.java.hw11.jnotepadpp.local;

/** Provides a bridge between the main provider and other parts of the system. */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {

    /** The cached language. */
    private String cachedLanguage;

    /** Are we connected to the parent. */
    private boolean connected;

    /** The parent provider. */
    private ILocalizationProvider parent;

    /** Caches language and notifies all out observers when parent changes. */
    private ILocalizationListener listener = () -> {
        cachedLanguage = parent.getCurrentLanguage();
        LocalizationProviderBridge.this.fire();
    };


    /**
     * Creates a new bridge and immediately connects to the parent.
     *
     * @param parent the parent
     */
    public LocalizationProviderBridge(ILocalizationProvider parent) {
        this.parent = parent;
        this.cachedLanguage = parent.getCurrentLanguage();
        this.connect();
    }

    /**
     * Connects to the parent.
     * <p>
     * Attaches the listener to the parent.
     * <p>
     * If the language has changed since the last time we were connected, updates the cached language
     * and fires a change.
     * <p>
     * If the bridge is already connected, does nothing.
     */
    public void connect() {

        if (connected) {
            return;
        }

        connected = true;
        parent.addLocalizationListener(listener);

        if (!cachedLanguage.equals(parent.getCurrentLanguage())) {
            cachedLanguage = parent.getCurrentLanguage();
            fire();
        }
    }

    /**
     * Disconnects from the parent.
     * <p>
     * Detaches the listener from the parent.
     * <p>
     * If the bridge was already disconnected, does nothing.
     */
    public void disconnect() {
        if (!connected) {
            return;
        }

        connected = false;
        parent.removeLocalizationListener(listener);
    }

    /** {@inheritDoc} */
    @Override
    public String getCurrentLanguage() {
        return parent.getCurrentLanguage();
    }

    /**
     * Gets the string associated with the key from the parent.
     *
     * @param key the key
     *
     * @return the string
     *
     * @throws IllegalStateException if the bridge is not connected
     */
    @Override
    public String getString(String key) {
        if (!connected) {
            throw new IllegalStateException("Bridge disconnected, can't get string.");
        }

        return parent.getString(key);
    }
}
