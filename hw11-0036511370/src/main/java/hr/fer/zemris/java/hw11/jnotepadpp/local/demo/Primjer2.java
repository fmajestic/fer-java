package hr.fer.zemris.java.hw11.jnotepadpp.local.demo;

import javax.swing.*;
import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class Primjer2 extends JFrame {
    private static final long serialVersionUID = 1L;
    private String language;

    public Primjer2(String language) throws HeadlessException {
        this.language = language;
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(0, 0);
        setTitle("Demo");
        initGUI();
        pack();
    }

    private void initGUI() {
        getContentPane().setLayout(new BorderLayout());
        Locale locale = Locale.forLanguageTag(language);
        ResourceBundle bundle =
            ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.local.prijevodi",
                locale);
        JButton gumb = new JButton(
            bundle.getString("login")
        );

        getContentPane().add(gumb,BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Očekivao sam oznaku jezika kao argument!");
            System.err.println("Zadajte kao parametar hr ili en.");
            System.exit(-1);
        }
        final String jezik = args[0];
        SwingUtilities.invokeLater(() -> new Primjer2(jezik).setVisible(true));
    }
}