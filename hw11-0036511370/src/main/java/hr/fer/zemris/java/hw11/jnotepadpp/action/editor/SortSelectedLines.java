package hr.fer.zemris.java.hw11.jnotepadpp.action.editor;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.action.JNotepadPPAction;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

/** Sorts selected lines in an ascending or descending order. */
public class SortSelectedLines extends JNotepadPPAction {

    /** Ascending or descending order. */
    private final boolean ascending;

    /** The string comparator for sorting. */
    private Comparator<Object> cmp;

    /**
     * Creates a new sort action
     *
     * @param lp        the localization provider
     * @param model     the model to get files from
     * @param app       unused
     * @param ascending should the sort be ascending or not
     */
    public SortSelectedLines(ILocalizationProvider lp, MultipleDocumentModel model, JNotepadPP app, boolean ascending) {

        super(ascending ? "sort-ascending" : "sort-descending", lp, model, app);
        this.ascending = ascending;

        updateComparator();

        lp.addLocalizationListener(this::updateComparator);
    }

    /**
     * Updates the comparator.
     * <p>
     * The comparator is set to a collator for the current language.
     *
     * @see Collator
     */
    private void updateComparator() {
        Locale loc = new Locale(provider.getCurrentLanguage());

        if (ascending) {
            cmp = Collator.getInstance(loc);
        } else {
            cmp = Collator.getInstance(loc).reversed();
        }
    }

    /**
     * Sorts selected lines of the current document.
     *
     * @param e ignored
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent editor = model.getCurrentDocument().getTextComponent();
        Document document = editor.getDocument();
        Element root = document.getDefaultRootElement();

        Element beginRow = root.getElement(root.getElementIndex(editor.getSelectionStart()));
        Element endRow = root.getElement(root.getElementIndex(editor.getSelectionEnd()));

        int beginTake = beginRow.getStartOffset();
        int endTake = endRow.getEndOffset();

        try {
            int len = endTake - beginTake;
            String[] sorted = document.getText(beginTake, len).split("\n");

            Arrays.sort(sorted, cmp);

            String toInsert = String.join("\n", sorted);

            document.remove(beginTake, len - 1);
            document.insertString(beginTake, toInsert, null);
        } catch (BadLocationException ex) {
            System.out.println(ex.toString());
        }
    }
}
