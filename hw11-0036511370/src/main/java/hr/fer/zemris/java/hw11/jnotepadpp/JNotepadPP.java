package hr.fer.zemris.java.hw11.jnotepadpp;

import hr.fer.zemris.java.hw11.jnotepadpp.action.app.*;
import hr.fer.zemris.java.hw11.jnotepadpp.action.editor.*;
import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.document.impl.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.*;
import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LJLabel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

/** A multi-document text editor */
public class JNotepadPP extends JFrame {

    /** The document model. */
    private final MultipleDocumentModel model = new DefaultMultipleDocumentModel(this);

    /** The localization provider. */
    private FormLocalizationProvider flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
    /** The new file action. */
    private final Action newFile = new NewFile(flp, model, this);
    /** The open file action. */
    private final Action openFile = new OpenFile(flp, model, this);
    /** The save file action. */
    private final Action saveFile = new SaveFile(flp, model, this);
    /** The save file as action. */
    private final Action saveFileAs = new SaveFileAs(flp, model, this);
    /** The close file action. */
    private final Action closeFile = new CloseFile(flp, model, this);
    /** The cut action. */
    private final Action cutText = new CutText(flp, model, this);
    /** The copy action. */
    private final Action copyText = new CopyText(flp, model, this);
    /** The past action. */
    private final Action pasteText = new PasteText(flp, model, this);
    /** The file stats action. */
    private final Action fileStats = new FileStats(flp, model, this);
    /** The exit action. */
    private final Action exitApp = new ExitApplication(flp, model, this);
    /** The lowercase action. */
    private final Action toLowercase = new ToLowercase(flp, model, this);
    /** The uppercase action. */
    private final Action toUppercase = new ToUppercase(flp, model, this);
    /** The toggle case action. */
    private final Action toggleCase = new ToggleCase(flp, model, this);
    /** The ascending sort action. */
    private final Action sortAscending =
        new SortSelectedLines(flp, model, this, true);
    /** The descending action. */
    private final Action sortDescending =
        new SortSelectedLines(flp, model, this, false);
    /** The unique lines action. */
    private final Action uniqueLines = new RemoveSelectionDuplicates(flp, model, this);
    /** The 'yes' dialog option. */
    private String dialogYes = flp.getString("dialog-yes");
    /** The 'no' dialog option. */
    private String dialogNo = flp.getString("dialog-no");
    /** The yes-no dialog options. */
    private final String[] yesNoOptions = new String[] { dialogYes, dialogNo };
    /** The 'cancel' dialog option. */
    private String cancel = flp.getString("dialog-cancel");
    /** The 'save' dialog option. */
    private String saveConfirm = flp.getString("save-confirm");
    /** The 'don't save' dialog option. */
    private String saveDecline = flp.getString("save-decline");
    /** The 'save changes' dialog options. */
    private final String[] saveOptions = new String[] { saveConfirm, saveDecline, cancel };
    /** 'Existing file' prompt title. */
    private String fileExistsTitle = flp.getString("file-exists_title");
    /** 'Existing file' prompt message. */
    private String fileExistsMessage = flp.getString("file-exists_message");
    /** 'Unsaved changes' prompt title. */
    private String unsavedChangesTitle = flp.getString("unsaved-changes_title");
    /** 'Unsaved changes' prompt message. */
    private String unsavedChangesMessage = flp.getString("unsaved-changes_message");


    /** Initializes app. */
    private JNotepadPP() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(800, 600);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                if (confirmExit()) {
                    dispose();
                }
            }
        });

        flp.addLocalizationListener(() -> {
            yesNoOptions[0] = dialogYes = flp.getString("dialog-yes");
            yesNoOptions[1] = dialogNo = flp.getString("dialog-no");

            saveOptions[0] = saveConfirm = flp.getString("save-confirm");
            saveOptions[1] = saveDecline = flp.getString("save-decline");
            saveOptions[2] = cancel = flp.getString("dialog-cancel");

            fileExistsTitle = flp.getString("file-exists_title");
            fileExistsMessage = flp.getString("file-exists_message");
            unsavedChangesTitle = flp.getString("unsaved-changes_title");
            unsavedChangesMessage = flp.getString("unsaved-changes_message");
        });

        initGUI();
    }

    /**
     * Prompts user to save any unsaved changes.
     * <p>
     * The user can cancel the exit by canceling a save operation or an overwrite operation.
     *
     * @return true if the user cancelled the exit, false otherwise
     */
    private boolean confirmExit() {

        for (var doc : model) {
            if (!saveIfModified(doc)) {
                return false;
            }
        }

        return true;
    }

    /**
     * If the document is modified, prompt the user to save it and return the success.
     *
     * @param doc the document to save
     *
     * @return true if the document was unmodified or saved successfully, false otherwise
     */
    public boolean saveIfModified(SingleDocumentModel doc) {
        if (doc.isModified()) {
            int result = promptSaveChanges(doc);

            if (result == JOptionPane.CANCEL_OPTION || result == -1) {
                return false;
            }

            if (result == JOptionPane.YES_OPTION) {
                return trySaveFile(doc);
            }
        }

        return true;
    }

    /**
     * Tries to save a file.
     * <p>
     * If the file has no path, the user is prompted for a new path.
     *
     * @param doc the file to save
     *
     * @return true if the file was saved successfully, false otherwise
     */
    public boolean trySaveFile(SingleDocumentModel doc) {
        if (doc.getFilePath() == null) {
            Path newPath = promptNewPath();

            if (newPath == null) {
                return false;
            }

            model.saveDocument(doc, newPath);
        } else {
            model.saveDocument(doc, null);
        }

        return true;
    }

    /**
     * Displays a dialog box prompting the user to save changes, discard them, or cancel the close operation.
     *
     * @param doc the document to save
     *
     * @return the dialog result
     */
    private int promptSaveChanges(SingleDocumentModel doc) {
        Path path = doc.getFilePath();
        String pathStr = path != null ? path.toString() : "(unnamed)";

        return JOptionPane.showOptionDialog(
            this,
            String.format(unsavedChangesMessage, pathStr),
            unsavedChangesTitle,
            JOptionPane.YES_NO_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            saveOptions,
            saveConfirm
        );
    }

    /**
     * Prompts the user for a new path via {@link JFileChooser}.
     *
     * @return the new path, or null if no path was chosen
     */
    private Path promptNewPath() {

        JFileChooser jfc = new JFileChooser(Paths.get(".").toAbsolutePath().normalize().toString());

        if (jfc.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
            return null;
        }

        Path path = jfc.getSelectedFile().toPath();

        if (Files.exists(path)) {
            int result = JOptionPane.showOptionDialog(
                this,
                fileExistsMessage,
                fileExistsTitle,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                yesNoOptions,
                dialogNo
            );

            if (result != JOptionPane.YES_OPTION) {
                return null;
            }
        }

        return path;
    }

    /** Initializes the GUI. */
    private void initGUI() {

        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());

        configureModel();

        configureActions();
        createMenus();
        createToolbar();
        createStatusBar();
    }

    /** Configures the document model. */
    private void configureModel() {

        getContentPane().add((DefaultMultipleDocumentModel) model, BorderLayout.CENTER);

        model.addMultipleDocumentListener(new ModifiedStatusUpdater());
        model.addMultipleDocumentListener(new WindowTitleUpdater(this));

        model.addMultipleDocumentListener(new EnableWhenHaveModification(saveFile));

        model.addMultipleDocumentListener(
            new EnableWhenHaveSelection(
                cutText, copyText, toLowercase, toUppercase, toggleCase, sortAscending, sortDescending, uniqueLines));

        model.addMultipleDocumentListener(
            new EnableWhenHaveDocument(saveFile, saveFileAs, closeFile, pasteText, fileStats));
    }

    /** Configures the actions. */
    private void configureActions() {

        newFile.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl N"));
        newFile.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);

        openFile.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl O"));
        openFile.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);

        saveFile.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
        saveFile.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);

        saveFileAs.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl alt S"));
        saveFileAs.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);

        closeFile.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl W"));
        closeFile.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);

        cutText.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl X"));
        cutText.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);

        copyText.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl C"));
        copyText.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);

        pasteText.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl V"));
        pasteText.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);

        fileStats.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl I"));
        fileStats.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);

        exitApp.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl Q"));
        exitApp.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
    }

    /** Creates the menu bar. */
    private void createMenus() {
        JMenuBar mb = new JMenuBar();

        JMenu file = new JMenu(new DefaultLocalizedAction("file-menu", flp));

        file.add(newFile);
        file.add(openFile);
        file.addSeparator();
        file.add(saveFile);
        file.add(saveFileAs);
        file.addSeparator();
        file.add(closeFile);
        file.addSeparator();
        file.add(exitApp);

        JMenu edit = new JMenu(new DefaultLocalizedAction("edit-menu", flp));
        edit.add(cutText);
        edit.add(copyText);
        edit.add(pasteText);

        JMenu info = new JMenu(new DefaultLocalizedAction("info-menu", flp));
        info.add(fileStats);

        JMenu tools = new JMenu(new DefaultLocalizedAction("tools-menu", flp));

        JMenu changeCase = new JMenu(new DefaultLocalizedAction("case-menu", flp));
        changeCase.add(toLowercase);
        changeCase.add(toUppercase);
        changeCase.add(toggleCase);

        JMenu sort = new JMenu(new DefaultLocalizedAction("sort-menu", flp));
        sort.add(sortAscending);
        sort.add(sortDescending);

        tools.add(changeCase);
        tools.add(sort);
        tools.add(uniqueLines);

        JMenu lang = new JMenu(new DefaultLocalizedAction("lang-menu", flp));
        lang.add(new ChangeLanguage("hr-name", flp, "hr"));
        lang.add(new ChangeLanguage("en-name", flp, "en"));
        lang.add(new ChangeLanguage("de-name", flp, "de"));

        mb.add(file);
        mb.add(edit);
        mb.add(info);
        mb.add(tools);
        mb.add(lang);

        setJMenuBar(mb);
    }

    /** Creates the toolbar. */
    private void createToolbar() {
        JToolBar tb = new JToolBar();

        tb.add(newFile);
        tb.add(openFile);
        tb.addSeparator();
        tb.add(saveFile);
        tb.add(saveFileAs);
        tb.add(closeFile);
        tb.addSeparator();
        tb.add(cutText);
        tb.add(copyText);
        tb.add(pasteText);
        tb.addSeparator();
        tb.add(fileStats);
        tb.addSeparator();
        tb.add(exitApp);

        getContentPane().add(tb, BorderLayout.PAGE_START);
    }

    /** Creates the status bar. */
    private void createStatusBar() {
        JPanel sb = new JPanel(new GridLayout(1, 0));
        sb.setPreferredSize(new Dimension(getWidth(), 25));
        sb.setBorder(new BevelBorder(BevelBorder.LOWERED));

        JPanel left = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 4));
        left.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.GRAY));

        JLabel lengthNum = new JLabel();
        left.add(new LJLabel("status-length", flp));
        left.add(lengthNum);


        JPanel middle = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 4));
        middle.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.GRAY));

        JLabel lineNum = new JLabel();
        JLabel colNum = new JLabel();
        JLabel selNum = new JLabel();

        middle.add(new LJLabel("status-row", flp));
        middle.add(lineNum);
        middle.add(new LJLabel("status-col", flp));
        middle.add(colNum);
        middle.add(new LJLabel("status-sel", flp));
        middle.add(selNum);

        JPanel right = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 4));
        JLabel time = new JLabel("", SwingConstants.RIGHT);
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY/MM/dd HH:mm:ss");
        new Timer(1000, e -> SwingUtilities.invokeLater(() -> time.setText(sdf.format(e.getWhen())))).start();
        right.add(time);

        sb.add(left);
        sb.add(middle);
        sb.add(right);

        model.addMultipleDocumentListener(new StatusBarUpdater(lengthNum, lineNum, colNum, selNum));

        getContentPane().add(sb, BorderLayout.PAGE_END);
    }

    /**
     * Entry point of the program.
     *
     * @param args no args
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JNotepadPP app = new JNotepadPP();
            app.setTitle("JNotepad++");
            app.setVisible(true);
        });
    }
}
