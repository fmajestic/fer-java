package hr.fer.zemris.java.hw11.jnotepadpp.action.app;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

import java.awt.event.ActionEvent;

/** Wrapper class for localized actions that do not override their default action. */
public class DefaultLocalizedAction extends LocalizableAction {

    /**
     * Creates a new default localized action.
     *
     * @param baseKey the base i18n key
     * @param lp      the localization provider
     */
    public DefaultLocalizedAction(String baseKey, ILocalizationProvider lp) {
        super(baseKey, lp);
    }

    /**
     * Does nothing.
     *
     * @param ignored ignored
     */
    @Override
    public void actionPerformed(ActionEvent ignored) {

    }
}
