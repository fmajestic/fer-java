package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.document.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.document.SingleDocumentModel;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Updates the document modified status on type.
 */
public class ModifiedStatusUpdater implements DocumentListener, MultipleDocumentListener {

    /** The model to update. */
    private SingleDocumentModel model;

    /**
     * Sets modified status to true.
     *
     * @param e unused
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        setModified();
    }

    /**
     * Sets modified status to true.
     *
     * @param e unused
     */
    @Override
    public void removeUpdate(DocumentEvent e) {
        setModified();
    }

    /**
     * Sets modified status to true.
     *
     * @param e unused
     */
    @Override
    public void changedUpdate(DocumentEvent e) {
        setModified();
    }

    /**
     * Sets modified status to true, if the model exists.
     */
    private void setModified() {
        if (model != null) {
            model.setModified(true);
        }
    }

    /**
     * Stores the current model, and if the previous/current model exist, removes/adds itself to ther document's
     * listeners.
     *
     * @param previousModel the old document
     * @param currentModel  the newly focused document
     */
    @Override
    public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {

        if (previousModel != null) {
            previousModel.getTextComponent().getDocument().removeDocumentListener(this);
        }

        model = currentModel;

        if (model != null) {
            model.getTextComponent().getDocument().addDocumentListener(this);
        }
    }

    /**
     * Does nothing.
     *
     * @param model unused
     */
    @Override
    public void documentAdded(SingleDocumentModel model) {

    }

    /**
     * Removes reference to document so the garbage collector can release it.
     *
     * @param model the removed document.
     */
    @Override
    public void documentRemoved(SingleDocumentModel model) {
        if (model == this.model) {
            this.model = null;
        }
    }
}
