package hr.fer.zemris.java.custom.scripting.exec;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

class ObjectMultistackTest {

    private ValueWrapper V1, V2;
    private ObjectMultistack multistack;

    @BeforeEach
    void setUp() {
        multistack = new ObjectMultistack();
        V1 = new ValueWrapper(null);
        V2 = new ValueWrapper(-4);
    }

    @Test
    void push() {
        multistack.push("hello", V1);
        multistack.push("cloud", V2);

        assertFalse(multistack.isEmpty("hello"));
        assertFalse(multistack.isEmpty("cloud"));
        assertTrue(multistack.isEmpty("HELLO"));
    }

    @Test
    void pop() {
        multistack.push("cloud", V1);
        multistack.push("cloud", V2);


        assertEquals(-4, multistack.pop("cloud").getValue());
        assertNull(multistack.pop("cloud").getValue());

        assertTrue(multistack.isEmpty("cloud"));
    }

    @Test
    void peek() {
        multistack.push("hello", V2);
        assertEquals(-4, multistack.peek("hello").getValue());
        assertFalse(multistack.isEmpty("hello"));
    }

    @Test
    void isEmpty() {
        assertTrue(multistack.isEmpty("empty"));

        multistack.push("yes", V1);
        multistack.push("yes", V2);

        assertFalse(multistack.isEmpty("yes"));

        multistack.pop("yes");
        multistack.pop("yes");

        assertTrue(multistack.isEmpty("yes"));
    }

    @Test
    void trowing() {
        assertThrows(EmptyStackException.class, () -> multistack.pop("hello"));
        assertThrows(EmptyStackException.class, () -> multistack.peek("world"));

        assertThrows(NullPointerException.class, () -> multistack.push("no", null));
    }
}