package hr.fer.zemris.java.custom.scripting.exec;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValueWrapperTest {

    private ValueWrapper V1, V3, V2;

    @BeforeEach
    void setUp() {
        V1 = new ValueWrapper(3.14);
        V2 = new ValueWrapper(55);
        V3 = new ValueWrapper("hello");
    }

    @Test
    void getValue() {
        assertNull(new ValueWrapper(null).getValue(), "Getter should not convert null.");
        assertEquals("5.0", new ValueWrapper("5.0").getValue(), "Getter should not convert string.");
        assertEquals(3, new ValueWrapper(3).getValue(), "Getter should not change value type.");
    }

    @Test
    void setValue() {
        V1.setValue(null);
        V2.setValue(6.28);
        V3.setValue("world");

        assertNull(V1.getValue(), "Setter should not convert null.");
        assertEquals(6.28, V2.getValue(), "Setter didn't update value.");
        assertEquals("world", V3.getValue(), "Setter didn't update value.");
    }

    @Test
    void throwing() {
        assertThrows(RuntimeException.class, () -> new ValueWrapper(Boolean.TRUE).add(5)); // Incompatible types
        assertThrows(RuntimeException.class, () -> V1.subtract("hello")); // Compatible types, not a number
        assertThrows(RuntimeException.class, () -> V2.divide("3e4gt")); // Compatible types, not a number
        assertThrows(RuntimeException.class, () -> V3.multiply(null)); // Compatible types, not a number
    }

    @Test
    void add() {
        V1.add(-3.14);
        assertEquals(0.0, V1.getValue());
        V1.add("3e3");
        assertEquals(3e3, V1.getValue());

        V2.add(null);
        assertEquals(55, V2.getValue());
        V2.add("5");
        assertEquals(60, V2.getValue());
    }

    @Test
    void subtract() {
        V1.subtract(null);
        assertEquals(3.14, V1.getValue());
        V1.subtract("3.0");
        assertEquals(0.14, (Double) V1.getValue(), 1e-8);


        V2.subtract("25");
        assertEquals(30, V2.getValue());
        V2.subtract(3e1);
        assertEquals(0.0, V2.getValue());
    }

    @Test
    void multiply() {
        V1.multiply("1");
        assertEquals(3.14, V1.getValue());
        V1.multiply(null);
        assertEquals(0.0, V1.getValue());


        V2.multiply(1e-1);
        assertEquals(5.5, V2.getValue());
    }

    @Test
    void divide() {
        V1.divide(3.14);
        assertEquals(1.0, V1.getValue());
        V1.divide("2e-1");
        assertEquals(5.0, V1.getValue());


        V2.divide("11");
        assertEquals(5, V2.getValue());
        V2.divide(1e-2);
        assertEquals(500.0, V2.getValue());
    }

    @Test
    void numCompare() {
        assertEquals(0, V1.numCompare(3.14));
        assertEquals(0, V2.numCompare(55));

        assertTrue(V1.numCompare(3.0) > 0);
        assertTrue(V2.numCompare(20) > 0);

        assertTrue(V1.numCompare("16") < 0);
        assertTrue(V2.numCompare(3e3) < 0);
    }
}