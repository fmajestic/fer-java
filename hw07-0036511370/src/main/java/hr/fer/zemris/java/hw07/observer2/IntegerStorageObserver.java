package hr.fer.zemris.java.hw07.observer2;

/** Models an observer for the {@link IntegerStorage} subject. */
public interface IntegerStorageObserver {

    /**
     * The method called when a subject's value changes.
     *
     * @param istorage the storage whose value has changed
     */
    void valueChanged(IntegerStorageChange iStoreChange);
}
