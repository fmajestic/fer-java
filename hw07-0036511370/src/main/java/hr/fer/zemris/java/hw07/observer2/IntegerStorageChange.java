package hr.fer.zemris.java.hw07.observer2;

import java.util.Objects;

/** Models a change in an {@link IntegerStorage}, containing detailed information about the change. */
public class IntegerStorageChange {

    /** The source of the change, the object whose stored value changed. */
    private IntegerStorage source;

    /** The previously stored value. */
    private int oldValue;

    /** The new value. */
    private int newValue;

    /**
     * Creates a new storage change with the given details.
     *
     * @param source   the source of the change
     * @param oldValue the previously stored value
     * @param newValue the new value
     */
    public IntegerStorageChange(IntegerStorage source, int oldValue, int newValue) {
        this.source = Objects.requireNonNull(source, "Change source can't be null.");
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    /**
     * Gets the source of the change.
     *
     * @return the object whose value changed
     */
    public IntegerStorage getSource() {
        return source;
    }

    /**
     * Gets the old value, the one stored before the change occurred.
     *
     * @return the old value
     */
    public int getOldValue() {
        return oldValue;
    }

    /**
     * Gets the new value in the storage.
     *
     * @return the new value
     */
    public int getNewValue() {
        return newValue;
    }
}
