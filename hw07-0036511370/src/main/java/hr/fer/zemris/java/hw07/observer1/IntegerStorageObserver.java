package hr.fer.zemris.java.hw07.observer1;

/** Models an observer for the {@link IntegerStorage} subject. */
public interface IntegerStorageObserver {

    /**
     * The method called when a subject's value changes.
     *
     * @param istorage the storage whose value has changed
     */
    void valueChanged(IntegerStorage istorage);
}
