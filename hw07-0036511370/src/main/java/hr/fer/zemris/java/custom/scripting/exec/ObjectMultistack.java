package hr.fer.zemris.java.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Multi-stack: a collection that can hold multiple stacks.
 * <p>
 * Each stack is referenced by a name (a string).
 *
 * Internally, instead of an using an actual stack collection, the stack is implemented as a singly-linked list.
 */
public class ObjectMultistack {

    /** The map of stack names to actual stacks. */
    private Map<String, MultistackEntry> map = new HashMap<>();

    /**
     * Pushes a new value onto a named stack.
     *
     * @param keyName      the name of the stack
     * @param valueWrapper the value to push
     *
     * @throws NullPointerException if any of the arguments are null
     */
    public void push(String keyName, ValueWrapper valueWrapper) {
        Objects.requireNonNull(keyName, "Stack name can't be null.");
        Objects.requireNonNull(valueWrapper, "Value wrapper can't be null");

        // One-liner:
        //      map.put(keyName, new MultistackEntry(valueWrapper, map.get(keyName)));

        MultistackEntry entry = new MultistackEntry(valueWrapper);

        entry.next = map.get(keyName);

        map.put(keyName, entry);
    }

    /**
     * Retrieves and removes the top value of a stack.
     * <p>
     * If the stack is empty, an exception is thrown.
     *
     * @param keyName the name of the stack
     *
     * @return the popped element
     *
     * @throws EmptyStackException  if the method is called on an empty stack
     * @throws NullPointerException if the key name is null
     */
    public ValueWrapper pop(String keyName) throws EmptyStackException {

        Objects.requireNonNull(keyName, "Stack name can't be null.");
        requireNonEmptyStack(keyName);

        MultistackEntry entry = map.get(keyName);

        map.put(keyName, entry.next);

        return entry.value;
    }

    /**
     * Retrieves, but does not remove, the top value of a stack.
     *
     * @param keyName the name of the stack to peek
     *
     * @return the peeked value
     *
     * @throws EmptyStackException  if the method is called on an empty stack
     * @throws NullPointerException if the stack name is null
     */
    public ValueWrapper peek(String keyName) throws EmptyStackException {
        Objects.requireNonNull(keyName, "Stack name can't be null.");
        requireNonEmptyStack(keyName);

        return map.get(keyName).value;
    }


    /**
     * Checks if the stack associated with the given name is empty.
     *
     * @param keyName the key to check
     *
     * @return true if the key's stack contains no elements, false otherwise
     */
    public boolean isEmpty(String keyName) {
        return map.get(keyName) == null;
    }

    /**
     * Checks if the given key's stack is empty and throws a {@link EmptyStackException} if it is.
     *
     * @param keyName the stack name to check
     */
    private void requireNonEmptyStack(String keyName) throws EmptyStackException {
        if (isEmpty(keyName)) {
            throw new EmptyStackException();
        }
    }


    /**
     * A single entry in a stack.
     */
    private static class MultistackEntry {

    /** The value of the entry. */
    private ValueWrapper value;

    /** The next element of the stack. */
    private MultistackEntry next;

        /**
         * Creates a new entry with a given non-null value.
         *
         * @param value the value of the entry, not null
         *
         * @throws NullPointerException if value is null
         */
        MultistackEntry(ValueWrapper value) {
            this.value = Objects.requireNonNull(value);
        }

        /**
         * Creates a new entry with a given non-null value, and the entry that comes after it.
         *
         * @param value the value of the entry, not null
         * @param next  the next entry, can be null
         *
         * @throws NullPointerException if value is null
         */
        MultistackEntry(ValueWrapper value, MultistackEntry next) {
            this(value);
            this.next = next;
        }

        /**
         * Gets the value stored in this entry.
         *
         * @return the value of the entry
         */
        public ValueWrapper getValue() {
            return value;
        }

        /**
         * Gets the entry that comes after this one.
         *
         * @return the next entry, or null if this entry is the last one.
         */
        public MultistackEntry getNext() {
            return next;
        }
    }
}
