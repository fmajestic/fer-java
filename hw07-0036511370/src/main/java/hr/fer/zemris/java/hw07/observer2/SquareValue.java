package hr.fer.zemris.java.hw07.observer2;

/** An observer that prints the square of every new value in an {@link IntegerStorage}. */
public class SquareValue implements IntegerStorageObserver {

    /**
     * Prints the square of the newly stored number.
     *
     * @param iStoreChange the storage change details
     */
    @Override
    public void valueChanged(IntegerStorageChange iStoreChange) {
        System.out.printf(
            "Provided new value: %d, square is %d%n",
            iStoreChange.getNewValue(),
            iStoreChange.getNewValue() * iStoreChange.getNewValue()
        );
    }
}
