package hr.fer.zemris.java.hw07.observer1;

/** An observer that prints the square of every new value in an {@link IntegerStorage}. */
public class SquareValue implements IntegerStorageObserver {

    /**
     * Prints the square of the newly stored number.
     *
     * @param istorage  the storage that changed
     */
    @Override
    public void valueChanged(IntegerStorage istorage) {
        System.out.printf(
            "Provided new value: %d, square is %d%n",
            istorage.getValue(),
            istorage.getValue() * istorage.getValue()
        );
    }
}
