package hr.fer.zemris.java.hw07.observer2;

/** An observer that prints the double of every new value in an {@link IntegerStorage}, up to n times. */
public class DoubleValue implements IntegerStorageObserver {

    /** The maximum number of times this observer will print the value. */
    private int n;

    /**
     * Creates a new observer with the specified number of responses.
     * <p>
     * After responding to an event n times, the observer removes itself from the subject.
     *
     * @param n the number of times to respond to an event
     */
    public DoubleValue(int n) {
        this.n = n;
    }

    /**
     * Prints the double of the new value, or removes itself from the object if the value has been printed n times.
     *
     * @param iStorageChange the storage change details
     */
    @Override
    public void valueChanged(IntegerStorageChange iStorageChange) {
        if (n-- > 0) {
            System.out.printf("Double value: %d%n", 2 * iStorageChange.getNewValue());
        } else {
            iStorageChange.getSource().removeObserver(this);
        }
    }
}
