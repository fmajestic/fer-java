package hr.fer.zemris.java.custom.scripting.exec;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.DoubleBinaryOperator;

/**
 * Holds a single value of any type, wrapped so it can be pushed onto an {@link ObjectMultistack}.
 * <p>
 * Additionally, arithmetic operations and comparison can be performed if and only if both the
 * current value and the other operand are of type {@code Integer}, {@code Double}, or {@code String}.
 * Note that the values do not have to be the same type, just one of the listed ones.
 * <p>
 * Source lines of code with no documentation: 90
 */
public class ValueWrapper {

    /** Maps all supported operations to lambdas that can be applied. */
    private static Map<Operation, DoubleBinaryOperator> operators = new HashMap<>();

    static {
        //noinspection Convert2MethodRef, using a+b instead of Double::sum for consistency
        operators.put(Operation.ADD, (a, b) -> a + b);
        operators.put(Operation.SUB, (a, b) -> a - b);
        operators.put(Operation.MUL, (a, b) -> a * b);
        operators.put(Operation.DIV, (a, b) -> a / b);
    }

    /** The currently stored value. */
    private Object value;

    /**
     * Creates a new wrapper around {@code value}.
     *
     * @param value the value to initially store
     */
    public ValueWrapper(Object value) { this.value = value; }

    /**
     * Gets the currently stored value.
     *
     * @return the currently stored value
     */
    public Object getValue() { return value; }

    /**
     * Sets a new value to be stored.
     *
     * @param value the new value to be stored
     */
    public void setValue(Object value) { this.value = value; }

    /**
     * Adds the value to the currently stored one, as if both were numbers.
     * <p>
     * The object to be added must be of type {@code Integer}, {@code Double}, or {@code String}.
     *
     * @param incValue the value to add
     *
     * @throws RuntimeException if {@code incValue} is not an {@code Integer}, {@code Double}, or {@code String}
     */
    public void add(Object incValue) { value = performOperation(value, incValue, Operation.ADD); }

    /**
     * Subtracts the value from the currently stored one, as if both were numbers.
     * <p>
     * The object to be subtracted must be of type {@code Integer}, {@code Double}, or {@code String}.
     *
     * @param decValue the value to subtract
     *
     * @throws RuntimeException if {@code decValue} is not an {@code Integer}, {@code Double}, or {@code String}
     */
    public void subtract(Object decValue) { value = performOperation(value, decValue, Operation.SUB); }

    /**
     * Multiplies the current value by the given value, as if both were numbers.
     * <p>
     * The object for multiplication must be of type {@code Integer}, {@code Double}, or {@code String}.
     *
     * @param mulValue the value to multiply by
     *
     * @throws RuntimeException if {@code mulValue} is not an {@code Integer}, {@code Double}, or {@code String}
     */
    public void multiply(Object mulValue) { value = performOperation(value, mulValue, Operation.MUL); }

    /**
     * Divides the currently stored value by the given value, as if both were numbers.
     * <p>
     * The object to be added must be of type {@code Integer}, {@code Double}, or {@code String}.
     *
     * @param divValue the value to divide by
     *
     * @throws RuntimeException if {@code divValue} is not an {@code Integer}, {@code Double}, or {@code String}
     */
    public void divide(Object divValue) { value = performOperation(value, divValue, Operation.DIV); }

    /**
     * Compares the given value by the currently stored value, as if both were numbers.
     * <p>
     * The object to comapre with must be of type {@code Integer}, {@code Double}, or {@code String}.
     *
     * @param withValue the value to add
     *
     * @throws RuntimeException if {@code incValue} is not an {@code Integer}, {@code Double}, or {@code String}
     */
    public int numCompare(Object withValue) {
        Number lhs = makeNumber(value);
        Number rhs = makeNumber(withValue);

        return Objects.compare(
            lhs,
            rhs,
            (v1, v2) -> v1 instanceof Double || v2 instanceof Double
                        ? Double.compare(v1.doubleValue(), v2.doubleValue())
                        : Integer.compare(v1.intValue(), v2.intValue())
        );
    }

    /**
     * Performs an arithmetic operation on two objects.
     * <p>
     * The objects must be of type {@code Integer}, {@code Double}, or {@code String}.
     * If not, an exception is thrown.
     *
     * @param first  the first operand
     * @param second the second operand
     * @param op     the operation to perform.
     *
     * @return the result of the arithmetic operation
     *
     * @throws RuntimeException     if {@code incValue} is not an {@code Integer}, {@code Double}, or {@code String}
     * @throws NullPointerException if {@code op} is null
     */
    private Number performOperation(Object first, Object second, Operation op) {
        Objects.requireNonNull(op, "Operation can't be null.");
        Number lhs = makeNumber(first);
        Number rhs = makeNumber(second);

        if (lhs instanceof Integer && rhs instanceof Integer
            && op == Operation.DIV && rhs.intValue() == 0) {
            throw new RuntimeException("Integer division by zero not allowed.");
        }

        Number ret = operators.get(op).applyAsDouble(lhs.doubleValue(), rhs.doubleValue());

        return (lhs instanceof Double || rhs instanceof Double) ? ret : ret.intValue();
    }

    /**
     * Makes a number from a supported object.
     * <p>
     * The supported types that can be converted are {@code Integer}, {@code Double}, and {@code String}.
     * If an object of an unsupported type is passed, an exception is thrown.
     * <p>
     * Additionally, the value {@code null} is interpreted as the integer {@code 0}.
     *
     * @param o the object to convert
     *
     * @return the object as a number ({@code Integer} or {@code Double})
     *
     * @throws RuntimeException if {@code o} is of an unsupported type
     */
    private Number makeNumber(Object o) {
        if (o == null) {
            return 0;
        }

        if (o instanceof Integer || o instanceof Double) {
            return (Number) o;
        }

        if (o instanceof String) {
            try {
                // Ternarni operator iz nekog razloga ne radi, natjera sve u double bez obzira o stvarnom tipu.
                if (((String) o).matches(".*[eE.].*")) { // Characters that appear in real numbers
                    return Double.parseDouble((String) o);
                } else {
                    return Integer.parseInt((String) o);
                }
            } catch (NumberFormatException ex) {
                throw new RuntimeException(String.format("Invalid number string: '%s'", o));
            }
        }

        throw new RuntimeException(
            String.format("Cannot perform arithmetic operators using value of type '%s'",
                value.getClass().getSimpleName()));
    }

    /** Lists all supported operations. */
    private enum Operation {

        /** The addition operation. */
        ADD,

        /** The subtraction operation. */
        SUB,

        /** The multiplication operation. */
        MUL,

        /** The division operation. */
        DIV
    }
}
