package hr.fer.zemris.java.hw07.observer1;

/** An observer that prints the double of every new value in an {@link IntegerStorage}, up to n times. */
public class DoubleValue implements IntegerStorageObserver {

    /** The maximum number of times this observer will print the value. */
    private int n;

    /**
     * Creates a new observer with the specified number of responses.
     * <p>
     * After printing the value n times, the observer removes itself from the subject.
     *
     * @param n the number of times to print the value
     */
    public DoubleValue(int i) {
        this.n = i;
    }

    /**
     * Prints the double of the new value, or removes itself from the object if the value has been printed n times.
     *
     * @param istorage the storage that changed
     */
    @Override
    public void valueChanged(IntegerStorage istorage) {
        if (n-- > 0) {
            System.out.printf("Double value: %d%n", 2 * istorage.getValue());
        } else {
            istorage.removeObserver(this);
        }
    }
}
