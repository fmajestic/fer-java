package hr.fer.zemris.java.hw07.demo4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentDemo {
    public static void main(String[] args) {

        List<String> lines;

        try {
            lines = Files.readAllLines(Paths.get("./studenti.txt"));
        } catch (IOException ex) {
            System.out.printf("Error reading file '%s'%n", ex.getMessage());
            return;
        }

        List<StudentRecord> records = convert(lines);


        System.out.println("Zadatak 1");
        System.out.println("=========");
        System.out.println(vratiBodovaViseOd25(records));
        System.out.println();

        System.out.println("Zadatak 2");
        System.out.println("=========");
        System.out.println(vratiBrojOdlikasa(records));
        System.out.println();

        System.out.println("Zadatak 3");
        System.out.println("=========");
        vratiListuOdlikasa(records).forEach(System.out::println);
        System.out.println();

        System.out.println("Zadatak 4");
        System.out.println("=========");
        vratiSortiranuListuOdlikasa(records).forEach(System.out::println);
        System.out.println();

        System.out.println("Zadatak 5");
        System.out.println("=========");
        vratiPopisNepolozenih(records).forEach(System.out::println);
        System.out.println();

        System.out.println("Zadatak 6");
        System.out.println("=========");
        razvrstajStudentePoOcjenama(records)
            .forEach((key, value) -> System.out.printf("%d => %s%n", key, arrayToLines(value.toArray())));
        System.out.println();

        System.out.println("Zadatak 7");
        System.out.println("=========");
        vratiBrojStudenataPoOcjename(records)
            .forEach((key, value) -> System.out.printf("%d => %d%n", key, value));
        System.out.println();

        System.out.println("Zadatak 8");
        System.out.println("=========");
        razvrstajProlazPad(records)
            .forEach((key, value) -> System.out.printf("%s => %s%n", key, arrayToLines(value.toArray())));
        System.out.println();
    }

    private static String arrayToLines(Object[] array) {
        var sb = new StringBuilder();

        sb.append("[\n");

        for (int i = 0; i < array.length - 1; i++) {
            sb.append('\t').append(array[i].toString()).append(",\n");
        }

        sb.append('\t').append(array[array.length - 1]).append("\n");
        sb.append("]");

        return sb.toString();
    }

    private static Map<Boolean, List<StudentRecord>> razvrstajProlazPad(List<StudentRecord> records) {
        return records.stream()
            .collect(Collectors.partitioningBy(
                sr -> sr.getGrade() > 1
            ));
    }

    private static Map<Integer, Integer> vratiBrojStudenataPoOcjename(List<StudentRecord> records) {
        return records.stream()
            .collect(Collectors.toMap(
                StudentRecord::getGrade,
                sr -> 1,
                Integer::sum
            ));
    }

    private static Map<Integer, List<StudentRecord>> razvrstajStudentePoOcjenama(List<StudentRecord> records) {
        return records.stream()
            .collect(Collectors.groupingBy(StudentRecord::getGrade));
    }

    private static List<String> vratiPopisNepolozenih(List<StudentRecord> records) {
        return records.stream()
            .filter(sr -> sr.getGrade() == 1)
            .sorted(Comparator.comparing(StudentRecord::getJmbag))
            .map(StudentRecord::getJmbag)
            .collect(Collectors.toList());
    }

    private static long vratiBodovaViseOd25(List<StudentRecord> records) {
        return records.stream()
            .map(sr -> sr.getMidtermPoints() + sr.getFinalPoints() + sr.getLabPoints())
            .filter(n -> n > 25)
            .count();
    }

    private static long vratiBrojOdlikasa(List<StudentRecord> records) {
        return records.stream()
            .filter(sr -> sr.getGrade() == 5)
            .count();
    }

    private static List<StudentRecord> vratiListuOdlikasa(List<StudentRecord> records) {
        return records.stream()
            .filter(sr -> sr.getGrade() == 5)
            .collect(Collectors.toList());
    }

    private static List<StudentRecord> vratiSortiranuListuOdlikasa(List<StudentRecord> records) {
        return records.stream()
            .filter(sr -> sr.getGrade() == 5)
            .sorted(Comparator.comparingDouble(StudentRecord::getFinalPoints).reversed())
            .collect(Collectors.toList());
    }

    private static List<StudentRecord> convert(List<String> lines) {
        return lines.stream()
            .filter(line -> !(line.isEmpty() || line.isBlank()))
            .map(line -> {
                String[] parts = line.split("\t");
                return new StudentRecord(
                    parts[0],
                    parts[1],
                    parts[2],
                    Double.parseDouble(parts[3]),
                    Double.parseDouble(parts[4]),
                    Double.parseDouble(parts[5]),
                    Integer.parseInt(parts[6]));
            })
            .collect(Collectors.toList());
    }
}
