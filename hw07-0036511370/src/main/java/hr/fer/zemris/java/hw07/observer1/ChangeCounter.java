package hr.fer.zemris.java.hw07.observer1;

/** An observer that prints the number of times an {@link IntegerStorage} has changed since registration. */
public class ChangeCounter implements IntegerStorageObserver {

    /** The current number of times a change has happened. */
    private int count = 0;

    /**
     * Prints the number of times the store's value has changed, since the registration of the observer.
     *
     * @param istorage the storage whose value has changed
     */
    @Override
    public void valueChanged(IntegerStorage istorage) {
        System.out.printf("Number of value changes since tracking: %d%n", ++count);
    }
}
