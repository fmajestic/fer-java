package hr.fer.zemris.java.hw07.demo2;

/** A demo program that prints the first 4 pairs of primes. */
public class PrimesDemo2 {
    public static void main(String[] args) {

        PrimesCollection primesCollection = new PrimesCollection(2);

        for (Integer prime1 : primesCollection) {
            for (Integer prime2 : primesCollection) {
                System.out.printf("Got prime pair: %d, %d%n", prime1, prime2);
            }
        }
    }
}
