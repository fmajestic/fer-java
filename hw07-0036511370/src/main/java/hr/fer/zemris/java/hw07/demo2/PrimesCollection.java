package hr.fer.zemris.java.hw07.demo2;

import java.util.Iterator;

/** A lazy collection of primes. */
public class PrimesCollection implements Iterable<Integer> {

    /** The number of primes in the collection. */
    private int n;

    /**
     * Creates a new collection that contains the first n prime numbers.
     *
     * @param n the number of primes in the collection
     */
    public PrimesCollection(int n) {
        this.n = n;
    }

    /**
     * Returns an iterator over the primes in the collection.
     *
     * @return an iterator over integers
     */
    @Override
    public Iterator<Integer> iterator() {
        return new PrimesCollectionIterator();
    }

    /**
     * Calculates the first prime that comes after n.
     * <p>
     * Even if n already is a prime, it is ignored and the next prime after n is still calculated.
     *
     * @param n the number to check from
     *
     * @return the next prime after n
     *
     * @throws IllegalStateException if n is {@link Integer#MAX_VALUE} (the biggest prime
     *                               that fits into a 32-bit signed integer)
     */
    private int primeAfter(int n) {
        if (n == Integer.MAX_VALUE) {
            throw new IllegalStateException("No more primes fit into a 32-bit unsigned integer.");
        }

        n++;

        while (!isPrime(n)) {
            n++;
        }

        return n;
    }

    /**
     * Checks if n is a prime number.
     *
     * @param n the number to check
     *
     * @return true if n is prime
     */
    private boolean isPrime(int n) {

        if (n == 2) {
            return true;
        }

        if (n < 2 || n % 2 == 0) {
            return false;
        }

        int bound = (int) Math.sqrt(n);

        for (int i = 3; i <= bound; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    /** The iterator for this lazy collection. */
    private class PrimesCollectionIterator implements Iterator<Integer> {

        /** The current prime of this iterator. */
        private int prime;

        /** The number of generated primes. */
        private int count = 0;

        /**
         * Checks if the collection contains any more primes, and if any more can be generated without overflowing.
         *
         * @return true if there are more primes available, false otherwise
         */
        @Override
        public boolean hasNext() {
            return count < PrimesCollection.this.n && prime != Integer.MAX_VALUE;
        }

        /**
         * Gets the next prime in the collection.
         *
         * @return the next prime number
         *
         * @throws IllegalStateException if called after {@link #hashCode()} starts returning false
         */
        @Override
        public Integer next() {
            if (!hasNext()) {
                throw new IllegalStateException("No more elements in collection.");
            }

            count++;
            return prime = PrimesCollection.this.primeAfter(prime);
        }
    }
}
