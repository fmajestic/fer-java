package hr.fer.zemris.java.hw07.observer2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Stores a single integer, and supports notifying registered observers when the stored value changes.
 */
public class IntegerStorage {

    /** The currently stored value. */
    private int value;

    /** A list of all registered observers. */
    private List<IntegerStorageObserver> observers;

    /**
     * Creates a new storage with an initial value.
     *
     * @param initialValue the initial value to store
     */
    public IntegerStorage(int initialValue) {
        this.value = initialValue;
        observers = new ArrayList<>();
    }

    /**
     * Adds a new observer to the subject, if it is not already present.
     *
     * @param observer the new observer.
     */
    public void addObserver(IntegerStorageObserver observer) {

        Objects.requireNonNull(observer);

        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    /**
     * Removes an observer from the subject.
     * <p>
     * If the observer to be removed is not present, this method does nothing.
     *
     * @param observer the observer to remove
     */
    public void removeObserver(IntegerStorageObserver observer) {

        Objects.requireNonNull(observer);

        observers.remove(observer);
    }

    /**
     * Clears the list of observers.
     */
    public void clearObservers() {
        observers.clear();
    }


    /**
     * Gets the currently stored value.
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets a new value to store, and notifies all registered by calling their
     * {@code valueChanged(IntegerStorage)} method with this object as the argument.
     * <p>
     * If the passed value is equal to the currently stored value, this method does nothing.
     *
     * @param value the new value
     *
     * @see IntegerStorageObserver
     */
    public void setValue(int value) {

        if (this.value != value) {

            var change = new IntegerStorageChange(this, this.value, value);

            this.value = value;

            if (observers != null) {

                IntegerStorageObserver[] forIteration = observers.toArray(new IntegerStorageObserver[0]);

                for (IntegerStorageObserver observer : forIteration) {
                    observer.valueChanged(change);
                }
            }
        }
    }
}
