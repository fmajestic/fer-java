package hr.fer.zemris.java.hw07.demo4;

import java.util.StringJoiner;

public class StudentRecord {

    private String jmbag;
    private String lastName;
    private String firstMame;
    private double midtermPoints;
    private double finalPoints;
    private double labPoints;
    private int grade;

    public StudentRecord(String jmbag, String lastName, String firstMame, double midtermPoints, double finalPoints,
                         double labPoints, int grade) {
        this.jmbag = jmbag;
        this.lastName = lastName;
        this.firstMame = firstMame;
        this.midtermPoints = midtermPoints;
        this.finalPoints = finalPoints;
        this.labPoints = labPoints;
        this.grade = grade;
    }

    public String getJmbag() {
        return jmbag;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstMame() {
        return firstMame;
    }

    public double getMidtermPoints() {
        return midtermPoints;
    }

    public double getFinalPoints() {
        return finalPoints;
    }

    public double getLabPoints() {
        return labPoints;
    }

    public int getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        return String.format(
            "%s\t%s\t%s\t%.2f\t%.2f\t%.2f\t%d",
            jmbag,
            lastName,
            firstMame,
            midtermPoints,
            finalPoints,
            labPoints,
            grade
        );
    }
}
