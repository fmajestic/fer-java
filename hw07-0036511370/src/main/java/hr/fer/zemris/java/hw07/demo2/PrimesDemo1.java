package hr.fer.zemris.java.hw07.demo2;

/** A demo program that prints the first 5 primes. */
public class PrimesDemo1 {
    public static void main(String[] args) {

        PrimesCollection primesCollection = new PrimesCollection(5);

        for(Integer prime : primesCollection) {
            System.out.println("Got prime: "+prime);
        }
    }
}
