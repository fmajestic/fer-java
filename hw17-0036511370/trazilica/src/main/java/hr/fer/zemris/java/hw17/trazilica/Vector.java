package hr.fer.zemris.java.hw17.trazilica;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;

@SuppressWarnings({ "WeakerAccess", "unused" })
/** A simple fixed n-dimensional vector. */
public class Vector implements Iterable<Double> {

    /** The elements. */
    private double[] parts;

    /**
     * Creates a new vector of n elements.
     *
     * @param n the number of elements
     */
    public Vector(int n) {
        parts = new double[n];
    }

    /**
     * Returns the length of the vector.
     *
     * @return the length
     */
    public int length() {
        return parts.length;
    }

    /**
     * Gets a value at an index.
     *
     * @param i the index
     *
     * @return the value
     */
    public double get(int i) {
        Objects.checkIndex(i, parts.length);
        return parts[i];
    }

    /**
     * Sets a value at an index.
     *
     * @param i     the index
     * @param value the new value
     */
    public void set(int i, double value) {
        Objects.checkIndex(i, parts.length);
        parts[i] = value;
    }

    /**
     * Calculates the cosine between two vectors and returns the result in a new vector.
     *
     * @param other the other vector
     *
     * @return the cosine
     */
    public double cosine(Vector other) {
        return piecewiseMultiply(other).sum() / (module() * other.module());
    }

    /**
     * Multiplies two vectors element-by-element, and returns a new vector with the result.
     *
     * @param other the other vector
     *
     * @return the product
     */
    public Vector piecewiseMultiply(Vector other) {
        if (this.length() != other.length()) {
            throw new IllegalArgumentException("Vector length mismatch.");
        }

        Vector res = new Vector(length());

        for (int i = 0; i < res.length(); i++) {
            res.set(i, get(i) * other.get(i));
        }

        return res;
    }

    /**
     * Calculates the module of the vector
     *
     * @return the module
     */
    public double module() {
        return Math.sqrt(Arrays.stream(parts).map(d -> d * d).sum());
    }

    /**
     * Sums all elements of the vector
     *
     * @return the sum
     */
    public double sum() {
        return Arrays.stream(parts).sum();
    }

    /**
     * Delegated to {@link Arrays#stream(double[])#iterator()}.
     *
     * @return the iterator.
     */
    @Override
    public Iterator<Double> iterator() {
        return Arrays.stream(parts).iterator();
    }

    /**
     * Applies the action to each element.
     *
     * @param action the action
     */
    @Override
    public void forEach(Consumer<? super Double> action) {
        for (double component : parts) {
            action.accept(component);
        }
    }
}
