package hr.fer.zemris.java.hw17.trazilica;

import hr.fer.zemris.java.hw17.trazilica.commands.ConsoleCommand;
import hr.fer.zemris.java.hw17.trazilica.commands.Query;
import hr.fer.zemris.java.hw17.trazilica.commands.Results;
import hr.fer.zemris.java.hw17.trazilica.commands.Type;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/** A simple search console. */
public class Konzola {

    @SuppressWarnings("FieldCanBeLocal")
    private static String PROMPT = "Enter command > ";

    /** All available commands. */
    private static Map<String, ConsoleCommand> commands = new HashMap<>();

    static {
        commands.put("type", new Type());
        commands.put("query", new Query());
        commands.put("results", new Results());
    }

    /**
     * Entry point of the program.
     *
     * @param args path to the directory to search in
     */
    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Expected exactly 1 argument: document directory. Got: " + args.length);
            return;
        }


        Scanner in = new Scanner(System.in);
        SearchEngine engine = new SearchEngine(Paths.get(args[0]));


        System.out.printf("Veličina rječnika je %d riječi.%n%n", engine.vocabularySize());

        while (true) {
            System.out.print(PROMPT);

            String line = in.nextLine();
            String[] parts = line.split("\\s+", 2);

            String command = parts.length > 0 ? parts[0] : "";
            String arguments = parts.length > 1 ? parts[1] : "";

            if (command.equals("exit")) {
                break;
            }


            ConsoleCommand cmd = commands.get(command);

            if (cmd != null) {
                cmd.exec(engine, arguments);
            } else {
                System.out.println("Nepoznata naredba.");
            }

            System.out.println();
        }
    }
}
