package hr.fer.zemris.java.hw17.trazilica.commands;

import hr.fer.zemris.java.hw17.trazilica.SearchEngine;

/** The results command. */
public class Results implements ConsoleCommand {

    /**
     * Gets the command name.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return "results";
    }

    /**
     * Tries to print the last query's results.
     *
     * @param engine the engine to get results from
     * @param args   empty
     */
    @Override
    public void exec(SearchEngine engine, String args) {

        if (!args.isEmpty()) {
            System.out.println("No arguments expected.");
            return;
        }

        SearchEngine.SearchResult sr = engine.getLastSearch();

        if (sr == null) {
            System.out.println("No previous search. Please execute a query command first.");
            return;
        }

        printResults(sr);
    }

    /**
     * Prints the top 10 results.
     *
     * @param result the results to print
     */
    static void printResults(SearchEngine.SearchResult result) {
        int limit = Math.min(result.size(), 10);

        for (int i = 0; i < limit; i++) {
            System.out.printf("[ %d] %s%n", i, result.get(i));
        }
    }
}
