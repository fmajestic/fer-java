package hr.fer.zemris.java.hw17.trazilica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;

/**
 * A simple "bag of words" search engine based on TF-IDF similarity.
 */
public class SearchEngine {

    /** The vocabulary stop words. */
    private static Set<String> stopwords;

    // Loads stopwords from resources.
    static {
        stopwords = new HashSet<>();

        try (
            BufferedReader is = new BufferedReader(new InputStreamReader(
                SearchEngine.class.getResourceAsStream("/stopwords.txt")))
        ) {
            String line;
            while ((line = is.readLine()) != null) {
                stopwords.add(line.toLowerCase());
            }
        } catch (IOException ex) {
            System.err.println("IO error: " + ex.getMessage());
        }
    }

    /** The directory being searched. */
    private Path directory;

    /** The vocabulary created from all documents in the directory. */
    private List<String> vocabulary;

    /** The Inverse-Document-Frequency vector for vocabulary words. */
    private Vector IDFVector;

    /** The TF-IDF vectors for all documents. */
    private List<DocumentVector> vectors;

    private SearchResult lastSearch;


    /**
     * Creates a new search engine for a directory.
     *
     * @param dir the directory to read documents from
     */
    public SearchEngine(Path dir) {
        directory = dir;

        loadVocabulary();
        loadVectors();
    }

    /**
     * Gets the generated vocabulary size.
     *
     * @return the size
     */
    public int vocabularySize() {
        return vocabulary.size();
    }

    public SearchResult getLastSearch() {
        return lastSearch;
    }

    /**
     * Runs a search and gets the results, sorted in descending order.
     *
     * @param query the query to run
     *
     * @return the result
     */
    public SearchResult search(String query) {

        List<String> words = new ArrayList<>();
        words(query).stream().filter(vocabulary::contains).forEach(words::add);

        Vector queryTFIDF = IDFVector.piecewiseMultiply(generateTFVector(words));

        List<SearchResultEntry> results = new ArrayList<>();
        for (DocumentVector vec : vectors) {
            double similarity = queryTFIDF.cosine(vec.vector);

            if (Math.abs(similarity) >= 1e-4) {
                results.add(new SearchResultEntry(vec.document, similarity));
            }
        }

        results.sort(Comparator.comparingDouble(SearchResultEntry::getSimilarity).reversed());

        lastSearch = new SearchResult(words, results);
        return lastSearch;
    }

    /** Loads the vocabulary. */
    private void loadVocabulary() {
        Set<String> tmpVocab = new HashSet<>();
        java.util.List<Vector> vectors = new ArrayList<>();

        try (DirectoryStream<Path> files = Files.newDirectoryStream(directory, Files::isRegularFile)) {

            for (Path file : files) {
                String text = Files.readString(file);
                tmpVocab.addAll(words(text));
            }
        } catch (IOException ex) {
            System.err.println("IO error: " + ex.getMessage());
        }

        vocabulary = new ArrayList<>(tmpVocab);
    }

    /** Constructs the document TFIDF vectors and the intermediate IDF vector. */
    private void loadVectors() {

        int documentCount = 0;
        vectors = new ArrayList<>(documentCount);

        try (DirectoryStream<Path> files = Files.newDirectoryStream(directory, Files::isRegularFile)) {
            for (Path file : files) {
                String text = Files.readString(file);
                Vector tf = generateTFVector(words(text));
                vectors.add(new DocumentVector(file, tf));
                documentCount += 1;
            }
        } catch (IOException ex) {
            System.err.println("IO error: " + ex.getMessage());
        }

        IDFVector = new Vector(vocabulary.size());

        for (int i = 0; i < vocabulary.size(); i++) {

            int sum = 0;
            for (DocumentVector vec : vectors) {
                if (vec.vector.get(i) > 0) {
                    sum += 1;
                }
            }

            IDFVector.set(i, Math.log10((double) documentCount / sum));
        }

        for (DocumentVector vec : vectors) {
            vec.vector = IDFVector.piecewiseMultiply(vec.vector);
        }
    }

    /**
     * Gets a list of non-stopword words from text.
     *
     * @param text the text to use
     *
     * @return the list of words
     */
    private List<String> words(String text) {
        List<String> result = new ArrayList<>();

        char[] chars = text.toCharArray();

        for (int i = 0; i < chars.length; i++) {

            if (!Character.isAlphabetic(chars[i])) {
                continue;
            }

            int start = i;
            while (i < chars.length && Character.isAlphabetic(chars[i])) {
                i++;
            }
            int end = i;

            String str = new String(chars, start, end - start).toLowerCase();

            if (!stopwords.contains(str)) {
                result.add(str);
            }
        }

        return result;
    }

    /**
     * Generates a Term-Frequency vector for some vocabulary words.
     *
     * @param words the words to analyze
     *
     * @return the TF vector
     */
    private Vector generateTFVector(List<String> words) {
        Vector tf = new Vector(vocabulary.size());

        for (int i = 0; i < vocabulary.size(); i++) {
            int count = 0;
            for (String textWord : words) {
                if (vocabulary.get(i).equals(textWord)) {
                    count++;
                }
            }

            tf.set(i, count);

        }

        return tf;
    }

    /** Holds information about a document's TF-IDF vector. */
    private static class DocumentVector {

        /** The document path. */
        private Path document;

        /** The document's vector. */
        private Vector vector;

        /**
         * Creates a new document vector.
         *
         * @param document the document
         * @param vector   the document's vector representation
         */
        DocumentVector(Path document, Vector vector) {
            this.document = document;
            this.vector = vector;
        }

        /**
         * Gets the file paht.
         *
         * @return the path
         */
        public Path getDocument() {
            return document;
        }

        /**
         * Gets the document vector.
         *
         * @return the vector
         */
        public Vector getVector() {
            return vector;
        }
    }

    /** Holds search info (query, results). */
    public static class SearchResult implements Iterable<SearchResultEntry> {

        /** The search query. */
        private List<String> query;

        /** The search results. */
        private List<SearchResultEntry> entries;

        /**
         * Creates a new search result.
         *
         * @param query   the query
         * @param entries the results
         */
        SearchResult(List<String> query, List<SearchResultEntry> entries) {
            this.query = query;
            this.entries = entries;
        }

        /**
         * Gets the query.
         *
         * @return the query
         */
        public List<String> getQuery() {
            return query;
        }

        /**
         * Gets the results.
         *
         * @return the results
         */
        public List<SearchResultEntry> getEntries() {
            return entries;
        }

        /**
         * Gets the result size.
         *
         * @return the result size
         */
        public int size() {
            return entries.size();
        }

        /**
         * Gets an entry at an index.
         *
         * @param i the index to get from
         *
         * @return the entry
         */
        public SearchResultEntry get(int i) {
            return entries.get(i);
        }

        /**
         * Delegated to {@link #getEntries()}'s iterator.
         *
         * @return the iterator
         */
        @Override
        public Iterator<SearchResultEntry> iterator() {
            return entries.iterator();
        }

        /**
         * Delegated to {@link #getEntries()}'s forEach method.
         *
         * @param action the action to perform on each entry
         */
        @Override
        public void forEach(Consumer<? super SearchResultEntry> action) {
            entries.forEach(action);
        }
    }

    /** Holds a search result entry. */
    public static class SearchResultEntry {

        /** The file path. */
        private Path file;

        /** The similarity with the query. */
        private double similarity;

        /**
         * Creates a new search result for a file.
         *
         * @param file       the file
         * @param similarity the similarity with the query
         */
        SearchResultEntry(Path file, double similarity) {
            this.similarity = similarity;
            this.file = file;
        }

        /**
         * Gets the similarity.
         *
         * @return the similarity
         */
        public double getSimilarity() {
            return similarity;
        }

        /**
         * Gets the file.
         *
         * @return the file
         */
        public Path getFile() {
            return file;
        }

        /**
         * Creates a string representation of the search result as
         * {@code "(similarity) [file]"}.
         *
         * @return the result as a string
         */
        @Override
        public String toString() {
            return String.format("(%.4f) %s", similarity, file);
        }
    }
}
