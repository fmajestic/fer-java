package hr.fer.zemris.java.hw17.trazilica.commands;

import hr.fer.zemris.java.hw17.trazilica.SearchEngine;

import java.io.IOException;
import java.nio.file.Files;

/** The type command. */
public class Type implements ConsoleCommand {

    @Override
    public String getName() {
        return "type";
    }

    @Override
    public void exec(SearchEngine engine, String args) {

        int index;

        try {
            index = Integer.parseInt(args);
        } catch (NumberFormatException ex) {
            System.out.println("Invalid index number.");
            return;
        }

        SearchEngine.SearchResult sr = engine.getLastSearch();

        if (sr == null) {
            System.out.println("No previous search. Please execute a query command first.");
            return;
        }

        try {
            Files.lines(sr.get(index).getFile()).forEach(System.out::println);
        } catch (IOException ex) {
            System.out.println("IO error, couldn't read file: " + ex.getMessage());
        }
    }
}
