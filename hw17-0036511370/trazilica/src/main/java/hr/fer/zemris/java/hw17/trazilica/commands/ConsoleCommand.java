package hr.fer.zemris.java.hw17.trazilica.commands;

import hr.fer.zemris.java.hw17.trazilica.SearchEngine;

/** Models a simple console command. */
public interface ConsoleCommand {

    /**
     * Gets the command name.
     *
     * @return the name
     */
    String getName();

    /**
     * Executes the command.
     *
     * @param engine the engine
     * @param args   the arguments
     */
    void exec(SearchEngine engine, String args);
}
