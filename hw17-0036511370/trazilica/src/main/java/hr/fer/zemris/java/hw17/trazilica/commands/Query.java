package hr.fer.zemris.java.hw17.trazilica.commands;

import hr.fer.zemris.java.hw17.trazilica.SearchEngine;
import hr.fer.zemris.java.hw17.trazilica.SearchEngine.SearchResult;

/** The query command. */
public class Query implements ConsoleCommand {

    /**
     * Gets the command name.
     *
     * @return the name
     */
    public String getName() {
        return "query";
    }

    /**
     * Asks the engine to run a query and displays the results.
     *
     * @param engine the engine
     * @param args   the query
     */
    @Override
    public void exec(SearchEngine engine, String args) {

        SearchResult result = engine.search(args);

        System.out.printf("Query is: %s%n", result.getQuery().toString());
        System.out.println("Top 10 results:");

        Results.printResults(result);
    }
}
