package hr.fer.zemris.java.hw17.jvdraw.actions;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;

import java.awt.event.ActionEvent;

/** The save action. */
public class Save extends JVDrawAction {

    private static final String NAME = "Save";

    public Save(JVDraw app, DrawingModel model) {
        super(NAME, app, model);
    }

    /**
     * Calls {@link JVDraw#saveFile(boolean)} with {@code false}.
     *
     * @param e the event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        app.saveFile(false);
    }
}
