package hr.fer.zemris.java.hw17.jvdraw.drawing.objects;

import hr.fer.zemris.java.hw17.jvdraw.editors.FilledCircleEditor;
import hr.fer.zemris.java.hw17.jvdraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectVisitor;

import java.awt.*;

/** The filled circle object. */
public class FilledCircle extends Circle {

    /** The background color. */
    private Color bgColor;

    public FilledCircle(Point center, int radius, Color fg, Color bg) {
        super(center, radius, fg);
        this.bgColor = bg;
    }

    /**
     * Gets the bg color.
     *
     * @return the color
     */
    public Color getBgColor() {
        return bgColor;
    }

    /**
     * Sets the bg color.
     *
     * @param bgColor the new color
     */
    public void setBgColor(Color bgColor) {
        if (!this.bgColor.equals(bgColor)) {
            this.bgColor = bgColor;
            notifyObservers();
        }
    }

    /** {@{inheritDoc} */
    @Override
    public void accept(GeometricalObjectVisitor v) {
        v.visit(this);
    }

    /** {@inheritDoc} */
    @Override
    public GeometricalObjectEditor createGeometricalObjectEditor() {
        return new FilledCircleEditor(this);
    }

    /**
     * Adds background color information to {@link super#toString()}.
     *
     * @return the circle as a string
     */
    @Override
    public String toString() {
        return String.format("%s %s", super.toString(), string(bgColor));
    }
}
