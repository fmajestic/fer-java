package hr.fer.zemris.java.hw17.jvdraw.editors;

import hr.fer.zemris.java.hw17.jvdraw.color.JColorArea;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Circle;

import javax.swing.*;
import java.awt.*;

public class CircleEditor extends GeometricalObjectEditor {

    /** The center x coordinate. */
    private final JTextField centerX;
    /** The center y coordinate. */
    private final JTextField centerY;
    /** The radius. */
    private final JTextField radius;
    /** The line color. */
    private final JColorArea fgArea;

    /** The circle being edited. */
    private final Circle circle;
    /** The new center. */
    private final Point newCenter;
    /** The new radius. */
    private int newRadius;

    public CircleEditor(Circle circle) {
        this.circle = circle;
        newCenter = new Point(circle.getCenter());
        newRadius = circle.getRadius();
        fgArea = new JColorArea("Foreground", circle.getColor());

        setLayout(new GridLayout(4, 2));

        add(new JLabel("Origin x"));
        add(centerX = new JTextField(String.valueOf(circle.getCenter().x)));

        add(new JLabel("Origin y"));
        add(centerY = new JTextField(String.valueOf(circle.getCenter().y)));

        add(new JLabel("Radius"));
        add(radius = new JTextField(String.valueOf(circle.getRadius())));

        add(new JLabel("Foreground"));
        add(fgArea);
    }


    /** {@inheritDoc} */
    @Override
    public void checkEditing() {
        try {
            newCenter.x = Integer.parseInt(centerX.getText().trim());
            newCenter.y = Integer.parseInt(centerY.getText().trim());
            newRadius = Integer.parseInt(radius.getText().trim());
        } catch (NumberFormatException ex) {
            throw new IllegalStateException("Invalid number; " + ex.getMessage(), ex);
        }

        if (newRadius <= 0) {
            throw new IllegalStateException("Radius must be greater than 0.");
        }
    }

    /** {@inheritDoc} */
    @Override
    public void acceptEditing() {
        checkEditing();
        circle.setCenter(newCenter);
        circle.setRadius(newRadius);
        circle.setFgColor(fgArea.getSelectedColor());
    }
}
