package hr.fer.zemris.java.hw17.jvdraw.editors;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditorOnDoubleClick extends MouseAdapter {

    private final JVDraw app;

    public EditorOnDoubleClick(JVDraw app) {this.app = app;}

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() != 2) {
            return;
        }

        //noinspection unchecked
        GeometricalObject clicked = ((JList<GeometricalObject>) e.getSource()).getSelectedValue();
        GeometricalObjectEditor editor = clicked.createGeometricalObjectEditor();

        while (true) {
            int result = JOptionPane.showConfirmDialog(app, editor, "Edit object", JOptionPane.OK_CANCEL_OPTION);

            if (result != JOptionPane.OK_OPTION) {
                break;
            }

            try {
                editor.checkEditing();
                editor.acceptEditing();
                break;
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(
                    app,
                    String.format("Invalid value: %s", ex.getMessage()),
                    "Error",
                    JOptionPane.ERROR_MESSAGE
                );
            }
        }
    }
}
