package hr.fer.zemris.java.hw17.jvdraw;

import hr.fer.zemris.java.hw17.jvdraw.actions.*;
import hr.fer.zemris.java.hw17.jvdraw.color.JColorArea;
import hr.fer.zemris.java.hw17.jvdraw.color.JColorLabel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.JDrawingCanvas;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModelImpl;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingObjectListModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.drawing.tools.*;
import hr.fer.zemris.java.hw17.jvdraw.editors.EditorOnDoubleClick;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectToJVD;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/** The main app. */
public class JVDraw extends JFrame {

    /** The model that manages drawn objects. */
    private final DrawingModel model = new DrawingModelImpl();

    /** The file we loaded from, if any. */
    private Path filePath;

    /**
     * The currently selected tool.
     * <p>
     * Initially set to the empty tool to avoid null checks in the canvas's producer.
     */
    private Tool currentTool = new EmptyTool();

    /**
     * Initializes the app.
     */
    private JVDraw() {
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setSize(800, 600);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {

                if (model.isModified()) {
                    int result = JOptionPane.showConfirmDialog(
                        JVDraw.this, "There are unsaved changes. Save modifications to file?", "Unsaved changes",
                        JOptionPane.YES_NO_CANCEL_OPTION
                    );

                    if (result == JOptionPane.CANCEL_OPTION) {
                        return;
                    }

                    if (result == JOptionPane.NO_OPTION || saveFile(false)) {
                        dispose();
                    }
                }

                dispose();
            }
        });

        initGUI();
    }

    /**
     * Saves the file, with an optional promt for a new path in case of an existing one.
     * <p>
     * Implements functionality for "Save" and "Save as..." this wasy.
     *
     * @param askPathIfExists should the user be prompted for a new path if one already exists
     *
     * @return true if the file was saved or the user doesn't want to save the file
     */
    public boolean saveFile(boolean askPathIfExists) {
        if (!model.isModified()) {
            return true;
        }

        if (askPathIfExists || filePath == null) {
            Path newFile = Util.promptSavePath(this, "JVDraw file, *.jvd", "jvd");

            if (newFile != null) {
                filePath = newFile;
            } else {
                return false;
            }
        }

        GeometricalObjectToJVD jvd = new GeometricalObjectToJVD();
        for (int i = 0; i < model.getSize(); i++) {
            model.getObject(i).accept(jvd);
        }

        try {
            Files.write(filePath, jvd.getLines());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Not saved: " + ex.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        model.clearModifiedFlag();
        return true;
    }

    /** Intializes the GUI. */
    private void initGUI() {

        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        initMenubar();
        initCentralAndToolbar();
    }

    /** Initializes the menubar. */
    private void initMenubar() {
        JMenuBar mb = new JMenuBar();

        JMenu file = new JMenu("File");
        file.add(new Open(this, model));
        file.add(new Save(this, model));
        file.add(new SaveAs(this, model));
        file.addSeparator();
        file.add(new Export(this, model));
        file.addSeparator();
        file.add(new Close(this, model));

        mb.add(file);
        setJMenuBar(mb);
    }

    /** Initializes the central area. */
    private void initCentralAndToolbar() {

        JColorArea fg = new JColorArea("foreground", Color.black);
        JColorArea bg = new JColorArea("background", Color.white);
        JColorLabel cl = new JColorLabel(fg, bg);
        JDrawingCanvas canvas = new JDrawingCanvas(model, this::getCurrentTool);

        cl.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        add(cl, BorderLayout.PAGE_END);

        JList<GeometricalObject> list = new JList<>(new DrawingObjectListModel(model));
        list.setMinimumSize(new Dimension(150, 0));
        list.addMouseListener(new EditorOnDoubleClick(this));
        list.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createMatteBorder(0, 3, 0, 0, Color.gray),
            BorderFactory.createEmptyBorder(5, 5, 5, 5)
        ));

        list.addKeyListener(new ListControls(list));

        model.addDrawingModelListener(canvas);

        add(canvas, BorderLayout.CENTER);
        add(list, BorderLayout.LINE_END);

        initToolbar(fg, bg);
    }

    /** Initialiezs the toolbar. */
    private void initToolbar(JColorArea fg, JColorArea bg) {

        JToggleButton l =
            new JToggleButton(new SetTool("Line", this::setCurrentTool, new LineTool(model, fg)));
        JToggleButton c =
            new JToggleButton(new SetTool("Circle", this::setCurrentTool, new CircleTool(model, fg)));
        JToggleButton fc =
            new JToggleButton(new SetTool("Filled circle", this::setCurrentTool, new FilledCircleTool(model, fg, bg)));

        ButtonGroup btns = new ButtonGroup();
        btns.add(l);
        btns.add(c);
        btns.add(fc);

        JToolBar tb = new JToolBar("Tools");
        tb.add(fg);
        tb.addSeparator();
        tb.add(bg);
        tb.addSeparator();
        tb.add(l);
        tb.add(c);
        tb.add(fc);

        add(tb, BorderLayout.PAGE_START);
    }


    /**
     * Gets the currently selected tool.
     *
     * @return the current tool
     */
    private Tool getCurrentTool() {
        return currentTool;
    }

    /**
     * Sets the current tool.
     *
     * @param t the new tool
     */
    private void setCurrentTool(Tool t) {
        this.currentTool = Objects.requireNonNull(t, "Tool can't be null.");
    }

    /**
     * Sets the current file path.
     *
     * @param path the new path
     */
    public void setFilePath(Path path) {
        filePath = Objects.requireNonNull(path);
    }

    /**
     * Entry point of the program
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JVDraw app = new JVDraw();
            app.setTitle("JVDraw");
            app.setVisible(true);
        });
    }

    /** Implements element reordering and deleting via +/-/Del */
    private class ListControls extends KeyAdapter {

        /** The list to control. */
        private final JList<GeometricalObject> list;

        /**
         * Creates a new controller.
         *
         * @param list the list
         */
        ListControls(JList<GeometricalObject> list) {this.list = list;}

        /**
         * Executes an action based on the key.
         * <p>
         * {@code keyReleased} is used becasue {@link #keyTyped(KeyEvent)} can't se Del.
         *
         * @param e the event
         */
        @Override
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();

            int i = list.getSelectedIndex();
            int size = list.getModel().getSize();
            GeometricalObject selected = list.getSelectedValue();

            if (key == KeyEvent.VK_PLUS || key == KeyEvent.VK_ADD) {
                if (i > 0) {
                    model.changeOrder(selected, -1);
                    list.setSelectedIndex(i - 1);
                }
            } else if (key == KeyEvent.VK_MINUS || key == KeyEvent.VK_SUBTRACT) {
                if (i < size - 1) {
                    model.changeOrder(selected, +1);
                    list.setSelectedIndex(i + 1);
                }
            } else if (key == KeyEvent.VK_DELETE) {
                model.remove(selected);
            }
        }
    }
}
