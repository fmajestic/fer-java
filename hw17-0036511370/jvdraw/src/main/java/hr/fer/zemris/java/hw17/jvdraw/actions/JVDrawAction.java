package hr.fer.zemris.java.hw17.jvdraw.actions;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;

import javax.swing.*;

/** A base class for all JVDraw actions. */
abstract class JVDrawAction extends AbstractAction {

    /** The app. */
    final JVDraw app;

    /** The model. */
    final DrawingModel model;

    JVDrawAction(String name, JVDraw app, DrawingModel model) {
        super(name);
        this.app = app;
        this.model = model;
    }
}
