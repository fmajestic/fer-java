package hr.fer.zemris.java.hw17.jvdraw.color;

import java.awt.*;

/** A color provider. */
public interface IColorProvider {

    /**
     * Gets the selected color.
     *
     * @return the color
     */
    Color getSelectedColor();

    /**
     * Adds a listener for color changes.
     *
     * @param l the listener
     */
    void addColorChangeListener(ColorChangeListener l);

    /**
     * Removes a listener.
     *
     * @param l the listener
     */
    void removeColorChangeListener(ColorChangeListener l);
}
