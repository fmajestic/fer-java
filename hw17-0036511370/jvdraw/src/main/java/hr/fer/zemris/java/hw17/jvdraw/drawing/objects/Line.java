package hr.fer.zemris.java.hw17.jvdraw.drawing.objects;

import hr.fer.zemris.java.hw17.jvdraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.editors.LineEditor;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectVisitor;

import java.awt.*;

/** The line object. */
public class Line extends GeometricalObject {

    /** The start point. */
    private Point start;

    /** The end point. */
    private Point end;

    public Line(Point start, Point end, Color fg) {
        super(fg);
        this.start = start;
        this.end = end;
    }

    /**
     * Gets the start point.
     *
     * @return the point
     */
    public Point getStart() {
        return start;
    }

    /**
     * Sets the start point.
     *
     * @param start the new point
     */
    public void setStart(Point start) {
        if (!this.start.equals(start)) {
            this.start = start;
            notifyObservers();
        }
    }

    /**
     * Gets the end point.
     *
     * @return the end point
     */
    public Point getEnd() {
        return end;
    }

    /**
     * Sets the end point.
     *
     * @param end the new end point
     */
    public void setEnd(Point end) {
        if (!this.end.equals(end)) {
            this.end = end;
            notifyObservers();
        }
    }


    /** {@inheritDoc} */
    @Override
    public void accept(GeometricalObjectVisitor v) {
        v.visit(this);
    }

    /** {@inheritDoc} */
    @Override
    public GeometricalObjectEditor createGeometricalObjectEditor() {
        return new LineEditor(this);
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return String.format("Line %s-%s", string(start), string(end));
    }
}
