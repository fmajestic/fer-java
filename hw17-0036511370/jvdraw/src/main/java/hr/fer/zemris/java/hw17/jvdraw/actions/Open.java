package hr.fer.zemris.java.hw17.jvdraw.actions;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw;
import hr.fer.zemris.java.hw17.jvdraw.drawing.model.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Line;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/** The open action. */
public class Open extends JVDrawAction {

    private static final String NAME = "Open";

    public Open(JVDraw app, DrawingModel model) {
        super(NAME, app, model);
    }

    /**
     * Attempts to open and read JVD file.
     *
     * @param e the event
     */
    @Override
    public void actionPerformed(ActionEvent e) {


        JFileChooser jfc = new JFileChooser(".");
        jfc.setAcceptAllFileFilterUsed(false);
        jfc.setFileFilter(new FileNameExtensionFilter("JVD file, *.jvd", "jvd"));

        if (jfc.showSaveDialog(app) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        Path path = jfc.getSelectedFile().toPath();
        List<GeometricalObject> objects;

        try {
            objects = Files
                .lines(path)
                .filter(str -> !str.isEmpty() && !str.isBlank())
                .map(Open::getObject)
                .collect(Collectors.toList());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(app, "IO error: " + ex.getMessage(), "IO error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        model.clear();
        objects.forEach(model::add);
        model.clearModifiedFlag();
        app.setFilePath(path);
    }

    /**
     * Gets an object from a string definition.
     * <p>
     * The supported definitions are:
     * <pre>
     *     LINE    x1 y1 x2 y2 r g b
     *     CIRCLE  x y rad r g b
     *     FCIRCLE x y rad r g b r g b
     * </pre>
     *
     * @param def the string definition as given above
     *
     * @return the object
     */
    private static GeometricalObject getObject(String def) {

        String[] parts = def.split("\\s+");

        if (parts.length < 6) {
            throw new IllegalArgumentException("Invalid object definition in file: " + def);
        }

        String name = parts[0];

        if (name.equals("LINE")) {
            return new Line(
                new Point(getInt(parts[1]), getInt(parts[2])),
                new Point(getInt(parts[3]), getInt(parts[4])),
                getColor(parts[5], parts[6], parts[7])
            );
        } else if (name.matches("F?CIRCLE")) {

            Point center = new Point(getInt(parts[1]), getInt(parts[2]));
            int radius = getInt(parts[3]);
            Color color = getColor(parts[4], parts[5], parts[6]);

            if (name.equals("CIRCLE")) {
                return new Circle(center, radius, color);
            } else {
                return new FilledCircle(center, radius, color, getColor(parts[7], parts[8], parts[9]));
            }
        } else {
            throw new IllegalArgumentException("Unknown object name: " + name);
        }
    }

    /**
     * Gets a color from string parts.
     *
     * @param r the red
     * @param g the green
     * @param b the blue
     *
     * @return the color
     */
    private static Color getColor(String r, String g, String b) {
        return new Color(
            Integer.parseInt(r),
            Integer.parseInt(g),
            Integer.parseInt(b)
        );
    }

    /**
     * Parses an int.
     *
     * @param s the string to parse
     *
     * @return the parsed number
     */
    private static int getInt(String s) {
        return Integer.parseInt(s);
    }
}