package hr.fer.zemris.java.hw17.jvdraw.drawing.model;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.GeometricalObject;

import javax.swing.*;

/**
 * A Swing list model for the actual DrawingModel, used for displaying the contents.
 */
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> implements DrawingModelListener {

    /** The model whose contents to display. */
    private final DrawingModel model;

    public DrawingObjectListModel(DrawingModel model) {
        this.model = model;
        model.addDrawingModelListener(this);
    }

    /**
     * Delegated to the stored model.
     *
     * @return the size
     */
    @Override
    public int getSize() {
        return model.getSize();
    }

    /**
     * Delegates to the stored model.
     *
     * @param index the index
     *
     * @return the object
     */
    @Override
    public GeometricalObject getElementAt(int index) {
        return model.getObject(index);
    }

    /**
     * Forwards the event to the abstract list model.
     *
     * @param source the source model
     * @param start  the start index, inclusive
     * @param end    the end index, inclusive
     */
    @Override
    public void objectsAdded(DrawingModel source, int start, int end) {
        this.fireIntervalAdded(source, start, end);
    }

    /**
     * Forwards the event to the abstract list model.
     *
     * @param source the source model
     * @param start  the start index, inclusive
     * @param end    the end index, inclusive
     */
    @Override
    public void objectsRemoved(DrawingModel source, int start, int end) {
        this.fireIntervalRemoved(source, start, end);
    }

    /**
     * Forwards the event to the abstract list model.
     *
     * @param source the source model
     * @param start  the start index, inclusive
     * @param end    the end index, inclusive
     */
    @Override
    public void objectsChanged(DrawingModel source, int start, int end) {
        this.fireContentsChanged(source, start, end);
    }
}
