package hr.fer.zemris.java.hw17.jvdraw.visitors;

import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.drawing.objects.Line;

import java.awt.*;

/**
 * The painter visitor.
 */
public class GeometricalObjectPainter implements GeometricalObjectVisitor {

    private Graphics2D g2d;

    public GeometricalObjectPainter(Graphics2D g2d) {
        this.g2d = g2d;
    }

    public GeometricalObjectPainter() { }

    public GeometricalObjectPainter setG2d(Graphics2D g2d) {
        this.g2d = g2d;
        return this;
    }

    @Override
    public void visit(Line line) {
        g2d.setColor(line.getColor());
        g2d.drawLine(line.getStart().x, line.getStart().y, line.getEnd().x, line.getEnd().y);
    }

    @Override
    public void visit(Circle circle) {
        int radius = circle.getRadius();
        int diameter = 2 * radius - 1;
        int x = circle.getCenter().x - radius;
        int y = circle.getCenter().y - radius;

        g2d.setColor(circle.getColor());
        g2d.drawOval(x, y, diameter, diameter);
    }

    @Override
    public void visit(FilledCircle filledCircle) {
        int radius = filledCircle.getRadius();
        int diameter = 2 * radius - 1;
        int x = filledCircle.getCenter().x - radius;
        int y = filledCircle.getCenter().y - radius;

        g2d.setColor(filledCircle.getBgColor());
        g2d.fillOval(x, y, diameter, diameter);

        visit((Circle) filledCircle);
    }
}