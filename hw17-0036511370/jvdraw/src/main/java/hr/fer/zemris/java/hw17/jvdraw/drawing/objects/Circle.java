package hr.fer.zemris.java.hw17.jvdraw.drawing.objects;

import hr.fer.zemris.java.hw17.jvdraw.editors.CircleEditor;
import hr.fer.zemris.java.hw17.jvdraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectVisitor;

import java.awt.*;

/** The circle object. */
public class Circle extends GeometricalObject {

    /** The center. */
    private Point center;

    /** The radius. */
    private int radius;

    /**
     * Creates a new circle.
     *
     * @param center the center
     * @param radius the radius
     * @param fg     the line color
     */
    public Circle(Point center, int radius, Color fg) {
        super(fg);
        this.center = center;
        this.radius = radius;
    }

    /**
     * Gets the center of the circle.
     *
     * @return the center
     */
    public Point getCenter() {
        return center;
    }

    /**
     * Sets the center of the circle.
     *
     * @param center the center
     */
    public void setCenter(Point center) {
        notifyObservers();
        this.center = center;
    }

    /**
     * Gets the radius of the circle.
     *
     * @return the radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Sets the radius of the circle.
     *
     * @param radius the new radius
     */
    public void setRadius(int radius) {
        notifyObservers();
        this.radius = radius;
    }

    /** {@inheritDoc} */
    @Override
    public void accept(GeometricalObjectVisitor v) {
        v.visit(this);
    }

    /** {@inheritDoc} */
    @Override
    public GeometricalObjectEditor createGeometricalObjectEditor() {
        return new CircleEditor(this);
    }

    /**
     * Creates a string representation of the cirle for the ListModel
     *
     * @return the circle as a string
     */
    @Override
    public String toString() {
        return String.format("Circle %s %d", string(center), radius);
    }
}
