package hr.fer.zemris.java.hw17.jvdraw.drawing.objects;

import hr.fer.zemris.java.hw17.jvdraw.visitors.GeometricalObjectVisitor;
import hr.fer.zemris.java.hw17.jvdraw.editors.GeometricalObjectEditor;

import java.awt.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A base class for all geometrical objects.
 * <p>
 * Implements basic behavior for adding/removing listeners, foreground color get/set,
 * and some utility methods for string conversion.
 *
 * @see Line
 * @see Circle
 * @see FilledCircle
 */
public abstract class GeometricalObject {

    /** The object's change listeners. */
    private final List<GeometricalObjectListener> listeners = new CopyOnWriteArrayList<>();

    /** The foreground color. */
    private Color fg;


    /**
     * Creates a new object with a foreground color.
     *
     * @param fg the initial foreground color
     */
    GeometricalObject(Color fg) {
        this.fg = fg;
    }

    /**
     * Accepts a visitor.
     *
     * @param v the visitor to accept
     */
    public abstract void accept(GeometricalObjectVisitor v);

    /**
     * Creates an editor for this object.
     *
     * @return the editor
     */
    public abstract GeometricalObjectEditor createGeometricalObjectEditor();

    /**
     * Gets the foreground color.
     *
     * @return the foreground color
     */
    public Color getColor() {
        return fg;
    }

    /**
     * Sets the foreground color.
     *
     * @param fg the new foreground color
     */
    public void setFgColor(Color fg) {
        this.fg = fg;
        notifyObservers();
    }

    /**
     * Adds a listener.
     * <p>
     * If the listener is {@code null}, nothing happens and the listener is not added.
     *
     * @param l the new listener
     */
    public void addGeometricalObjectListener(GeometricalObjectListener l) {
        if (l != null) {
            listeners.add(l);
        }
    }

    /**
     * Removes a listener.
     * <p>
     * If the listener is {@code null} or not registered for this object, nothing happens.
     *
     * @param l the listener to remove
     */
    public void removeGeometricalObjectListener(GeometricalObjectListener l) {
        listeners.remove(l);
    }

    /**
     * Notifies all observers that the object's property has changed.
     */
    void notifyObservers() {
        listeners.forEach(l -> l.geometricalObjectChanged(this));
    }

    /**
     * Returns the string representation of a point in the format (x,y).
     * <p>
     * If the point is {@code null}, this method returns {@code "null"}.
     *
     * @param p the point
     *
     * @return the point as a string
     */
    static String string(Point p) {
        return p != null
               ? String.format("(%d,%d)", p.x, p.y)
               : "null";
    }

    /**
     * Returns a color as a hex string in the format #RRGGBB.
     * <p>
     * If the color is {@code null}, this method returns {@code "null"n.
     *
     * @param c the color
     *
     * @return the color as a string
     */
    static String string(Color c) {
        return c != null
               ? String.format("#%02X%02X%02X", c.getRed(), c.getGreen(), c.getBlue())
               : "null";
    }
}
