package hr.fer.zemris.java.hw17.jvdraw.drawing.model;

/** A listener for model changes. */
public interface DrawingModelListener {

    /**
     * Fired when objects are added to the model.
     *
     * @param source the source model
     * @param start  the start index, inclusive
     * @param end    the end index, inclusive
     */
    void objectsAdded(DrawingModel source, int start, int end);

    /**
     * Fired when objects are removed from the model.
     *
     * @param source the source model
     * @param start  the start index, inclusive
     * @param end    the end index, inclusive
     */
    void objectsRemoved(DrawingModel source, int start, int end);

    /**
     * Fired when objects are changed in the model.
     *
     * @param source the source model
     * @param start  the start index, inclusive
     * @param end    the end index, inclusive
     */
    void objectsChanged(DrawingModel source, int start, int end);
}
