package hr.fer.zemris.java.hw17.jvdraw.actions;

import hr.fer.zemris.java.hw17.jvdraw.drawing.tools.Tool;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Objects;
import java.util.function.Consumer;

/** The set tool action. */
public class SetTool extends AbstractAction {

    private final Tool tool;
    private final Consumer<Tool> setter;

    public SetTool(String name, Consumer<Tool> toolSetter, Tool tool) {
        super(name);
        this.tool = Objects.requireNonNull(tool, "Tool can't be null");
        this.setter = Objects.requireNonNull(toolSetter, "Tool setter can't be null");
    }

    /** Sets the provided tool as the current tool. */
    @Override
    public void actionPerformed(ActionEvent e) {
        setter.accept(tool);
    }
}
