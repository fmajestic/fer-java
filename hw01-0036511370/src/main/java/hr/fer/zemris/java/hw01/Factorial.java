package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * A program that calculates factorials of whole numbers.
 * <p>
 * Enter 'kraj' to finish inputting numbers.
 * 
 * @author Filip Majetic
 */
public class Factorial {

    /**
     * Main method of the program.
     * 
     * @param args unused
     */
    public static void main(String[] args) {
        var in = new Scanner(System.in);

        do {
            System.out.print("Unesite broj > ");
            var input = in.nextLine();

            if (input.toLowerCase().equals("kraj")) {
                break;
            }

            try {
                var n = Long.parseLong(input);

                if (n < 3 || n > 20) {
                    System.out.printf("%d nije broj u dozvoljenom rasponu.%n", n);
                    continue;
                }

                var factorial = calculateFactorial(n);

                System.out.printf("%d! = %d%n", n, factorial);

            } catch (NumberFormatException ex) {
                System.out.printf("'%s' nije cijeli broj.%n", input);
            }
        } while (true);

        System.out.println("Doviđenja.");
        in.close();
    }

    /**
     * Recursively calculates the factorial of {@code n}.
     * 
     * @param n the number whose factorial is to be calculated
     * 
     * @return factorial of the number
     * 
     * @throws IllegalArgumentException if {@code n} is negative or greater than 20
     */
    static long calculateFactorial(long n) {
        if (n < 0 || n > 20)
            throw new IllegalArgumentException("argument n is outside of valid range [0, 20]: " + n);

        if (n == 0)
            return 1;
        return n * calculateFactorial(n - 1);
    }
}
