package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * A program that calculates the area and perimeter of a rectangle. If there are
 * no command line arguments, the user will be prompted for input.
 * 
 * @author Filip Majetic
 */
public class Rectangle {

    /**
     * Main method of the program.
     *
     * @param args Must be either empty, or contain the two sides to be used.
     */
    public static void main(String[] args) {
        double width, height;

        if (args.length == 1 || args.length > 2) {
            System.out.println("Neispravan broj argumenata, mora biti 0 ili 2.");
            return;
        }

        if (args.length == 2) {
            try {
                width = parseSide(args[0]);
                height = parseSide(args[1]);
            } catch (IllegalArgumentException ex) {
                System.out.println("Argumenti programa moraju biti prazni ili sadržavati dva pozitivna broja!");
                return;
            }
        } else {
            var sc = new Scanner(System.in);

            width = getValidSide("Unesite širinu > ", sc);
            height = getValidSide("Unesite visinu > ", sc);

            sc.close();
        }

        System.out.printf("Pravokutnik širine %s i visine %s ima površinu %s te opseg %s.%n", Double.toString(width),
                Double.toString(height), Double.toString(width * height), Double.toString(2 * (width + height)));
    }

    /**
     * Prompts user to enter a side of the rectangle until valid input is entered.
     * This method uses {@link parseSide} to parse user input.
     * 
     * @param prompt the prompt to display each time before reading input
     * @param in     the text scanner to use for reading input
     * 
     * @return rectangle side
     */
    public static double getValidSide(String prompt, Scanner in) {
        do {
            System.out.print(prompt);

            String input = in.next();

            try {
                return parseSide(input);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
            }
        } while (true);
    }

    /**
     * Parses a string into a double, checking if the number is a valid rectangle
     * side.
     * 
     * @param input the user input to be parsed
     * 
     * @return the parsed number
     * 
     * @throws IllegalArgumentException if the input is not a valid double, or the
     *                                  parsed value is negative
     */
    public static double parseSide(String input) throws IllegalArgumentException {
        try {
            double num = Double.parseDouble(input);

            if (num < 0.0) {
                throw new IllegalArgumentException("Unijeli ste negativnu vrijednost.");
            }

            return num;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException(String.format("'%s' se ne može protumačiti kao broj.", input));
        }
    }
}
