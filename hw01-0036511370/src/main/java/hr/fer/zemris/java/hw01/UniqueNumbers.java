package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * A program that reads unique whole numbers, then outputs them sorted in
 * ascending and descending order.
 * <p>
 * Enter 'kraj' to finish inputting numbers.
 * 
 * @author Filip Majetic
 */
public class UniqueNumbers {

	/**
	 * Helper class that holds tree node info.
	 */
	static class TreeNode {
		public TreeNode left;
		public TreeNode right;
		public int value;
	}

	/**
	 * Main method of the program.
	 * 
	 * @param args unused
	 */
	public static void main(String[] args) {
		TreeNode head = null;

		var in = new Scanner(System.in);

		do {
			System.out.print("Unesite broj > ");
			var input = in.nextLine();

			if (input.toLowerCase().equals("kraj")) {
				break;
			}

			try {
				var value = Integer.parseInt(input);

				if (containsValue(head, value)) {
					System.out.println("Broj već postoji. Preskačem.");
				} else {
					head = addNode(head, value);
					System.out.println("Dodano.");
				}

			} catch (NumberFormatException ex) {
				System.out.printf("'%s' nije cijeli broj.%n", input);
			}
		} while (true);

		in.close();

		System.out.println("Ispis od najmanjeg: " + treeToStringAscending(head));
		System.out.println("Ispis od najvećeg: " + treeToStringDescending(head));
	}

	/**
	 * Returns a string representation of the tree sorted in a descending order.
	 * 
	 * @param head the tree head
	 * 
	 * @see treeToStringAscending
	 */
	public static String treeToStringDescending(TreeNode head) {
		if (head == null)
			return "";

		return treeToStringDescending(head.right) + Integer.toString(head.value) + " "
				+ treeToStringDescending(head.left);
	}

	/**
	 * Returns a string representation of the tree sorted in a ascending order.
	 * 
	 * @param head the tree head
	 * 
	 * @see treeToStringDescending
	 */
	public static String treeToStringAscending(TreeNode head) {
		if (head == null)
			return "";

		return treeToStringAscending(head.left) + Integer.toString(head.value) + " "
				+ treeToStringAscending(head.right);
	}

	/**
	 * Adds a new node with value {@code value} to the tree.
	 * <p>
	 * If {@code head} is {@code null}, the new element is returned, otherwise the
	 * old {@code head} element is returned.
	 * 
	 * @param head  the tree head
	 * @param value value of the new node to be added
	 * 
	 * @return the tree head
	 */
	public static TreeNode addNode(TreeNode head, int value) {
		if (head == null) {
			var node = new TreeNode();
			node.value = value;
			return node;
		}

		if (value < head.value) {
			head.left = addNode(head.left, value);
		} else {
			head.right = addNode(head.right, value);
		}

		return head;
	}

	/**
	 * Recursively checks if tree contains a value.
	 * 
	 * @param head  the tree head
	 * @param value the value to search for
	 * 
	 * @return {@code true</code> if the value is found, <code>flase} otherwise
	 */
	public static boolean containsValue(TreeNode head, int value) {
		if (head == null) {
			return false;
		}

		if (head.value == value) {
			return true;
		}

		return head.value < value ? containsValue(head.left, value) : containsValue(head.right, value);
	}

	/**
	 * Recursively calculates the tree size.
	 * 
	 * @param head the tree head
	 * 
	 * @return the size of the tree
	 */
	public static int treeSize(TreeNode head) {
		if (head == null) {
			return 0;
		}

		return treeSize(head.left) + treeSize(head.right) + 1;
	}
}
