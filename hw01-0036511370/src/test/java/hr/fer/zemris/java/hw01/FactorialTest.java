package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import static hr.fer.zemris.java.hw01.Factorial.calculateFactorial;

public class FactorialTest {

    @Test
    void positiveInput() {
        var n = calculateFactorial(4);
        assertTrue(n > 0);
        assertEquals(24, n);
    }

    @Test
    public void zeroInput() {
        var n = calculateFactorial(0);
        assertEquals(1, n);
    }

    @Test
    public void negativeInput() {
        assertThrows(IllegalArgumentException.class, () -> calculateFactorial(-4));
    }
}
