package hr.fer.zemris.java.hw01;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static hr.fer.zemris.java.hw01.Rectangle.*;

public class RectangleTest {

    @Test
    public void validInput() {
        var num = parseSide("3.14");
        assertEquals(3.14, num);

        num = parseSide("30");
        assertEquals(30.0, num);
    }

    @Test
    public void negativeInput() {
        var input = "-4";

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> parseSide(input));

        assertEquals(ex.getMessage(), "Unijeli ste negativnu vrijednost.");
    }

    @Test
    public void inputIsNotNumber() {
        var input = "Štefica";

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> parseSide(input));

        assertEquals(ex.getMessage(), "'" + input + "' se ne može protumačiti kao broj.");
    }

    @Test
    public void wrongDecimalSeparator() {
        var input = "4,2";

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> parseSide(input));

        assertEquals(ex.getMessage(), "'" + input + "' se ne može protumačiti kao broj.");
    }
}
