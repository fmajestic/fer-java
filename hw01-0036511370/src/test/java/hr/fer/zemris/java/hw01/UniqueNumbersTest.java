package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw01.UniqueNumbers.TreeNode;
import static hr.fer.zemris.java.hw01.UniqueNumbers.*;

/**
 * UniqueNumbesTest
 */
public class UniqueNumbersTest {

    @Test
    public void emptyTreeInsert() {
        TreeNode head = null;

        head = addNode(head, 0);

        assertNotNull(head);
    }

    @Test
    public void emptyTreeSize() {
        TreeNode head = null;

        assertEquals(0, treeSize(head));
    }

    @Test
    public void nonEmptyTreeSize() {
        TreeNode head = null;
        head = addNode(head, 5);
        head = addNode(head, 2);
        head = addNode(head, 7);
        head = addNode(head, 6);

        assertEquals(4, treeSize(head));
    }

    @Test
    public void insertingDuplicate() {
        TreeNode head = null;
        head = addNode(head, 5);
        head = addNode(head, 2);
        head = addNode(head, 7);
        head = addNode(head, 2);

        assertEquals(3, treeSize(head));
    }

    // Dodano kao ispravak domaće zadaće
    @Test
    public void treeContainsValue() {
        TreeNode head = null;
        head = addNode(head, 42);
        head = addNode(head, 21);

        assertTrue(containsValue(head, 21));
        assertFalse(containsValue(head, 6));
    }

    @Test
    public void ascendingString() {
        TreeNode head = null;
        head = addNode(head, 42);
        head = addNode(head, 21);
        head = addNode(head, 76);
        head = addNode(head, 35);

        assertEquals(treeToStringAscending(head).trim(), "21 35 42 76");
    }

    @Test
    public void descendingString() {
        TreeNode head = null;
        head = addNode(head, 42);
        head = addNode(head, 21);
        head = addNode(head, 76);
        head = addNode(head, 35);

        assertEquals(treeToStringDescending(head).trim(), "76 42 35 21");
    }
}
