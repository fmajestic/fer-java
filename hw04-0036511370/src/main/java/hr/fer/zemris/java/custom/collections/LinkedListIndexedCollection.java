package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * An indexed collection that uses a doubly linked list as a backing field.
 * <p>
 * Storing null values is not allowed.
 *
 * @author Filip Majetic
 * @see ArrayIndexedCollection
 */
public class LinkedListIndexedCollection<T> implements List<T> {

    /** The first node of the list. */
    private ListNode<T> first;

    /** The last node of the list. */
    private ListNode<T> last;

    /** The current size of the collection. */
    private int size;

    /** Number of times the collection has been modified. */
    private long modificationCount;

    /**
     * Default constructor. Initializes an empty list.
     */
    public LinkedListIndexedCollection() {
        first = last = null;
        size = 0;
        modificationCount = 0;
    }

    /**
     * A constructor that takes another {@code Collection} to be added as initial
     * elements of the list.
     *
     * @param other the initial collection to add to the list
     */
    public LinkedListIndexedCollection(Collection<? extends T> other) {
        this();
        this.addAll(other);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return first == null && last == null && size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * Adds the non-null value to the end of the list.
     *
     * @param value the value to be added, not null
     *
     * @throws NullPointerException if value is null
     * @see #insert(Object, int)
     * @see #addAll(Collection)
     */
    @Override
    public void add(T value) {
        Objects.requireNonNull(value);

        var node = new ListNode<>(value);

        if (this.isEmpty()) {
            first = last = node;
        } else {
            node.previous = last;
            last.next = node;
            last = node;
        }

        this.size += 1;

        updateModificationCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return indexOf(value) >= 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(Object value) {
        boolean nodeRemoved = false;

        if (first.value.equals(value)) {
            var oldFirst = first;

            first = oldFirst.next;
            first.previous = null;

            oldFirst.next = null;

            nodeRemoved = true;
        } else if (last.value.equals(value)) {
            var oldLast = last;

            last = oldLast.previous;
            last.next = null;

            oldLast.previous = null;

            nodeRemoved = true;
        } else {
            var toRemove = first;

            while (toRemove != null) {
                if (toRemove.value.equals(value)) {
                    toRemove.previous.next = toRemove.next;
                    toRemove.next.previous = toRemove.previous;

                    nodeRemoved = true;
                    break;
                }

                toRemove = toRemove.next;
            }
        }

        if (nodeRemoved) {
            size -= 1;
            updateModificationCount();
        }

        return nodeRemoved;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] toArray() {
        var elements = new Object[size];

        var walk = first;

        // This could be done with a while loop,
        // but that would unnecessarily expose the counter variable
        for (int i = 0; i < size; i++) {
            elements[i] = walk.value;
            walk = walk.next;
        }

        return elements;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        while (first != null) {
            var next = first.next;

            first.previous = null;
            first.next = null;

            first = next;
        }

        first = last = null;
        size = 0;
        updateModificationCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ElementsGetter<T> createElementsGetter() {
        return new LinkedListElementsGetter(modificationCount);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(int index) {
        Objects.checkIndex(index, size);

        var node = walkToIndex(index);

        return node.value;
    }

    /**
     * Inserts the given non-null {@code value} at the position, shifting the
     * remaining elements to the right.
     *
     * @param value    the value to be inserted, not null
     * @param position the position at which to insert, valid range is
     *                 {@code [0, size]}
     *
     * @throws NullPointerException      if {@code value} is {@code null}
     * @throws IndexOutOfBoundsException if {@code position} is negative, or greater
     *                                   than {@code size}
     * @see #add(Object)
     */
    public void insert(T value, int position) {
        Objects.requireNonNull(value);

        Objects.checkIndex(position, size + 1);

        // Special case: inserting into an empty list.
        if (this.isEmpty()) {
            add(value);
            return;
        }

        var node = new ListNode<>(value);

        // Special cases: inserting to the beginning or end
        if (position == 0) {
            node.next = first;
            first.previous = node;
            first = node;
        } else if (position == size) {
            last.next = node;
            node.previous = last;
            last = node;
        } else {
            ListNode<T> insertAfter = walkToIndex(position - 1);

            // Since the previous case handles inserting to the end,
            // we don't need to worry about insertAfter.next.previous
            // throwing a NullPointerException
            node.next = insertAfter.next;
            insertAfter.next.previous = node;

            node.previous = insertAfter;
            insertAfter.next = node;
        }

        updateModificationCount();

        size += 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object value) {
        int index = 0;

        var walk = first;

        while (walk != null) {
            if (walk.value.equals(value)) {
                return index;
            }

            walk = walk.next;
            index += 1;
        }

        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(int index) {
        Objects.checkIndex(index, size);

        ListNode<T> toRemove = walkToIndex(index);

        if (size == 1) {
            clear();
            return;
        }

        if (toRemove == first) {
            toRemove.next.previous = null;
            first = toRemove.next;
        } else if (toRemove == last) {
            toRemove.previous.next = null;
            last = toRemove.previous;
        } else {
            toRemove.previous.next = toRemove.next;
            toRemove.next.previous = toRemove.previous;
        }

        size -= 1;
        updateModificationCount();
    }

    /**
     * Gets the {@code ListNode} at the specified index.
     *
     * @param index the index to get from
     *
     * @return the node
     */
    private ListNode<T> walkToIndex(int index) {
        ListNode<T> walk;

        // Optimizacija hodanja po listi:
        // Ako se nalazi u prvoj polovici, krećemo od početka.
        // Ako se nalazi u drugoj polovici, krećemo od kraja.
        if (index <= (size - 1) / 2) {
            walk = first;

            for (int i = 0; i < index; i++) {
                walk = walk.next;
            }
        } else {
            walk = last;

            for (int i = 0; i < size - index - 1; i++) {
                walk = walk.previous;
            }
        }

        return walk;
    }

    /**
     * Increments modificationCount by 1
     */
    private void updateModificationCount() {
        this.modificationCount += 1;
    }

    /**
     * Represents a single list node.
     *
     * @param <T> the type of the node's value
     */
    private static final class ListNode<T> {

        /** The previous node in the list. */
        private ListNode<T> previous;

        /** The next node in the list. */
        private ListNode<T> next;

        /** The value of this node. */
        private T value;

        /**
         * Initializes a new node with the given value
         *
         * @param value initial value of this node
         */
        ListNode(T value) {
            this.value = value;
            previous = null;
            next = null;
        }
    }

    private class LinkedListElementsGetter implements ElementsGetter<T> {
        private ListNode<T> getterHead;
        private long savedModificationCount;

        /**
         * Creates a new ElementsGetter for this list.
         *
         * @param modificationCount the modification count at creation of getter
         */
        LinkedListElementsGetter(long modificationCount) {
            this.getterHead = first;
            this.savedModificationCount = modificationCount;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNextElement() {
            requireNoModifications();

            return getterHead != null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T getNextElement() {

            if (!hasNextElement()) {
                throw new NoSuchElementException("Returned all elements.");
            }

            var ret = this.getterHead.value;

            this.getterHead = this.getterHead.next;

            return ret;
        }

        private void requireNoModifications() {
            if (savedModificationCount != modificationCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
