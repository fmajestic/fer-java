package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Represents a hash table that maps key-value pairs.
 * <p>
 * The general contract is: null keys <b>are not permitted</b>, null values <b>are</b>.
 * <p>
 * For performance reasons, if the backing table ever reaches 75% capacity, it is expanded by doubling its capacity
 * and remapping all entries to new positions.
 * No new entries are created, the old instances are instead moved around.
 *
 * @param <K> the key type
 * @param <V> the value type
 */
@SuppressWarnings( "WeakerAccess" )
public class SimpleHashtable<K, V> implements Iterable<SimpleHashtable.TableEntry<K, V>> {

    /** The default size of the table */
    private static final int DEFAULT_TABLE_SIZE = 16;

    /** The backing table */
    private TableEntry<K, V>[] table;

    /** The number of entries currently in the table */
    private int size;

    /**
     * Number of times the table has been modified.
     * This excludes updating an existing key with a new value.
     */
    private long modificationCount;

    /**
     * Number of non-null table slots.
     * <p>
     * When this number exceeds 75% of total table capacity, the table is doubled in size and all entry positions are
     * recalculated.
     */
    private int occupiedSlotCount;


    /**
     * Default constructor. Creates a new table with a default size.
     */
    public SimpleHashtable() {
        this(DEFAULT_TABLE_SIZE);
    }

    /**
     * Creates a new hash table with the specified initial capacity.
     * <p>
     * The actual table initialCapacity is set to the current closest power of 2.
     *
     * @param initialCapacity the initial initialCapacity of the table
     *
     * @throws IllegalArgumentException if {@code initialCapacity} is less than 1
     */
    public SimpleHashtable(int initialCapacity) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("Initial initialCapacity can't be less than 1. Got: " + initialCapacity);
        }

        // Calculates current power of 2 after initialCapacity.
        // By doing 'initialCapacity - 1' we take into account if initialCapacity already is a power of 2.
        int capacity = Integer.highestOneBit(initialCapacity - 1) << 1;

        //noinspection unchecked
        this.table = (TableEntry<K, V>[]) new TableEntry[capacity];
        this.modificationCount = 0;
        this.occupiedSlotCount = 0;
    }

    /**
     * Gets the number of entries in this table.
     *
     * @return the number of entries
     */
    public int size() { return size; }

    /**
     * Checks if this table is empty.
     *
     * @return true if the table contains no entries, false otherwise
     */
    public boolean isEmpty() { return size == 0; }

    /**
     * Creates a new iterator through the table.
     *
     * @return the new iterator
     */
    @Override
    public Iterator<SimpleHashtable.TableEntry<K, V>> iterator() { return new SimpleHashtableIterator(modificationCount); }


    /**
     * Adds a new entry to the table, or updates an existing one.
     * <p>
     * If the key already exists, the key's value is updated.
     * Otherwise, a new entry is added.
     *
     * @param key   the key to add
     * @param value the value to add
     *
     * @throws NullPointerException if {@code key} is {@code null}
     */
    public void put(K key, V value) {
        Objects.requireNonNull(key, "Null keys are not permitted.");

        // Try to update an existing key
        if (update(key, value)) {
            return;
        }

        putEntry(new TableEntry<>(key, value));

        updateModificationCount();
        ensureOptimalTableCapacity();
    }

    /**
     * Gets the value associated with the specified key.
     * <p>
     * If no matching key was found, this method returns {@code null}.
     * Since storing {@code null} values (but not keys) is permitted, notice that there is no way
     * of telling whether this method returned {@code null} because no key was found,
     * or the key had {@code null} stored with it.
     *
     * @param key the key to get the value of
     *
     * @return the value associated with the key, or {@code null} if no such key exists in the table
     */
    public V get(Object key) {
        var entry = getEntry(key);

        return entry != null ? entry.value : null;
    }

    /**
     * Checks if the dictionary contains a key.
     *
     * @param key the key to search for
     *
     * @return true if a matching key was found, false otherwise
     */
    public boolean containsKey(Object key) { return getEntry(key) != null; }

    /**
     * Checks if the dictionary contains a value.
     *
     * @param value the value to search for
     *
     * @return true if the value was found, false otherwise
     */
    public boolean containsValue(Object value) {
        for (var entry : table) {
            for (var head = entry; head != null; head = head.next) {
                if (head.value.equals(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Removes the table entry with the specified key.
     * <p>
     * If no matching key was found, this method does nothing.
     *
     * @param key the key to remove
     */
    public void remove(Object key) {
        int address = getAddress(key);

        // Make sure we don't dereference null
        if (table[address] == null) {
            return;
        }

        // Special case: removing the first entry
        if (table[address].key.equals(key)) {
            table[address] = table[address].next;

            // Special case: it is also the only key
            if (table[address] == null) {
                occupiedSlotCount--;
            }
        } else {
            for (var head = table[address]; head.next != null; head = head.next) {
                if (head.next.key.equals(key)) {
                    head.next = head.next.next;
                    break;
                }
            }
        }

        size--;
        updateModificationCount();
    }

    /**
     * Removes all entries from the table.
     */
    public void clear() {
        for (int i = 0; i < table.length; i++) {
            table[i] = null;
        }

        updateModificationCount();
        occupiedSlotCount = 0;
        size = 0;
    }


    /**
     * Creates a string representation of this hash table in the form:
     * <pre>[ key1=value1, key2=value2, ... ]</pre>
     *
     * @return the hash table as a string
     */
    @Override
    public String toString() {
        var sb = new StringBuilder();

        sb.append("[ ");


        var iter = this.iterator();

        while (iter.hasNext()) {
            var entry = iter.next();

            sb.append(entry.toString());

            if (iter.hasNext()) {
                sb.append(", ");
            }
        }

        sb.append(" ]");

        return sb.toString();
    }


    /**
     * Internal utility method used to update a key with a value.
     * <p>
     * If the update was successful, this method returns true. If the table contains no such key, this method returns
     * false.
     *
     * @param key   the key to update
     * @param value the new value
     *
     * @return true if the entry was updated, false if no matching key exists
     */
    private boolean update(K key, V value) {
        var existing = getEntry(key);

        if (existing != null) {
            existing.value = value;
            return true;
        }

        return false;
    }

    /**
     * Internal utility method for getting the entire table entry associated with a key.
     *
     * @param key the key to search for
     *
     * @return the table entry with the specified key, or {@code null} if no matching key was found
     */
    private TableEntry<K, V> getEntry(Object key) {
        if (key == null) {
            return null;
        }

        int address = getAddress(key);

        for (var head = table[address]; head != null; head = head.next) {
            if (head.key.equals(key)) {
                return head;
            }
        }

        return null;
    }

    /**
     * Internal utility method for generating an address in a table from a key.
     * <p>
     * By passing a table to the method, we remove code duplication when expanding the table capacity - the new,
     * bigger table can be the argument.
     *
     * @param key   the key to generate the hash from
     *
     * @return the table address
     */
    private int getAddress(Object key) {
        int address = key.hashCode() % table.length;
        return address >= 0 ? address : -address;
    }

    /** Updates the modification count of this table. */
    private void updateModificationCount() {
        modificationCount++;
    }

    /**
     * Ensures that the table never goes over 75% of occupied slots.
     * <p>
     * This is done by doubling the table size and reinserting all entries once the 75% is reached.
     * <p>
     * Instead of constructing a copy of each entry and adding it to the new table, the most efficient way is to add
     * all the current entries to a list, then erase {@code .next} on each of them and add them to the new table.
     * <p>
     * The {@code .next} property must be erased because the two nodes might not come after each other in the new table.
     * <p>
     * As for deleting {@code .next} right away, this would break the iterator, so it is done after building the list.
     */
    private void ensureOptimalTableCapacity() {
        if ((double) occupiedSlotCount / table.length < 0.75) {
            return;
        }

        // This is the most optimal method: we know exactly how many entries the list will hold.
        var allEntries = new ArrayIndexedCollection<TableEntry<K, V>>(this.size);

        // Use the iterator we implemented for this class. It will conveniently not return any nulls.
        for (var existingEntry : this) {
            allEntries.add(existingEntry);
        }

        clear();

        int newCapacity = table.length * 2;

        //noinspection unchecked
        table = (TableEntry<K, V>[]) new TableEntry[newCapacity];

        // Each entry.next must be reset, but it can only be done outside of the iterator.
        allEntries.forEach(entry -> {
            entry.next = null;
            putEntry(entry);
        });
    }

    /**
     * Internal utility method that inserts a new entry into a specified table.
     * <p>
     * By specifying a table to insert into, we remove code duplication when expanding the hash table capacity - the new
     * insertionTable can be passed instead of {@code this.insertionTable}.
     * <p>
     * The first argument's next must be null.
     *
     * @param entry the entry to add, next must be null
     */
    private void putEntry(TableEntry<K, V> entry) {
        int address = getAddress(entry.key);

        // Special case: no entries at this position yet
        if (table[address] == null) {
            table[address] = entry;
            occupiedSlotCount++;
        } else {
            // If we are here, there was no existing key,
            // therefore we have to add the new entry to the end of the list.
            // This means it is OK to leave newEntry.current as null.

            var insertAfter = table[address];

            // Move to the end of the linked list
            while (insertAfter.next != null) {
                insertAfter = insertAfter.next;
            }

            insertAfter.next = entry;
        }

        size++;
    }


    /**
     * Represents a single key-value pair stored in the table.
     * <p>
     * Additionally, it points to the current entry that is in the same slot of the table.
     * <p>
     * Only the value can be changed, the key and current entry must be set up during instantiation.
     */
    public static class TableEntry<K, V> {

        /** The key of this entry. */
        private K key;

        /** The value of this entry. */
        private V value;

        /** The current entry at this table position. */
        private TableEntry<K, V> next;

        /**
         * Creates a new entry with the given key and value.
         *
         * @param key   the new key
         * @param value the new value
         */
        TableEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        /**
         * Gets the key of this entry.
         *
         * @return the key
         */
        public K getKey() {
            return key;
        }

        /**
         * Gets the value currently stored in this entry.
         *
         * @return the currentIndex value
         */
        public V getValue() {
            return value;
        }

        /**
         * Sets the value of this entry.
         *
         * @param value the new value
         */
        public void setValue(V value) {
            this.value = value;
        }

        /**
         * Returns a string representation of this entry in the form:
         * <prev>key=value</prev>
         *
         * @return this entry as a string
         */
        @Override
        public String toString() { return String.format("%s=%s", key.toString(), value.toString()); }
    }

    /**
     * The iterator for this hashtable.
     */
    private class SimpleHashtableIterator implements Iterator<SimpleHashtable.TableEntry<K, V>> {

        /** Holds and retrieves all non-null heads of the table. */
        private ElementsGetter<TableEntry<K, V>> validHeads;

        /** Keeps track of whether {@link #remove()} was called for the current entry. */
        private boolean canRemove;

        /**
         * The modification count of the iterator at time of creation.
         * Updated only by the {@link #remove()}} method.
         */
        private long savedModificationCount;

        /** The current entry of the iterator */
        private TableEntry<K, V> current;

        /**
         * Creates a new iterator locked to the specified modification count.
         * <p>
         * Also creates an {@link ElementsGetter} for getting all non-null heads of the list.
         *
         * @param currentModificationCount the modification count at time of creation
         */
        SimpleHashtableIterator(long currentModificationCount) {

            // Prevents a call to remove() before at least one next()
            this.canRemove = false;

            this.savedModificationCount = currentModificationCount;

            var validHeadList = new ArrayIndexedCollection<TableEntry<K, V>>();

            // Construct a list of valid (non-null) heads.
            // A foreach loop is safe here, because it is not creating this iterator, but the table's.
            for (TableEntry<K, V> entry : table) {
                if (entry != null) {
                    validHeadList.add(entry);
                }
            }

            this.validHeads = validHeadList.createElementsGetter();
        }

        /**
         * Checks if the iterator has any entries left to return.
         *
         * @return true if there are entries left to return, false otherwise
         *
         * @throws ConcurrentModificationException if the table has been modified by anything other than
         *                                         {@link #remove()}
         */
        @Override
        public boolean hasNext() {
            requireNoModifications();

            // If the current entry has no next element, check if there are any valid entries left
            return (current != null && current.next != null) || validHeads.hasNextElement();
        }

        /**
         * Returns the next entry of the table.
         * <p>
         * If no entries remain to be returned, an exception is thrown.
         *
         * @return the next element
         *
         * @throws NoSuchElementException          if all entries have been returned
         * @throws ConcurrentModificationException if it is called after {@link #hasNext()}} returned {@code false}
         */
        @Override
        public TableEntry<K, V> next() {
            if (!hasNext()) {
                throw new NoSuchElementException("Iterator out of elements.");
            }

            if (current == null || current.next == null) {
                current = validHeads.getNextElement();
            } else {
                current = current.next;
            }

            canRemove = true;

            return current;
        }

        /**
         * Removes the current iterator entry from the table.
         * <p>
         * This is the only legal way of modifying the table after during iteration.
         * <p>
         * If this method is called more than once without advancing the iterator, an exception is is thrown.
         *
         * @throws ConcurrentModificationException if the table has been modified by anything other than this method
         * @throws IllegalStateException           if this method is called more than once without advancing the
         *                                         iterator with {@link #next()}
         */
        @Override
        public void remove() {
            requireNoModifications();

            if (!canRemove) {
                throw new IllegalStateException("Cannot call remove more than once without advancing the iterator.");
            }

            canRemove = false;
            savedModificationCount++;

            SimpleHashtable.this.remove(current.key);
        }

        /**
         * Utility method for checking if the table we are iterating through has been modified.
         *
         * @throws ConcurrentModificationException if the saved modification count of this iterator does not mach the
         *                                         table's modification count
         */
        private void requireNoModifications() {
            if (savedModificationCount != SimpleHashtable.this.modificationCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
