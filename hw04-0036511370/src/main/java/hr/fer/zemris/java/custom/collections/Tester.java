package hr.fer.zemris.java.custom.collections;

/**
 * Tests a single value for a condition.
 */
public interface Tester<T> {

    /**
     * The test to perform on a value.
     * 
     * @param obj the value to be tested.
     * 
     * @return {@code true} if the condition was satisfied,
     *         {@code false} otherwise
     */
    boolean test(T obj);
}
