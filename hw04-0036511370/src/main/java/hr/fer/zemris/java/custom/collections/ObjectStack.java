package hr.fer.zemris.java.custom.collections;

/**
 * Represents a stack with the usual functionality.
 * <p>
 * This stack is built as an adapter around an {@link ArrayIndexedCollection}.
 *
 * @author Filip Majetic
 */
public class ObjectStack<T> {
    /** The backing array used for storing elements. */
    private ArrayIndexedCollection<T> elements;

    /**
     * Default constructor. Creates an empty stack;
     */
    public ObjectStack() {
        elements = new ArrayIndexedCollection<>();
    }

    /**
     * Creates a stack with an initial capacity.
     *
     * @param initialCapacity the initial capacity.
     *
     * @throws IllegalArgumentException if {@code initialCapacity} is less than 1
     */
    public ObjectStack(int initialCapacity) {
        elements = new ArrayIndexedCollection<>(initialCapacity);
    }

    /**
     * Checks if the stack is empty.
     *
     * @return true if the stack contains no elements, false otherwise
     */
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    /**
     * Gets the size of this stack.
     *
     * @return the number of elements on the stack
     */
    public int size() {
        return elements.size();
    }

    /**
     * Pushes a value onto the stack.
     *
     * @param value the value to be pushed
     *
     * @see #pop
     * @see #peek
     */
    public void push(T value) {
        elements.add(value);
    }

    /**
     * Gets and removes the top element of the stack.
     *
     * @return the popped element
     *
     * @see #push
     * @see #peek
     */
    public T pop() throws EmptyStackException {
        if (size() == 0) {
            throw new EmptyStackException("Tried to pop from empty stack");
        }

        var ret = elements.get(elements.size() - 1);
        elements.remove(elements.size() - 1);
        return ret;
    }

    /**
     * Gets the top element of the stack, but does not remove it.
     *
     * @return the peeked element
     *
     * @see #push
     * @see #pop
     */
    public T peek() throws EmptyStackException {
        if (size() == 0) {
            throw new EmptyStackException("Tried to peek empty stack");
        }

        return elements.get(elements.size() - 1);
    }

    /**
     * Removes all elements from the stack
     */
    public void clear() {
        elements.clear();
    }
}
