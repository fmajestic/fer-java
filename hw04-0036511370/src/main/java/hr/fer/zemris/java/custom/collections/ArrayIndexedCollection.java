package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

import static java.util.Arrays.copyOf;

/**
 * An indexed collection that uses an array as a backing field.
 * <p>
 * Storing null values is not allowed.
 *
 * @author Filip Majetic
 */
public class ArrayIndexedCollection<T> implements List<T> {
    /** The default collection capacity. */
    private static final int DEFAULT_CAPACITY = 16;

    /** The current size of the collection. */
    private int size;

    /** The backing array. */
    private Object[] elements;

    /** Number of times the collection has been modified. */
    private long modificationCount;

    /**
     * Default constructor. Creates an empty collection with a default capacity.
     *
     * @see ArrayIndexedCollection#DEFAULT_CAPACITY
     */
    public ArrayIndexedCollection() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Creates an empty collection with an initial capacity.
     *
     * @param initialCapacity the initial capacity
     *
     * @throws IllegalArgumentException if {@code initialCapacity} is less than 1
     */
    public ArrayIndexedCollection(int initialCapacity) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("Initial size must be greater than 1");
        }

        this.size = 0;
        this.modificationCount = 0;
        this.elements = new Object[initialCapacity];
    }

    /**
     * Creates a new collection with an initial capacity using {@code source} for
     * initial data.
     * <p>
     * Note that if {@code initialCapacity} is smaller than {@code source.size()},
     * the source's size will be used instead.
     *
     * @param source          the source collection, not null
     * @param initialCapacity the initial capacity of this collection
     *
     * @throws NullPointerException if source is null
     */
    public ArrayIndexedCollection(Collection<? extends T> source, int initialCapacity) {
        this(Integer.max(initialCapacity, source.size()));

        this.addAll(source);
    }

    /**
     * Creates a new collection using {@code source} for initial data.
     *
     * @param source the source collection
     *
     * @throws NullPointerException if source is null
     * @see #ArrayIndexedCollection(Collection, int)
     */
    public ArrayIndexedCollection(Collection<? extends T> source) {
        this(source, DEFAULT_CAPACITY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * Adds the non-null {@code value} to the collection.
     *
     * @param value the value to be added, not null
     *
     * @throws NullPointerException if value is null
     * @throws OutOfMemoryError     if the collection has reached max size
     * @see #insert(Object, int)
     */
    @Override
    public void add(T value) {
        Objects.requireNonNull(value);

        ensureCapacity();

        updateModificationCount();

        elements[size] = value;
        size += 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return indexOf(value) >= 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(T value) {
        var index = indexOf(value);

        if (index < 0) {
            return false;
        }

        remove(index);

        updateModificationCount();

        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elements, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }

        size = 0;

        updateModificationCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ElementsGetter<T> createElementsGetter() {
        return new ArrayElementsGetter(modificationCount);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(int index) {
        Objects.checkIndex(index, size);

        //noinspection unchecked
        return (T) elements[index];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(T value, int index) {
        Objects.requireNonNull(value);

        Objects.checkIndex(index, size + 1);

        ensureCapacity();
        updateModificationCount();

        // Čitaj sve od 'index' na dalje, a počni zapisivati na jednom mjestu dalje.
        // Ovime je ostvareno stvaranje mjesta za novi element i pomak ostatka udesno.
        // arraycopy je siguran iako su source i destination isti objekti, pogledati
        // javadoc.
        System.arraycopy(elements, index, elements, index + 1, size - index);
        elements[index] = value;
        size += 1;
    }

    /**
     * {@inheritDoc}
     */
    public int indexOf(Object value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(int index) {
        Objects.checkIndex(index, size);

        // Čitaj sve elemente desno od indeksa, a počni ih zapisivati od indeksa.
        // Ovime se ostvaruje prebrisavanje elementa na 'index' i pomak ostatka ulijevo.
        System.arraycopy(elements, index + 1, elements, index, size - index - 1);

        size -= 1;

        updateModificationCount();
    }

    /**
     * Ensures that the array expands when adding a value to a full array.
     * <p>
     * Doubles the array capacity if it is possible to do so without overflowing, or
     * expands to {@code Integer.MAX_VALUE} otherwise.
     *
     * @throws OutOfMemoryError if the collection size is {@code Integer.MAX_VALUE}
     *                          and a value is added/inserted
     * @see Integer#MAX_VALUE
     */
    private void ensureCapacity() {
        if (size == elements.length) {
            if (elements.length < Integer.MAX_VALUE) {
                int newCapacity = elements.length * 2;

                // Check for overflow from multiplication
                newCapacity = newCapacity > 0 ? newCapacity : Integer.MAX_VALUE;

                // Expand the array to the new capacity
                elements = copyOf(elements, newCapacity);
            } else {
                throw new OutOfMemoryError("Maximum list size reached.");
            }
        }
    }

    /**
     * Increments modificationCount by 1
     */
    private void updateModificationCount() {
        this.modificationCount += 1;
    }

    /** The {@link ElementsGetter} implementation for this collection */
    private class ArrayElementsGetter implements ElementsGetter<T> {

        /** The modification count at time of creation */
        private final long savedModificationCount;

        /** The current index of the getter. */
        private int current = 0;

        /**
         * Creates a new ElementsGetter for this array.
         *
         * @param modificationCount the modification count at creation of getter
         */
        ArrayElementsGetter(long modificationCount) {
            this.savedModificationCount = modificationCount;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNextElement() {
            requireNoModifications();

            return this.current < size;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T getNextElement() {
            if (!hasNextElement()) {
                throw new NoSuchElementException("Returned all elements.");
            }

            //noinspection unchecked
            return (T) elements[current++];
        }

        /**
         * Check that no modifications have been made to the parent collection, and
         * throw if there were.
         *
         * @throws ConcurrentModificationException if the parent collection has been
         *                                         modified
         */
        private void requireNoModifications() {
            if (this.savedModificationCount != modificationCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
