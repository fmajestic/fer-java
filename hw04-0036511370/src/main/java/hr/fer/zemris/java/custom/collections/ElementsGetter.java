package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * Provides an iterator-like mechanism for getting elements of a collection.
 */
public interface ElementsGetter<T> {

    /**
     * Checks whether the collection has any elements left to get.
     *
     * @return true if there are elements left not retrieved, false otherwise
     * @throws ConcurrentModificationException if the collection has been modified
     *                                         since creation of getter
     */
    boolean hasNextElement();

    /**
     * Returns the next element of the collection, or throws an exception if there
     * are no elements left.
     *
     * @return the next object of the collection
     * @throws NoSuchElementException          if there is no more elements to be
     *                                         retrieved
     * @throws ConcurrentModificationException if the collection has been modified
     *                                         since creation of getter
     */
    T getNextElement();

    /**
     * Applies the action contained in the {@code Processor} on each remaining
     * element of this getter.
     * <p>
     * This method uses the interface's functionality {@link #hasNextElement()} and
     * {@link #getNextElement()}, so it will throw an exception if the collection
     * has been modified.
     *
     * @param p the processor containing the action to be performed
     * @throws ConcurrentModificationException if the collection has been modified
     *                                         since creation of getter
     */
    default void processRemaining(Processor<T> p) {
        while (this.hasNextElement()) {
            p.process(this.getNextElement());
        }
    }
}
