package hr.fer.zemris.math;

import static java.lang.Math.abs;

/**
 * Represents a two-dimensional vector.
 * <p>
 * This class provides methods for vector manipulation, both in-place methods that change the vector
 * they are called on, and fluent methods that return a new vector with the updated values.
 *
 * @author Filip Majetic
 */
public class Vector2D {

    /**
     * The tolerance used when comparing two coordinates
     */
    static final double DELTA_TOLERANCE = 1e-6;

    /**
     * The x coordinate
     */
    private double x;

    /**
     * The y coordinate
     */
    private double y;


    /**
     * Creates a new two-dimensional vector.
     *
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets the x coordinate of this vector.
     *
     * @return the x coordinate
     */
    public double getX() {
        return x;
    }

    /**
     * Gets the y coordinate of this vector.
     *
     * @return the y coordinate.
     */
    public double getY() {
        return y;
    }


    /**
     * Translates this vector by another vector in-place.
     * <p>
     * The offset vector's coordinates are added to this vector's coordinates.
     *
     * @param offset the vector to translate by
     */
    public void translate(Vector2D offset) {
        x += offset.getX();
        y += offset.getY();
    }

    /**
     * Translates this vector by another vector, and returns the result of the operation.
     * <p>
     * The offset vector's coordinates are added to this vector's coordinates.
     *
     * @param offset the vector to translate by
     *
     * @return the translated vector
     */
    public Vector2D translated(Vector2D offset) {
        var copy = this.copy();
        copy.translate(offset);

        return copy;
    }


    /**
     * Rotates this vector by a given angle (in radians) in-place.
     * <p>
     * This is done by calculating the current magnitude and angle of the vector,
     * then rotating it as a complex number in exponential form:
     *
     * <pre>x = magnitude * cos(old + angle)</pre>
     * <pre>y = magnitude * sin(old + angle)</pre>
     *
     * @param angle the angle to rotate by, in radians.
     */
    public void rotate(double angle) {
        double mag = Math.hypot(x, y);
        double newAngle = getNewAngle(angle);

        x = rotateX(mag, newAngle);
        y = rotateY(mag, newAngle);
    }

    /**
     * Rotates this vector by a given angle (in radians), and returns the result of the operation.
     * <p>
     * This is done by calculating the current magnitude and angle of the vector,
     * then rotating it as a complex number in exponential form:
     *
     * <pre>
     * x = magnitude * cos(old + angle)
     * y = magnitude * sin(old + angle)
     * </pre>
     *
     * @param angle the angle to rotate by, in radians.
     *
     * @return the result of the operation
     */
    public Vector2D rotated(double angle) {
        var copy = this.copy();
        copy.rotate(angle);

        return copy;
    }


    /**
     * Calculates a new angle for rotation.
     * <p>
     * The given angle must be in radians.
     *
     * @param angle the angle to rotate by, in radians
     *
     * @return the new angle in radians
     */
    private double getNewAngle(double angle) {
        return Math.atan2(y, x) + angle;
    }


    /**
     * Calculates the new y coordinate rotated by {@code angle}
     * <p>
     * This is according to the formula:
     * <pre>magnitude * cos(angle)</pre>
     *
     * @param magnitude the magnitude of this vector
     * @param angle     the angle to rotate {@code x} by, in radians
     *
     * @return the new x coordinate
     */
    private double rotateX(double magnitude, double angle) {
        return magnitude * Math.cos(angle);
    }

    /**
     * Calculates the new y coordinate rotated by {@code angle}
     * <p>
     * This is done by the formula:
     * <pre>magnitude * sin(angle)</pre>
     *
     * @param magnitude the magnitude of this vector
     * @param angle     the angle to rotate {@code y} by, in radians
     *
     * @return the new y coordinate
     */
    private double rotateY(double magnitude, double angle) {
        return magnitude * Math.sin(angle);
    }


    /**
     * Scales this vector by a given scalar value in-place.
     * <p>
     * This is done by multiplying the x and y coordinates by the scalar.
     *
     * @param scalar the value to scale by
     */
    public void scale(double scalar) {
        x *= scalar;
        y *= scalar;
    }

    /**
     * Scales this vector by a given scalar value, and returns the result of the operation.
     * <p>
     * This is done by multiplying the x and y coordinates by the scalar.
     *
     * @param scalar the value to scale by
     *
     * @return the result of the operation
     */
    public Vector2D scaled(double scalar) {
        var copy = this.copy();
        copy.scale(scalar);

        return copy;
    }


    /**
     * Returns a copy of this vector with the same x and y coordinates.
     * <p>
     * A new object is returned, so there is no connection to this vector,
     * and the returned object is free to be modified.
     *
     * @return a copy of this vector.
     */
    public Vector2D copy() {
        return new Vector2D(x, y);
    }


    /**
     * Returns the hash code of this vector, calculated from both coordinates,
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /**
     * Checks whether this vector equals another object.
     * <p>
     * If the object is a {@code Vector2D}, equality is calculated by comparing respective coordinates, using
     * {@link #DELTA_TOLERANCE} as a maximum difference.
     *
     * @param o the object to check
     *
     * @return true if the objects are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Vector2D)) {
            return false;
        }

        Vector2D other = (Vector2D) o;

        if (abs(x - other.x) <= DELTA_TOLERANCE) {
            return true;
        }

        return abs(y - other.y) <= DELTA_TOLERANCE;

    }

    /**
     * Returns a string representation of this vector in the form of {@code ax+by}
     *
     * @return the string representing this vector
     */
    @Override
    public String toString() {
        return String.format("%fx%+fy", x, y);
    }
}
