package hr.fer.zemris.math;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static hr.fer.zemris.math.Vector2D.DELTA_TOLERANCE;
import static org.junit.jupiter.api.Assertions.*;

class Vector2DTest {
    Vector2D vec;

    @BeforeEach
    void setUp() {
        vec = new Vector2D(3,4);
    }

    @Test
    void getX() {
        assertEquals(3, vec.getX(), DELTA_TOLERANCE, "X getter should return x coordinate.");
    }

    @Test
    void getY() {
        assertEquals(4, vec.getY(), DELTA_TOLERANCE, "Y getter should return y coordinate.");
    }

    @Test
    void translate() {
        Vector2D moveBy = new Vector2D(3.14, -4.5);
        Vector2D expected = new Vector2D(6.14, -0.5);

        vec.translate(moveBy);

        assertEquals(expected, vec, "In-place vector translation not working.");
    }

    @Test
    void translated() {
        Vector2D moveBy = new Vector2D(3.14, -4.5);
        Vector2D expected = new Vector2D(6.14, -0.5);

        assertEquals(expected, vec.translated(moveBy), "Fluent vector translation not working.");
    }

    @Test
    void rotate() {
        double rotateBy = Math.PI / 2;
        Vector2D expected = new Vector2D(-4, 3);

        vec.rotate(rotateBy);

        assertEquals(expected, vec, "In-place vector rotation not working.");
    }

    @Test
    void rotated() {
        double rotateBy = Math.PI / 2;
        Vector2D expected = new Vector2D(-4, 3);

        assertEquals(expected, vec.rotated(rotateBy), "Fluent vector rotation not working.");
    }

    @Test
    void scale() {
        double scaleBy = 6;
        Vector2D expected = new Vector2D(18, 24);

        vec.scale(scaleBy);

        assertEquals(expected, vec, "In-place vector scaling not working.");
    }

    @Test
    void scaled() {
        double scaleBy = 6;
        Vector2D expected = new Vector2D(18, 24);

        assertEquals(expected, vec.scaled(scaleBy), "Fluent vector scaling not working.");
    }

    @Test
    void copy() {
        Vector2D expected = new Vector2D(3, 4);

        assertEquals(expected, vec.copy(), "Copy not working.");
    }
}