package hr.fer.zemris.java.custom.collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class SimpleHashtableTest {

    private SimpleHashtable<String, Integer> hashTable;

    @BeforeEach
    void setUp() {
        hashTable = new SimpleHashtable<>();
    }

    @Test
    void basicOperations() {
        hashTable.put("Ivana", 2);
        hashTable.put("Ante", 2);
        hashTable.put("Jasna", 2);
        hashTable.put("Kristina", 5);
        hashTable.put("Ivana", 5); // overwrites old value

        // query collection:
        assertEquals(5, hashTable.get("Kristina"), "Existing key should be updated by new value.");

        // query collection:
        assertEquals(4, hashTable.size(), "Duplicate keys should not increase table size.");
    }

    @Test
    void iteratorNonNull() {
        hashTable.put("Ivana", 2);
        hashTable.put("Ante", 2);
        hashTable.put("Jasna", 2);
        hashTable.put("Kristina", 5);
        hashTable.put("Ivana", 5);
        hashTable.put("Yes", 1);
        hashTable.put("Maybe", 2);
        hashTable.put("No", 3);
        hashTable.put("Hello", 4);
        hashTable.put("World", 5);
        hashTable.put("Cloud", 6);
        hashTable.put("Day", 7);

        for (var entry : hashTable) {
            assertNotNull(entry, "Iterator should not return null entries.");
        }
    }

    @Test
    void iteratorRemove() {
        hashTable.put("Yes", 1);
        hashTable.put("Maybe", 2);
        hashTable.put("No", 3);
        hashTable.put("Hello", 4);
        hashTable.put("World", 5);
        hashTable.put("Cloud", 6);
        hashTable.put("Day", 7);

        var iter = hashTable.iterator();

        while (iter.hasNext()) {
            if (iter.next().getValue() % 2 == 0) {
                iter.remove();
            }
        }

        assertEquals(4, hashTable.size(), "Iterator remove should decrement size.");
        assertFalse(hashTable.containsKey("Cloud"), "Removed key should not be present.");
        assertFalse(hashTable.containsValue(4), "Removed value should not be present.");


        int count = 0;

        for (var ignored : hashTable) {
            count++;
        }

        assertEquals(hashTable.size(), count, "Iterator should return exactly size elements after removal.");
    }

    @Test
    void iteratorRemoveBeforeFirstNext() {
        hashTable.put("Yes", 1);

        assertThrows(IllegalStateException.class, () -> {
            var iter = hashTable.iterator();
            iter.remove();
        }, "Calling remove without calling next at least once should throw an exception.");
    }

    @Test
    void iteratorRemoveTwice() {
        hashTable.put("Yes", 1);

        var iter = hashTable.iterator();

        iter.next();
        iter.remove();

        assertThrows(IllegalStateException.class, iter::remove,
                "Calling remove more than once on the same entry should throw an exception.");
    }

    @Test
    void iteratorModification() {
        hashTable.put("Yes", 1);
        hashTable.put("Maybe", 1);

        var iter = hashTable.iterator();

        iter.next();

        hashTable.remove("Yes");

        assertThrows(ConcurrentModificationException.class, iter::next,
                "Any iterator method call after externally changing the table should throw an exception.");
    }

    @Test
    void iteratorOutOfElements() {
        hashTable.put("Yes", 1);
        hashTable.put("Maybe", 1);

        var iter = hashTable.iterator();

        iter.next();
        iter.next();

        assertThrows(NoSuchElementException.class, iter::next,
                "Calling next() after returning all entries should throw an exception.");
    }

    @Test
    void clearResetsEverything() {
        hashTable.put("Yes", 1);
        hashTable.put("Maybe", 1);
        hashTable.put("No", 2);

        hashTable.clear();

        assertFalse(hashTable.containsKey("Yes"), "Clearing should remove all entries");
        assertFalse(hashTable.containsKey("Maybe"), "Clearing should remove all entries");
        assertFalse(hashTable.containsKey("No"), "Clearing should remove all entries");
        assertTrue(hashTable.isEmpty());
    }
}
