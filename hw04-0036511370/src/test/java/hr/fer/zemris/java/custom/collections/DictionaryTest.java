package hr.fer.zemris.java.custom.collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DictionaryTest {

    private Dictionary<String, Integer> dictionary;

    @BeforeEach
    void setUp() {
        dictionary = new Dictionary<>();
    }

    @Test
    void isEmpty() {
        assertTrue(dictionary.isEmpty(),
                "Dictionary initialised with empty constructor should be empty.");

        dictionary.put("Yes", 1);
        dictionary.put("No", 2);
        dictionary.put("Maybe", 3);

        dictionary.clear();

        assertTrue(dictionary.isEmpty(),
                "isEmpty() should return true after calling clear().");
    }

    @Test
    void size() {
        assertEquals(0, dictionary.size(),
                "Empty dictionary should have size 0.");

        dictionary.put("Yes", 1);
        dictionary.put("No", 2);
        dictionary.put("Maybe", 3);

        assertEquals(3, dictionary.size(),
                "Dictionary should have 3 entries.");
    }

    @Test
    void clear() {
        dictionary.clear();

        assertEquals(0, dictionary.size(),
                "Clearing an empty dictionary should leave size as 0.");
    }

    @Test
    void put() {
        dictionary.put("Yes", 1);
        dictionary.put("No", 2);
        dictionary.put("Maybe", 3);

        assertEquals(3, dictionary.size(),
                "Each put with a distinct key should update the size by 1.");
    }

    @Test
    void putNull() {
        assertThrows(NullPointerException.class, () -> dictionary.put(null, 5),
                "Put should not accept null keys.");
    }

    @Test
    void putUpdate() {
        dictionary.put("Yes", 1);
        dictionary.put("No", 2);
        dictionary.put("Maybe", 3);

        dictionary.put("No", 16);

        assertEquals(16, dictionary.get("No"),
                "Calling put with an existing key should update the value.");
    }

    @Test
    void get() {
        dictionary.put("Yes", 1);
        dictionary.put("No", 2);
        dictionary.put("Maybe", 3);

        assertEquals(1, dictionary.get("Yes"),
                "Dictionary should properly map values to keys.");
    }

    @Test
    void getNonExisting() {
        dictionary.put("Yes", 1);

        assertNull(dictionary.get("Maybe"),
                "Get should return null for non-existing keys.");
    }
}
